import { IS_ACTIVE, MEDIA_TYPE, POST_STATUS, IS_POSTED } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const Post = sequelize.define(
    'Post',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      topic_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      product_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      content: { type: DataTypes.TEXT },
      count_like: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      count_comment: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      is_posted: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_POSTED),
        validate: {
          isIn: {
            args: [Object.values(IS_POSTED)],
            msg: 'Invalid is posted a post.',
          },
        },
      },

      schedule: {
        allowNull: true,
        type: DataTypes.DATE,
      },

      status: {
        allowNull: true,
        type: DataTypes.INTEGER,
        values: Object.values(POST_STATUS),
        validate: {
          isIn: {
            args: [Object.values(POST_STATUS)],
            msg: 'Invalid post status.',
          },
        },
      },

      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'post',
      version: true,
      hooks: {},
    },
  );

  Post.associate = (db) => {
    // one - many
    db.Post.hasMany(db.Comment, { foreignKey: { name: 'post_id' } });
    db.Post.hasMany(db.Reaction, { foreignKey: { name: 'post_id' } });
    db.Post.hasMany(db.PostMedia, { foreignKey: { name: 'post_id' } });
    // many - one
    db.Post.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Post.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.Post.belongsTo(db.Topic, { foreignKey: { name: 'topic_id' } });
    db.Post.belongsTo(db.Product, { foreignKey: { name: 'product_id' } });
  };

  return Post;
};
