import { IS_ACTIVE } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const ProductCustomAttributeOption = sequelize.define(
    'ProductCustomAttributeOption',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      product_custom_attribute_id: { allowNull: true, type: DataTypes.INTEGER },
      name: { allowNull: false, type: DataTypes.STRING },
      display_order: DataTypes.INTEGER,
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'product_custom_attribute_option',
      version: true,
      hooks: {},
    },
  );

  ProductCustomAttributeOption.associate = (db) => {
    // one - many
    db.ProductCustomAttributeOption.hasOne(db.ProductMedia, {
      foreignKey: {
        name: 'product_custom_attribute_option_id',
      },
    });
    db.ProductCustomAttributeOption.hasOne(db.ProductPrice, {
      foreignKey: {
        name: 'custom_attribute_option_id_1',
      },
    });
    db.ProductCustomAttributeOption.hasOne(db.ProductPrice, {
      foreignKey: {
        name: 'custom_attribute_option_id_2',
      },
    });
    // many - one
    db.ProductCustomAttributeOption.belongsTo(db.ProductCustomAttribute, {
      foreignKey: {
        name: 'product_custom_attribute_id',
      },
    });
  };

  return ProductCustomAttributeOption;
};
