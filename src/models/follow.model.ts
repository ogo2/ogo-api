import { IS_ACTIVE, IS_FOLLOW_ACTIVE, IS_GIFT_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';

module.exports = function (sequelize, DataTypes) {
  const Follow = sequelize.define(
    'Follow',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_FOLLOW_ACTIVE),
        defaultValue: IS_FOLLOW_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_FOLLOW_ACTIVE)],
            msg: 'Invalid follow status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'follow',
      version: true,
      hooks: {},
    },
  );

  Follow.associate = (db) => {
    // one - many
    // db.Follow.hasMany(db.PurchasedFollow, { foreignKey: { name: 'follow_id' } });

    // many - one
    db.Follow.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Follow.belongsTo(db.Shop, { foreignKey: { name: 'user_id' } });
  };

  return Follow;
};
