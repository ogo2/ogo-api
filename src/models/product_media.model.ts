import { IS_ACTIVE, MEDIA_TYPE } from '@utils/constants';
import { Utils } from '@utils/Utils';

module.exports = function (sequelize, DataTypes) {
  const ProductMedia = sequelize.define(
    'ProductMedia',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      product_id: { allowNull: true, type: DataTypes.INTEGER },
      media_url: {
        allowNull: false,
        type: DataTypes.STRING,
        get() {
          return Utils.getFullUrl(this.getDataValue('media_url'));
        },
      },
      type: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(MEDIA_TYPE),
        validate: {
          isIn: {
            args: [Object.values(MEDIA_TYPE)],
            msg: 'Invalid type media.',
          },
        },
      },
      display_order: DataTypes.INTEGER,
      product_custom_attribute_option_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },

      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      // indexes: [{ unique: true, fields: ["phone_number"] }],
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'product_media',
      version: true,
      hooks: {},
    },
  );

  ProductMedia.associate = (db) => {
    db.ProductMedia.belongsTo(db.Product, {
      foreignKey: {
        name: 'product_id',
      },
    });
    db.ProductMedia.belongsTo(db.ProductCustomAttributeOption, {
      foreignKey: {
        name: 'product_custom_attribute_option_id',
      },
    });
  };
  return ProductMedia;
};
