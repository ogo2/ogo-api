import { createJWToken } from '@utils/AuthtJWToken';
import { USER_STATUS, GENDER, IS_ACTIVE, STATUS_REFERAL } from '@utils/constants';
import { Utils } from '@utils/Utils';
import * as bcrypt from 'bcryptjs';
import { Sequelize } from 'sequelize';

module.exports = function (sequelize, DataTypes) {
  const User = sequelize.define(
    'User',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      shop_id: { allowNull: true, type: DataTypes.INTEGER },
      df_type_user_id: { allowNull: false, type: DataTypes.INTEGER },
      user_name: { allowNull: false, type: DataTypes.STRING },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
        validate: { notEmpty: true, len: [6, 100] },
      },
      token: { type: DataTypes.STRING },
      app_token: { type: DataTypes.STRING },
      last_login_date: DataTypes.DATE,
      code_reset_password: DataTypes.STRING,
      expired_reset_password: DataTypes.DATE,
      device_id: DataTypes.STRING,
      name: { allowNull: false, type: DataTypes.STRING },
      phone: {
        // unique: true,
        allowNull: false,
        type: DataTypes.STRING,
      },
      email: {
        // unique: true,
        allowNull: true,
        type: DataTypes.STRING,
        // validate: {
        //   len: {
        //     args: [6, 128],
        //     msg: 'Email address must be between 6 and 128 characters in length',
        //   },
        //   isEmail: {
        //     msg: 'Email address must be valid',
        //   },
        // },
      },
      referal_customer_id: { allowNull: true, type: DataTypes.STRING },
      point: {
        allowNull: true,
        type: DataTypes.BIGINT,
        defaultValue: 0,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(USER_STATUS),
        defaultValue: USER_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(USER_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      status_referal: {
        allowNull: true,
        type: DataTypes.INTEGER,
        defaultValue: STATUS_REFERAL.PENDING,
      },
      date_of_birth: { type: DataTypes.DATE, allowNull: true },
      gender: {
        allowNull: true,
        type: DataTypes.INTEGER,
        values: Object.values(GENDER),
        validate: {
          isIn: {
            args: [Object.values(GENDER)],
            msg: 'Invalid gender.',
          },
        },
      },
      profile_picture_url: {
        type: DataTypes.STRING,
        get() {
          return Utils.getFullUrl(this.getDataValue('profile_picture_url'));
        },
      },

      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      // indexes: [{ unique: true, fields: ["user_name"] }],
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'user',
      version: true,
      hooks: {},
    },
  );

  User.associate = (db) => {
    // one - many
    // db.User.hasMany(db.Shop, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.PointTransactionHistory, {
      foreignKey: { name: 'user_id' },
    });
    db.User.hasMany(db.RotationLuckHistory, {
      foreignKey: { name: 'user_id' },
    });
    db.User.hasMany(db.Livestream, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.UserInterestTopic, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.UserIgnoreTopic, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.PurchasedGift, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Notification, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.UserAddress, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Post, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Comment, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Comment, { foreignKey: { name: 'target_user_id' } });
    db.User.hasMany(db.Reaction, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Order, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Review, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.UserWatched, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.TopicMessage, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Message, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Cart, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Wishlist, { foreignKey: { name: 'user_id' } });
    db.User.hasMany(db.Follow, { foreignKey: { name: 'user_id' } });

    // many - one
    db.User.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.User.belongsTo(db.DFTypeUser, {
      foreignKey: { name: 'df_type_user_id' },
    });
  };

  User.beforeSave((user, options) => {
    if (user.changed('password')) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    }
  });

  User.prototype.generateToken = function generateToken() {
    return createJWToken({
      phone: this.phone,
      id: this.id,
      shop_id: this.shop_id,
      df_type_user_id: this.df_type_user_id,
    });
  };

  User.prototype.authenticate = function authenticate(value) {
    if (bcrypt.compareSync(value, this.password)) return true;
    else return false;
  };

  User.generatePassword = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
  };

  User.generateOTP = function generateOTP() {
    const digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < 6; i++) {
      OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
  };

  return User;
};
