import { IS_ACTIVE } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const DFNotification = sequelize.define(
    'DFNotification',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        // unique: true,
        type: DataTypes.STRING,
      },
      // content: {
      //   allowNull: false,
      //   type: DataTypes.STRING,
      // },
      // description: {
      //   type: DataTypes.STRING,
      // },
      // url_icon: {
      //   allowNull: false,
      //   type: DataTypes.STRING,
      //   get() {
      //     return getFullUrl(this.getDataValue('url_icon'));
      //   },
      // },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'df_notification',
      version: true,
      hooks: {},
    },
  );

  DFNotification.associate = (db) => {
    // one - many
    db.DFNotification.hasMany(db.Notification, {
      foreignKey: { name: 'df_notification_id' },
    });
  };

  return DFNotification;
};
