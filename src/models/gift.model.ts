import { IS_ACTIVE, IS_GIFT_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';

module.exports = function (sequelize, DataTypes) {
  const Gift = sequelize.define(
    'Gift',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      df_type_gift_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      price: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      quantity: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      discount_percent: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      max_discount_money: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      icon_url: {
        allowNull: false,
        type: DataTypes.STRING,
        get() {
          return Utils.getFullUrl(this.getDataValue('icon_url'));
        },
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_GIFT_ACTIVE),
        defaultValue: IS_GIFT_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_GIFT_ACTIVE)],
            msg: 'Invalid gift status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'gift',
      version: true,
      hooks: {},
    },
  );

  Gift.associate = (db) => {
    // one - many
    db.Gift.hasMany(db.PurchasedGift, { foreignKey: { name: 'gift_id' } });

    // many - one
    db.Gift.belongsTo(db.DFTypeGift, {
      foreignKey: { name: 'df_type_gift_id' },
    });
  };

  return Gift;
};
