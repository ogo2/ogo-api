import { IS_ACTIVE, CATEGORY_STATUS } from '@utils/constants';
import { Utils } from '@utils/Utils';

module.exports = function (sequelize, DataTypes) {
  const Category = sequelize.define(
    'Category',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      order: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      parent_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      icon_url: {
        type: DataTypes.STRING,
        get() {
          return Utils.getFullUrl(this.getDataValue('icon_url'));
        },
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(CATEGORY_STATUS),
        defaultValue: CATEGORY_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(CATEGORY_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'category',
      version: true,
      hooks: {},
    },
  );

  Category.associate = (db) => {
    // one - many
    // db.Category.hasMany(db.Category, { foreignKey: { name: "parent_id" } });
    db.Category.hasMany(db.Product, { foreignKey: { name: 'category_id' } });
    // many - one
    db.Category.belongsTo(db.Category, {
      as: 'parent_category',
      foreignKey: 'parent_id',
    });
    db.Category.hasMany(db.Category, {
      as: 'children_category',
      foreignKey: 'parent_id',
    });
  };

  return Category;
};
