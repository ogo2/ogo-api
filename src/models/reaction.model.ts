import { IS_ACTIVE } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const Reaction = sequelize.define(
    'Reaction',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      post_id: {
        type: DataTypes.INTEGER,
      },
      livestream_id: {
        type: DataTypes.INTEGER,
      },
      comment_id: {
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      df_reaction_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'reaction',
      version: true,
      hooks: {},
    },
  );

  Reaction.associate = (db) => {
    // many - one
    db.Reaction.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Reaction.belongsTo(db.Post, { foreignKey: { name: 'post_id' } });
    db.Reaction.belongsTo(db.Livestream, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Reaction.belongsTo(db.Comment, { foreignKey: { name: 'comment_id' } });
    db.Reaction.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.Reaction.belongsTo(db.DFReaction, {
      foreignKey: { name: 'df_reaction_id' },
    });
  };

  return Reaction;
};
