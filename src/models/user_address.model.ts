import { createJWToken } from '@utils/AuthtJWToken';
import { IS_ACTIVE } from '@utils/constants';
import * as bcrypt from 'bcryptjs';
import { Sequelize } from 'sequelize';

module.exports = function (sequelize, DataTypes) {
  const UserAddress = sequelize.define(
    'UserAddress',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: { allowNull: false, type: DataTypes.STRING },
      phone: { allowNull: false, type: DataTypes.STRING },
      user_id: { allowNull: false, type: DataTypes.INTEGER },
      df_province_id: { allowNull: false, type: DataTypes.INTEGER },
      df_district_id: { allowNull: false, type: DataTypes.INTEGER },
      df_ward_id: { allowNull: false, type: DataTypes.INTEGER },
      lat: { allowNull: true, type: DataTypes.INTEGER },
      long: { allowNull: true, type: DataTypes.INTEGER },
      location_address: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      address: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      is_default: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'user_address',
      version: true,
      hooks: {},
    },
  );

  UserAddress.associate = (db) => {
    // one - many
    db.UserAddress.hasMany(db.Order, {
      foreignKey: { name: 'user_address_id' },
    });

    // many - one
    db.UserAddress.belongsTo(db.DFProvince, {
      foreignKey: { name: 'df_province_id' },
    });
    db.UserAddress.belongsTo(db.DFDistrict, {
      foreignKey: { name: 'df_district_id' },
    });
    db.UserAddress.belongsTo(db.DFWard, { foreignKey: { name: 'df_ward_id' } });
    db.UserAddress.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
  };

  return UserAddress;
};
