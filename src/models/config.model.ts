import { IS_ACTIVE, CONFIG_TYPE, CONFIG_STATUS } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const Config = sequelize.define(
    'Config',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: { allowNull: false, type: DataTypes.STRING },
      type: {
        allowNull: false,
        unique: 'column',
        type: DataTypes.INTEGER,
        values: Object.values(CONFIG_TYPE),
        validate: {
          isIn: {
            args: [Object.values(CONFIG_TYPE)],
            msg: 'Invalid config type.',
          },
        },
      },
      value: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(CONFIG_STATUS),
        defaultValue: CONFIG_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(CONFIG_STATUS)],
            msg: 'Invalid config status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'config',
      version: true,
      hooks: {},
    },
  );
  return Config;
};
