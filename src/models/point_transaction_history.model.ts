import { IS_ACTIVE, IS_DEFAULT, TYPE_POINT_TRANSACTION_HISTORY } from '@utils/constants';

module.exports = function (sequelize, DataTypes) {
  const PointTransactionHistory = sequelize.define(
    'PointTransactionHistory',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      point: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      current_point: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      content: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      type: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(TYPE_POINT_TRANSACTION_HISTORY),
        validate: {
          isIn: {
            args: [Object.values(TYPE_POINT_TRANSACTION_HISTORY)],
            msg: 'Invalid type point transaction.',
          },
        },
      },
      df_transaction_point_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'point_transaction_history',
      version: true,
      hooks: {},
    },
  );

  PointTransactionHistory.associate = (db) => {
    // many - one
    db.PointTransactionHistory.belongsTo(db.User, {
      foreignKey: { name: 'user_id' },
    });
    db.PointTransactionHistory.belongsTo(db.DFTypeTransactionPoint, {
      foreignKey: { name: 'df_transaction_point_id' },
    });
  };

  return PointTransactionHistory;
};
