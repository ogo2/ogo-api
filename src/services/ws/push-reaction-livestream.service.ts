export type TypeReaction = {
  id: number;
  name: string;
  url_icon: string;
};
/**
 * Service để bắn các list danh sách các reaction về cho các livestream đang phát trực tiếp
 */
export class LivestreamPushReactionService {
  private _livestreamReactionsCached: Map<number, TypeReaction[]>; // livestream_id - TypeReaction
  constructor() {
    this._livestreamReactionsCached = new Map();
  }
  public push(livestream_id: number, typeReaction: TypeReaction) {
    if (this._livestreamReactionsCached.has(livestream_id)) {
      this._livestreamReactionsCached.get(livestream_id).push(typeReaction);
    } else {
      this._livestreamReactionsCached.set(livestream_id, [typeReaction]);
    }
  }
  public getAndClearAll() {
    const reactionsLivestream = new Map(this._livestreamReactionsCached);
    this._livestreamReactionsCached = new Map();
    return reactionsLivestream;
  }
}
export const livestreamPushReactionService = new LivestreamPushReactionService();
