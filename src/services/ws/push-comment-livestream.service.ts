import { IS_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';
import { Transaction } from 'sequelize/types';

export type CommentPushCached = {
  content: string;
  livestream_id;
  User: {
    id: number;
    name: string;
    profile_picture_url?: string | null;
  };
  Shop?: {
    id: number;
    name: string;
    profile_picture_url?: string | null;
  };
};

const db = require('@models');
const { Sequelize, sequelize, Reaction, Livestream } = db.default;
const { Op } = Sequelize;
/**
 * Service để bắn các list danh sách các bình luận về cho các livestream đang phát trực tiếp
 */
export class LivestreamPushCommentService {
  private _commentsPushCached: Map<number, CommentPushCached[]>;
  private _timeout: NodeJS.Timeout;
  private _ruleTimeout = 5000;
  constructor() {
    this._commentsPushCached = new Map();
  }
  public push(livestream_id: number, comment: CommentPushCached) {
    if (this._commentsPushCached.has(livestream_id)) {
      this._commentsPushCached.get(livestream_id).push(comment);
    } else {
      this._commentsPushCached.set(livestream_id, [comment]);
    }
  }
  public getAndClearAll() {
    const commentLivestream = new Map(this._commentsPushCached);
    this._commentsPushCached = new Map();
    return commentLivestream;
  }
}
export const livestreamPushCommentService = new LivestreamPushCommentService();
