import { RedisChannel } from './channels/redis/redis.channel';
import { IChannel } from './interfaces/IChannel';

export class PublisherService {
  private _channel: IChannel;
  constructor(channel: IChannel) {
    this._channel = channel;
  }
  public async publish<T>(event: string, data: T) {
    try {
      const res = await this._channel.publish(event, data);
      return res;
    } catch (error) {
      throw error;
    }
  }
}

export const publisherService = new PublisherService(new RedisChannel());
