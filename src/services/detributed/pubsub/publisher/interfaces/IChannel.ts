export interface IChannel {
  publish<T>(event: string, data: T): Promise<void>;
}
