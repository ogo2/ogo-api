import { IChannel } from '../../interfaces/IChannel';
import { RedisClient } from 'redis';
import { promisify } from 'util';
import { getRefRedisConnection } from './redis.provider';

export class RedisChannel implements IChannel {
  private _redisClient: RedisClient;
  constructor() {
    this._redisClient = getRefRedisConnection();
  }
  public async publish<T>(event: string, data: T) {
    const publishAsync = promisify(this._redisClient.publish).bind(this._redisClient);
    if (data) {
      return await publishAsync(event, JSON.stringify(data));
    }
    return null;
  }
}
