import { ONESIGNAL } from '@utils/constants';
import axios from 'axios';
import * as _ from 'lodash';
export class OnesignalService {
  private static apiInstance = axios.create({
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Basic ${ONESIGNAL.AUTHORIZATION}`,
    },
    timeout: 20000,
    responseType: 'json',
    baseURL: 'https://onesignal.com',
  });
  public async pushNotification(
    data: any,
    title_heading: string,
    title_content: string,
    device_id: Array<string> | string,
  ) {
    console.log('OnesignalService.apiInstance', OnesignalService.apiInstance);
    const toDevices = _.isArray(device_id) ? device_id : [device_id];
    const message = {
      app_id: ONESIGNAL.APP_ID,
      data,
      headings: { en: title_heading },
      contents: { en: title_content },
      android_channel_id: ONESIGNAL.ANDROID_CHANNEL_ID,
      include_player_ids: toDevices,
    };
    try {
      const result = await OnesignalService.apiInstance.post('/api/v1/notifications', message);
    } catch (error) {
      console.log(error.response.data);
    }
  }
}
