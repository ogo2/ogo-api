import { TOPIC_STATUS } from '@utils/constants';
import Joi from '@utils/JoiValidate';

export class TopicService {
  public TopicCreateSchema = Joi.object({
    name: Joi.string().required(),
    order: Joi.number().integer().required(),
    description: Joi.string().allow('', null),
    icon_url: Joi.string().allow('', null),
  });

  public TopicUpdateSchema = Joi.object({
    name: Joi.string().required(),
    order: Joi.number().integer().required(),
    description: Joi.string().allow('', null),
    status: Joi.number()
      .integer()
      .required()
      .default(TOPIC_STATUS.ACTIVE)
      .valid(TOPIC_STATUS.ACTIVE, TOPIC_STATUS.INACTIVE),
    icon_url: Joi.string().allow('', null),
  });
}
