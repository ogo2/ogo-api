import { IS_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';
import { Transaction } from 'sequelize/types';
const db = require('@models');
const { Sequelize, sequelize, Reaction, Livestream } = db.default;
const { Op } = Sequelize;

class LivestreamReactionService {
  private _countReactionsCached: Map<number, number>; // livestream_id - count cache
  private _timeout: NodeJS.Timeout;
  private _ruleTimeout = 5000;
  constructor() {
    this._countReactionsCached = new Map();
    this._timeout = null;
  }

  public incrementReaction(livestream_id: number) {
    // this._commentsCached.push(comment);
    if (this._countReactionsCached.has(livestream_id)) {
      const currentCountLivestreamReaction = this._countReactionsCached.get(livestream_id);
      this._countReactionsCached.set(livestream_id, currentCountLivestreamReaction + 1);
    } else {
      this._countReactionsCached.set(livestream_id, 1);
    }
    if (!this._timeout) {
      this._timeout = setTimeout(async () => {
        let isUpdated = false;
        while (true) {
          try {
            const updateReactions = new Map(this._countReactionsCached);
            await sequelize.transaction(async (transaction: Transaction) => {
              for (let livestreamId of updateReactions.keys()) {
                const countCachedReaction = updateReactions.get(livestreamId);
                await Livestream.increment(
                  { count_reaction: countCachedReaction },
                  { where: { id: livestreamId, is_active: IS_ACTIVE.ACTIVE } },
                );
              }
            });
            // need improve bellow in future
            this._countReactionsCached = new Map();
            isUpdated = true;
          } catch (error) {
            isUpdated = false;
          }
          if (isUpdated) break;
          else await Utils.timer(1000);
        }
        this._timeout = null;
      }, this._ruleTimeout);
    }
  }
  // public push({ df_reaction_id }: { df_reaction_id: number }) {}
}
export const livestreamReactionService = new LivestreamReactionService();
