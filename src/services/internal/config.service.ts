import Joi from '@utils/JoiValidate';

export class ConfigService {
  public ConfigLuckySpinGiftSchema = Joi.object({
    name: Joi.string().required(),
    value: Joi.number().integer().required(),
    percent: Joi.number().integer().min(0).max(100).required(),
    max_per_day: Joi.number().integer().min(0).required(),
  });
}
