import { REACTION_TYPE } from '@utils/constants';
import Joi from '@utils/JoiValidate';

export class ShopService {
  public AddShopSchema = Joi.object({
    name: Joi.string().required().label('Tên gian hàng.'),
    name_user: Joi.string().allow('', null).label('Tên chủ gian hàng. '),
    phone: Joi.string()
      .required()
      .label('Số điện thoại')
      .pattern(new RegExp(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/im))
      .message('Số điện thoại không hợp lệ'),
    email: Joi.string()
      .allow('', null)
      // .required()
      .label('Email')
      .pattern(
        new RegExp(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ),
      )
      .message('Email không hợp lệ'),

    profile_picture_url: Joi.string().allow('', null),
  });
  public UpdateShopSchema = Joi.object({
    name: Joi.string().required().label('Tên'),
    email: Joi.string()
      .allow('', null)
      .label('Email')
      .pattern(
        new RegExp(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ),
      )
      .message('Email không hợp lệ'),
    status: Joi.number().allow('', null),
    profile_picture_url: Joi.string().allow('', null),
  });
}
