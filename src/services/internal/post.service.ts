import Joi from '@utils/JoiValidate';
import { IS_ACTIVE, IS_POSTED, POST_STATUS } from '@utils/constants';
import { PostTypes } from 'types';
const db = require('@models');
const { sequelize, Sequelize, Post } = db.default;
const { Op } = Sequelize;

export class PostService {
  public PostCreateSchema = Joi.object({
    user_id: Joi.number().integer().required(),
    shop_id: Joi.number().integer().allow('', null),
    topic_id: Joi.number().integer().required(),
    content: Joi.string().required().label('Nội dung bài đăng'),
  });
  public PostUpdateSchema = Joi.object({
    topic_id: Joi.number().integer().allow('', null),
    content: Joi.string().allow('', null).label('Nội dung bài đăng'),
    image_delete: Joi.array().items(Joi.number().integer()).allow('', null),
  });

  public AdminPostCreateSchema = Joi.object({
    user_id: Joi.number().integer().required(),
    topic_id: Joi.number().integer().required(),
    content: Joi.string().required().label('Nội dung bài đăng.'),
    schedule: Joi.date().min(new Date()).allow(null, '').label('Giờ đăng bài.'),
    status: Joi.number()
      .integer()
      .required()
      .default(POST_STATUS.ALL)
      .valid(POST_STATUS.ALL, POST_STATUS.CUSTOMER, POST_STATUS.SHOP)
      .label('Thông báo đối tượng'),
  });
  public AdminPostUpdateSchema = Joi.object({
    topic_id: Joi.number().integer().allow('', null),
    content: Joi.string().allow('', null).label('Nội dung bài đăng'),
    schedule: Joi.date().min(new Date()).allow(null, '').label('Hẹn giờ đăng bài.'),
    status: Joi.number()
      .integer()
      .allow(null, '')
      .valid(POST_STATUS.ALL, POST_STATUS.CUSTOMER, POST_STATUS.SHOP)
      .label('Thông báo đối tượng'),
    image_delete: Joi.array().items(Joi.number().integer()).allow('', null),
  });

  public getListPostNotPosted = async (): Promise<Array<PostTypes.PostItem>> => {
    const listPost = await Post.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        is_posted: IS_POSTED.NOT_POSTED,
      },
    });
    return listPost.map((item: any) => item.dataValues);
  };
}
