import { REACTION_TYPE } from '@utils/constants';
import Joi from '@utils/JoiValidate';

export class ReactionService {
  public ReactionSchema = Joi.object({
    df_reaction_id: Joi.number().integer().label('Loại cảm xúc.').required(),
  });
  public getTypeReaction = (type: number): string => {
    switch (type) {
      case REACTION_TYPE.LIKE:
        return 'like';
      case REACTION_TYPE.HEART:
        return 'heart';
      case REACTION_TYPE.LOVE:
        return 'love';
      case REACTION_TYPE.HAHA:
        return 'haha';
      case REACTION_TYPE.SURPRISE:
        return 'surprise';
      case REACTION_TYPE.SAD:
        return 'sad';
      case REACTION_TYPE.ANGRY:
        return 'angry';
    }
    return '';
  };
}
