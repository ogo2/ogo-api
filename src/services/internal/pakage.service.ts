import { IS_DEFAULT } from '@utils/constants';
import Joi from '@utils/JoiValidate';

export class PakageService {
  public CreatePakageSchema = Joi.object({
    name: Joi.string().required(),
    minus: Joi.number().integer().required(),
    pakage_category_id: Joi.number().integer().required(),
    price: Joi.number().integer().required(),
    is_default: Joi.number().valid(IS_DEFAULT.ACTIVE, IS_DEFAULT.INACTIVE).required(),
  });
}
