import Joi from '@utils/JoiValidate';

export class CommentService {
  public CommentSchema = Joi.object({
    content: Joi.string().required().label('Bình luận.'),
    user_id: Joi.number().integer().required(),
    post_id: Joi.number().integer().allow('', null),
    livestream_id: Joi.number().integer().allow('', null),
    parent_id: Joi.number().integer().allow('', null),
  });

  public CommentPostSchema = Joi.object({
    content: Joi.string().required().label('Bình luận.'),
    parent_id: Joi.number().integer().allow('', null),
    target_user_id: Joi.number().integer().allow('', null),
  });

  public CommentLivestreamSchema = Joi.object({
    content: Joi.string().required().label('Bình luận.'),
  });
}
