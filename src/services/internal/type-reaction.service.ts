import { IS_ACTIVE } from '@utils/constants';
import { logger } from '@utils/Logger';
const db = require('@models');
const { Sequelize, sequelize, DFReaction } = db.default;
const { Op } = Sequelize;

export type TypeReaction = {
  id: number;
  name: string;
  url_icon: string;
};

export class LivestreamTypeReactionService {
  private _typeReactions: Map<number, TypeReaction>; // type_reaction_id - TypeReaction
  constructor() {
    this._typeReactions = new Map();
    this.loadAllTypeReaction();
  }

  private async loadAllTypeReaction() {
    const typeReactions: any[] = await DFReaction.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE },
      attributes: ['id', 'name', 'url_icon'],
    });
    typeReactions.forEach((typeReaction) => {
      const type_reaction_id = typeReaction.id;
      this._typeReactions.set(type_reaction_id, typeReaction.dataValues);
    });
    logger.info({ message: `Load ${typeReactions.length} type reaction.` });
  }
  public getTypeReaction(df_reaction_id: number): TypeReaction | null {
    if (this._typeReactions.size === 0) this.loadAllTypeReaction();
    if (this._typeReactions.has(df_reaction_id)) {
      const typeReaction: TypeReaction = this._typeReactions.get(df_reaction_id);
      return typeReaction;
    }
    return null;
  }
}
export const livestreamTypeReactionService = new LivestreamTypeReactionService();
