import { MEDIA_TYPE } from '@utils/constants';
import Joi from '@utils/JoiValidate';
import { MessageTypes } from 'types';
export class MessageService {
  public CreateChannelSchema = Joi.object({
    user_id: Joi.number().integer().allow('', null),
    shop_id: Joi.number().integer().allow('', null),
    content: Joi.string().allow('', null),
    message_media_url: Joi.string().allow('', null),
    type_message_media: Joi.number().valid(MEDIA_TYPE.IMAGE, MEDIA_TYPE.VIDEO).allow('', null),
  });
  public CreateMessageSchema = Joi.object({
    topic_message_id: Joi.number().integer().required(),
    content: Joi.string().allow('', null),
    message_media_url: Joi.string().allow('', null),
    type_message_media: Joi.number().valid(MEDIA_TYPE.IMAGE, MEDIA_TYPE.VIDEO).allow('', null),
  });
  public validateCreateMessage = ({
    user_id,
    shop_id,
    content,
    message_media_url,
  }: MessageTypes.CreateChannelModel): boolean => {
    if (!user_id && !shop_id) return false;
    if (!content && message_media_url) return false;
    return true;
  };
}
