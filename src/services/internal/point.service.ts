import {
  CONFIG_STATUS,
  CONFIG_TYPE,
  IS_ACTIVE,
  TYPE_POINT_TRANSACTION_HISTORY,
  TYPE_TRANSACTION_POINT,
} from '@utils/constants';

const db = require('@models');
const { sequelize, Sequelize, User, Config, PointTransactionHistory } = db.default;
const { Op } = Sequelize;

export class PointService {
  // đăng kí thành công
  public registerSuccess = async (user_id: number, transaction?: any) => {
    const configRegister = await Config.findOne({
      where: { type: CONFIG_TYPE.REGISTER, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    if (configRegister) {
      await User.increment(
        { point: configRegister.value },
        { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      // lưu lịch sử
      await PointTransactionHistory.create(
        {
          user_id,
          point: configRegister.value,
          current_point: 0,
          type: TYPE_POINT_TRANSACTION_HISTORY.ADD,
          // content: `Bạn nhận được ${configRegister.value} cho lần đăng kí ứng dụng đầu tiên.`,
          df_transaction_point_id: TYPE_TRANSACTION_POINT.REGISTER_SUCCESS,
        },
        { transaction },
      );
    }
  };
  // giới thiệu app thành công
  public referralApp = async (user_id: number, current_point: number, transaction?: any) => {
    const configRefCode = await Config.findOne({
      where: { type: CONFIG_TYPE.REFERRAL_APP, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    if (configRefCode) {
      await User.increment(
        { point: configRefCode.value },
        { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );

      // lưu lịch sử
      await PointTransactionHistory.create(
        {
          user_id,
          point: configRefCode.value,
          current_point,
          type: TYPE_POINT_TRANSACTION_HISTORY.ADD,
          // content: `Bạn nhận được ${configRefCode.value} cho lần đăng kí ứng dụng đầu tiên.`,
          df_transaction_point_id: TYPE_TRANSACTION_POINT.REFERRAL_APP,
        },
        { transaction },
      );
    }
  };

  // giới thiệu nhập mã thành công
  public referralCode = async (user_id: number, current_point: number, transaction?: any) => {
    const configRefCode = await Config.findOne({
      where: { type: CONFIG_TYPE.REFERRAL_CODE, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    if (configRefCode) {
      await User.increment(
        { point: configRefCode.value },
        { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );

      // lưu lịch sử
      await PointTransactionHistory.create(
        {
          user_id,
          point: configRefCode.value,
          current_point,
          type: TYPE_POINT_TRANSACTION_HISTORY.ADD,
          // content: `Bạn nhận được ${configRefCode.value} cho lần đăng kí ứng dụng đầu tiên.`,
          df_transaction_point_id: TYPE_TRANSACTION_POINT.REFERRAL_CODE,
        },
        { transaction },
      );
    }
  };

  // hoàn trả mua hàng
  public orderPromotion = async (user_id: number, current_point: number, total_price: number, transaction?: any) => {
    const configRefCode = await Config.findOne({
      where: { type: CONFIG_TYPE.ORDER_PROMOTION, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    const detailUser = await User.findOne({ where: { id: user_id, is_active: IS_ACTIVE.ACTIVE } });
    // return withSuccess(configRefCode);
    if (configRefCode) {
      await User.update(
        { point: detailUser.point + total_price },
        { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );

      // lưu lịch sử
      await PointTransactionHistory.create(
        {
          user_id,
          point: total_price,
          current_point,
          type: TYPE_POINT_TRANSACTION_HISTORY.ADD,
          // content: `Bạn nhận được ${configRefCode.value} cho lần đăng kí ứng dụng đầu tiên.`,
          df_transaction_point_id: TYPE_TRANSACTION_POINT.ORDER_PROMOTION,
        },
        { transaction },
      );
    }
  };

  // đổi quà tặng bằng điểm
  public purchaseGift = async (user_id: number, gift_price: number, current_point: number, transaction?: any) => {
    await User.decrement({ point: gift_price }, { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction });
    // lưu lịch sử
    await PointTransactionHistory.create(
      {
        user_id,
        point: gift_price,
        current_point,
        type: TYPE_POINT_TRANSACTION_HISTORY.SUB,
        // content: ``,
        df_transaction_point_id: TYPE_TRANSACTION_POINT.PURCHASE_GIFT,
      },
      { transaction },
    );
  };

  // đổi mã giảm giá bằng điểm
  public purchaseDiscount = async (user_id: number, gift_price: number, current_point: number, transaction?: any) => {
    await User.decrement({ point: gift_price }, { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE }, transaction });
    // lưu lịch sử
    await PointTransactionHistory.create(
      {
        user_id,
        point: gift_price,
        current_point,
        type: TYPE_POINT_TRANSACTION_HISTORY.SUB,
        // content: ``,
        df_transaction_point_id: TYPE_TRANSACTION_POINT.PURCHASE_DISCOUNT,
      },
      { transaction },
    );
  };
}
