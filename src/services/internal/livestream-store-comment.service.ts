import { IS_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';
import { Transaction } from 'sequelize/types';

const db = require('@models');
const { Sequelize, sequelize, Comment, Livestream } = db.default;
const { Op } = Sequelize;

export type CommentCached = {
  user_id: number;
  shop_id?: number;
  livestream_id: number;
  content: string;
};
/**
 * Service để lưu các bình luận  vào database theo interval
 */
class LivestreamCommentService {
  private _commentsCached: CommentCached[];
  private _countCommentCached: Map<number, number>; // livestream_id - count cache
  private _timeout: NodeJS.Timeout;
  private _ruleTimeout: number;
  constructor() {
    this._commentsCached = [];
    this._timeout = null;
    const FIVE_SECOND = 5000;
    this._ruleTimeout = FIVE_SECOND; // cấu hình thời gian lưu, đơn vị mls
  }
  public push(comment: CommentCached) {
    this._commentsCached.push(comment);
    if (!this._timeout) {
      this._timeout = setTimeout(async () => {
        let isInserted = false;
        while (true) {
          try {
            const insertComments = [...this._commentsCached];
            await Comment.bulkCreate(insertComments);
            this._commentsCached.splice(0, insertComments.length);
            isInserted = true;
          } catch (error) {
            isInserted = false;
          }
          if (isInserted) break;
          else await Utils.timer(1000);
        }
        this._timeout = null;
      }, this._ruleTimeout);
    }
  }
  // public incrementComment(livestream_id: number) {
  //   // this._commentsCached.push(comment);
  //   if (this._countCommentCached.has(livestream_id)) {
  //     const currentCountLivestreamReaction = this._countCommentCached.get(livestream_id);
  //     this._countCommentCached.set(livestream_id, currentCountLivestreamReaction + 1);
  //   } else {
  //     this._countCommentCached.set(livestream_id, 1);
  //   }
  //   if (!this._timeout) {
  //     this._timeout = setTimeout(async () => {
  //       let isUpdated = false;
  //       while (true) {
  //         try {
  //           const updateReactions = new Map(this._countCommentCached);
  //           await sequelize.transaction(async (transaction: Transaction) => {
  //             for (let livestreamId of updateReactions.keys()) {
  //               const countCachedReaction = updateReactions.get(livestreamId);
  //               await Livestream.increment(
  //                 { count_reaction: countCachedReaction },
  //                 { where: { id: livestreamId, is_active: IS_ACTIVE.ACTIVE } },
  //               );
  //             }
  //           });
  //           // need improve bellow in future
  //           this._countCommentCached = new Map();
  //           isUpdated = true;
  //         } catch (error) {
  //           isUpdated = false;
  //         }
  //         if (isUpdated) break;
  //         else await Utils.timer(1000);
  //       }
  //       this._timeout = null;
  //     }, this._ruleTimeout);
  //   }
  // }
  // public push({ df_reaction_id }: { df_reaction_id: number }) {}
}
export const livestreamCommentService = new LivestreamCommentService();
