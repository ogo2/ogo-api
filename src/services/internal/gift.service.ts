import Joi from '@utils/JoiValidate';

export class GiftService {
  public CreateGiftSchema = Joi.object({
    name: Joi.string().required(),
    price: Joi.number().integer().required(),
    discount_percent: Joi.number().allow('', null),
    max_discount_money: Joi.number().allow('', null),
    quantity: Joi.number().integer().required(),
    icon_url: Joi.string().required(),
    df_type_gift_id: Joi.number().integer().required(),
  });

  public UpdateGiftSchema = Joi.object({
    name: Joi.string().required(),
    price: Joi.number().integer().required(),
    discount_percent: Joi.number().allow('', null),
    max_discount_money: Joi.number().allow('', null),
    quantity: Joi.number().integer().required(),
    icon_url: Joi.string().required(),
    df_type_gift_id: Joi.number().integer().required(),
  });
}
