import { IS_ACTIVE, USER_STATUS, ROLE, ADMIN_ROLE } from '@utils/constants';
import Joi from '@utils/JoiValidate';

const db = require('@models');
const { sequelize, Sequelize, User } = db.default;

export class UserService {
  public UserSchema = Joi.object({
    name: Joi.string().required().label('Tên'),
    phone: Joi.string()
      .required()
      .label('Số điện thoại')
      .pattern(new RegExp(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/im))
      .message('Số điện thoại không hợp lệ'),
    email: Joi.string()
      .allow('', null)
      // .required()
      .label('Email')
      .pattern(
        new RegExp(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ),
      )
      .message('Email không hợp lệ'),

    profile_picture_url: Joi.string().allow('', null),
    device_id: Joi.string().allow('', null),
  });

  public RequestResetPasswordSchema = Joi.object({
    email: Joi.string()
      // .allow('', null)
      .required()
      .label('Email')
      .pattern(
        new RegExp(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ),
      )
      .message('Email không hợp lệ'),
  });

  public ResetPasswordSchema = Joi.object({
    email: Joi.string()
      // .allow('', null)
      .required()
      .label('Email')
      .pattern(
        new RegExp(
          /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        ),
      )
      .message('Email không hợp lệ'),
    code: Joi.string().required().label('Mã xác minh'),
    password: Joi.string().trim().min(6).required().label('Mật khẩu'),
  });

  public UserTypeSchema = Joi.object({
    df_type_user_id: Joi.number()
      .empty(['', null])
      .valid(...Object.values(ROLE))
      .required(),
  });
  public PasswordSchema = Joi.object({
    password: Joi.string().trim().min(6).required(),
  });

  public CustomerReferalSchema = Joi.object({
    code: Joi.string().empty(['', null, 0, 'null']),
  });
  public CustomerFollowShopSchema = Joi.object({
    shop_id: Joi.array().items(Joi.number().integer()).sparse().allow(null, ''),
  });

  public UpdateUserAdminInfor = Joi.object({
    name: Joi.string().allow('', null),
    email: Joi.string().allow('', null),
    profile_picture_url: Joi.string().allow('', null),
    role: Joi.number()
      .valid(...Object.values(ADMIN_ROLE))
      .allow('', null),
    status: Joi.number().allow('', null),
  });
  public UpdateUserCustomerInforSchema = Joi.object({
    name: Joi.string().allow('', null),
    email: Joi.string().allow('', null),
    profile_picture_url: Joi.string().allow('', null),
    status: Joi.number().allow('', null),
    date_of_birth: Joi.string().allow(null, ''),
    gender: Joi.number().allow('', null),
    address: Joi.string().allow('', null),
  });

  async findOneActiveUser(user_id: number) {
    const userInfo = await User.findOne({
      where: { id: user_id, is_active: IS_ACTIVE.ACTIVE, status: USER_STATUS.ACTIVE },
    });
    return userInfo.dataValues;
  }
}
