import { IS_ACTIVE, PRODUCT_TYPE, ORDER_STATUS, PRODUCT_STATUS, SHOP_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { Utils } from '@utils/Utils';
import { withPagingSuccess } from '@utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  Product,
  Category,
  ProductMedia,
  ProductPrice,
  ProductCustomAttributeOption,
  ProductCustomAttribute,
  Stock,
  // DFShipmerchant,
  // ProductShipMerchant,
  Level,
  OrderItem,
  Order,
  Shop,
  ProductStock,
  User,
  UserAddress,
  DFWard,
  DFDistrict,
  DFProvince,
} = db.default;
const { Op } = Sequelize;

export class ProductService {
  async isProductCodeExist(code: string): Promise<any> {
    const foundEnterprise = await Product.findOne({
      attributes: ['id', 'code'],
      where: { code, is_active: IS_ACTIVE.ACTIVE },
    });
    return foundEnterprise;
  }

  public async productDetail(product_id: number, request?: any): Promise<any> {
    const baseUrl = Utils.getBaseServer(request);
    const stock = await ProductPrice.findOne({ where: { product_id, is_active: IS_ACTIVE.ACTIVE } });
    // return withSuccess(productId);

    return await Product.findOne({
      attributes: [
        'id',
        'name',
        'description',
        'category_id',
        'shop_id',
        [sequelize.literal('IFNULL((star ),0)'), 'star'],
        [
          sequelize.literal(`
                    IFNULL((SELECT SUM(order_item.amount) as amount
                    FROM order_item
                    join \`order\` as od on od.id = order_item.order_id
                    WHERE
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                    and order_item.product_id = ${product_id}
                    and od.status = ${ORDER_STATUS.SUCCCESS}
                    ),0)`),
          'quantity_items',
        ],

        [
          sequelize.literal(`(SELECT MIN(stock_id) as stock_id
                    FROM product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = ${product_id}
                    )`),
          'stock_id',
        ],
        [
          sequelize.literal(`
                IFNULL((SELECT is_active
                FROM wishlist
                WHERE
                is_active = ${IS_ACTIVE.ACTIVE}
                and product_id = ${product_id}
                order by id desc
                limit 1
                ),0)`),
          'check_like',
        ],
        [
          sequelize.literal(`(
          select group_concat(attributes.attribute_media separator ' ') as attribute_media
            from (
          select concat("${baseUrl}","/",group_concat(DISTINCT pro_media.media_url separator ', ')) as attribute_media
          FROM product_price as pro_price
          join product_media pro_media on pro_price.custom_attribute_option_id_1 = pro_media.product_custom_attribute_option_id
          where pro_price.product_id = ${product_id}
          and pro_media.is_active = ${IS_ACTIVE.ACTIVE}
          and pro_media.product_custom_attribute_option_id IS NOT NULL
          group by pro_price.custom_attribute_option_id_1 , pro_media.media_url) as attributes
          )`),
          'attribute_media',
        ],
        [
          sequelize.literal(`(
          select group_concat(attributes.attribute_media separator ', ') as attribute_media
            from (
          select concat(COUNT(pro_option.id), " " ,group_concat(DISTINCT pro_att.name separator ', ')) as attribute_media
          FROM product_custom_attribute as pro_att
          join product_custom_attribute_option pro_option on pro_att.id = pro_option.product_custom_attribute_id
          where pro_att.product_id = ${product_id}
          and pro_att.is_active = ${IS_ACTIVE.ACTIVE}
          group by pro_att.name) as attributes
          )`),
          'attribute_custom',
        ],
        [
          sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = ${product_id}
                )`),
          'min_max_price',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT COUNT(id) as amount
                    FROM review
                    WHERE
                    review.is_active = ${IS_ACTIVE.ACTIVE}
                    and review.product_id = ${product_id}
                    ),0)`),
          'quantity_review',
        ],
      ],
      where: { is_active: IS_ACTIVE.ACTIVE, id: product_id, status: PRODUCT_STATUS.AVAILABLE },
      include: [
        {
          model: Shop,
          attributes: ['id', 'name'],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
          required: false,
        },
        {
          model: ProductMedia,
          // required: false,
          attributes: ['id', [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_url'], 'type'],
          where: { is_active: IS_ACTIVE.ACTIVE, product_custom_attribute_option_id: null },
          order: [['Products->ProductMedia.type', 'asc']],
        },
        {
          model: OrderItem,
          required: false,
          attributes: ['id'],
          where: { is_active: IS_ACTIVE.ACTIVE, product_id: product_id },
          include: {
            model: Order,
            // required: false,
            attributes: ['id'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
      ],
      group: [['ProductMedia.id']],
      // having: sequelize.where(sequelize.col('ProductPrices.agent_id'), '=', null),
      logging: console.log,
    });
    // }
  }

  public async findById(
    id: number,
    id_images?: number,
    stock_id?: number,
    request?: any,
    type?: number,
    shop_id?: number,
  ): Promise<any> {
    const baseUrl = Utils.getBaseServer(request);
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (type == PRODUCT_TYPE.DETAIL) {
      return await Product.findOne({
        attributes: {
          include: [
            [
              sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.id = ${id_images}
                ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`
                    IFNULL((SELECT MIN(product_price.stock_id)
                    FROM product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = Product.id
                    ),0)`),
              'stock_id',
            ],
            [
              sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.type = 1
                and product_media.product_id = Product.id
                limit 1
                ),null)`),
              'video_url',
            ],
            [sequelize.literal(`${id_images}`), 'media_id'],
            [
              Sequelize.literal(`(
              SELECT IF((SELECT IFNULL(SUM(product_price.amount), 0) from product_price
                where product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id) > 0,true,false)
              )`),
              'status_stock',
            ],
          ],
        },
        where: { id, is_active: IS_ACTIVE.ACTIVE },
        include: [
          {
            required: false,
            model: Category,
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
            include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
          },
          {
            model: ProductPrice,
            required: false,
            where: { is_active: IS_ACTIVE.ACTIVE, stock_id: stock_id != 0 ? stock_id : { [Op.ne]: null } },
            include: [
              {
                model: ProductCustomAttributeOption,
                required: false,
                attributes: ['name'],
                as: 'product_attribute_name_1',
                include: [
                  {
                    model: ProductCustomAttribute,
                    required: false,
                    where: { is_active: IS_ACTIVE.ACTIVE },
                    attributes: ['name'],
                  },
                  {
                    model: ProductMedia,
                    required: false,
                    where: { is_active: IS_ACTIVE.ACTIVE },
                    attributes: ['media_url'],
                  },
                ],
              },
              {
                model: ProductCustomAttributeOption,
                required: false,
                attributes: ['name'],
                as: 'product_attribute_name_2',
                include: {
                  model: ProductCustomAttribute,
                  where: { is_active: IS_ACTIVE.ACTIVE },
                  attributes: ['name'],
                },
              },
              {
                model: Stock,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['name', 'pancake_stock_id'],
              },
            ],
          },
          {
            model: ProductCustomAttribute,
            required: false,
            attributes: ['name', 'display_order', 'id'],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: {
              model: ProductCustomAttributeOption,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: [
                'name',
                'id',
                // [sequelize.literal('`ProductCustomAttributes->ProductCustomAttributeOptions->ProductMedium`.`media_url`'), 'media_url'],
                [
                  sequelize.fn(
                    'CONCAT',
                    baseUrl,
                    '/',
                    sequelize.literal(
                      '`ProductCustomAttributes->ProductCustomAttributeOptions->ProductMedium`.`media_url`',
                    ),
                  ),
                  'media_url',
                ],
              ],
              include: {
                model: ProductMedia,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: [],
              },
            },
          },
          {
            model: ProductMedia,
            attributes: [
              'id',
              'type',
              'product_custom_attribute_option_id',
              'is_active',
              // [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_urls'],
              [sequelize.fn('CONCAT', baseUrl, '/', sequelize.literal('`ProductMedia`.`media_url`')), 'media_url'],
              [
                sequelize.literal(`IFNULL((
              SELECT  product_media.media_url as media_url
              FROM product_media
              WHERE
              product_media.is_active = ${IS_ACTIVE.ACTIVE}
              and product_media.id = ProductMedia.id
              ),null)`),
                'path_url',
              ],
            ],
            required: false,
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              id: id_images ? { [Op.ne]: id_images } : { [Op.ne]: null },
              product_custom_attribute_option_id: null,
            },
            order: [['display_order', 'asc']],
          },
        ],
        logging: true,
        group: [
          'ProductCustomAttributes.id',
          'ProductMedia.id',
          'ProductPrices.id',
          'ProductCustomAttributes->ProductCustomAttributeOptions.id',
          'ProductCustomAttributes->ProductCustomAttributeOptions->ProductMedium.id',
          'ProductMedia.id',
        ],
      });
      // return withSuccess(detailProduct);
    }
    if (type == PRODUCT_TYPE.SELL) {
      const { rows, count } = await User.findAndCountAll({
        attributes: [
          'id',
          'name',
          'phone',
          [
            Sequelize.literal(`(SELECT 
            IFNULL(sum(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE od.user_id = User.id
            AND
            ot.product_id = ${id}
            AND
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
            'total_amount_order',
          ],
          [
            Sequelize.literal(`(
            SELECT IFNULL(SUM(\`ot\`.\`price\` * \`ot\`.\`amount\`), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE od.user_id = User.id
            AND
            ot.product_id = ${id}
            AND
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
            'total_price',
          ],
          // [
          //   Sequelize.literal(`(SELECT
          //     IFNULL(SUM(ot.price * ot.amount), 0)
          //     FROM ogo.order as od
          //     join order_item as ot on ot.order_id = od.id
          //     WHERE od.user_id = user.id
          //     AND
          //     od.is_active = ${IS_ACTIVE.ACTIVE}
          //     AND
          //     od.status = ${ORDER_STATUS.SUCCCESS}
          //     )`),
          //   'total_price',
          // ],
        ],
        where: { is_active: IS_ACTIVE.ACTIVE },
        include: [
          {
            model: Order,
            // required: false,
            attributes: [],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              shop_id: shop_id != null && shop_id != undefined ? shop_id : { [Op.ne]: null },
              status: ORDER_STATUS.SUCCCESS,
            },
            include: {
              attributes: [],
              model: OrderItem,
              where: { is_active: IS_ACTIVE.ACTIVE, product_id: id },
            },
          },
        ],
        group: ['User.id'],
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
    }

    if (type == PRODUCT_TYPE.ORDER) {
      const { rows, count } = await Order.findAndCountAll({
        attributes: [
          'id',
          'code',
          [
            Sequelize.literal(`(SELECT 
            IFNULL(sum(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE od.id = Order.id
            AND
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            ot.product_id = ${id}
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
            'total_amount_order',
          ],
          'total_price',
          [
            Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                FROM \`order\` as od
                join order_item as order_item
                ON order_item.order_id = od.id
                WHERE
                    od.is_active = ${IS_ACTIVE.ACTIVE}
                    AND
                    od.id = Order.id
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                )`),
            'total_price_old',
          ],
          [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
          [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
          [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
          [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
          [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
          // 'total_price',
          // [
          //   Sequelize.literal(`(SELECT
          //   IFNULL(SUM(ot.price * ot.amount), 0)
          //   FROM ogo.order as od
          //   join order_item as ot on ot.order_id = od.id
          //   WHERE od.id = Order.id
          //   AND
          //   od.is_active = ${IS_ACTIVE.ACTIVE}
          //   AND
          //   od.status = ${ORDER_STATUS.SUCCCESS}
          //   )`),
          //   'total_price',
          // ],
        ],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id != null && shop_id != undefined ? shop_id : { [Op.ne]: null },
          status: ORDER_STATUS.SUCCCESS,
        },
        include: [
          {
            model: User,
            // required: false,
            attributes: ['id', 'name', 'phone'],
            where: { is_active: IS_ACTIVE.ACTIVE },
            // include: {
            //   attributes: [],
            //   model: OrderItem,
            //   where: { is_active: IS_ACTIVE.ACTIVE, product_id: id }
            // }
          },
          {
            model: UserAddress,
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          {
            attributes: ['id', 'product_id'],
            model: OrderItem,
            where: { is_active: IS_ACTIVE.ACTIVE, product_id: id },
          },
        ],
        group: ['Order.id'],
        order: [['id', 'desc']],
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
    }
  }

  public async checkExistProduct(id: number): Promise<any> {
    const product = await Product.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id },
    });
    if (!product) {
      throw new AppError(ApiCodeResponse.NOT_FOUND).with('Sản phẩm không tồn tại');
    }
  }
}
