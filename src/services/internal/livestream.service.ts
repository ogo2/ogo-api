import { LivestreamTypes } from 'types';
import { LivestreamYoutubeService } from '@services/google/youtube.service';
import { IS_ACTIVE, LIVESTREAM_STATUS } from '@utils/constants';
import Joi from '@utils/JoiValidate';
import { Credentials as GoogleCredentials } from 'google-auth-library';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, Livestream, Shop } = db.default;

export class LivestreamService {
  private livestreamYoutubeService: LivestreamYoutubeService;
  constructor() {
    this.livestreamYoutubeService = new LivestreamYoutubeService();
  }
  public CreateLivestreamSchema = Joi.object({
    title: Joi.string().required().label('Tiêu đề.'),
    cover_image_url: Joi.string().required().label('Ảnh bìa.'),
    products: Joi.array()
      .items(
        Joi.object()
          .keys({
            id: Joi.number().integer().required(),
            code_product_livestream: Joi.string().allow('', null),
          })
          .unknown(true),
      )
      .required(),
  });
  public CreateOrUpdateLivestreamProductSchema = Joi.object({
    products: Joi.array()
      .items(
        Joi.object()
          .keys({
            id: Joi.number().integer().required(),
            code_product_livestream: Joi.string().allow('', null),
          })
          .unknown(true),
      )
      .required(),
  });

  public UpdateBroadcast = Joi.object({
    broadcast_id: Joi.string().min(10).max(15).required(),
  });

  async getStreaming() {
    const streamings = await Livestream.findAll({
      where: { status: LIVESTREAM_STATUS.STREAMING, is_active: IS_ACTIVE.ACTIVE },
    });
    return streamings;
  }

  public async stopStreaming(foundStreaming: LivestreamTypes.Livestream, tokens: GoogleCredentials): Promise<any> {
    // open here in production
    // stop live api
    // ATTENTION: you must call setYoutubeService to set youtube service, detected who u are
    await this.livestreamYoutubeService.setYoutubeService(tokens);
    const resultStopLivestream = await this.livestreamYoutubeService.stopLivestream({
      broadcast_id: foundStreaming.broadcast_id,
    });
    // // số phút shop đã sử dụng
    const usedMinus = Utils.calMinusUsed(foundStreaming.start_at);
    // // số phút còn lại
    const remainingTime: number = foundStreaming?.Shop?.stream_minutes_available - usedMinus;
    await sequelize.transaction(async (transaction) => {
      await Livestream.update(
        {
          status: LIVESTREAM_STATUS.FINISHED,
          finish_at: new Date(),
        },
        { where: { id: foundStreaming.id }, transaction },
      );
      await Shop.update(
        { stream_minutes_available: remainingTime >= 0 ? remainingTime : 0 },
        { where: { id: foundStreaming.shop_id }, transaction },
      );
    });
    return { resultStopLivestream };
  }
}
