import { FOLLOW_STATUS, IS_ACTIVE } from '@utils/constants';
import Joi from '@utils/JoiValidate';

const db = require('@models');
const { sequelize, Sequelize, Shop, DFNotification, Notification, User, Follow } = db.default;
const { Op } = Sequelize;

export class NotificationService {
  public async findOneTypeNotification(id: number): Promise<any> {
    const notiData = await DFNotification.findOne({ where: { id } });
    return notiData;
  }
  public async shopName(id: number): Promise<any> {
    const notiData = await Shop.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE, status_follow: FOLLOW_STATUS.ACTIVE },
    });
    return notiData;
  }
  public async shopNameFollow(id: number, user_id: number): Promise<any> {
    const notiData = await Shop.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE, status_follow: FOLLOW_STATUS.ACTIVE },
      include: {
        model: Follow,
        required: true,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        where: { is_active: IS_ACTIVE.ACTIVE, user_id: user_id, status: FOLLOW_STATUS.ACTIVE },
      },
    });
    return notiData;
  }
  public async customerName(id: number): Promise<any> {
    const notiData = await User.findOne({ where: { id, is_active: IS_ACTIVE.ACTIVE } });
    return notiData;
  }
  public async createNotification(
    user_id,
    title,
    content,
    is_read,
    df_notification_id,
    data,
    shop_id,
    transaction,
  ): Promise<any> {
    const dataNoti = await Notification.create(
      {
        user_id,
        title,
        content,
        is_read,
        df_notification_id,
        data,
        shop_id,
      },
      { transaction },
    );
    return dataNoti.id;
  }
  public async createMultiNotification(notifications?: [], transaction?: any) {
    const schema = Joi.object({
      notifications: Joi.array().items(
        Joi.object({
          df_notification_id: Joi.number().integer().required(),
          content: Joi.string().required(),
          data: Joi.object().required(),
          user_id: Joi.number().integer().allow(null, ''),
          title: Joi.string().required(),
        }).unknown(true),
      ),
    }).unknown(true);
    await schema.validateAsync({ notifications });

    return Notification.bulkCreate(notifications, { transaction });
  }
}
