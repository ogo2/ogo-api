import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { CredentialsInput, GoogleAuth2Service } from './googleOAuth2.service';
import * as _ from 'lodash';
import { Credentials as GoogleCredentials, OAuth2Client } from 'google-auth-library';
import { youtube_v3 } from 'googleapis';
import { Utils } from '@utils/Utils';
export interface IGetStatusLivestream {
  broadcast_id: string;
}

export interface IGetExistLivestream {
  broadcast_id: string;
  status?: Array<string> | string;
}

export interface ICreateBroadcast {
  title: string;
  scheduledStartTime: string; // iso date
}

export interface IActiveLivestream {
  broadcast_id: string;
}

export class LivestreamYoutubeService {
  private googleAuth2Service: GoogleAuth2Service;
  private youtubeService: youtube_v3.Youtube;
  constructor() {
    // const fakeCredentials: CredentialsInput = Utils.fakeReadJsonFile();
    // use here if u resolve upon problem
    this.googleAuth2Service = new GoogleAuth2Service();
  }

  // inject tokens to set youtube service
  public async setYoutubeService(tokens: GoogleCredentials): Promise<void> {
    this.youtubeService = await this.googleAuth2Service.getYoutubeService(tokens);
  }

  public async getStatusLivestream({ broadcast_id }: IGetStatusLivestream) {
    const listData = {
      id: [broadcast_id],
      part: ['id', 'snippet', 'cdn', 'status'],
    };
    const listResult = await this.youtubeService.liveStreams.list(listData);
    return listResult.data;
  }

  /**
   * get existed Broadcast livestream youtube
   */

  public async getExistLivestream({ broadcast_id, status }: IGetExistLivestream) {
    const listliveBroadcastsData = {
      part: ['id', 'status'],
      id: [broadcast_id],
    };

    const listliveBroadcastsResult = await this.youtubeService.liveBroadcasts.list(listliveBroadcastsData);
    let filterBroadcast;
    if (!status) return listliveBroadcastsResult.data.items[0];
    else if (_.isArray(status)) {
      filterBroadcast = listliveBroadcastsResult.data.items.find((item) =>
        status.includes(item.status.lifeCycleStatus),
      );
      return filterBroadcast;
    } else if (_.isString(status)) {
      filterBroadcast = listliveBroadcastsResult.data.items.find((item) => item.status.lifeCycleStatus === status);
    }
    return filterBroadcast;
  }

  public async createYoutubeBroadcast({ title, scheduledStartTime }: ICreateBroadcast) {
    const broadcastData = {
      part: ['id', 'snippet', 'contentDetails', 'status'],
      requestBody: {
        snippet: {
          title: title,
          scheduledStartTime: scheduledStartTime,
        },
        contentDetails: {
          enableClosedCaptions: false,
          enableContentEncryption: false,
          enableDvr: false,
          // enableEmbed: true,
          recordFromStart: true,
          startWithSlate: false,
          enableAutoStart: true,
          enableAutoStop: false,
          monitorStream: {
            enableMonitorStream: true,
          },
          enableLowLatency: false,
          latencyPreference: 'ultraLow',
        },
        status: {
          privacyStatus: 'unlisted',
          selfDeclaredMadeForKids: false,
        },
      },
    };

    const streamData = {
      part: ['id', 'snippet', 'contentDetails', 'cdn'],
      requestBody: {
        snippet: {
          title: title,
        },
        cdn: {
          frameRate: '30fps',
          ingestionType: 'rtmp',
          resolution: '720p',
        },
        contentDetails: {
          isReusable: false,
        },
      },
    };
    try {
      const broadcastResult = await this.youtubeService.liveBroadcasts.insert(broadcastData);
      const broadcast_id = broadcastResult.data.id;
      const streamResult = await this.youtubeService.liveStreams.insert(streamData);
      const stream_id = streamResult.data.id;

      const updateData = {
        part: ['id', 'snippet', 'status'],
        requestBody: {
          id: broadcast_id,
          snippet: {
            title: title,
            categoryId: '24', // entertainment , // https://developers.google.com/youtube/v3/docs/videoCategories/list
          },
          status: {
            selfDeclaredMadeForKids: false,
            embeddable: true,
          },
        },
      };
      await this.youtubeService.videos.update(updateData);

      if (broadcast_id && stream_id) {
        const bindData = {
          id: broadcast_id,
          part: ['id', 'status'],
          // part: ['id', 'snippet', 'contentDetails', 'status'],
          streamId: stream_id,
        };
        await this.youtubeService.liveBroadcasts.bind(bindData);
        const listData = {
          id: [stream_id],
          part: ['id', 'cdn', 'status'],
        };
        const listResult = await this.youtubeService.liveStreams.list(listData);
        if (listResult.data.items && listResult.data.items.length > 0) {
          const cdn = listResult.data.items[0].cdn;
          const status = listResult.data.items[0].status;
          return { broadcast_id, stream_id, cdn, status };
        }
      }
    } catch (error) {
      if (error.code == 403 && error.response.data.error == 'access_denied') {
        console.log(error?.response?.data);
        throw new AppError(ApiCodeResponse.GOOGLE_ACCESS_ERROR);
      }
      if (error.code == 401 && error.response.data.error == 'unauthorized_client') {
        throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
      }
      console.log('error', error);
      throw new AppError(ApiCodeResponse.GOOGLE_COMMON_ERROR);
    }
  }

  public async activeLivestream({ broadcast_id }: IActiveLivestream) {
    const transitionData = {
      broadcastStatus: 'live',
      id: broadcast_id,
      part: ['id', 'snippet', 'contentDetails', 'status'],
    };

    try {
      const transitionResult = await this.youtubeService.liveBroadcasts.transition(transitionData);
      console.log('transitionResult - live', transitionResult);
      return transitionResult;
    } catch (error) {
      console.log(error?.response?.data);
      if (error.code === 401) {
        throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
      }
      if (error.code === 403) {
        console.log(error);
        if (error.errors[0].reason == 'errorStreamInactive' || error.errors[0].reason == 'invalidTransition') {
          // throw new AppError(ApiCodeResponse.GOOGLE_LIVESTREAM_ERROR);
        }
        if (error.code === 403 && error.errors[0].reason == 'redundantTransition') {
          // throw new AppError(ApiCodeResponse.GOOGLE_LIVESTREAM_ERROR);
        }
      }
      // throw new AppError(ApiCodeResponse.GOOGLE_COMMON_ERROR);
    }
  }

  public async stopLivestream({ broadcast_id }: IActiveLivestream) {
    try {
      const transitionData = {
        broadcastStatus: 'complete',
        id: broadcast_id,
        part: ['id', 'snippet', 'contentDetails', 'status'],
      };
      const transitionResult = await this.youtubeService.liveBroadcasts.transition(transitionData);

      console.log('transitionResult - complete', transitionResult);
      return transitionResult;
    } catch (error) {
      console.log(error?.response?.data);
      if (error.code == 401 && error.response.data.error == 'unauthorized_client') {
        throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
      }
      if (error.code == 403 && error.response.data.error == 'access_denied') {
        // throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
      }
      if (
        error.code == 403 &&
        (error.response.data.error_description == 'errorStreamInactive' ||
          error.response.data.error_description == 'invalidTransition')
      ) {
        // throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
      }

      // throw new AppError(ApiCodeResponse.GOOGLE_AUTHORIZED_ERROR);
    }
  }

  public async statisticsLivestream({ broadcast_id }: IActiveLivestream) {}
}
