import { AppError } from '@utils/AppError';
import axios from 'axios';

const BASE_GOOGLE_API = 'https://www.googleapis.com/oauth2/v3';

/**
 * Get google profile information by access_token
 *
 */
export class GoogleService {
  private static apiInstance = axios.create({
    headers: {
      'Content-Type': 'application/json',
    },
    timeout: 20000,
    responseType: 'json',
  });
  public async getProfileInfo(access_token: string) {
    const url = `${BASE_GOOGLE_API}/userinfo?access_token=${access_token}`;
    try {
      const res = await GoogleService.apiInstance.get(url);
      return res.data;
    } catch (error) {
      throw new AppError(error);
    }
  }
}
