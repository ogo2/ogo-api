import * as nodemailer from 'nodemailer';
import { environment } from '../../config';
export interface ISendMailResetPassword {
  email_receiver: string;
  code: number;
}

export class MailService {
  public async sendMailResetPassword({ email_receiver, code }: ISendMailResetPassword) {
    /**
     * click here to active feat gmail send code verify
     * https://myaccount.google.com/lesssecureapps
     */
    const smtpTransport = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: environment.user_email,
        pass: environment.password_email,
      },
    });
    const mailOptions = {
      from: environment.user_email,
      to: email_receiver,
      subject: 'Ogo Reset pasword',
      generateTextFromHTML: true,
      html: `<div>Mã xác minh đổi mật khẩu ogo của bạn là: <b>${code}</b></div>`,
    };
    const res = await smtpTransport.sendMail(mailOptions);
    return res;
  }
}
