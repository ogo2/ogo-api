require('dotenv').config();
import { google, youtube_v3 } from 'googleapis';
import { Credentials as GoogleCredentials } from 'google-auth-library';
import { Utils } from '@utils/Utils';
import { environment } from '@config/environment';
const { OAuth2 } = google.auth;

export interface CredentialsInput {
  web: {
    client_id: string;
    project_id: string;
    auth_uri: string;
    token_uri: string;
    auth_provider_x509_cert_url: string;
    client_secret: string;
    redirect_uris: Array<string>;
    javascript_origins: Array<string>;
  };
}

export class GoogleAuth2Service extends OAuth2 {
  constructor() {
    const credentials: CredentialsInput = Utils.readJsonFile<CredentialsInput>(environment.name_google_secret_file);
    const clientId = credentials.web.client_id;
    const clientSecret = credentials.web.client_secret;
    const redirectUrl = credentials.web.redirect_uris[process.env.NODE_ENV === 'production' ? 1 : 0];
    super(clientId, clientSecret, redirectUrl);
  }
  /**
   * Docstring google scope:
   * https://developers.google.com/identity/protocols/oauth2/scopes#people
   */
  /**
   * Must active this feature at google console:
   * + Google People API
   * + YouTube Data API v3
   *  */
  private static SCOPES = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/youtube',
    'https://www.googleapis.com/auth/youtube.force-ssl',
  ];

  // setter
  private async setTokensCredentials(tokens: GoogleCredentials) {
    this.credentials = tokens;
    google.options({ auth: this });
    return google;
  }

  public async refreshTokenOveride(refresh_token: string): Promise<GoogleCredentials> {
    const response = await this.refreshToken(refresh_token);
    const tokens = response.tokens;
    return tokens;
  }

  public async getNewUrlRequestToken(token: string) {
    const authUrl = this.generateAuthUrl({
      access_type: 'offline',
      scope: GoogleAuth2Service.SCOPES,
      response_type: 'code',
      prompt: 'consent',
      state: `{ "token": "${token}" }`,
    });

    return authUrl;
  }

  public async getAuthTokens(code: string) {
    const { tokens } = await this.getToken(code);
    return tokens;
  }

  public async getYoutubeService(tokens: GoogleCredentials): Promise<youtube_v3.Youtube> {
    const googleAuth = await this.setTokensCredentials(tokens);
    const youtubeService: youtube_v3.Youtube = googleAuth.youtube({
      version: 'v3',
    });
    return youtubeService;
  }

  public async getTokensByRefreshToken(refresh_token: string): Promise<GoogleCredentials> {
    const tokens = await this.refreshTokenOveride(refresh_token);
    return tokens;
  }
}
