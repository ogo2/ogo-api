import { logger } from '@utils/Logger';
import * as fs from 'fs';
import * as express from 'express';
import { environment } from '../config';
export class Utils {
  /**
   * Docstring Utils class
   * this class containts static helpers function for all project
   */
  private static cleanupText(text) {
    return (
      text &&
      text // Ensure text exists
        .trim() // Trim left and right spaces
        .replace(/\n{2,}/g, '\n\n') // Replace 2+ linebreaks with 2 ones
        .replace(/ +/g, ' ')
    ); // Replace consecutive spaces with one
  }

  public static trimStringProperties(obj) {
    if (obj !== null && typeof obj === 'object') {
      for (let prop in obj) {
        // if the property is an object trim it too
        if (typeof obj[prop] === 'object') {
          // eslint-disable-next-line no-undef
          return Utils.trimStringProperties(obj[prop]);
        }
        // if it's a string remove begin and end whitespaces
        if (typeof obj[prop] === 'string') {
          // obj[prop] = obj[prop].trim();
          obj[prop] = Utils.cleanupText(obj[prop]);
        }
      }
    }
  }

  public static readJsonFile<T>(file_name: string): T | null {
    try {
      const buff = fs.readFileSync(`./${file_name}`);
      return JSON.parse(buff.toString());
    } catch (error) {
      logger.error({ message: `Error loading json file: ${error}` });
      return null;
    }
  }

  public static fakeReadJsonFile() {
    return {
      web: {
        client_id: '1080157083853-3a5hcuqmegbu2t66q3b644d6j1femjub.apps.googleusercontent.com',
        project_id: 'ogo-live',
        auth_uri: 'https://accounts.google.com/o/oauth2/auth',
        token_uri: 'https://oauth2.googleapis.com/token',
        auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
        client_secret: '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',
        redirect_uris: [
          'http://localhost:2024/api/v1/shop/config-livestream/goolge/callback',
          'http://api.ogo.winds.vn/api/v1/shop/config-livestream/goolge/callback',
        ],
        javascript_origins: ['http://localhost:2024', 'http://api.ogo.winds.vn'],
      },
    };
  }

  public static getBaseServer(request: express.Request): string {
    return environment.s3url;
    // return `${request.protocol}://${request.headers.host}`;
  }
  public static getBaseImageUrl(): string {
    return environment.s3url;
  }

  public static replaceAllImageUrl(path?: string): string {
    let string = path;
    let i = 1;
    console.log('first', string);
    while (i <= 5) {
      if (string.includes(environment.s3url)) string.replace(environment.s3url, '');
      console.log('first', string);
      i++;
    }
    return string;
  }
  public static getFullUrl(path?: string): string {
    if (!path) {
      return null;
    }
    if (!path.startsWith('http')) {
      const correctPath = path.replace(/^\//g, '');
      return `${environment.s3url}/${correctPath}`;
    }
    return path;
  }

  public static timer(ms: number) {
    return new Promise((res) => setTimeout(res, ms));
  }

  public static cvtSecondsToMinus(seconds: number) {
    return Math.round(seconds / 60);
  }

  public static calculateExpireTimeLivestream(minutes_available: number): Date {
    const expireTime = 60 * minutes_available;
    const currentTime = Math.floor(Date.now() / 1000);
    const privilegeExpireTime = currentTime + expireTime;
    return new Date(privilegeExpireTime * 1000);
  }

  /**
   *
   * @param startAt start time
   * @returns minus from startAt to now
   */
  public static calMinusUsed(startAt: Date | string): number {
    const timestampStart = new Date(startAt).getTime();
    const timestampNow = Date.now();
    if (timestampStart > timestampNow) return 0;
    return Math.round((timestampNow - timestampStart) / 1000 / 60);
  }
}
