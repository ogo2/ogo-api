import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { AppTypes } from 'types';

export function withError(error: any, errors?: any): AppTypes.ErrorResponseModel {
  return {
    status: 0,
    code: error.code || ApiCodeResponse.DB_ERROR.code,
    message: error.message || ApiCodeResponse.DB_ERROR.message,
    debug: process.env.NODE_ENV === 'development' ? error : undefined,
    // error: error,
    errors,
  };
}
export function withSuccess<T>(data: T, code?: number): AppTypes.SuccessResponseModel<T> {
  return {
    status: 1,
    code: 1,
    message: 'Thành công',
    data,
  };
}

export function withPagingSuccess<T>(data: T, paging?: AppTypes.PagingModel): AppTypes.PagingResponseModel<T> {
  return {
    status: 1,
    code: 1,
    message: 'Thành công',
    data,
    paging,
  };
}

export function withIOSuccess<T>(
  data: T,
  type_action: number,
  option?: { message?: string; code?: number },
): AppTypes.SuccessIOResponseModel<T> {
  return {
    status: 1,
    code: option?.code || 1,
    message: option?.message || 'Thành công',
    data,
    type_action,
    create_at: new Date(),
  };
}
