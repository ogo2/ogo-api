import 'module-alias/register';
import * as express from 'express';
import { Server as SocketServer } from 'socket.io';
import { Server } from 'http';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { json, urlencoded } from 'body-parser';
import { Express, Response, NextFunction } from 'express';
import { AppTypes } from './types';
import * as routes from './routes';
import { environment } from './config/';
import * as startSetup from '../setup';
import { logger } from './utils/Logger';
import { socketAuthentication } from './middleware/socketAuthMiddleware';
import initSocketEvent from './websocket';
import { TaskRegister } from './crons';
import { RedisAdapter } from './websocket/adapter/redis/redis.adapter';
import { serve } from 'swagger-ui-express';
const socketIO = require('socket.io');
const http = require('http');

export class MyServer {
  private app: Express;
  private server: Server;
  public io: SocketServer;
  /**
   * constructor class
   */
  constructor() {
    this.app = express();
    this.useExpressMiddleware();
    routes.initRoutes(this.app);
    this.createServerSocket();
    TaskRegister.enforce();
  }

  /**
   * @summary
   * @returns void
   */
  public createServerSocket(): void {
    this.server = http.createServer(this.app);
    this.io = socketIO(this.server, {
      cors: {
        allowedHeaders: ['token'],
        credentials: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS'],
        origin: '*',
        preflightContinue: false,
        optionsSuccessStatus: 200,
      },
    });
    const adapter = new RedisAdapter();
    this.io.adapter(adapter.createAdapter());
    this.useSocketMiddleWare();
    initSocketEvent(this.io);
  }

  /**
   * @summary use socket middleWare
   */
  public useSocketMiddleWare() {
    this.io.use(socketAuthentication);
  }

  /**
   * @summary use express middleware
   * @returns void
   */
  public useExpressMiddleware(): void {
    this.app.use((req: AppTypes.RequestIOAuth, res: Response, next: NextFunction) => {
      req.io = this.io;
      next();
    });
    this.app.use(json());
    this.app.use(cors());
    this.app.use(cors({ optionsSuccessStatus: 200 }));
    this.app.use(express.static('public'));
    // this.app.use(upload.any());
    this.app.use('/uploads', express.static('uploads'));
    this.app.use(urlencoded({ extended: true }));
    this.app.use(morgan('combined'));
  }

  /**
   * server listen
   */
  public listen(): void {
    const PORT: number = environment.port;
    this.server.listen(PORT, () => {
      logger.info({ message: `Server successfully started at port ${PORT}` });
    });
  }
}
const server: MyServer = new MyServer();
server.listen();
startSetup();
export const socketServer = server.io;
