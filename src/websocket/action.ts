import { withIOSuccess } from '@utils/BaseResponse';
import { Server } from 'socket.io';
import {
  withPostChannel,
  withLiveStreamChannel,
  withTopicMessageChannel,
  withUserChannel,
  withShopChannel,
  LIVESTREAM_EVENT,
  POST_EVENT,
  MESSAGE_EVENT,
  USER_EVENT,
  SHOP_EVENT,
} from './constants';

export const MessageAction = (io: Server) => {
  return {
    sendMessage: (topic_message_id: number, data: any) => {
      io.to(withTopicMessageChannel(topic_message_id)).emit(
        withTopicMessageChannel(topic_message_id),
        withIOSuccess(data, MESSAGE_EVENT.SEND_MESSAGE),
      );
    },
    readMessage: (topic_message_id: number, data: any) => {
      io.to(withTopicMessageChannel(topic_message_id)).emit(
        withTopicMessageChannel(topic_message_id),
        withIOSuccess(data, MESSAGE_EVENT.READ),
      );
    },
    typingMesssage: (topic_message_id: number, data: any) => {
      io.to(withTopicMessageChannel(topic_message_id)).emit(
        withTopicMessageChannel(topic_message_id),
        withIOSuccess(data, MESSAGE_EVENT.TYPING),
      );
    },
  };
};

export const UserAction = (io: Server) => {
  return {
    sendMessage: (user_id: number, data: any) => {
      io.emit(withUserChannel(user_id), withIOSuccess(data, USER_EVENT.NEW_MESSAGE));
    },
    sendMessageNewChannel: (user_id: number, data: any) => {
      io.emit(withUserChannel(user_id), withIOSuccess(data, USER_EVENT.NEW_CHANNEL_MESSAGE));
    },
    sendNotification: (user_id: number, data: any) => {
      io.emit(withUserChannel(user_id), withIOSuccess(data, USER_EVENT.NEW_NOTIFICATION));
    },
  };
};

export const ShopAction = (io: Server) => {
  return {
    sendMessage: (shop_id: number, data: any) => {
      io.emit(withShopChannel(shop_id), withIOSuccess(data, SHOP_EVENT.NEW_MESSAGE));
    },
    sendMessageNewChannel: (shop_id: number, data: any) => {
      io.emit(withShopChannel(shop_id), withIOSuccess(data, SHOP_EVENT.NEW_CHANNEL_MESSAGE));
    },
    sendNotification: (shop_id: number, data: any) => {
      io.emit(withShopChannel(shop_id), withIOSuccess(data, SHOP_EVENT.NEW_NOTIFICATION));
    },
  };
};

export const PostAction = (io: Server) => {
  return {
    sendReactionPost: (post_id: number, data: any) => {
      io.emit(withPostChannel(post_id), withIOSuccess(data, POST_EVENT.REACTION_POST));
    },
    sendUnReactionPost: (post_id: number, data: any) => {
      io.emit(withPostChannel(post_id), withIOSuccess(data, POST_EVENT.UNREACTION_POST));
    },
    sendReactionCommentPost: (post_id: number, data: any) => {
      io.emit(withPostChannel(post_id), withIOSuccess(data, POST_EVENT.REACTION_COMMENT_POST));
    },
    sendUnReactionCommentPost: (post_id: number, data: any) => {
      io.emit(withPostChannel(post_id), withIOSuccess(data, POST_EVENT.UNREACTION_COMMENT_POST));
    },
    sendComment: (post_id: number, data: any) => {
      io.emit(withPostChannel(post_id), withIOSuccess(data, POST_EVENT.COMMENT));
    },
  };
};
