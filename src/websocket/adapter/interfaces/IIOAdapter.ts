import { Server as SocketServer } from 'socket.io';
import { Adapter } from 'socket.io/node_modules/socket.io-adapter/dist';

export interface IIOAdapter {
  createAdapter(): Adapter | any;
}
