import { RedisClient } from 'redis';
import { createAdapter } from '@socket.io/redis-adapter';
import { getRefRedisConnection } from '@services/detributed/pubsub/publisher/channels/redis/redis.provider';
import { IIOAdapter } from '../interfaces/IIOAdapter';

export class RedisAdapter implements IIOAdapter {
  private pubClient: RedisClient;
  private subClient: RedisClient;
  constructor() {
    this.pubClient = getRefRedisConnection();
    this.subClient = getRefRedisConnection();
  }
  createAdapter() {
    const adapter: any = createAdapter(this.pubClient, this.subClient);
    return adapter;
  }
}
