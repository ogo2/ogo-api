import { createClient, RedisClient } from 'redis';
import { environment } from '@config/environment';
export const redisProvider: RedisClient = createClient({ ...environment.redis });
export const getRefRedisConnection = () => redisProvider.duplicate();
