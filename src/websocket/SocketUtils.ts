import { Server } from 'socket.io';
import { withLiveStreamChannel } from './constants';

export class SocketUtils {
  // room: id topic message, set uid in room
  private static _chatRooms: Map<number, Set<number>> = new Map();

  public static countUserInSocketChannel(socket: Server, channel: string) {
    const hasRoomExisted = socket.sockets.adapter.rooms.has(channel);
    if (hasRoomExisted) {
      // reason -1: subtract user publish livestream
      return socket.sockets.adapter.rooms.get(channel).size - 1;
    } else return null;
  }

  public static async countUserInAdapterSocketLivestreamChannel(socket: Server, livestream_id: number) {
    const channel = withLiveStreamChannel(livestream_id);
    const sSockets = await socket.in(channel).allSockets();
    if (sSockets.size === 0) return null;
    return {
      livestream_id,
      count_subscribe: sSockets.size,
      channel,
    };
  }

  private static createRoom(topic_message_id: number, uid: number) {
    const setInit = new Set<number>();
    setInit.add(uid);
    SocketUtils._chatRooms.set(topic_message_id, setInit);
  }

  public static joinRoom(topic_message_id: number, uid: number) {
    if (SocketUtils._chatRooms.has(topic_message_id)) {
      SocketUtils._chatRooms.get(topic_message_id).add(uid);
    } else {
      SocketUtils.createRoom(topic_message_id, uid);
    }
  }
  public static leaveRoom(topic_message_id: number, uid: number) {
    if (SocketUtils._chatRooms.has(topic_message_id)) {
      const setSocket = SocketUtils._chatRooms.get(topic_message_id);
      if (setSocket.has(uid)) setSocket.delete(uid);
    }
  }

  public static leaveAllRoom(uid: number) {
    for (let [key, value] of SocketUtils._chatRooms) {
      if (value.has(uid)) value.delete(uid);
    }
  }

  public static hasUserIdInRoom(topic_message_id: number, uid: number): boolean {
    if (SocketUtils._chatRooms.has(topic_message_id)) {
      return SocketUtils._chatRooms.get(topic_message_id).has(uid);
    }
    return false;
  }
}
