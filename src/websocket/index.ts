import { Socket, Server } from 'socket.io';
import { DEFAULT_EVENT } from './constants';
import { UserTypes } from 'types';
import { SocketUtils } from './SocketUtils';
import { initGateWay } from './gateway';

const initSocketEvent = (socket: Server) => {
  // setInterval(async () => {
  //   const all = await socket.allSockets();
  //   const inRoom1 = await socket.in('livestream_2623').allSockets();
  //   // const inRoom2 = await socket.in('livestream_2621').allSockets();
  //   console.log(`${new Date()}: all at `, all.size);
  //   console.log(`${new Date()}: inRoom 1`, inRoom1.size);
  //   // console.log(`${new Date()}: inRoom 2`, inRoom2.size);
  // }, 1000);
  socket.on(DEFAULT_EVENT.CONNECTION, async (io: Socket) => {
    const user: UserTypes.UserAuth = io.handshake.auth.user;
    /** init gateway event  */
    initGateWay(socket, io, user);
    io.on(DEFAULT_EVENT.DISCONNECT, async (reason: any) => {
      SocketUtils.leaveAllRoom(user.data.id);
      console.log('disconnect', reason);
    });
  });
};
export default initSocketEvent;
