import { Socket, Server } from 'socket.io';
import { UserTypes } from 'types';
import {
  SOCKET_ON_LIVESTREAM_EVENT,
  SOCKET_ON_MESSAGE_CHANNEL_EVENT,
  withLiveStreamChannel,
  withTopicMessageChannel,
} from '../constants';
import { SocketUtils } from '../SocketUtils';

export const initGateWay = (socket: Server, io: Socket, user: UserTypes.UserAuth) => {
  // io.on(SOCKET_ON_LIVESTREAM_EVENT.SUBSCRIBE_LIVESTREAM_CHANNEL, (livestream_id: number) => {
  //   io.join(withLiveStreamChannel(livestream_id));
  // });

  // io.on(SOCKET_ON_LIVESTREAM_EVENT.UNSUBSCRIBE_LIVESTREAM_CHANNEL, (livestream_id: number) => {
  //   io.leave(withLiveStreamChannel(livestream_id));
  // });

  io.on(SOCKET_ON_MESSAGE_CHANNEL_EVENT.SUBSCRIBE_MESSAGE_CHANNEL, (topic_message_id: number) => {
    io.join(withTopicMessageChannel(topic_message_id));
    SocketUtils.joinRoom(topic_message_id, user.data.id);
  });

  io.on(SOCKET_ON_MESSAGE_CHANNEL_EVENT.UNSUBSCRIBE_MESSAGE_CHANNEL, (topic_message_id: number) => {
    io.leave(withTopicMessageChannel(topic_message_id));
    SocketUtils.leaveRoom(topic_message_id, user.data.id);
  });
};
