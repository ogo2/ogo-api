import { getAxiosInstance } from '@config/axiosInstance';
import { AxiosInstance } from 'axios';
import { LivestreamTypes } from 'types';
import { WARNING_EXPIRE_TIME_TYPE } from '../websocket/constants';
import { LivestreamService } from '@services/internal/livestream.service';
import { ICron } from 'crons/interfaces/ICron';
import { logger, loggerJob } from '@utils/Logger';
import { CronTime, GlobalTime } from './config';

import { CronJob } from 'cron';

export class LivestreamCron implements ICron {
  // 1 minus per call
  public cronTime = CronTime.EveryMin;
  public apiInstance: AxiosInstance;
  public livestreamService: LivestreamService;
  constructor() {
    this.apiInstance = getAxiosInstance();
    this.livestreamService = new LivestreamService();
  }

  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        try {
          const listStreaming = await this.livestreamService.getStreaming();
          await this.killLivestreamExpired(listStreaming);
          await this.warningExpiredTimeListLivestream(listStreaming);
        } catch (error) {
          loggerJob.error(error);
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }

  async killLivestreamExpired(listStreaming: Array<LivestreamTypes.Livestream>) {
    // logger.info({
    //   message: `expire_at: ${listStreaming.map((item: LivestreamTypes.Livestream) => (JSON.stringify({exp: new Date(item.expire_at).getTime(), str: item.expire_at})))}`,
    // });
    // logger.info({
    //   message: `ping_at: ${listStreaming.map((item: LivestreamTypes.Livestream) => (JSON.stringify({exp: new Date(item.ping_at).getTime(), str: item.ping_at})))}`,
    // });

    // expire live
    const listPromiseStopExpireLivestream = listStreaming
      .filter((item: LivestreamTypes.Livestream) => new Date(item.expire_at).getTime() <= Date.now())
      .map((item) => this.apiInstance.post(`/task/${item.id}/stop-livestream`));
    // disconnected (Not ping)
    // const listPromiseLivestreamDisconnected = listStreaming
    //   .filter((item: LivestreamTypes.Livestream) => new Date(item.ping_at).getTime() + 2 * 60000 < Date.now())
    //   .map((item) => this.apiInstance.post(`/task/${item.id}/stop-disconnected-livestream`));

    try {
      await Promise.all(listPromiseStopExpireLivestream); //.concat(listPromiseLivestreamDisconnected)
    } catch (error) {
      console.log('stop live error', error);
    }
  }

  async warningExpiredTimeListLivestream(listStreaming: Array<LivestreamTypes.Livestream>) {
    const listPromise = listStreaming.map((item) => {
      const expTimestamp = new Date(item.expire_at).getTime();
      if (expTimestamp - Date.now() <= WARNING_EXPIRE_TIME_TYPE.ONE_MINUS * 60000 && expTimestamp - Date.now() > 0) {
        return this.apiInstance.post(`/task/${item.id}/warning-livestream`, {
          warning_type: WARNING_EXPIRE_TIME_TYPE.ONE_MINUS,
        });
      } else if (
        expTimestamp - Date.now() <= WARNING_EXPIRE_TIME_TYPE.FIVE_MINUS * 60000 &&
        expTimestamp - Date.now() > 4 * 60000
      ) {
        return this.apiInstance.post(`/task/${item.id}/warning-livestream`, {
          warning_type: WARNING_EXPIRE_TIME_TYPE.FIVE_MINUS,
        });
      } else if (
        expTimestamp - Date.now() <= WARNING_EXPIRE_TIME_TYPE.TEN_MINUS * 60000 &&
        expTimestamp - Date.now() > 9 * 60000
      ) {
        return this.apiInstance.post(`/task/${item.id}/warning-livestream`, {
          warning_type: WARNING_EXPIRE_TIME_TYPE.TEN_MINUS,
        });
      }
    });
    try {
      await Promise.all(listPromise.filter((item) => item !== undefined));
    } catch (error) {
      console.log('warning live error', error);
    }
  }
}
