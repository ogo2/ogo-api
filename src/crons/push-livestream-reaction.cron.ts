import { ICron } from './interfaces/ICron';
import { CronTime, GlobalTime } from './config';
import { socketServer } from '../index';
import { CronJob } from 'cron';
import { LIVESTREAM_EVENT, withLiveStreamChannel } from '../websocket/constants';
import { withIOSuccess } from '@utils/BaseResponse';
import { livestreamPushReactionService, TypeReaction } from '@services/ws/push-reaction-livestream.service';

/**
 * Background task trả về reaction livestream về cho client chạy interval mỗi giây trả về một list
 */

export class PushLivestreamReactionCron implements ICron {
  public cronTime = CronTime.EverySecond;
  constructor() {}
  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        const mTypeReaction: Map<number, TypeReaction[]> = livestreamPushReactionService.getAndClearAll();
        for (let livestreamId of mTypeReaction.keys()) {
          const room = withLiveStreamChannel(livestreamId);
          const reactions = mTypeReaction.get(livestreamId);
          socketServer.to(room).emit(room, withIOSuccess(reactions, LIVESTREAM_EVENT.REACTION));
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }
}
