import { AxiosInstance } from 'axios';
import { CronJob } from 'cron';
import { ICronTime } from '../config';

export interface ICron {
  cronTime: ICronTime;
  execute: () => CronJob;
}
