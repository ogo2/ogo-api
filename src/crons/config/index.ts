import { Moment } from 'moment';

export interface ICronTime {
  rule: string | Date | Moment | any;
  signature: string;
}
export const CronTime = {
  EveryHour: {
    rule: '0 0 * * * *',
    signature: 'everyHourJobs',
  },
  EveryDay: {
    rule: '0 0 16 * * *',
    signature: 'everyDayJobs',
  },
  EveryWednesday: {
    rule: '0 0 16 * * 3',
    signature: 'everyWednesdayJobs',
  },
  EverySecond: {
    rule: '* * * * * *',
    signature: 'everySecondJobs',
  },
  EveryMin: {
    rule: '* * * * *',
    signature: 'everyMinuteJobs',
  },
  PerThreeSecond: {
    rule: '*/3 * * * * *',
    signature: 'perThreeSecond',
  },
};

export enum GlobalTime {
  AsiaHoChiMinh = 'Asia/Ho_Chi_Minh',
}
