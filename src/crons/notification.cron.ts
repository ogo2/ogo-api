import { ICron } from './interfaces/ICron';
import { NotificationController } from '@controllers/notification.controller';
import { CronTime, GlobalTime } from './config';
import { CronJob } from 'cron';
import { loggerJob } from '@utils/Logger';

export class NotificationCron implements ICron {
  // 1 second per call
  public cronTime = CronTime.EverySecond;
  public notificationService: NotificationController;
  constructor() {
    this.notificationService = new NotificationController();
  }
  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        try {
          await this.notificationService.pushNotification();
        } catch (error) {
          loggerJob.error(error);
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }
}
