import { getAxiosInstance } from '@config/axiosInstance';
import { PostTypes } from 'types';
import { PostService } from '@services/internal/post.service';
import { ICron } from './interfaces/ICron';
import { AxiosInstance } from 'axios';
import { CronTime, GlobalTime } from './config';
import { CronJob } from 'cron';
import { loggerJob } from '@utils/Logger';

export class PostCron implements ICron {
  public cronTime = CronTime.EverySecond;
  public apiInstance: AxiosInstance;
  public postService: PostService;
  constructor() {
    this.apiInstance = getAxiosInstance();
    this.postService = new PostService();
  }
  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        try {
          const listPost = await this.postService.getListPostNotPosted();
          await this.postPostNotPosted(listPost);
        } catch (error) {
          loggerJob.error(error);
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }
  public async postPostNotPosted(listPost: Array<PostTypes.PostItem>) {
    const listPromise = listPost
      .filter((item: PostTypes.PostItem) => Date.now() >= new Date(item.schedule).getTime())
      .map((item) => this.apiInstance.post(`/task/${item.id}/post`));
    try {
      await Promise.all(listPromise);
    } catch (error) {
      console.log('posts post error', error);
    }
  }
}
