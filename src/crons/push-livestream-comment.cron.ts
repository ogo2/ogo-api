import { ICron } from './interfaces/ICron';
import { CronTime, GlobalTime } from './config';
import { socketServer } from '../index';
import { CronJob } from 'cron';
import { CommentPushCached, livestreamPushCommentService } from '@services/ws/push-comment-livestream.service';
import { LIVESTREAM_EVENT, withLiveStreamChannel } from '../websocket/constants';
import { withIOSuccess } from '@utils/BaseResponse';

/**
 * Background task trả về comment livestream về cho client chạy interval mỗi giây trả về một list
 */

export class PushLivestreamCommentCron implements ICron {
  public cronTime = CronTime.EverySecond;
  constructor() {}
  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        const mComments: Map<number, CommentPushCached[]> = livestreamPushCommentService.getAndClearAll();
        for (let livestreamId of mComments.keys()) {
          const room = withLiveStreamChannel(livestreamId);
          const comments = mComments.get(livestreamId);
          socketServer.to(room).emit(room, withIOSuccess(comments, LIVESTREAM_EVENT.COMMENT));
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }
}
