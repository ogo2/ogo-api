import { ICron } from './interfaces/ICron';
import { CronTime, GlobalTime } from './config';
import { LIVESTREAM_EVENT, withLiveStreamChannel } from '../websocket/constants';
import { withIOSuccess } from '@utils/BaseResponse';
import * as _ from 'lodash';
import { CronJob } from 'cron';
import { socketServer } from '../index';
import { SocketUtils } from '../websocket/SocketUtils';
import { LivestreamService } from '@services/internal/livestream.service';

/**
 * Background task trả về số lượng người xem trực truyến của các livestream đang phát chạy interval mỗi giây một lần
 */

export class PushCountUserWatchingLivestream implements ICron {
  public cronTime = CronTime.PerThreeSecond;
  private livestreamService: LivestreamService;
  constructor() {
    this.livestreamService = new LivestreamService();
  }
  public execute() {
    return new CronJob(
      this.cronTime.rule,
      async function () {
        const streamings: any[] = await this.livestreamService.getStreaming();
        for (const item of streamings) {
          const res = await SocketUtils.countUserInAdapterSocketLivestreamChannel(socketServer, item.id);
          if (res) {
            const { count_subscribe, channel } = res;
            socketServer
              .to(channel)
              .emit(
                channel,
                withIOSuccess({ count_subscribe, livestream_id: item.id }, LIVESTREAM_EVENT.COUNT_SUBSCRIBER),
              );
          }
        }
      }.bind(this),
      null,
      true,
      GlobalTime.AsiaHoChiMinh,
    );
  }
}
