import { ICron } from 'crons/interfaces/ICron';
import { LivestreamCron } from './livestream.cron';
import { NotificationCron } from './notification.cron';
import { PostCron } from './post.cron';
import { PushLivestreamCommentCron } from './push-livestream-comment.cron';
import { PushLivestreamReactionCron } from './push-livestream-reaction.cron';
import { PushCountUserWatchingLivestream } from './push-livestream-count-user-watching.cron';
export class TaskRegister {
  private static readonly _crons: ICron[] = [
    new LivestreamCron(),
    new PostCron(),
    new NotificationCron(),
    new PushLivestreamCommentCron(),
    new PushLivestreamReactionCron(),
    new PushCountUserWatchingLivestream(),
  ];
  /**
   * register cron job
   * @param cron ICronJob
   * @returns none
   */
  public static register = (cron: ICron) => {
    TaskRegister._crons.push(cron);
    return TaskRegister;
  };

  /**
   * enforce all cron job registed
   * @param cron ICronJob
   * @returns none
   */
  public static enforce = () => {
    TaskRegister._crons.map((cron: ICron) => cron.execute());
    return TaskRegister;
  };
}
