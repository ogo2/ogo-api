import { verifyJWTToken } from '@utils/AuthtJWToken';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
function socketAuthentication(socket: any, next: (error?: Error) => void): void {
  const { token } = socket.handshake.auth;
  if (token) {
    verifyJWTToken(token as string)
      .then((decodedToken: any) => {
        socket.handshake.auth.user = decodedToken;
        next();
      })
      .catch((error: Error) => {
        next(new AppError(ApiCodeResponse.UNAUTHORIZED).with('token unauthorized to connect socket'));
      });
  } else {
    next(new AppError(ApiCodeResponse.UNAUTHORIZED).with(`token isn't provided to connect socket`));
  }
}
export { socketAuthentication };
