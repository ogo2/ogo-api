import * as useragent from 'express-useragent';

function detectedDeviceMiddleware(req): { isMobile: any } {
  const source = req.headers.platform;
  let isMobile: boolean = false;
  if (source === 'app') {
    isMobile = true;
  } else if (source === 'web') {
    isMobile = false;
  }
  return { isMobile };
}
export { detectedDeviceMiddleware };
