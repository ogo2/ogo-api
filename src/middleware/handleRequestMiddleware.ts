import { Utils } from '@utils/Utils';

// trimRequest middleware: trim all request object: body, params, query
const all = function (req, res, next) {
  if (req.body) {
    Utils.trimStringProperties(req.body);
  }

  if (req.params) {
    Utils.trimStringProperties(req.params);
  }

  if (req.query) {
    Utils.trimStringProperties(req.query);
  }

  //   next();
};

// trimBody middleware: trim only the body object
const body = function (req, res, next) {
  if (req.body) {
    Utils.trimStringProperties(req.body);
  }
  next();
};

const param = function (req, res, next) {
  if (req.params) {
    Utils.trimStringProperties(req.params);
  }
  next();
};

const query = function (req, res, next) {
  if (req.query) {
    Utils.trimStringProperties(req.query);
  }
  next();
};

export { all, body, param, query };
