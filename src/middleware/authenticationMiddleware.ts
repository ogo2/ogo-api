import * as express from 'express';
import { ROLE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';

import { verifyJWTToken } from '@utils/AuthtJWToken';
import { environment } from '../config';
import { detectedDeviceMiddleware } from './detectedDevice.middleware';
import { UserService } from '@services/internal/user.service';
export interface DecodeData {
  id: number;
  phone: string;
  shop_id: number | null;
  df_type_user_id: number;
}
export interface DecodeDataModel {
  data: DecodeData;
  iat: number;
  exp: number;
}

export function expressAuthentication(request: express.Request, securityName: string, scopes?: string[]): Promise<any> {
  const { isMobile } = detectedDeviceMiddleware(request);
  const userService: UserService = new UserService();

  // recall api on this server
  if (securityName === 'server_session') {
    const { server_session } = request.headers;
    // console.log('request', request);
    console.log('header', request.headers);
    return Promise.resolve({});
    if (server_session === environment.server_session) return Promise.resolve({});
    return Promise.reject(new Error('Không có quyền'));
  }

  // authen google
  if (securityName === 'google_auth') {
    const { state } = request.query;
    const stateObject = JSON.parse(state as string);
    const { token } = stateObject;
    return verifyJWTToken(token)
      .then(async (decodedToken: DecodeDataModel) => {
        const userInfo = await userService.findOneActiveUser(decodedToken.data.id);
        // request từ mobbile
        if (isMobile && userInfo?.app_token !== token) {
          throw new AppError(ApiCodeResponse.UNAUTHORIZED);
        }
        // request từ web
        else if (!isMobile && userInfo?.token !== token) {
          throw new AppError(ApiCodeResponse.UNAUTHORIZED);
        }
        const foundRole = Object.keys(ROLE).find((k) => ROLE[k] == decodedToken?.data?.df_type_user_id);
        if (scopes && scopes.length > 0) {
          if (foundRole && scopes.includes(foundRole.toLowerCase())) {
            return decodedToken;
          }
          throw new AppError(ApiCodeResponse.UNAUTHORIZED);
        }
        return decodedToken;
      })
      .catch((err) => {
        return Promise.reject(ApiCodeResponse.UNAUTHORIZED);
      });
  }

  /// authen access token
  if (securityName === 'jwt') {
    const token = request.headers['token'];
    if (!token) {
      return Promise.reject(ApiCodeResponse.UNAUTHORIZED);
    }
    return verifyJWTToken(token)
      .then(async (decodedToken: DecodeDataModel) => {
        const userInfo = await userService.findOneActiveUser(decodedToken.data.id);
        // request từ mobbile
        if (isMobile && userInfo?.app_token !== token) {
          throw new AppError(ApiCodeResponse.UNAUTHORIZED);
        }
        // request từ web
        else if (!isMobile && userInfo?.token !== token) {
          throw new AppError(ApiCodeResponse.UNAUTHORIZED);
        }
        const foundRole = Object.keys(ROLE).find((k) => ROLE[k] == decodedToken?.data?.df_type_user_id);
        if (scopes && scopes.length > 0) {
          if (foundRole && scopes.includes(foundRole.toLowerCase())) {
            return decodedToken;
          }
          throw new AppError(ApiCodeResponse.FORBIDDEN);
        }
        return decodedToken;
      })
      .catch((err) => {
        return Promise.reject(ApiCodeResponse.UNAUTHORIZED);
      });
  }
  return Promise.reject({});
}
