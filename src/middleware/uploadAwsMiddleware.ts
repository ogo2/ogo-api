import { MEDIA_TYPE } from '@utils/constants';
import * as multer from 'multer';
import * as path from 'path';
import { environment } from '../config';
const AWS = require('aws-sdk');
const multerS3 = require('multer-s3');
const { v4: uuidv4 } = require('uuid');

AWS.config.update({
  accessKeyId: environment.s3accessKey,
  secretAccessKey: environment.s3secretKey,
  region: environment.s3region,
});

const s3 = new AWS.S3();

// const storage = multer.diskStorage({
//   destination: (req: any, file, callback) => {
//     callback(null, path.join('uploads', req.asset_type == PRODUCT_MEDIA_TYPE.VIDEO ? 'video' : 'image'));
//   },

//   filename: (req, file, cb) => {
//     const id = uuidv4().replace(/-/g, '');
//     cb(null, `${file.fieldname}_${id}${path.extname(file.originalname)}`);
//   },
// });

const storageS3 = multerS3({
  acl: 'public-read',
  s3,
  bucket: environment.s3bucket,
  metadata: (req, file, callBack) => {
    callBack(null, { fieldName: file.fieldname });
  },
  key: function (req, file, cb) {
    let fullPath = '';
    const id = uuidv4().replace(/-/g, '');
    let filename = `${file.fieldname}_${id}${path.extname(file.originalname)}`;
    fullPath = 'uploads/' + (req.asset_type == MEDIA_TYPE.VIDEO ? 'video/' : 'image/') + filename;
    cb(null, fullPath);
  },
});
const fileFilter = (req: any, file: any, cb: any) => {
  if (req.asset_type == MEDIA_TYPE.VIDEO) {
    if (file.mimetype === 'video/mp4') {
      cb(null, true);
    } else {
      cb(new Error('Video không đúng định dạng'), false);
    }
  } else if (file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(new Error('Ảnh không đúng định dạng'), false);
  }
};

var limitimageCate = { fileSize: 2 * 1024 * 1024 }; // 2 MB
var limitimage = { fileSize: 10 * 1024 * 1024 }; // 10 MB
var limitVideo = { fileSize: 60 * 1024 * 1024 }; // 60 MB
const imageUploaderCate = multer({ storage: storageS3, fileFilter, limits: limitimageCate });
const imageUploader = multer({ storage: storageS3, fileFilter, limits: limitimage });
const videoUploader = multer({ storage: storageS3, fileFilter, limits: limitVideo });

export async function handleSingleFile(request: any, name: string, mediaType: number): Promise<any> {
  request.asset_type = mediaType;
  const multerSingle =
    mediaType == MEDIA_TYPE.VIDEO
      ? videoUploader.single(name)
      : mediaType == MEDIA_TYPE.IMAGE
      ? imageUploader.single(name)
      : imageUploaderCate.single(name);
  return new Promise((resolve, reject) => {
    multerSingle(request, undefined, async (error) => {
      if (error instanceof multer.MulterError) {
        if (error.code == 'LIMIT_FILE_SIZE') {
          if (mediaType == MEDIA_TYPE.VIDEO) {
            error.message = 'Dung lượng tối đa là 60 MB';
          } else if (mediaType == MEDIA_TYPE.IMAGE) {
            error.message = 'Dung lượng ảnh tối đa là 10 MB';
          } else if (mediaType == MEDIA_TYPE.IMAGE) {
            error.message = 'Dung lượng ảnh tối đa là 2 MB';
          }
        }
        reject(error);
      }
      if (error) {
        reject(error);
      }
      resolve({});
    });
  });
}
export async function handleFiles(request: any, name: string, mediaType: number): Promise<any> {
  request.asset_type = mediaType;
  const multerSingle = mediaType == MEDIA_TYPE.VIDEO ? videoUploader.any() : imageUploader.any();
  return new Promise((resolve, reject) => {
    multerSingle(request, undefined, async (error) => {
      if (error instanceof multer.MulterError) {
        if (error.code == 'LIMIT_FILE_SIZE') {
          if (mediaType == MEDIA_TYPE.VIDEO) {
            error.message = 'Dung lượng tối đa là 30 MB';
          } else if (mediaType == MEDIA_TYPE.IMAGE) {
            error.message = 'Dung lượng ảnh tối đa là 10 MB';
          } else if (mediaType == MEDIA_TYPE.IMAGE) {
            error.message = 'Dung lượng ảnh tối đa là 2 MB';
          }
        }
        reject(error);
      }
      if (error) {
        reject(error);
      }
      resolve({});
    });
  });
}
export async function uploadToAWS({ images, name, mimetype }) {
  try {
    if (images == null) return null;
    s3.putObject({
      Bucket: environment.s3bucket,
      Body: images.data,
      Key: name,
      ACL: 'public-read',
      ContentType: mimetype,
    })
      .promise()
      .then((response) => {
        console.log('done! - ', response);
      })
      .catch((err) => {
        console.log('failed:', err);
      });
    return name;
  } catch (error) {
    console.log(error);
    return null;
  }
}
