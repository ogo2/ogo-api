require('dotenv').config();
export const environment = {
  // mysql
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  db_port: parseInt(process.env.MYSQL_PORT),
  // server config
  port: parseInt(process.env.PORT),
  secret: process.env.SECRET,
  server_session: process.env.SERVER_SESSION,
  server_host: process.env.SERVER_HOST,
  ws_host: process.env.WS_HOST,
  // s3
  s3bucket: process.env.S3BUCKET,
  s3accessKey: process.env.S3ACCESS_KEY,
  s3secretKey: process.env.S3SECRET_KEY,
  s3region: process.env.S3REGION,
  s3url: process.env.S3URL,
  // email
  user_email: process.env.USER_EMAIL,
  password_email: process.env.PASSWORD_EMAIL,
  name_google_secret_file: process.env.NAME_GOOGLE_SECRET_FILE,
  // redis
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD,
  },
};
