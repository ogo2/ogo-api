import axios from 'axios';
import { environment } from './';

export const getAxiosInstance = () => {
  const apiInstance = axios.create({
    headers: {
      'Content-Type': 'application/json',
    },
    timeout: 20000,
    responseType: 'json',
    baseURL: environment.server_host,
  });
  apiInstance.interceptors.request.use(
    async (config: any) => {
      config.headers.server_session = environment.server_session;
      return config;
    },
    (error) => Promise.reject(error),
  );
  return apiInstance;
};
