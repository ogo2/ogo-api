export declare namespace PostTypes {
  interface PostCreateModel {
    user_id: number;
    shop_id?: number;
    topic_id: number;
    content: string;
  }

  interface AdminPostUpdateModel {
    topic_id?: number;
    content?: string;
    schedule?: Date;
    status?: number;
    image_delete?: Array<number> | number;
  }
  interface PostUpdateModel {
    topic_id?: number;
    content?: string;
    image_delete?: Array<number> | number;
  }

  interface AdminPostCreateModel {
    user_id: number;
    topic_id: number;
    content: string;
    schedule: Date;
    status: number;
  }

  export interface PostItem {
    id: number;
    user_id: number;
    shop_id?: number;
    topic_id: number;
    content?: string;
    count_like: number;
    count_comment: number;
    is_posted: 0 | 1;
    schedule: Date;
    status: number;
    is_active: 0 | 1;
    create_by: number;
    update_by: number;
    delete_by: number;
    create_at: Date;
    update_at: Date;
    delete_at?: Date;
  }
}
