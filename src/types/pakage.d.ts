export declare namespace PakageTypes {
  interface CreateUpdatePakageModel {
    name: string;
    minus: number;
    pakage_category_id: number;
    price: number;
    is_default: number;
  }
}
