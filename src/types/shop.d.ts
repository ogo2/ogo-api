export declare namespace ShopTypes {
  interface AddShopModel {
    name: string;
    name_user?: string;
    phone: string;
    email: string;
    password: string;
    profile_picture_url?: string;
    pancake_shop_key?: string;
    pancake_shop_id?: string;
  }

  interface UpdateShopModel {
    name: string;
    email: string;
    status: number;
    profile_picture_url?: string;
  }
}
