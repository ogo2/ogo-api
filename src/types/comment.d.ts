export declare namespace CommentTypes {
  interface CommentModel {
    content: string;
    user_id: number;
    post_id?: number;
    livestream_id?: number;
    parent_id?: number;
  }
  interface CommentPostModel {
    content: string;
    parent_id?: number;
    target_user_id?: number;
  }
  interface CommentLivestreamModel {
    content: string;
  }
  interface NotificationLivestreamModel {
    content?: string;
  }
}
