export declare namespace ConfigTypes {
  interface ConfigLuckySpinGiftModel {
    name: string;
    value: number;
    percent: number;
    max_per_day: number;
  }
}
