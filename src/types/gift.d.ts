export declare namespace GiftTypes {
  interface CreateGiftModel {
    name: string;
    price: number;
    quantity: number;
    discount_percent?: number;
    max_discount_money?: number;
    icon_url: string;
    df_type_gift_id: number;
  }
  interface UpdateGiftModel {
    name: string;
    price: number;
    quantity: number;
    discount_percent?: number;
    max_discount_money?: number;
    icon_url: string;
    df_type_gift_id: number;
  }
}
