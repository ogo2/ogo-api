import * as express from 'express';
import { Server } from 'socket.io';
import { UserTypes } from './user';
export declare namespace AppTypes {
  interface RequestAuth extends express.Request {
    user: {
      data: UserTypes.DecodeModel;
      iat: number;
      exp: number;
    };
  }
  interface RequestIOAuth extends express.Request {
    user: {
      data: UserTypes.DecodeModel;
      iat: number;
      exp: number;
    };
    io?: Server;
  }
  interface RequestIO extends express.Request {
    io?: Server;
  }

  interface ErrorResponseModel {
    status: number;
    code: number | string;
    message?: string;
    // error: any;
    errors?: any;
    debug?: any;
  }

  interface PagingModel {
    page: number;
    totalItemCount: number;
    limit: number;
  }

  interface SuccessResponseModel<T> {
    status: number;
    code: number;
    message?: string;
    data: T;
    create_at?: Date;
  }

  interface PagingResponseModel<T> extends AppTypes.SuccessResponseModel<T> {
    paging?: AppTypes.PagingModel;
  }

  interface SuccessIOResponseModel<T> {
    status: number;
    code: number;
    message: string;
    data: T;
    create_at: Date;
    type_action: number;
  }
}
