export declare namespace TopicTypes {
  interface TopicCreateModel {
    name: string;
    order: number;
    description?: string;
    icon_url?: string;
  }
  interface TopicUpdateModel {
    name: string;
    order: number;
    description?: string;
    status?: number;
    icon_url?: string;
  }
}
