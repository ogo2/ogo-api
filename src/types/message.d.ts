export declare namespace MessageTypes {
  interface CreateChannelModel {
    user_id?: number;
    shop_id?: number;
    content?: string;
    message_media_url?: string;
    type_message_media?: number;
  }
  interface CreateMessageModel {
    topic_message_id: number;
    content?: string;
    message_media_url?: string;
    type_message_media?: number;
  }
}
