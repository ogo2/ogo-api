export declare namespace UserTypes {
  interface UserRequestModel {
    email: string;
    name: string;
    user_name: string;
    df_type_user_id: number;
    phone: string;
    // gender?: number;
    // date_of_birth?: string;
    // address?: string;
    province_id?: number;
  }

  interface AgentUserRegisterModel {
    email: string;
    name: string;
    df_type_user_id: number;
    phone: string;
    enterprise_code?: string;
    referal_id?: string;
  }

  interface UserWithPasswordModel extends UserRequestModel {
    password: string;
  }
  interface UserWithStatusModel extends UserRequestModel {
    status: number;
  }
  interface UserUpdateModel {
    name?: string;
    email?: string;
    profile_picture_url?: string;
    role?: number;
    status?: number;
  }

  interface CustomerUpdateModel {
    name?: string;
    email?: string;
    profile_picture_url?: any;
    date_of_birth?: string;
    gender?: number;
    address?: string;
  }
  interface UserUpdatePayloadModel extends UserUpdateModel {
    update_by: number;
    update_at: Date;
    df_type_user_id?: number;
  }

  interface UserUpdatePayloadCustomerModel extends CustomerUpdateModel {
    update_by: number;
    update_at: Date;
  }
  interface DecodeModel {
    id: number;
    phone: string;
    shop_id: number | null;
    df_type_user_id: number;
  }
  interface UserAuth {
    data: UserTypes.DecodeModel;
    iat: number;
    exp: number;
  }
}
