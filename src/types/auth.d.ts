export declare namespace AuthTypes {
  interface AuthorizedUser {
    phone: string;
    id: number;
    enterprise_id?: number;
    df_type_user_id?: number;
  }
}
