export declare namespace LivestreamTypes {
  interface ProductLivestream {
    id: number;
    code_product_livestream?: string;
  }
  interface CreateLivestreamModel {
    title?: string;
    cover_image_url?: string;
    products: Array<LivestreamTypes.ProductLivestream>;
  }

  interface Livestream {
    id: number;
    user_id: number;
    shop_id: number;
    title?: string;
    cover_image_url?: string;
    broadcast_id?: string;
    stream_id?: string;
    count_viewed: number;
    count_reaction: number;
    count_comment: number;
    start_at?: Date;
    finish_at?: Date;
    expire_at?: Date;
    ping_at?: Date;
    status: number;
    is_active: number;
    create_by: number;
    update_by: number;
    delete_by: number;
    create_at: Date;
    update_at: Date;
    delete_at: Date;
    version: number;
    Shop: any;
    User: any;
  }
}
