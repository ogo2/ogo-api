import { Security, Get, Query, Route, Delete, Tags, Request } from 'tsoa';
import { AppTypes } from 'types';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';

import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '.';
const db = require('@models');
const { sequelize, Sequelize, Shop, User, UserAddress, Comment, DFProvince, DFWard, DFDistrict } = db.default;
const { Op } = Sequelize;

@Route('comment')
@Tags('comments')
export class CommentController extends ApplicationController {
  constructor() {
    super('Comment');
  }
  /**
   * @summary get list comment (any)
   */
  @Security('jwt')
  @Get('/all')
  public async getAllListComment(
    @Request() request: AppTypes.RequestAuth,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('post_id') post_id?: number,
    @Query('livestream_id') livestream_id?: number,
    @Query('comment_id') comment_id?: number,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,

      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (post_id) {
      whereOption.post_id = post_id;
    }
    if (livestream_id) {
      whereOption.livestream_id = livestream_id;
    }
    if (comment_id) {
      whereOption.parent_id = comment_id;
    }

    const listComment = await Comment.findAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          include: {
            model: UserAddress,
            required: false,
            separate: true,
            attributes: ['id', 'name', 'phone', 'user_id', 'location_address', 'address'],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [['id', 'desc']],
    });
    return withSuccess(listComment);
  }

  /**
   * @summary get list comment (any)
   */
  @Security('jwt')
  @Get('/')
  public async listComment(
    @Request() request: AppTypes.RequestAuth,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('post_id') post_id?: number,
    @Query('livestream_id') livestream_id?: number,
    @Query('comment_id') comment_id?: number,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,

      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (post_id) {
      whereOption.post_id = post_id;
    }
    if (livestream_id) {
      whereOption.livestream_id = livestream_id;
    }
    if (comment_id) {
      whereOption.parent_id = comment_id;
    } else {
      whereOption.parent_id = null;
    }

    const { count, rows } = await Comment.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              AND reaction.user_id = ${loggedInUser.id}
              AND reaction.comment_id = Comment.id
            )`),
            'is_reaction',
          ],
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`comment\` as CM
              WHERE CM.is_active = ${IS_ACTIVE.ACTIVE}
              AND CM.parent_id = \`Comment\`.id
                )`),
            'count_sub_comment',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: false,
          attributes: {
            include: [],
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          include: {
            model: UserAddress,
            required: false,
            separate: true,
            attributes: ['id', 'name', 'phone', 'user_id', 'location_address', 'address'],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Comment,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            include: [
              [
                sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              AND reaction.user_id = ${loggedInUser.id}
              AND reaction.comment_id = Comment.id
            )`),
                'is_reaction',
              ],
            ],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          include: [
            {
              model: User,
              required: false,
              attributes: {
                exclude: [
                  'code_reset_password',
                  'expired_reset_password',
                  'token',
                  'password',
                  'create_by',
                  'update_by',
                  'delete_by',
                  'version',
                ],
              },
              include: {
                model: UserAddress,
                required: false,
                separate: true,
                attributes: ['id', 'name', 'phone', 'user_id', 'location_address', 'address'],
                where: { is_active: IS_ACTIVE.ACTIVE },
                include: [
                  {
                    model: DFWard,
                    where: { is_active: IS_ACTIVE.ACTIVE },
                    required: false,
                    attributes: ['id', 'name'],
                  },
                  {
                    model: DFDistrict,
                    required: false,
                    where: { is_active: IS_ACTIVE.ACTIVE },
                    attributes: ['id', 'name'],
                  },
                  {
                    model: DFProvince,
                    required: false,
                    where: { is_active: IS_ACTIVE.ACTIVE },
                    attributes: ['id', 'name'],
                  },
                ],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: User,
              required: false,
              as: 'target_user',
              attributes: {
                exclude: [
                  'code_reset_password',
                  'expired_reset_password',
                  'token',
                  'password',
                  'create_by',
                  'update_by',
                  'delete_by',
                  'version',
                ],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: Shop,
              required: false,
              attributes: {
                exclude: ['create_by', 'update_by', 'delete_by', 'version'],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
          order: [['id', 'desc']],
          limit: 2,
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    console.log('first', rows);
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary get list comment (no secute) | last_comment_id: id smallest of currient sub comment (default: 0) | amount: amount of sub comment want to get (default: 5)
   *
   */
  @Security('jwt')
  @Get('/{comment_id}/more')
  public async listSubComment(
    @Request() request: AppTypes.RequestAuth,
    comment_id: number,
    @Query('last_comment_id') last_comment_id?: number,
    @Query('amount') amount?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    last_comment_id = last_comment_id ? last_comment_id : 0;
    amount = amount ? amount : 5;
    const listData = await Comment.findAll({
      where: {
        id: { [Op.lt]: last_comment_id },
        parent_id: comment_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              AND reaction.user_id = ${loggedInUser.id}
              AND reaction.comment_id = Comment.id
            )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },

      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit: amount,
      order: [['id', 'desc']],
    });
    return withSuccess(listData);
  }

  /**
   * @summary delete comment (any)
   */
  @Security('jwt')
  @Delete('/{comment_id}')
  public async deleteLiveStream(
    @Request() request: AppTypes.RequestAuth,
    comment_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundComment = await this._findOne({
      where: {
        id: comment_id,
        [Op.or]: [{ user_id: loggedInUser.id }, { shop_id: loggedInUser.shop_id }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (!foundComment) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bạn không thể xóa bình luận này.');
    await foundComment.update({
      is_active: IS_ACTIVE.INACTIVE,
      delete_by: loggedInUser.id,
      delete_at: new Date(),
    });
    return withSuccess({});
  }
}
