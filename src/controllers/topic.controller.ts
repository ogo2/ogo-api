import { Get, Query, Route, Tags, Request } from 'tsoa';
import { TOPIC_STATUS, TOPIC_TYPE, USER_STATUS, USER_TOPIC_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { AppTypes } from 'types';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '.';

const db = require('@models');
const { sequelize, Sequelize, Topic, UserIgnoreTopic, User } = db.default;
const { Op } = Sequelize;

@Route('topic')
@Tags('topics')
export class TopicController extends ApplicationController {
  constructor() {
    super('Topic');
  }

  /**
   *  @summary get list topic (no security)
   */
  @Get('/')
  public async getListTopic(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let listTopicIgnore = [];
    if (tokens) {
      const loginUser = await User.findOne({
        where: {
          [Op.or]: [{ token: tokens }, { app_token: tokens }],
          status: USER_STATUS.ACTIVE,
          is_active: IS_ACTIVE.ACTIVE,
        },
      });
      if (loginUser) {
        const topicIgnore = await UserIgnoreTopic.findAll({
          where: { user_id: loginUser.id, is_active: IS_ACTIVE.ACTIVE, status: USER_TOPIC_STATUS.ACTIVE },
        });
        listTopicIgnore = topicIgnore.map((item: any) => item.dataValues.topic_id);
      }
    }
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      id: { [Op.notIn]: listTopicIgnore },
      type_topic: TOPIC_TYPE.NORMAL,
      is_active: IS_ACTIVE.ACTIVE,
      status: TOPIC_STATUS.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          description: { [Op.like]: `%${search}%` },
        },
      }),
    };
    const { count, rows } = await Topic.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [
        ['order', 'ASC'],
        ['create_at', 'DESC'],
      ],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   *  @summary get detail topic (no security)
   */
  @Get('/{id}')
  public async getDetailTopic(id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundTopic = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE, status: TOPIC_STATUS.ACTIVE, type_topic: TOPIC_TYPE.NORMAL },
      attributes: {
        include: [
          [
            sequelize.literal(`
              (
                SELECT COUNT(*)
                FROM user
                WHERE user.is_active = ${IS_ACTIVE.ACTIVE}
              )
                  -
              (
                SELECT COUNT(*)
                FROM user_ignore_topic
                WHERE user_ignore_topic.is_active = ${IS_ACTIVE.ACTIVE}
                AND user_ignore_topic.id = ${id}
              ) 
                `),
            'count_care',
          ],
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM post
              WHERE post.is_active = ${IS_ACTIVE.ACTIVE}
              AND post.topic_id = ${id}
                )`),
            'count_post',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    return withSuccess(foundTopic);
  }
}
