import { Body, Security, Get, Post, Put, Query, Route, Delete, Tags, Request } from 'tsoa';
import { AppTypes } from 'types';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import {
  IS_ACTIVE,
  MEDIA_TYPE,
  ROLE,
  REACTION_TYPE,
  DF_NOTIFICATION,
  IS_POSTED,
  USER_TOPIC_STATUS,
  USER_STATUS,
  PRODUCT_STOCK_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '.';
import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { Utils } from '@utils/Utils';
import { PostService } from '@services/internal/post.service';
import { CommentTypes, PostTypes } from 'types';
import { CommentService } from '@services/internal/comment.service';
import { PostAction } from '../websocket/action';
import { NotificationService } from '@services/internal/notification.service';
import { detectedDeviceMiddleware } from '@middleware/detectedDevice.middleware';
import { environment } from '@config/environment';
const db = require('@models');
const {
  sequelize,
  Sequelize,
  Post: Posts,
  PostMedia,
  Shop,
  User,
  Reaction,
  Comment,
  Topic,
  UserIgnoreTopic,
  Product,
} = db.default;
const { Op } = Sequelize;

@Route('post')
@Tags('posts')
export class PostsController extends ApplicationController {
  private postService: PostService;
  private commentService: CommentService;
  private notificationService: NotificationService;

  constructor() {
    super('Post');
    this.postService = new PostService();
    this.notificationService = new NotificationService();
    this.commentService = new CommentService();
  }
  /**
   *  @summary create a post by form data (any)
   */
  @Security('jwt')
  @Post('/')
  public async createPostFormData(
    @Request() request: AppTypes.RequestIOAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    let array_image = [];
    const uploads = await uploadMiddleware.handleFiles(request, 'post_media', MEDIA_TYPE.IMAGE);
    if (uploads) {
      array_image = (request as any).files.map((file) => file.key);
    }
    const dataValidated: PostTypes.PostCreateModel = await this.postService.PostCreateSchema.validateAsync({
      ...request.body,
      user_id: loggedInUser.id,
      shop_id: loggedInUser.shop_id,
    });
    const postTransaction = await sequelize.transaction(async (transaction) => {
      const createdPost = await this._create({ ...dataValidated, is_posted: IS_POSTED.POSTED }, { transaction });
      const post_media = array_image.map((media_url: string) => {
        return {
          post_id: createdPost.id,
          media_url: media_url ? media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '') : null,
        };
      });
      const mediaCreated = await PostMedia.bulkCreate(post_media, { transaction });
      createdPost.setDataValue('PostMedia', mediaCreated);
      return createdPost;
    });
    return withSuccess(postTransaction);
  }

  /**
   *  @summary get list post (no security)
   */
  @Get('/')
  public async getListPost(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('topic_id') topic_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    // @Query('status') status?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    const { isMobile } = detectedDeviceMiddleware(request);
    const token = request.headers.token;
    let loggedInUser = null;
    let listTopicIgnore = [];
    if (token) {
      if (isMobile) {
        loggedInUser = await User.findOne({
          where: { app_token: token, status: USER_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
        });
      } else {
        loggedInUser = await User.findOne({
          where: { token: token, status: USER_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
        });
      }
      if (loggedInUser) {
        const topicIgnore = await UserIgnoreTopic.findAll({
          where: { user_id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE, status: USER_TOPIC_STATUS.ACTIVE },
        });
        listTopicIgnore = topicIgnore.map((item: any) => item.dataValues.topic_id);
      }
    }

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      is_posted: IS_POSTED.POSTED,
      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          // { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          // { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          // { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
      topic_id: { [Op.notIn]: listTopicIgnore },
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (topic_id) {
      whereOption.topic_id = topic_id;
    }

    const whereSubOption = { is_active: IS_ACTIVE.ACTIVE };
    const baseUrl = Utils.getBaseServer(request);
    const { count, rows } = await Posts.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              ${loggedInUser?.id ? `AND reaction.user_id = ${loggedInUser?.id}` : 'AND reaction.user_id is null'}
              AND reaction.post_id = Post.id
              AND reaction.comment_id is null
            )`),
            'is_reaction',
          ],
          //AND reaction.user_id = ${loggedInUser?.id || null}
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'app_token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Reaction,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          include: {
            model: User,
            required: false,
            attributes: {
              include: ['profile_picture_url', 'id', 'shop_id', 'name'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['id'],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   *  @summary get detail post (any)
   */
  @Security('jwt')
  @Get('/{id}')
  public async getDetailPost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const foundPost = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
        is_posted: IS_POSTED.POSTED,
      },
      attributes: {
        include: [
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM reaction AS reaction
            WHERE
            reaction.is_active = ${IS_ACTIVE.ACTIVE}
            AND reaction.user_id = ${loggedInUser.id}
            AND reaction.post_id = Post.id
            AND reaction.comment_id is null
          )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Reaction,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          include: {
            model: User,
            required: false,
            attributes: {
              include: ['profile_picture_url', 'id', 'shop_id', 'name'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: { model: Shop, where: { is_active: IS_ACTIVE.ACTIVE } },
        },
      ],
      // logging: console.log,
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn  tại.');
    return withSuccess(foundPost);
  }

  /**
   * @summary update post by id (any)
   */
  @Security('jwt')
  @Put('/{id}')
  public async updatePost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPost = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    if (foundPost.dataValues.user_id !== loggedInUser.id && foundPost.dataValues.shop_id !== loggedInUser.shop_id)
      throw new AppError(ApiCodeResponse.NO_PERMISSION).with('Bạn không có quyền sửa bài đăng này');
    let array_image = [];
    const uploads = await uploadMiddleware.handleFiles(request, 'post_media', MEDIA_TYPE.IMAGE);
    if (uploads) {
      array_image = (request as any).files.map((file) => file.key);
    }

    let list_id_image_delete: Array<number> = [];
    if (request.body.image_delete) {
      if (typeof request.body.image_delete === 'number' || typeof request.body.image_delete === 'string')
        list_id_image_delete[0] = request.body.image_delete;
      if (Array.isArray(request.body.image_delete)) list_id_image_delete = [...request.body.image_delete];
    }
    const dataValidated: PostTypes.PostUpdateModel = await this.postService.PostUpdateSchema.validateAsync({
      ...request.body,
      image_delete: list_id_image_delete,
    });
    const payload: PostTypes.PostUpdateModel = {};
    if (dataValidated.content) {
      payload.content = dataValidated.content;
    }
    if (dataValidated.topic_id) {
      payload.topic_id = dataValidated.topic_id;
    }
    await sequelize.transaction(async (transaction) => {
      await PostMedia.update(
        {
          update_by: loggedInUser.id,
          delete_at: new Date(),
          is_active: IS_ACTIVE.INACTIVE,
        },
        {
          where: {
            id: { [Op.in]: dataValidated.image_delete },
            post_id: id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
      await this._update(payload, {
        where: { id, is_active: IS_ACTIVE.ACTIVE },
        transaction,
      });
      const post_media = array_image.map((media_url: string) => {
        return {
          post_id: id,
          media_url: media_url ? media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '') : null,
        };
      });
      await PostMedia.bulkCreate(post_media, { transaction });
    });
    const foundPostUpdated = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM reaction AS reaction
            WHERE
            reaction.is_active = ${IS_ACTIVE.ACTIVE}
            AND reaction.user_id = ${loggedInUser.id}
            AND reaction.post_id = Post.id
            AND reaction.comment_id is null
          )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      // logging: console.log,
    });
    return withSuccess(foundPostUpdated);
  }

  /**
   * @summary delete post by id (any)
   */
  @Security('jwt')
  @Delete('/{id}')
  public async deletePost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const foundPost = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    if (
      foundPost.dataValues.user_id !== loggedInUser.id &&
      foundPost.dataValues.shop_id !== loggedInUser.shop_id &&
      loggedInUser.df_type_user_id !== ROLE.ADMIN &&
      loggedInUser.df_type_user_id !== ROLE.ADMIN_EDITOR
    )
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bạn không có quyền xóa bài đăng này.');
    await foundPost.update({
      is_active: IS_ACTIVE.INACTIVE,
      delete_by: loggedInUser.id,
      delete_at: new Date(),
    });
    return withSuccess({});
  }

  /**
   * @summary reaction like or unlike post (any)
   */
  @Security('jwt')
  @Post('/{post_id}/reaction')
  public async createReactionPost(
    @Request() request: AppTypes.RequestIOAuth,
    // @Body() body: ReactionModel,
    post_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    // return withSuccess(loggedInUser);
    const foundPost = await this._findOne({
      where: { id: post_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    // const { df_reaction_id }: ReactionModel = await ReactionSchema.validateAsync(body);
    const createdReactionTransaction = await sequelize.transaction(async (transaction) => {
      const updatedReaction = await Reaction.update(
        { is_active: IS_ACTIVE.INACTIVE },
        {
          where: {
            post_id,
            comment_id: { [Op.eq]: null },
            user_id: loggedInUser.id,
            // [Op.or]: [{ user_id: loggedInUser.id }, { shop_id: loggedInUser.shop_id }],
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
      if (updatedReaction[0] === 0) {
        const createdReaction = await Reaction.create(
          {
            post_id,
            user_id: loggedInUser.id,
            shop_id: loggedInUser.shop_id,
            df_reaction_id: REACTION_TYPE.LIKE,
          },
          { transaction },
        );
        await foundPost.increment({ count_like: 1 }, { transaction });
        // noti
        const df_noti = DF_NOTIFICATION.LIKE_POST;

        await this.createNotificationLike(
          df_noti,
          post_id,
          loggedInUser.id,
          loggedInUser.shop_id,
          foundPost.user_id,
          loggedInUser.id,
          transaction,
        );
        // end noti
        return createdReaction;
      } else {
        await foundPost.decrement({ count_like: 1 }, { transaction });
        return null;
      }
    });

    if (createdReactionTransaction) {
      createdReactionTransaction.setDataValue('count_like', foundPost.dataValues.count_like + 1);
      PostAction(request.io).sendReactionPost(post_id, createdReactionTransaction);
    } else {
      PostAction(request.io).sendUnReactionPost(post_id, {
        user_id: loggedInUser.id,
        shop_id: loggedInUser.shop_id,
        count_like: foundPost.dataValues.count_like - 1,
      });
    }
    return withSuccess(createdReactionTransaction);
  }

  /**
   * @summary create reaction for comment of post (any)
   */
  @Security('jwt')
  @Post('/{post_id}/reaction/{comment_id}/comment')
  public async createReactionCommentOfPost(
    @Request() request: AppTypes.RequestIOAuth,
    // @Body() body: ReactionModel,
    post_id: number,
    comment_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundComment = await Comment.findOne({
      where: { id: comment_id, post_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundComment) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bình luận không tồn tại.');
    // const { df_reaction_id }: ReactionModel = await ReactionSchema.validateAsync(body);
    const createdReactionTransaction = await sequelize.transaction(async (transaction) => {
      const updatedReaction = await Reaction.update(
        { is_active: IS_ACTIVE.INACTIVE },
        {
          where: {
            comment_id,
            user_id: loggedInUser.id,
            // [Op.or]: [{ user_id: loggedInUser.id }, { shop_id: loggedInUser.shop_id }],
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
      if (updatedReaction[0] === 0) {
        const createdReaction = await Reaction.create(
          {
            post_id,
            comment_id,
            user_id: loggedInUser.id,
            shop_id: loggedInUser.shop_id,
            df_reaction_id: REACTION_TYPE.LIKE,
          },
          { transaction },
        );
        await foundComment.increment({ count_like: 1 }, { transaction });
        // noti
        const df_noti = DF_NOTIFICATION.LIKE_COMMENT;

        await this.createNotificationLike(
          df_noti,
          post_id,
          loggedInUser.id,
          loggedInUser.shop_id,
          foundComment.user_id,
          loggedInUser.id,
          transaction,
        );
        // end noti
        return createdReaction;
      } else {
        await foundComment.decrement({ count_like: 1 }, { transaction });
        return null;
      }
    });

    if (createdReactionTransaction) {
      createdReactionTransaction.setDataValue('parent_id', foundComment.dataValues.parent_id);
      createdReactionTransaction.setDataValue('count_like', foundComment.dataValues.count_like + 1);
      PostAction(request.io).sendReactionCommentPost(post_id, createdReactionTransaction);
      return withSuccess(createdReactionTransaction);
    } else {
      PostAction(request.io).sendUnReactionCommentPost(post_id, {
        user_id: loggedInUser.id,
        shop_id: loggedInUser.shop_id,
        count_like: foundComment.dataValues.count_like - 1,
        comment_id: foundComment.id,
        parent_id: foundComment.dataValues.parent_id,
      });
    }
    return withSuccess(createdReactionTransaction);
  }

  // create notification like
  public async createNotificationLike(id, post_id, id_user, shop_id, user_comment_id, df_user_id, transaction) {
    const shopName = await this.notificationService.shopName(shop_id);
    if (shopName) {
      const userData = shop_id
        ? await this.notificationService.shopName(shop_id)
        : await this.notificationService.customerName(id_user);

      const notiType = await this.notificationService.findOneTypeNotification(id);
      if (!notiType) throw ApiCodeResponse.NOT_FOUND;
      const content =
        notiType.id == DF_NOTIFICATION.LIKE_POST
          ? userData.name + ' đã thích bài viết của bạn'
          : userData.name + ' đã thích bình luận của bạn';
      const df_notification_id = notiType.id;
      const data = { post_id: post_id };
      const user_id = user_comment_id;
      const titleNoti = notiType.title;
      // await this.notificationService.createNotification(
      //   user_id,
      //   'Thích bài viết',
      //   userData.name + ' đã thích bài viết của bạn',
      //   0,
      //   DF_NOTIFICATION.LIKE_POST,
      //   data,
      //   null,
      //   transaction,
      // );
      if (user_comment_id != df_user_id)
        await this.notificationService.createNotification(
          user_id,
          titleNoti,
          content,
          0,
          df_notification_id,
          data,
          null,
          transaction,
        );
    }
  }

  /**
   * @summary create comment post (any)
   */
  @Security('jwt')
  @Post('/{post_id}/comment')
  public async createCommentPost(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: CommentTypes.CommentPostModel,
    post_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPost = await this._findOne({
      where: { id: post_id, is_active: IS_ACTIVE.ACTIVE },
    });
    const defaultUser = await User.findOne({ where: { id: loggedInUser.id } });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');

    const { content, parent_id, target_user_id } = await this.commentService.CommentPostSchema.validateAsync(body);

    const createdCommentTransaction = await sequelize.transaction(async (transaction) => {
      const createdComment = await Comment.create({
        user_id: loggedInUser.id,
        content,
        post_id: foundPost.id,
        parent_id,
        target_user_id,
      });
      const df_noti =
        target_user_id != null && target_user_id != undefined
          ? DF_NOTIFICATION.SEND_COMMENT
          : DF_NOTIFICATION.COMMENT_POST;
      // lấy comment của thằng cha
      const user_comment_id = await Comment.findOne({ where: { id: parent_id ? parent_id : null } });
      await this.createNotificationComment(
        df_noti,
        post_id,
        loggedInUser.id,
        defaultUser.shop_id,
        foundPost.user_id,
        parent_id,
        user_comment_id,
        target_user_id,
        defaultUser.id,
        transaction,
      );
      await foundPost.increment({ count_comment: 1 }, { transaction });
      return createdComment;
    });

    const foundComment = await Comment.findOne({
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { id: createdCommentTransaction.id },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    // sk
    PostAction(request.io).sendComment(post_id, foundComment);
    return withSuccess(foundComment);
  }
  // create notification comment
  public async createNotificationComment(
    id,
    post_id,
    id_user,
    shop_id,
    user_post_id,
    parent_id,
    user_comment_id,
    target_user_id,
    default_user_id,
    transaction,
  ) {
    const shopData = await this.notificationService.shopName(shop_id);
    // const shopData = await this.notificationService.shopNameFollow(shop_id, id_user);
    // console.log('shopData', shopData);
    const customerName = await this.notificationService.customerName(id_user);
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    // if (shopData) {
    const content =
      parent_id != null && parent_id > 0
        ? shopData
          ? shopData?.name + ' đã trả lời bình luận của bạn'
          : customerName?.name + ' đã trả lời bình luận của bạn'
        : shopData
        ? shopData?.name + ' đã trả lời bình luận của bạn'
        : customerName?.name + ' đã bình luận viết của bạn';
    const df_notification_id = notiType.id;
    const data = { post_id: post_id };
    const user_id =
      target_user_id != null
        ? target_user_id
        : parent_id == null
        ? user_post_id
        : user_comment_id
        ? user_comment_id.user_id
        : null;

    const titleNoti = notiType.title;
    // await this.notificationService.createNotification(
    //   id_user,
    //   'Bình luận bài viết',
    //   customerName?.name + ` đã trả lời bài viết của bạn`,
    //   0,
    //   DF_NOTIFICATION.COMMENT_POST,
    //   data,
    //   null,
    //   transaction,
    // );
    if (default_user_id != user_id)
      await this.notificationService.createNotification(
        user_id,
        titleNoti,
        content,
        0,
        df_notification_id,
        data,
        null,
        transaction,
      );
    // }
  }
}
