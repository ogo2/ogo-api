import { IS_ACTIVE, USER_STATUS, LIVESTREAM_STATUS, SHOP_STATUS, ADMIN_ROLE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import { Body, Request, Get, Put, Route, Tags, Security } from 'tsoa';
import { withSuccess } from '../utils/BaseResponse';
import { UserTypes, AppTypes } from 'types';
import { Utils } from '@utils/Utils';
import { detectedDeviceMiddleware } from '@middleware/detectedDevice.middleware';
import { UserService } from '@services/internal/user.service';
const db = require('@models');
const { sequelize, Sequelize, User, Shop, DFTypeUser, UserAddress, DFProvince, DFDistrict, DFWard, Livestream } =
  db.default;
const { Op } = Sequelize;

interface UserLoginParams {
  user_name: string;
  password: string;
  device_id?: string;
}

@Route('users')
@Tags('user')
export class UsersController extends ApplicationController {
  private userService: UserService;
  constructor() {
    super('User');
    this.userService = new UserService();
  }

  /**
   * @summary change password customer
   */
  @Security('jwt')
  @Put('/change-password-customer')
  public async changePasswordCustomer(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { old_password: string; new_password: string },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user.data;
    const foundUser = await User.findOne({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser || !foundUser.authenticate(body.old_password)) {
      throw new AppError(ApiCodeResponse.PASSWORD_FAIL);
    }
    const new_token: string = foundUser.generateToken();
    await User.update(
      {
        password: User.generatePassword(body.new_password),
        update_at: new Date(),
      },
      { where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE } },
    );
    foundUser.setDataValue('password', null);
    foundUser.setDataValue('token', new_token);

    return withSuccess(foundUser);
  }

  /**
   * @summary update user customer
   */
  @Security('jwt')
  @Put('update-info')
  public async updateCustomerInfo(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: UserTypes.CustomerUpdateModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const user = await User.findOne({
      where: { id: loginUser.id, status: USER_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!user) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    const dataUpdateBody: UserTypes.CustomerUpdateModel =
      await this.userService.UpdateUserCustomerInforSchema.validateAsync(body);
    const payload: UserTypes.UserUpdatePayloadCustomerModel = {
      update_by: loginUser.id,
      update_at: new Date(),
    };
    if (dataUpdateBody.name) {
      payload.name = dataUpdateBody.name;
    }
    payload.date_of_birth = dataUpdateBody.date_of_birth;
    if (dataUpdateBody.email) {
      const userCheckEmail = await this._findOne({
        where: {
          id: { [Op.ne]: loginUser.id },
          email: dataUpdateBody.email,
          is_active: IS_ACTIVE.ACTIVE,
        },
      });
      if (userCheckEmail) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Email này đã được sử dụng.');
      payload.email = dataUpdateBody.email;
    }
    const baseUrl = Utils.getBaseServer(request) + '/';
    payload.profile_picture_url = dataUpdateBody.profile_picture_url.replace(baseUrl, '');
    // if (dataUpdateBody.date_of_birth) {
    //   payload.date_of_birth = dataUpdateBody.date_of_birth;
    // }
    if (dataUpdateBody.gender) {
      payload.gender = dataUpdateBody.gender;
    }
    if (dataUpdateBody.address) {
      payload.address = dataUpdateBody.address;
    }

    await User.update(payload, {
      where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
    });
    const updatedUser = await this._findOne({
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(updatedUser);
  }
  /**
   * @summary login account with any role  (any)
   */
  @Put('/login')
  public async login(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: UserLoginParams,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { isMobile } = detectedDeviceMiddleware(request);
    const foundUser = await this._findOne({
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },

      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: ['id', 'name', 'phone', 'livestream_enable', 'pancake_shop_id', 'status', 'profile_picture_url'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { user_name: body.user_name, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser || !foundUser.authenticate(body.password)) {
      throw new AppError(ApiCodeResponse.LOGIN_FAIL);
    }
    if (foundUser.shop_id) {
      if (!foundUser.Shop) {
        throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST).with(
          'Tài khoản(Gian hàng) của bạn đã bị xóa. Vui lòng đăng liên hệ quản trị viên để được hỗ trợ!',
        );
      }
      if (foundUser.Shop.dataValues.status === SHOP_STATUS.INACTIVE) {
        throw new AppError(ApiCodeResponse.LOGIN_FAIL).with(
          'Tài khoản(Gian hàng) ngừng hoạt động. Vui lòng đăng liên hệ quản trị viên để được hỗ trợ!',
        );
      }
    }
    if (foundUser.status === USER_STATUS.INACTIVE) {
      throw new AppError(ApiCodeResponse.LOGIN_FAIL).with('Tài khoản của bạn đã bị vô hiệu hóa.');
    }

    const token: string = foundUser.generateToken();
    // Đang đăng nhập bằng app mobile
    if (isMobile) {
      // check xem tài khoản này có đang live hay ko, nếu đang live trên app thì ko cho đăng nhập
      if (foundUser.shop_id) {
        const foundLivestream = await Livestream.findOne({
          where: {
            user_id: foundUser.id,
            shop_id: foundUser.shop_id,
            status: LIVESTREAM_STATUS.STREAMING,
            expire_at: { [Op.gt]: new Date() },
            is_active: IS_ACTIVE.ACTIVE,
          },
        });
        if (foundLivestream) {
          throw new AppError(ApiCodeResponse.LOGIN_FAIL).with(
            'Tài khoản của bạn đang phát livestream ở nơi khác, không thể đăng nhập.',
          );
        }
      }
      // không cho admin đăng nhập trên app
      if (Object.values(ADMIN_ROLE).includes(foundUser.df_type_user_id))
        throw new AppError(ApiCodeResponse.LOGIN_FAIL).with('Tài khoản quản trị viên. Vui lòng đăng ký SĐT khác');

      await User.update(
        { app_token: token, device_id: body.device_id ? body.device_id : null, last_login_date: new Date() },
        { where: { id: foundUser.id, is_active: IS_ACTIVE.ACTIVE } },
      );
      foundUser.setDataValue('app_token', undefined);
      foundUser.setDataValue('password', null);
      foundUser.setDataValue('token', token);
    }
    // Đăng đăng nhập trên web
    else {
      await User.update(
        { token: token, last_login_date: new Date() },
        { where: { id: foundUser.id, is_active: IS_ACTIVE.ACTIVE } },
      );
      foundUser.setDataValue('app_token', undefined);
      foundUser.setDataValue('password', null);
      foundUser.setDataValue('token', token);
    }
    return withSuccess(foundUser);
  }
  /**
   * @summary logout account with any role  (any)
   */
  @Security('jwt')
  @Put('/logout')
  public async logout(@Request() request: AppTypes.RequestAuth): Promise<AppTypes.SuccessResponseModel<any>> {
    const { isMobile } = detectedDeviceMiddleware(request);
    const loggedInUser = request.user?.data;
    const foundUser = await this._findOne({
      where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Tài khoản không tồn tại');
    // đăng xuất trên app mobile
    if (isMobile) await foundUser.update({ app_token: null, device_id: null, last_login_date: new Date() });
    // đăng xuất trên web
    else await foundUser.update({ token: null, last_login_date: new Date() });

    return withSuccess({});
  }

  /**
   * @summary change password (any)
   */
  @Security('jwt')
  @Put('/change-password')
  public async changePassword(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { old_password: string; new_password: string },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user.data;
    // return withSuccess(loggedInUser);
    const foundUser = await User.findOne({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser || !foundUser.authenticate(body.old_password)) {
      throw new AppError(ApiCodeResponse.PASSWORD_FAIL);
    }
    // return withSuccess(foundUser);
    const new_token: string = foundUser.generateToken();
    await User.update(
      {
        password: User.generatePassword(body.new_password),
        // token: new_token,
        update_at: new Date(),
      },
      { where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE } },
    );
    foundUser.setDataValue('password', null);
    foundUser.setDataValue('token', new_token);

    return withSuccess(foundUser);
  }

  /**
   * @summary get user information by instance token (any)
   */
  @Security('jwt')
  @Get('/info')
  public async getAuthorizedUserInfo(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const { isMobile } = detectedDeviceMiddleware(request);
    const loginUser = request.user.data;
    const foundUser = await this._findOne({
      where: {
        id: loginUser.id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: [
            'id',
            'name',
            'phone',
            'livestream_enable',
            'status',
            'stream_minutes_available',
            'profile_picture_url',
          ],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: UserAddress,
          required: false,
          attributes: ['id', 'name', 'lat', 'long', 'location_address', 'address', 'is_default'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
      ],
    });

    if (!foundUser) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    }

    if (isMobile) {
      foundUser.setDataValue('token', foundUser.app_token);
    }
    foundUser.setDataValue('app_token', undefined);

    return withSuccess(foundUser);
  }

  /**
   * @summary get detail user info by id (any)
   */
  @Security('jwt')
  @Get('/{id}')
  public async getDetailUserInfo(@Request() request: any, id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundUser = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: [
            'id',
            'name',
            'phone',
            'livestream_enable',
            'status',
            'stream_minutes_available',
            'profile_picture_url',
          ],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: UserAddress,
          required: false,
          attributes: ['id', 'name', 'lat', 'long', 'location_address', 'address', 'is_default'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
      ],
    });
    if (!foundUser) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    }
    return withSuccess(foundUser);
  }
}
