import { IS_ACTIVE, CONFIG_STATUS, CONFIG_TYPE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import {
  Body,
  Request,
  Controller,
  Get,
  Path,
  Post,
  Put,
  Query,
  Route,
  SuccessResponse,
  Delete,
  Tags,
  Security,
} from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes, ConfigTypes } from 'types';
import { ConfigService } from '@services/internal/config.service';
import * as _ from 'lodash';
import * as express from 'express';

const db = require('@models');
const { sequelize, Sequelize, Config, ConfigLuckySpinGift } = db.default;
const { Op } = Sequelize;

@Route('config')
@Tags('configs')
export class ConfigController extends ApplicationController {
  private configService: ConfigService;
  constructor() {
    super('Config');
    this.configService = new ConfigService();
  }
  /**
   * @summary get all config (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/type')
  public async getAllConfig(@Request() request: AppTypes.RequestAuth): Promise<AppTypes.SuccessResponseModel<any>> {
    const listConfig = await this._list({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(listConfig);
  }

  /**
   * @summary detail config by config_type (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/{config_type}/type')
  public async getDetailConfig(
    @Request() request: AppTypes.RequestAuth,
    config_type: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const configData = await this._findOne({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { type: config_type, is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(configData);
  }

  /**
   * @summary update data value of a config (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{config_type}/type')
  public async updateValueConfig(
    @Request() request: AppTypes.RequestAuth,
    config_type: number,
    @Body() body: { value: number; status: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundConfig = await this._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, type: config_type },
    });
    if (!foundConfig) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Dữ liệu không tồn tại.');
    if (foundConfig.dataValues.type === CONFIG_TYPE.ORDER_PROMOTION && (body.value < 0 || body.value > 100))
      throw new AppError(ApiCodeResponse.INVALID_PARAM).with('Phần trăm khuyến mãi không hợp lệ.');
    if (body.value < 0) throw new AppError(ApiCodeResponse.INVALID_PARAM).with('Giá trị không hợp lệ.');
    await foundConfig.update({
      ...body,
      update_by: loggedInUser.id,
      update_at: new Date(),
    });
    return withSuccess({});
  }

  /**
   * @summary change config status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{config_type}/type/status')
  public async changeConfigStatus(
    @Request() request: AppTypes.RequestAuth,
    config_type: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundConfig = await this._findOne({
      where: { type: config_type, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundConfig) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST).with('Loại cấu hình không tồn tại');
    await foundConfig.update({
      status: foundConfig.status === CONFIG_STATUS.ACTIVE ? CONFIG_STATUS.INACTIVE : CONFIG_STATUS.ACTIVE,
      update_by: loggedInUser.id,
      update_at: new Date(),
    });
    return withSuccess({
      status: foundConfig.status,
    });
  }

  /**
   * @summary delete list lucky spin gift id: Array<number> (admin)
   */
  @Security('jwt', ['admin'])
  @Delete('/lucky-spin-gift')
  public async deleteUserAdmin(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const resUpdate = await ConfigLuckySpinGift.update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    if (resUpdate[0] === 0) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Quà tặng không tồn tại.');
    return withSuccess({});
  }

  /**
   * @summary get lucky spin gift (any)
   */
  @Security('jwt')
  @Get('/lucky-spin-gift')
  public async getConfigLuckySpinGift(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
    };
    const { count, rows } = await ConfigLuckySpinGift.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary get detail lucky spin gift (any)
   */
  @Security('jwt')
  @Get('/{id}/lucky-spin-gift')
  public async getDetailConfigLuckySpinGift(id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundData = await ConfigLuckySpinGift.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Quà tặng không tồn tại.');
    return withSuccess(foundData);
  }

  /**
   * @summary create lucky spin gift (admin)
   */
  @Security('jwt', ['admin'])
  @Post('/lucky-spin-gift')
  public async createConfigLuckySpinGift(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: ConfigTypes.ConfigLuckySpinGiftModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const bodyValidate: ConfigTypes.ConfigLuckySpinGiftModel =
      await this.configService.ConfigLuckySpinGiftSchema.validateAsync(body);
    const dataCreate = await ConfigLuckySpinGift.create({ ...bodyValidate, create_by: loggedInUser.id });
    return withSuccess(dataCreate);
  }
  /**
   * @summary update data lucky spin gift (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/lucky-spin-gift')
  public async updateConfigLuckySpinGift(
    @Request() request: AppTypes.RequestAuth,
    id: number,
    @Body() body: ConfigTypes.ConfigLuckySpinGiftModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundData = await ConfigLuckySpinGift.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Quà tặng không tồn tại.');
    const bodyValidate: ConfigTypes.ConfigLuckySpinGiftModel =
      await this.configService.ConfigLuckySpinGiftSchema.validateAsync(body);
    await foundData.update({ ...bodyValidate, update_by: loggedInUser.id, update_at: new Date() });
    return withSuccess(foundData);
  }

  /**
   * @summary change lucky spin gift status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/lucky-spin-gift/status')
  public async changeConfigLuckySpinGiftStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const foundData = await ConfigLuckySpinGift.findOne({ where: { id, is_active: IS_ACTIVE.ACTIVE } });
    if (!foundData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Quà tặng không tồn tại.');
    await foundData.update(
      {
        status: foundData.status === CONFIG_STATUS.ACTIVE ? CONFIG_STATUS.INACTIVE : CONFIG_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      { where: { id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess({
      status: foundData.status === CONFIG_STATUS.ACTIVE ? CONFIG_STATUS.INACTIVE : CONFIG_STATUS.ACTIVE,
    });
  }
}
