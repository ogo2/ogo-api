import {
  IS_ACTIVE,
  IS_GIFT_ACTIVE,
  GIFT_STATUS,
  DF_NOTIFICATION,
  SOCKET,
  GIFT_TYPE,
  GIFT_OWNER_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { NotificationService } from '@services/internal/notification.service';
import { Body, Request, Get, Post, Query, Route, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes } from 'types';
import { PointService } from '@services/internal/point.service';

const db = require('@models');
const { sequelize, Sequelize, Gift, DFTypeGift, PurchasedGift, User, UserAddress, Order } = db.default;
const { Op } = Sequelize;

@Route('client/gift')
@Tags('client/gift')
export class ClientGiftController extends ApplicationController {
  private pointService: PointService;
  private notificationService: NotificationService;
  constructor() {
    super('Gift');
    this.pointService = new PointService();
    this.notificationService = new NotificationService();
  }

  // create noti
  public async createNotificationAdmin(id, gift_id, socket, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);

    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Có yêu cầu đổi quà mới cần xác nhận';
    const df_notification_id = notiType.id;
    const data = { gift_id: gift_id };
    const user_id = null;
    const titleNoti = notiType.title;
    // socket.emit(SOCKET.ADMIN, { title: titleNoti, type: notiType.id, content, data: { gift_id: gift_id } });
    const noti = await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
    socket.emit(SOCKET.ADMIN, { title: titleNoti, type: notiType.id, id: noti, content, data: { gift_id: gift_id } });
  }
  // create noti point
  public async createNotificationPoint(id, gift_id, id_user, transaction) {
    const foundGift = await this._findOne({
      where: { id: gift_id, is_active: IS_ACTIVE.ACTIVE },
    });
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn bị trừ ' + foundGift.dataValues.price + ' điểm sau khi đổi quà ' + foundGift.dataValues.name;
    const df_notification_id = notiType.id;
    const data = { gift_id: gift_id };
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      1,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  // create gift noti
  public async createNotification(id, id_user, status, purchase_gifts_id, name, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content =
      status == GIFT_STATUS.CONFIRMED
        ? 'Yêu cầu đổi quà ' + name + ' của bạn đã được xác nhận'
        : 'Yêu cầu đổi quà ' + name + ' của bạn đã bị từ chối';
    const df_notification_id = notiType.id;
    const data = { purchase_gifts_id: purchase_gifts_id };
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  /**
   * @summary list gift (no security)
   */
  @Get('/')
  public async getListGift(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);

    const whereOption: any = { is_active: IS_ACTIVE.ACTIVE, status: IS_GIFT_ACTIVE.ACTIVE };
    if (search) {
      whereOption.name = { [Op.like]: `%${search}%` };
    }
    const { count, rows } = await Gift.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [[sequelize.literal('`DFTypeGift`.`name`'), 'type_gift_name']],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeGift,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary request purchase gift (any)
   */
  @Security('jwt')
  @Post('/{gift_id}/purchase-gift')
  public async requestPurchaseGift(
    @Request() request: AppTypes.RequestIOAuth,
    gift_id: number,
    @Body() body: { user_address_id?: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundUser = await User.findOne({ where: { id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE } });
    if (!foundUser) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);

    await sequelize.transaction(async (transaction: any) => {
      const foundGift = await this._findOne(
        {
          where: { id: gift_id, quantity: { [Op.gt]: 0 }, is_active: IS_ACTIVE.ACTIVE },
        },
        { transaction },
      );
      if (!foundGift)
        throw new AppError(ApiCodeResponse.NOT_ENOUGH).with(
          'Quà tặng không tồn tại hoặc đã hết, hãy quay lại sớm hơn vào lần sau.',
        );
      if (foundUser.point < foundGift.price)
        throw new AppError(ApiCodeResponse.NOT_ENOUGH).with('Số điểm của bạn không đủ để đổi quà');

      let requestPurchase;
      // đổi quà tặng là mã giảm giá
      if (foundGift.df_type_gift_id === GIFT_TYPE.DISCOUNT_CODE) {
        requestPurchase = await PurchasedGift.create(
          {
            user_id: loggedInUser.id,
            gift_id: foundGift.id,
            status: GIFT_STATUS.CONFIRMED,
            create_by: loggedInUser.id,
          },
          { transaction },
        );
        // trừ điểm + lưu lịch sử
        this.pointService.purchaseDiscount(foundUser.id, foundGift.price, foundUser.point, transaction);
      }
      // đổi quà tặng là hiện vật
      else {
        if (!body.user_address_id)
          throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(`Vui lòng chọn địa chỉ nhận quà`);
        const foundAddress = await UserAddress.findOne({
          where: {
            id: body.user_address_id,
            user_id: loggedInUser.id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
        });
        if (!foundAddress)
          throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(`Địa chỉ này của bạn không tồn tại.`);
        requestPurchase = await PurchasedGift.create(
          {
            user_id: loggedInUser.id,
            gift_id: foundGift.id,
            status: GIFT_STATUS.PENDING,
            create_by: loggedInUser.id,
            address: foundAddress.dataValues,
          },
          { transaction },
        );
        // trừ điểm + lưu lịch sử
        this.pointService.purchaseGift(foundUser.id, foundGift.price, foundUser.point, transaction);
        // thông báo cho admin
        await this.createNotificationAdmin(DF_NOTIFICATION.PURCHASE_GIFT, requestPurchase.id, request.io, transaction);
      }
      if (foundGift.quantity) await foundGift.decrement({ quantity: 1 }, { transaction });
      // thông báo trừ điểm
      await this.createNotificationPoint(DF_NOTIFICATION.GIFT_EXCHANGE, foundGift.id, loggedInUser.id, transaction);
    });
    return withSuccess({});
  }

  /**
   * @summary list gift (no security)
   */
  @Security('jwt')
  @Get('/owner')
  public async getListGiftOwner(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = {
      user_id: userDecoded.id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    // if (search) {
    //   whereOption.name = { [Op.like]: `%${search}%` };
    // }
    // trạng thái mã giảm giá đã được sử dụng hoặc quà đã được gửi
    if (status === GIFT_OWNER_STATUS.USED) {
      whereOption.status = { [Op.in]: [GIFT_STATUS.USED, GIFT_STATUS.SENT_GIFT] };
    }
    // trạng thái mã giảm giá chưa sử dụng hoặc quà đang đợi duyện
    else if (status === GIFT_OWNER_STATUS.NOT_USED) {
      whereOption.status = { [Op.in]: [GIFT_STATUS.PENDING, GIFT_STATUS.CONFIRMED] };
    }
    const { count, rows } = await PurchasedGift.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Gift,
          required: true,
          where: { is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            include: [[sequelize.literal('`Gift->DFTypeGift`.`name`'), 'type_gift_name']],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          include: [
            {
              model: DFTypeGift,
              required: false,
              attributes: [],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
        {
          model: Order,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
        }
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }
}
