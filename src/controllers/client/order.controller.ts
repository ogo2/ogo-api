import {
  IS_ACTIVE,
  ORDER_STATUS,
  DF_NOTIFICATION,
  PANCAKE,
  GIFT_STATUS,
  MEDIA_TYPE,
  SHOP_STATUS,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { AppError } from '@utils/AppError';
import { ApplicationController } from '..';

import Joi from '../../utils/JoiValidate';
import { NotificationService } from '@services/internal/notification.service';
import { ShopAction } from '../../websocket/action';
import { logger } from '@utils/Logger';
import { Body, Get, Request, Security, Post, Put, Query, Route, Delete, Tags } from 'tsoa';
import { withError, withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Utils } from '@utils/Utils';
import { AppTypes } from 'types';
import * as _ from 'lodash';

const db = require('@models');
const axios = require('axios');

const {
  sequelize,
  Sequelize,
  OrderStatusHistory,
  OrderItem,
  Gift,
  Cart,
  Order,
  ProductCustomAttributeOption,
  ProductPrice,
  Product,
  UserAddress,
  User,
  Shop,
  Stock,
  PurchasedGift,
  ProductMedia,
  Category,
  DFOrderStatusHistory,
  Review,
  DFProvince,
  DFDistrict,
  DFWard,
} = db.default;
const { Op } = Sequelize;
@Route('client/order')
@Tags('client/order')
export class OrderController extends ApplicationController {
  private notificationService: NotificationService;
  constructor() {
    super('Order');
    this.notificationService = new NotificationService();
  }

  /**
   * @summary Danh sách voucher giảm giá
   */
  @Security('jwt')
  @Get('/voucher')
  public async listVouCher(
    @Request() request: any,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const baseUrl = Utils.getBaseServer(request);
    const listGift = await PurchasedGift.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId, status: GIFT_STATUS.CONFIRMED },
      include: {
        model: Gift,
        // attributes: [],
        where: { is_active: IS_ACTIVE.ACTIVE, df_type_gift_id: 2 },
      },
    });
    return withSuccess(listGift);
  }

  /**
   * @summary Danh sách trạng thái đơn hàng
   */
  @Security('jwt')
  @Get('/')
  public async listOrder(
    @Request() request: any,
    @Query('status') status?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const baseUrl = Utils.getBaseServer(request);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const { offset, limit, page } = handlePagingMiddleware(request);

    const { rows, count } = await Order.findAndCountAll({
      where: {
        user_id: userId,
        is_active: IS_ACTIVE.ACTIVE,
        status: { [Op.in]: ![null, ''].includes(status) ? [status] : Object.values(ORDER_STATUS) },
      },
      attributes: [
        'id',
        'code',
        'status',
        'user_address_id',
        'shop_id',
        'create_at',
        ['total_price', 'total_discount'],
        [
          Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                FROM order_item AS order_item
                WHERE order_item.order_id = Order.id
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
            )`),
          'total_price',
        ],
        [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
        [Sequelize.literal('IFNULL((`Reviews`.`is_active`),0)'), 'check_review'],
        [Sequelize.literal('OrderItems.product->"$.name"'), 'product_name'],
        [Sequelize.literal('OrderItems.product->"$.id"'), 'product_price_id'],
        [Sequelize.literal('OrderItems.product->"$.product_attribute_name_1"'), 'product_attribute_name_1'],
        [Sequelize.literal('OrderItems.product->"$.product_attribute_name_2"'), 'product_attribute_name_2'],
        [Sequelize.literal('OrderItems.product->"$.category_name"'), 'category_name'],
        [Sequelize.literal('OrderItems.amount'), 'product_amount'],
        [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
        [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
        [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
        [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
        [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
        [Sequelize.literal('OrderItems.product->"$.media_url"'), 'media_url'],
        [
          Sequelize.literal(
            `(SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url FROM product_media WHERE product_id = OrderItems.product_id and is_active = 1 and type = 0 and product_custom_attribute_option_id is null limit 1 )`,
          ),
          'default_image',
        ],
        [Sequelize.literal('OrderItems.product->"$.price"'), 'product_price'],
        // [Sequelize.literal('OrderItems.product->"$.media_url"'), 'media_url'],
        [Sequelize.literal('OrderItems.product->"$.media_url"'), 'media_url'],
        [
          Sequelize.literal(
            `(SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url FROM product_media WHERE product_id = OrderItems.product_id and is_active = 1 and type = 0 and product_custom_attribute_option_id is null limit 1 )`,
          ),
          'default_image',
        ],
        // [sequelize.fn('CONCAT', baseUrl, '/', sequelize.literal('OrderItems.product->"$.media_url"')), 'media_url']
      ],
      include: [
        {
          model: OrderItem,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: Review,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      group: ['Order.id'],
      order: [['id', 'desc']],
      page,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary Chi tiết đơn hàng
   */
  @Security('jwt')
  @Get('/{order_id}')
  public async detailOrder(@Request() request: any, order_id?: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const baseUrl = Utils.getBaseServer(request);
    // return withSuccess(userId);
    // return userId;
    const { offset, limit, page } = handlePagingMiddleware(request);

    const detail = await Order.findOne({
      where: { id: order_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
      attributes: [
        'id',
        'code',
        'status',
        'user_address_id',
        'shop_id',
        ['total_price', 'total_discount'],
        [
          Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                FROM order_item AS order_item
                WHERE order_item.order_id = ${order_id}
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
            )`),
          'total_price',
        ],
        [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
        [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
        [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
        [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
        [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
        [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
      ],
      include: [
        {
          model: OrderItem,
          where: { is_active: IS_ACTIVE.ACTIVE },
          // limit: 1,
          attributes: [
            'product_id',
            'order_id',
            'stock_id',
            'amount',
            'price',
            [Sequelize.literal('OrderItems.product->"$.name"'), 'product_name'],
            [Sequelize.literal('OrderItems.product->"$.id"'), 'product_price_id'],
            [Sequelize.literal('OrderItems.amount'), 'product_amount'],
            [
              Sequelize.literal(
                `IFNULL((OrderItems.product->"$.image"),(SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url FROM product_media WHERE product_id = OrderItems.product_id and is_active = 1 and type = 0  and product_custom_attribute_option_id is null limit 1 ))`,
              ),
              'product_image',
            ],
            [Sequelize.literal('OrderItems.product->"$.price"'), 'product_price'],
            [Sequelize.literal('OrderItems.product->"$.product_attribute_name_1"'), 'product_attribute_name_1'],
            [Sequelize.literal('OrderItems.product->"$.product_attribute_name_2"'), 'product_attribute_name_2'],
            [Sequelize.literal('OrderItems.product->"$.category_name"'), 'category_name'],
            [Sequelize.literal('OrderItems.product->"$.media_url"'), 'media_url'],
            [
              Sequelize.literal(
                `(SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url FROM product_media WHERE product_id = OrderItems.product_id and is_active = 1 and type = 0 and product_custom_attribute_option_id is null limit 1 )`,
              ),
              'default_image',
            ],
            // [sequelize.fn('CONCAT', baseUrl, '/', sequelize.literal('OrderItems.product->"$.media_url"')), 'media_url']
          ],
        },
        {
          model: UserAddress,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
      ],
      group: ['OrderItems.id'],
    });
    if (!detail) throw ApiCodeResponse.ORDER_NOT_EXIST;
    const listOrderHistory = await OrderStatusHistory.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, order_id: order_id },
      include: [
        {
          model: DFOrderStatusHistory,
          attributes: ['id', 'order', 'describe'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    return withSuccess({ detail, listOrderHistory });
  }

  /**
   * @summary  Thanh toán
   */

  @Security('jwt')
  @Post('/order')
  public async createOrderNew(
    @Request() request: any,
    @Body()
    body: {
      // total_price?: number;
      // total_discount?: number;
      user_address_id?: number;
      carts?: {
        shop_id?: number;
        pancake_shop_key?: any;
        items?: {
          total_discount?: number;
          stock_id?: number;
          purchase_gift_id?: number;
          note?: string;
          total_money?: number;
          cart_id?: number[];
          products?: {
            product_price_id?: number;
            amount?: number;
          }[];
        }[];
      }[];
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const schema = Joi.object({
      // total_price: Joi.number().empty(['', null, 0, '0', '']),
      // total_discount: Joi.number().empty(['', null, 0, '0', '']),
      user_address_id: Joi.number().empty(['', null, 0, '0', '']),
      carts: Joi.array()
        .items(
          Joi.object({
            shop_id: Joi.number().empty(['', null, 0, '0', '']),
            pancake_shop_key: Joi.string().allow(null, ''),
            items: Joi.array()
              .items(
                Joi.object()
                  .keys({
                    purchase_gift_id: Joi.number().empty(['', null, 0, '0', '']),
                    stock_id: Joi.number().empty(['', null, 0, '0', '']),
                    note: Joi.string().allow(null, ''),
                    cart_id: Joi.array().allow('', null, 0, '0', ''),
                    total_money: Joi.number().empty(['', null, 0, '0', '']),
                    total_discount: Joi.number().empty(['', null, 0, '0', '']),
                    products: Joi.array().items(
                      Joi.object()
                        .keys({
                          product_price_id: Joi.number().empty(['', null, 0, '0', '']),
                          amount: Joi.number().empty(['', null, 0, '0', '']),
                        })
                        .unknown(true),
                    ),
                  })
                  .unknown(true),
              )
              .required(),
          }),
        )
        .required(),
    });

    const { carts, user_address_id, total_price } = await schema.validateAsync(body);
    // return withSuccess(carts);

    // Check địa chỉ user_address
    const userAddress = await UserAddress.findOne({
      where: { id: user_address_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
    });
    // return withSuccess(customer)
    if (!userAddress) throw withError(ApiCodeResponse.CUSTOMER_ISNT_OF_U);
    // let userGift;
    let dataGitfs;
    // let detailGitfs;
    const baseUrl = Utils.getBaseServer(request);
    for (let ca = 0; ca < carts.length; ca++) {
      for (let index = 0; index < carts[ca].items.length; index++) {
        const productDetail = await ProductPrice.findAll({
          raw: true,
          where: {
            id: { [Op.in]: carts[ca].items[index].products.map((product) => product.product_price_id) },
            is_active: IS_ACTIVE.ACTIVE,
            stock_id: carts[ca].items[index].stock_id,
          },
          include: [
            {
              model: Product,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: [],
              include: {
                model: Category,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: [],
              },
            },
            {
              model: Stock,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE, id: carts[ca].items[index].stock_id },
              attributes: [],
            },
            {
              model: ProductCustomAttributeOption,
              required: false,
              attributes: [],
              as: 'product_attribute_name_1',
              include: {
                model: ProductMedia,
                required: false,
                attributes: [],
                where: { is_active: IS_ACTIVE.ACTIVE, type: MEDIA_TYPE.IMAGE },
              },
            },
            {
              model: ProductCustomAttributeOption,
              required: false,
              attributes: [],
              as: 'product_attribute_name_2',
            },
          ],
          attributes: [
            'id',
            'product_id',
            ['id', 'product_price_id'],
            'custom_attribute_option_id_1',
            'custom_attribute_option_id_2',
            'price',
            'stock_id',
            'custom_attribute_option_pancake_id_1',
            'custom_attribute_option_pancake_id_2',
            'product_pancake_id',
            'variration_pancake_id',
            [Sequelize.col('product.name'), 'name'],
            [Sequelize.col('product.code'), 'code'],
            [Sequelize.col('product.category_id'), 'category_id'],
            [Sequelize.col('product.description'), 'description'],
            [Sequelize.col('stock.name'), 'stock_name'],
            [Sequelize.col('product_attribute_name_1.name'), 'product_attribute_name_1'],
            [Sequelize.col('product_attribute_name_2.name'), 'product_attribute_name_2'],
            [
              sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('product_attribute_name_1->ProductMedium.media_url')),
              'media_url',
            ],
            [sequelize.col('product_attribute_name_1->ProductMedium.media_url'), 'product_image'],
            // [Sequelize.col('product_attribute_name_1->ProductMedium.media_url'), 'media_url'],
            // [Sequelize.col('product_attribute_name_1->ProductMedium.id'), 'media_id'],
            [
              Sequelize.literal(`(
                            SELECT \`name\` FROM \`category\`
                            AS \`category\`
                            WHERE (\`category\`.\`id\` = product.category_id
                            AND \`category\`.\`is_active\` = ${IS_ACTIVE.ACTIVE}) LIMIT 1
                            )`),
              'category_name',
            ],
          ],
          // order: [['id', 'asc']],
        });
        // return withSuccess(productDetail);
        const datapro = carts[ca].items[index].products.sort(
          (a, b) => parseFloat(a.product_price_id) - parseFloat(b.product_price_id),
        );

        const userGift = await PurchasedGift.findOne({
          where: {
            id: carts[ca].items[index].purchase_gift_id ? carts[ca].items[index].purchase_gift_id : null,
            user_id: userId,
            is_active: IS_ACTIVE.ACTIVE,
          },
        });
        let detailGitfs = null;
        // return withSuccess(userGift);
        if (userGift != null && userGift != undefined) {
          detailGitfs = await Gift.findOne({
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              id: userGift != null && userGift != undefined ? userGift.gift_id : null,
            },
            attributes: { include: [[sequelize.literal('`PurchasedGifts`.`id`'), 'purchase_gift_id']] },
            include: {
              model: PurchasedGift,
              required: false,
              attributes: [],
              where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId, id: userGift.id },
            },
          });
        }
        // return withSuccess(detailGitfs);
        // if (detailGitfs != undefined && detailGitfs != null) {
        //   // dataGitfs.purchase_gift_id = userGift.id;
        //   dataGitfs = detailGitfs.map((data, indexs) => ({
        //     id: data.id,
        //     df_type_gift_id: data.df_type_gift_id,
        //     price: data.price,
        //     discount_percent: data.discount_percent,
        //     max_discount_money: data.max_discount_money,
        //     quantity: data.quantity,
        //     icon_url: data.icon_url,
        //     status: data.status,
        //     is_active: data.is_active,
        //     name: data.name,
        //     purchase_gift_id: userGift[indexs].id,
        //     shop_id: carts[indexs].shop_id,
        //     stock_id: carts[ca].items[index].purchase_gift_id == data.purchase_gift_id ? carts[ca].items[index].stock_id : null,
        //   }));
        //   // return withSuccess(customer)
        //   if (!userGift) throw withError(ApiCodeResponse.GIFT_NOT_FOUNT);
        // }
        // return withSuccess(dataGitfs);
        const shopDetail = await Shop.findOne({
          where: { id: carts[ca].shop_id, is_active: IS_ACTIVE.ACTIVE, pancake_shop_key: { [Op.ne]: null } },
        });
        // return withSuccess(shopDetail);
        const stockDetail = await Stock.findOne({
          where: { id: carts[ca].items[index].stock_id, is_active: IS_ACTIVE.ACTIVE },
        });
        // return withSuccess(datapro);
        let itemsData;
        if (shopDetail != null && shopDetail != undefined) {
          if (carts[ca].pancake_shop_key == shopDetail.pancake_shop_key) {
            itemsData = productDetail.map((product, indexs) => ({
              product_id: product.product_pancake_id,
              variation_id: product.variration_pancake_id,
              quantity: datapro[indexs].amount,
              variation_info: { retail_price: product.price },
            }));
          }
        }
        // return withSuccess(itemsData);
        axios.defaults.baseURL = PANCAKE;
        axios.defaults.headers.post['Content-Type'] = 'application/json';
        if (productDetail == null && productDetail.length == 0) {
          throw new AppError(ApiCodeResponse.PRODUCT_NOT_EXIST);
        }
        // return withSuccess({
        //   warehouse_id: stockDetail.pancake_stock_id,
        //   discount: total_price - carts[ca].items[index].total_discount,
        //   total_price: total_price,
        //   shipping_address: {
        //     address: userAddress.address,
        //     full_address: userAddress.location_address,
        //     commune_id: UserAddress.df_ward_id,
        //     district_id: UserAddress.district_id,
        //     province_id: UserAddress.province_id,
        //     full_name: UserAddress.name,
        //     phone_number: UserAddress.phone,
        //   },
        //   shipping_fee: 30000,
        //   shop_id: shopDetail.pancake_shop_key,
        //   items: itemsData,
        // });
        const order_id = await sequelize.transaction(async (transaction) => {
          let pancakeData;
          if (itemsData) {
            pancakeData = await axios
              .post('/shops/' + shopDetail.pancake_shop_id + '/orders?api_key=' + shopDetail.pancake_shop_key, {
                warehouse_id: stockDetail.pancake_stock_id,
                total_price: total_price,
                discount: carts[ca].items[index].total_discount ? carts[ca].items[index].total_discount : 0,
                shipping_address: {
                  address: userAddress.address,
                  full_address: userAddress.location_address,
                  commune_id: UserAddress.df_ward_id,
                  district_id: UserAddress.district_id,
                  province_id: UserAddress.province_id,
                  full_name: UserAddress.name,
                  phone_number: UserAddress.phone,
                },
                shipping_fee: 30000,
                shop_id: shopDetail.pancake_shop_key,
                items: itemsData,
              })
              .then((result) => {
                console.log('logData', result);
                // if (result.data.data == false) {
                //   return false;
                // }
                return result.data.data;
              })
              .catch((err) => {
                console.error(err);
              });
          }

          console.log('pancakeData', pancakeData);
          //   return withSuccess(pancakeData);
          // const giftData = dataGitf.map((data, indexs) => ({
          // const detailGitfs = await Gift.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, id: { [Op.in]: userGift.map((data) => data.gift_id) } } });

          const order = await Order.create(
            {
              code: Order.generateCodeOrder(),
              stock_id: carts[ca].items[index].stock_id,
              user_id: userId,
              // gift_code: dataGitfs.map(function (obj) {
              //   if (obj.stock_id == carts[ca].items[index].stock_id) {
              //     return dataGitfs[ca];
              //   }
              // }).filter(function (el) {
              //   return el != null;
              // })[0],
              gift_code: detailGitfs,
              user_address_id: user_address_id,
              shop_id: carts[ca].shop_id,
              note: carts[ca].items[index].note,
              // gift_code: gift_id,
              address: 'Hà Nội',
              status: ORDER_STATUS.PENDING,
              pancake_order_id: pancakeData != undefined ? pancakeData.id : null,
              total_price: carts[ca].items[index].total_discount,
              // total_price:
              //   carts[ca].items[index].purchase_gift_id != 0 && carts[ca].items[index].purchase_gift_id != undefined
              //     ? carts[ca].items[index].total_money * (detailGitfs.discount_percent / 100) > detailGitfs.max_discount_price ? carts[ca].items[index].total_money - detailGitfs.max_discount_price : carts[ca].items[index].total_money - carts[ca].items[index].total_money * (detailGitfs.discount_percent / 100)
              //     : carts[ca].items[index].total_money,
            },
            { transaction },
          );
          // }));
          // const order = await Order.bulkCreate(giftData, { transaction });
          // return withSuccess(order);
          for (let indexProduct = 0; indexProduct < carts[ca].items[index].products.length; indexProduct++) {
            await ProductPrice.decrement(
              { amount: carts[ca].items[index].products[indexProduct].amount },
              { where: { id: carts[ca].items[index].products[indexProduct].product_price_id }, transaction },
            );
          }
          await OrderStatusHistory.create(
            { order_id: order.id, df_order_status_history_id: ORDER_STATUS.PENDING },
            { transaction },
          );
          await this.createNotification(
            DF_NOTIFICATION.NEW_ORDER,
            carts[ca].shop_id,
            order.id,
            order.code,
            request.io,
            transaction,
          );

          const orderProductData = productDetail.map((product, indexs) => ({
            order_id: order.id,
            product_id: product.product_id,
            // price: gift_id ? (product.price - product.price * (dataGitf.discount_percent / 100)) > dataGitf.max_discount_money ? (product.price - dataGitf.max_discount_money) : product.price * (dataGitf.discount_percent / 100) : product.price,
            price: product.price,
            product,
            amount: datapro[indexs].amount,
            stock_id: product.stock_id,
          }));
          // return withSuccess(orderProductData);

          if (carts[ca].items[index].purchase_gift_id != 0 && carts[ca].items[index].purchase_gift_id != undefined) {
            PurchasedGift.update(
              { status: GIFT_STATUS.USED, order_id: order.id },
              {
                where: {
                  id: carts[ca].items[index].purchase_gift_id,
                  user_id: userId,
                  is_active: IS_ACTIVE.ACTIVE,
                },
                transaction,
              },
            );
          }

          // await OrderItem.bulkCreate(orderProductData, { transaction });
          await Promise.all([
            OrderItem.bulkCreate(orderProductData, { transaction }),
            Cart.update(
              { is_active: IS_ACTIVE.INACTIVE },
              { where: { id: { [Op.in]: carts[ca].items[index].cart_id } }, transaction },
            ),
            // PurchasedGift.update({ status: GIFT_STATUS.USED }, { where: { id: gift_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE }, transaction }),

            // orderService.handleNotificationAddOrder(order, store_id, transaction),
          ]);

          return 1;
        });
      }
    }

    return withSuccess({});
  }

  // create noti
  public async createNotification(id, shop_id, order_id, code, socket, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    const checkShop = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id } });
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn có đơn hàng ' + code + ' cần xác nhận';
    const df_notification_id = notiType.id;
    const user_id = checkShop?.id ? checkShop.id : null;
    const titleNoti = notiType.title;
    const data = { order_id: order_id, title: titleNoti, content, type: notiType.id, shop_id: shop_id };
    // socket.emit(SOCKET.SHOP, {
    //   title: titleNoti,
    //   content,
    //   type: notiType.id,
    //   data: { order_id: order_id, shop_id: shop_id },
    // });
    const noti_id = await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      shop_id,
      transaction,
    );
    const dataSocket = {
      order_id: order_id,
      title: titleNoti,
      content,
      type: notiType.id,
      shop_id: shop_id,
      id: noti_id,
    };
    ShopAction(socket).sendNotification(shop_id, dataSocket);
    logger.error({ message: `order confirm ${shop_id}` });
  }

  /**
   * @summary Sửa đơn hàng đơn hàng
   */
  @Security('jwt')
  @Put('/order')
  public async updateOrder(
    @Request() request: any,
    @Body()
    body: {
      order_id?: number;
      user_address_id?: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const schema = Joi.object({
      order_id: Joi.number().empty(['', null, 0, '0', '']),
      user_address_id: Joi.number().empty(['', null, 0, '0', '']),
    });

    const { order_id, user_address_id } = await schema.validateAsync(body);

    // Check địa chỉ user_address
    const userAddress = await UserAddress.findOne({
      where: { id: order_id, user_address_id: user_address_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
    });
    // return withSuccess(customer)
    if (!userAddress) throw withError(ApiCodeResponse.CUSTOMER_ISNT_OF_U);

    // kiểm tra trạng thái order
    const order = await Order.findOne({
      where: { id: order_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
    });
    if (order == null) throw ApiCodeResponse.ORDER_NOT_EXIST;
    if (order.status > 0) throw ApiCodeResponse.ORDER_NOT_UPDATE;
    await Order.update(
      { user_address_id: user_address_id },
      { where: { id: order_id, user_address_id: user_address_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE } },
    );

    return withSuccess({});
  }

  /**
   * @summary Huỷ đơn hàng đơn hàng
   */
  @Security('jwt')
  @Delete('/{order_id}/order')
  public async deleteOrder(@Request() request: any, order_id?: any): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(1);
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;

    // kiểm tra trạng thái order
    const order = await Order.findOne({
      where: { id: order_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
    });
    if (order == null) throw ApiCodeResponse.ORDER_NOT_EXIST;
    if (order.status > 1) throw ApiCodeResponse.ORDER_NOT_DELETE;
    const shopDetail = await Shop.findOne({
      where: { id: order.shop_id, is_active: IS_ACTIVE.ACTIVE, pancake_shop_key: { [Op.ne]: null } },
    });
    // return withSuccess(shopDetail)
    axios.defaults.baseURL = PANCAKE;
    axios.defaults.headers.post['Content-Type'] = 'application/json';

    if (shopDetail != null) {
      await axios
        .put(
          '/shops/' +
            shopDetail.pancake_shop_id +
            '/orders/' +
            order.pancake_order_id +
            '?api_key=' +
            shopDetail.pancake_shop_key,
          {
            status: 6,
          },
        )
        .then((result) => {
          console.log('logData', result);
          // if (result.data.data == false) {
          //   return false;
          // }
          return result.data.data;
        })
        .catch((err) => {
          console.error(err);
        });
    }
    const order_data = await sequelize.transaction(async (transaction) => {
      await Order.update(
        { status: ORDER_STATUS.CANCELED },
        { where: { id: order_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE } },
      );
      if (order.gift_code != null && order.gift_code != undefined) {
        await PurchasedGift.update(
          { status: GIFT_STATUS.CONFIRMED },
          { where: { id: order.gift_code.purchase_gift_id } },
        );
      }
      await this.createNotificationShop(
        DF_NOTIFICATION.ORDER_CANCEL,
        order.shop_id,
        order.id,
        order.code,
        request.io,
        transaction,
      );
    });

    return withSuccess({});
  }

  public async createNotificationShop(id, shop_id, order_id, code, socket, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    const checkShop = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id } });
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Đơn hàng ' + code + ' đã bị hủy';
    const df_notification_id = notiType.id;
    const user_id = null;
    const titleNoti = notiType.title;
    const data = { order_id: order_id, title: titleNoti, content, type: notiType.id, shop_id: shop_id };
    ShopAction(socket).sendNotification(shop_id, data);
    logger.error({ message: `order cancel ${shop_id}` });
    // socket.emit(SOCKET.SHOP, {
    //   title: titleNoti,
    //   content,
    //   type: notiType.id,
    //   data: { order_id: order_id, shop_id: shop_id },
    // });
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      shop_id,
      transaction,
    );
  }
}
