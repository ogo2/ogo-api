import {
  Body,
  Security,
  Controller,
  Get,
  Path,
  Post,
  Put,
  Query,
  Route,
  SuccessResponse,
  Delete,
  Tags,
  Header,
  Request,
} from 'tsoa';
import { AppTypes, PostTypes } from 'types';
import { withError, withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import {
  IS_ACTIVE,
  MEDIA_TYPE,
  POST_STATUS,
  ROLE,
  DF_NOTIFICATION,
  IS_POSTED,
  PRODUCT_STOCK_STATUS,
  SHOP_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { Utils } from '@utils/Utils';
import { PostService } from '@services/internal/post.service';
import { NotificationService } from '@services/internal/notification.service';
const db = require('@models');
const { sequelize, Sequelize, Post: _Post, PostMedia, Shop, User, Reaction, Comment, Topic, Product } = db.default;
const { Op } = Sequelize;

@Route('client/post')
@Tags('client/posts')
export class ClientPostsController extends ApplicationController {
  private postService: PostService;
  private notificationService: NotificationService;
  constructor() {
    super('Post');
    this.notificationService = new NotificationService();
    this.postService = new PostService();
  }

  /**
   *  @summary get detail post
   */
  @Get('/{id}')
  public async getDetailPost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const baseUrl = Utils.getBaseServer(request);
    const foundPost = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn  tại.');
    const { rows, count } = await Comment.findAndCountAll({
      where: { post_id: id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`comment\` as CM
              WHERE CM.is_active = ${IS_ACTIVE.ACTIVE}
              AND CM.parent_id = \`Comment\`.id
                )`),
            'count_sub_comment',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Comment,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          include: [
            {
              model: User,
              required: true,
              attributes: {
                exclude: [
                  'code_reset_password',
                  'expired_reset_password',
                  'token',
                  'password',
                  'create_by',
                  'update_by',
                  'delete_by',
                  'version',
                ],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: User,
              required: false,
              as: 'target_user',
              attributes: {
                exclude: [
                  'code_reset_password',
                  'expired_reset_password',
                  'token',
                  'password',
                  'create_by',
                  'update_by',
                  'delete_by',
                  'version',
                ],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: Shop,
              required: false,
              attributes: {
                exclude: ['create_by', 'update_by', 'delete_by', 'version'],
              },
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
          order: [['id', 'desc']],
          limit: 2,
        },
      ],
      limit: 10,
      order: [['id', 'desc']],
    });

    return withSuccess({ post: foundPost, comment: rows });
  }

  /**
   * @summary get list comment (any)
   */
  @Get('/comment/all')
  public async getAllListComment(
    @Request() request: AppTypes.RequestAuth,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('post_id') post_id?: number,
    @Query('livestream_id') livestream_id?: number,
    @Query('comment_id') comment_id?: number,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,

      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (post_id) {
      whereOption.post_id = post_id;
    }
    if (livestream_id) {
      whereOption.livestream_id = livestream_id;
    }
    if (comment_id) {
      whereOption.parent_id = comment_id;
    }

    const listComment = await Comment.findAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: false,
          as: 'target_user',
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [['id', 'desc']],
    });
    return withSuccess(listComment);
  }
}
