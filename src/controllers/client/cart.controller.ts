import { IS_ACTIVE, MEDIA_TYPE, PRODUCT_STATUS, SHOP_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import Joi from '../../utils/JoiValidate';
import { Body, Get, Request, Post, Put, Query, Route, Delete, Tags } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Utils } from '@utils/Utils';
import { AppTypes } from 'types';

const db = require('@models');

const {
  sequelize,
  Sequelize,
  Order,
  OrderItem,
  ReviewMedia,
  Cart,
  Review,
  ProductPrice,
  Product,
  ProductCustomAttributeOption,
  User,
  Shop,
  Stock,
  ProductMedia,
} = db.default;
const { Op } = Sequelize;
@Route('user-cart')
@Tags('client/user-cart')
export class CartController extends ApplicationController {
  constructor() {
    super('Cart');
  }

  /**
   * @summary Tổng số lượng cart
   */

  /**
   * @summary Tổng số lượng cart
   */
  @Get('/count-cart')
  public async detailProductAgent(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId = null;
    if (tokens) {
      const user = await User.findOne({
        where: { [Op.or]: [{ token: tokens }, { app_token: tokens }], is_active: IS_ACTIVE.ACTIVE },
      });
      userId = user ? user.id : null;
      // if (userId) throw ApiCodeResponse.UNAUTHORIZED;
    }
    // return withSuccess(userId);
    const dtAll = await Cart.findAll({
      where: { user_id: userId, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: ProductPrice,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
          },
          include: [
            {
              model: Product,
              where: { is_active: IS_ACTIVE.ACTIVE, status: PRODUCT_STATUS.AVAILABLE },
              include: {
                model: Shop,
                where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
              },
            },
          ],
        },
      ],
    });
    // return withSuccess(dtAll);
    // return dtAll;
    // const countCart = await Cart.findAll({
    //     where: { is_active: IS_ACTIVE.ACTIVE, user_id: userID ? userID : 0 },
    //     order: [['id', 'desc']],
    // })
    return withSuccess(dtAll.length);
  }
  /**
   * @summary Thêm mới giỏ hàng
   */
  // @Security('jwt', ['agent'])
  @Post('/')
  public async createCart(
    @Request() request: any,
    @Body()
    body: {
      product_price_id: number;
      amount: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }
    const schema = Joi.object({
      product_price_id: Joi.number().empty(['', null, 'null']).required(),
      amount: Joi.number().empty(['', null, 0, '0']).required(),
    });
    const bodyData = await schema.validateAsync(body);
    if (body.amount <= 0 || body.amount == undefined) throw ApiCodeResponse.AMOUNT_NOT_FOUNT;

    var checkData = await Cart.count({
      where: { product_price_id: body.product_price_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
    });
    const dataCart = await sequelize.transaction(async (transaction) => {
      if (checkData) {
        const findCart = await Cart.findOne({
          where: { product_price_id: body.product_price_id, user_id: userId, is_active: IS_ACTIVE.ACTIVE },
        });
        const cartId = await Cart.update(
          {
            amount: findCart.amount + bodyData.amount,
            is_active: IS_ACTIVE.ACTIVE,
            update_at: Date.now(),
            create_at: Date.now(),
          },
          {
            where: { id: findCart.id },
          },
          { transaction },
        );
        return findCart.id;
      } else {
        const createdCart = await Cart.create(
          {
            product_price_id: bodyData.product_price_id,
            amount: bodyData.amount,
            user_id: userId,
            is_active: IS_ACTIVE.ACTIVE,
          },
          { transaction },
        );
        return createdCart.id;
      }
    });
    // return withSuccess(dataCart);
    const detail = await this.agentCartDetail(dataCart);
    return withSuccess(detail);
  }

  // /**
  //  * @summary Danh sách giỏ hàng
  //  */
  // // @Security('jwt', ['agent'])
  @Get('/')
  public async getCart(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }

    const baseUrl = Utils.getBaseServer(request);

    const dtAll = await Shop.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
      attributes: [
        'id',
        'name',
        'phone',
        'profile_picture_url',
        'pancake_shop_key',
        [sequelize.literal('MAX(`Stocks->ProductPrices->Carts`.create_at)'), 'maxCreatedAt'],
      ],
      // subQuery: false,
      // row: true,
      include: [
        {
          model: Stock,
          // required: false,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
          },
          attributes: ['id', 'name'],
          include: [
            {
              model: ProductPrice,
              attributes: [
                'id',
                'product_id',
                'stock_id',
                'custom_attribute_option_id_1',
                'custom_attribute_option_id_2',
                'price',
                [sequelize.literal('`Stocks->ProductPrices->Product`.`name`'), 'product_name'],
                [
                  sequelize.fn(
                    'CONCAT',
                    baseUrl,
                    '/',
                    sequelize.literal(
                      'IFNULL((`Stocks->ProductPrices->product_attribute_name_1->ProductMedium`.`media_url`),(`Stocks->ProductPrices->Product->ProductMedia`.`media_url`))',
                    ),
                  ),
                  'media_url',
                ],
                // [sequelize.literal('`Stocks->ProductPrices->product_attribute_name_1->ProductMedium`.`media_url`'), "media_url"],
                [
                  sequelize.literal(`(
                                     SELECT create_at FROM cart AS c
                                     WHERE 
                                     c.id = \`Stocks->ProductPrices->Carts\`.id
                                     AND 
                                     c.product_price_id = \`Stocks->ProductPrices->Carts\`.product_price_id 
                                     AND 
                                     c.is_active = ${IS_ACTIVE.ACTIVE}
                                     AND 
                                     c.user_id =  ${userId}
                                     )`),
                  'create_at_Cart',
                ],
              ],
              // required: false,
              where: {
                is_active: IS_ACTIVE.ACTIVE,
              },

              include: [
                {
                  model: Cart,
                  attributes: ['id', 'user_id', 'amount', 'product_price_id', 'create_at'],
                  // required: false,
                  where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId },
                },
                {
                  model: Product,
                  attributes: ['id'],
                  // required: false,
                  where: { is_active: IS_ACTIVE.ACTIVE },
                  include: {
                    model: ProductMedia,
                    required: false,
                    attributes: ['id', 'media_url'],
                    where: {
                      is_active: IS_ACTIVE.ACTIVE,
                      product_custom_attribute_option_id: null,
                      type: MEDIA_TYPE.IMAGE,
                    },
                    // limit: 1,
                  },
                },
                {
                  attributes: ['id', 'product_custom_attribute_id', 'name', 'is_active'],
                  model: ProductCustomAttributeOption,
                  required: false,
                  as: 'product_attribute_name_1',
                  where: {
                    is_active: IS_ACTIVE.ACTIVE,
                  },
                  include: {
                    model: ProductMedia,
                    required: false,
                    attributes: [],
                    where: { is_active: IS_ACTIVE.ACTIVE },
                  },
                },
                {
                  attributes: ['id', 'product_custom_attribute_id', 'name', 'is_active'],
                  model: ProductCustomAttributeOption,
                  as: 'product_attribute_name_2',
                },
              ],
            },
          ],
        },
      ],
      order: [[sequelize.literal('MAX(`Stocks->ProductPrices->Carts`.create_at)'), 'desc']],
      group: [sequelize.literal('`Stocks->ProductPrices->Carts`.create_at'), 'Stocks->ProductPrices.id', 'Shop.id'],
    });
    const listData: Array<any> = dtAll.map((item: any) => {
      if (!item.Stocks) {
        return item;
      }
      item.Stocks = item.Stocks.map((s) => {
        s.ProductPrices.forEach((pp) => {
          pp.setDataValue('Cart', pp.Carts[0]);
          pp.setDataValue('Carts', null);
        });
        s.ProductPrices.sort(function (a, b) {
          return Number(a.dataValues.create_at_Cart) == Number(b.dataValues.create_at_Cart)
            ? 0
            : Number(a.dataValues.create_at_Cart) > Number(b.dataValues.create_at_Cart)
              ? -1
              : 1;
        });
        return s;
      });
      return item;
    });
    return withSuccess(listData);
  }
  /**
   * @summary Sửa giỏ hàng
   */
  // @Security('jwt', ['agent'])
  @Put('/{id}')
  public async updateCart(
    id: number,
    @Request() request: any,
    @Body()
    body: {
      amount: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }
    const checkIdCart = await Cart.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE, user_id: userId },
    });
    if (!checkIdCart) throw ApiCodeResponse.NOT_FOUND;
    const schema = Joi.object({
      amount: Joi.number().empty(['', null, 0, '0']),
    });
    const bodyData = await schema.validateAsync(body);
    const cartId = await sequelize.transaction(async (transaction) => {
      if (bodyData.amount == 0 || bodyData.amount == undefined) {
        await Cart.update(
          {
            amount: bodyData.amount,
            is_active: IS_ACTIVE.INACTIVE,
            update_at: Date.now(),
          },
          {
            where: { id },
            transaction,
          },
        );
      } else {
        await Cart.update(
          {
            amount: bodyData.amount,
          },
          {
            where: { id },
            transaction,
          },
        );
      }
      // return checkIdCart.id;
    });
    const detailCard = await this.agentCartDetail(id);
    return withSuccess(detailCard);
  }
  /**
   * @summary Xóa giỏ hàng
   */
  // @Security('jwt', ['agent'])
  @Delete('/{id}')
  public async deleteCart(id: number, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }
    if (!userId) throw new AppError(ApiCodeResponse.REASON_AGENT_EXITS);
    const checkIdCart = await Cart.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE, user_id: userId },
    });
    if (!checkIdCart) throw ApiCodeResponse.NOT_FOUND;
    const deleteCart = await Cart.update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        update_at: Date.now(),
        delete_at: Date.now(),
      },
      {
        where: { id },
      },
    );
    return withSuccess({});
  }

  @Get('/{id}')
  public async agentCartDetail(id: number) {
    // return withSuccess(id);
    const dt = await Cart.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: ProductPrice,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
          },
          include: [
            {
              model: Product,
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              attributes: ['id', 'product_custom_attribute_id', 'name', 'is_active'],
              model: ProductCustomAttributeOption,
              as: 'product_attribute_name_1',
            },
            {
              attributes: ['id', 'product_custom_attribute_id', 'name', 'is_active'],
              model: ProductCustomAttributeOption,
              as: 'product_attribute_name_2',
            },
          ],
        },
      ],
    });
    return withSuccess(dt);
  }

  /**
   * @summary Đánh giá đơn hàng
   */
  // @Security('jwt', ['agent'])
  @Put('/{id_order}/review')
  public async reviewOrder(
    @Request() request: any,
    @Body()
    body: {
      items: {
        product_id?: number;
        rating?: number;
        comment?: string;
        note?: string;
        array_image?: string[];
      }[];
    },
    id_order: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }
    // const baseurl = getBaseUrl(request);
    // if (type == PRODUCT_MEDIA_TYPE.IMAGE) {
    //   await uploadMiddleware.handleSingleFile(request, 'image', type);
    // } else if (type == PRODUCT_MEDIA_TYPE.VIDEO) {
    //   await uploadMiddleware.handleSingleFile(request, 'video', type);
    // } else {
    //   throw new AppError(ApiCodeResponse.INVALID_PARAM).with('Kiểu resource không hợp lệ');
    // }
    // let array_image = [];
    // const uploads = await uploadMiddleware.handleFiles(request, 'image', MEDIA_TYPE.IMAGE);
    // if (uploads) {
    //     array_image = (request as ProductMulterRequests).files.map((file) => file.path);
    // }

    const schema = Joi.object({
      items: Joi.array().items(
        Joi.object()
          .keys({
            product_id: Joi.number().integer().required(),
            rating: Joi.number().allow('', null, 0, '0'),
            comment: Joi.string().allow('', null, 'null', ''),
            array_image: Joi.array().items(Joi.string()).allow(null, ''),
          })
          .unknown(true),
      ),
    }).unknown(true);
    const { items } = await schema.validateAsync(request.body);
    // const product_id = review.map((rate) => rate.product_id);
    for (let index = 0; index < items.length; index++) {
      const productReview = await Product.findAll({
        raw: true,
        attributes: [
          'id',
          'star',
          [
            Sequelize.literal(`(
                    SELECT IFNULL(COUNT(review.id), 0)
                    FROM review AS review
                    WHERE review.product_id = product.id
                        AND
                        review.is_active = ${IS_ACTIVE.ACTIVE}
                    )`),
            'count_star_vote',
          ],
          [
            Sequelize.literal(`IFNULL((
                        SELECT SUM(star)
                        FROM review AS review
                        WHERE review.product_id = product.id
                            AND
                            review.is_active = ${IS_ACTIVE.ACTIVE}
                    ),0)`),
            'sum_star',
          ],
        ],
        where: {
          id: items[index].product_id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      });
      // return withSuccess(productReview);

      // const checkReview = await Review.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, order_id: id_order, user_id: userId } });
      // // return withSuccess(checkReview);
      // if (checkReview.length > 0) return withSuccess(RATE.REVIEW_EXIT);

      // check xem sp có trong đơn hàng hay ko
      const checkOrderProduct = await Order.findAll({
        where: { is_active: IS_ACTIVE.ACTIVE, id: id_order },
        include: { model: OrderItem, where: { is_active: IS_ACTIVE.ACTIVE, product_id: items[index].product_id } },
      });
      // return withSuccess(checkOrderProduct);
      if (checkOrderProduct.length == 0) throw ApiCodeResponse.ORDER_PRODUCT_EXIT;
      const newProductReview = productReview.map((product) => ({
        id: product.id,
        value: {
          star:
            Math.round(
              (10 * (items[index].rating + parseInt(product.sum_star))) / (1 + parseInt(product.count_star_vote)),
            ) / 10,
        },
      }));
      const baseUrl = Utils.getBaseServer(request) + '/';
      // payload.profile_picture_url = dataUpdateBody.profile_picture_url.replace(baseUrl, '');
      const data = await sequelize.transaction(async (transaction) => {
        const review = await Review.create({
          product_id: items[index].product_id,
          order_id: id_order,
          user_id: userId,
          content: items[index].comment,
          note: items[index].note,
          star: items[index].rating,
        });
        await Promise.all([
          newProductReview.map(async ({ id, value }) =>
            Product.update(value, { where: { id, is_active: IS_ACTIVE.ACTIVE }, transaction }),
          ),
          ReviewMedia.bulkCreate(
            items[index].array_image.map((image) => ({
              type: MEDIA_TYPE.IMAGE,
              media_url: image.replace(baseUrl, ''),
              review_id: review.id,
            })),
            { transaction },
          ),
        ]);
        return review.id;
        // await ReviewMedia.create({ media_url: path, type: PRODUCT_MEDIA_TYPE.IMAGE, review_id: review.id });
      });
    }

    // Kiểm tra xem đơn hàng đó đã hoàn thành hay chưa
    // Kiểm tra xem đơn hàng đó đã đánh giá hay chưa
    return withSuccess({});
  }

  /**
   * @summary Chi tiết đánh giá sản phẩm
   */
  @Get('/{product_id}/review-product')
  public async getDetailProductReview(
    product_id: number,
    @Request() request: any,
    @Query('star') star?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    const baseUrl = Utils.getBaseServer(request);
    let reviewCount;
    let detail;
    // return withSuccess(product_id);
    if (product_id) {
      reviewCount = await Review.findAll({
        attributes: {
          include: [
            [Sequelize.literal('`User`.`name`'), 'user_name'],
            // [Sequelize.literal('`User`.`id`'), 'user_id'],
          ],
        },
        where: {
          product_id,
          is_active: IS_ACTIVE.ACTIVE,
          // star: star ? star : { [Op.ne]: null }
        },
        include: [
          {
            model: User,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: ReviewMedia,
            attributes: ['id', 'media_url'],
          },
        ],
        order: [['id', 'DESC']],
      });
    }
    const data = { counts: 0, average: 0 };
    let sum = 0;
    let votes;
    reviewCount.forEach((item) => {
      if (!data[item.star]) data[item.star] = { counts: 0 };
      // data[item.star].rows.push(item);
      // eslint-disable-next-line no-plusplus
      data[item.star].counts++;
      sum += item.star;
    });
    // data.rating = rating;
    data.average = Math.round(sum / reviewCount.length);
    data.counts = reviewCount.length;
    const { rows, count } = await Review.findAndCountAll({
      attributes: {
        include: [
          [Sequelize.literal('`User`.`name`'), 'user_name'],
          [
            sequelize.fn('CONCAT', baseUrl, '/', sequelize.literal('`User`.`profile_picture_url`')),
            'profile_picture_url',
          ],
          // [Sequelize.literal('`User`.`id`'), 'user_id'],
        ],
      },
      where: {
        product_id,
        is_active: IS_ACTIVE.ACTIVE,
        star: star ? star : { [Op.ne]: null },
      },
      include: [
        {
          model: User,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: ReviewMedia,
          attributes: ['id', 'media_url'],
        },
      ],
      order: [['id', 'desc']],
      limit,
      offset,
    });
    // data.vote = votes;
    return withPagingSuccess(
      { rows, data },
      { page, limit, totalItemCount: count instanceof Array ? count.length : count },
    );
  }

  /**
   * @summary Chi tiết đánh giá đơn hàng
   */
  @Get('/{order_id}/review-order')
  public async getDetailReview(order_id: number, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    let review;
    if (order_id) {
      review = await Review.findAll({
        attributes: {
          include: [
            [Sequelize.literal('`User`.`name`'), 'user_name'],
            // [Sequelize.literal('`User`.`id`'), 'user_id'],
          ],
        },
        where: {
          order_id,
          is_active: IS_ACTIVE.ACTIVE,
        },
        include: [
          {
            model: User,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: ReviewMedia,
            attributes: ['id', 'media_url'],
          },
        ],
        order: [['id', 'DESC']],
      });
    }
    const data = {};
    let sum = 0;
    let votes;
    review.forEach((item) => {
      if (!data[item.star]) data[item.star] = { counts: 0, rows: [] };
      data[item.star].rows.push(item);
      // eslint-disable-next-line no-plusplus
      data[item.star].counts++;
      sum += item.star;
    });
    // data.rating = rating;
    // data.average = Math.round(sum / review.length);
    // data.counts = review.length;
    // data.vote = votes;
    return withSuccess(data);
  }

  // /**
  //  * @summary Số lượng sản phẩm trong giỏ hàng
  //  */
  // @Security('jwt', ['agent'])
  // @Get('/countProduct')
  // public async countProduct(@Request() request: any,): Promise<AppTypes.SuccessResponseModel<any>> {
  //     const _agent = await Agent.findOne({
  //         where: { user_id: loggedInUser.id, is_active: IS_ACTIVE.ACTIVE },
  //     });
  //     if (!_agent) throw new AppError(ApiCodeResponse.REASON_AGENT_EXITS);
  //     const product_price_id: Array<number> = [];
  //     var dataEn = await Enterprise.findAll({
  //         where: { is_active: IS_ACTIVE.ACTIVE },
  //         include:
  //         {
  //             model: Stock,
  //             // required: false,
  //             where: {
  //                 is_active: IS_ACTIVE.ACTIVE,
  //             },
  //             include: [
  //                 {
  //                     model: ProductPrice,
  //                     attributes: [
  //                         'id',
  //                         'product_id',
  //                         'stock_id',
  //                         'level_id',
  //                         'custom_attribute_option_id_1',
  //                         'custom_attribute_option_id_2',
  //                         [
  //                             sequelize.literal(
  //                                 `if(\`Stocks->ProductPrices\`.\`custom_attribute_option_id_2\` is null,
  //                                     (SELECT price FROM product_price AS pp
  //                                         WHERE
  //                                         level_id is null
  //                                         AND
  //                                         pp.product_id = \`Stocks->ProductPrices\`.product_id
  //                                         AND
  //                                         pp.agent_id =  ${_agent.id}
  //                                         AND
  //                                         pp.stock_id = \`Stocks->ProductPrices\`.stock_id
  //                                         AND
  //                                         pp.custom_attribute_option_id_1 = \`Stocks->ProductPrices\`.custom_attribute_option_id_1)
  //                                     ,
  //                                         (SELECT price FROM product_price AS pp
  //                                         WHERE
  //                                         level_id is null
  //                                         AND
  //                                         pp.product_id = \`Stocks->ProductPrices\`.product_id
  //                                         AND
  //                                         pp.agent_id =  ${_agent.id}
  //                                         AND
  //                                         pp.stock_id = \`Stocks->ProductPrices\`.stock_id
  //                                         AND
  //                                         pp.custom_attribute_option_id_1 = \`Stocks->ProductPrices\`.custom_attribute_option_id_1
  //                                         AND
  //                                         pp.custom_attribute_option_id_2 = \`Stocks->ProductPrices\`.custom_attribute_option_id_2)
  //                                     )`,
  //                             ),
  //                             'price',
  //                         ],
  //                         [
  //                             sequelize.literal(
  //                                 `if(\`Stocks->ProductPrices\`.\`custom_attribute_option_id_2\` is null,
  //                                     (SELECT price FROM product_price AS pp
  //                                         WHERE
  //                                         level_id is not null
  //                                         AND
  //                                         pp.id = \`Stocks->ProductPrices\`.id
  //                                         AND
  //                                         pp.product_id = \`Stocks->ProductPrices\`.product_id
  //                                         AND
  //                                         pp.agent_id =  ${_agent.id}
  //                                         AND
  //                                         pp.stock_id = \`Stocks->ProductPrices\`.stock_id
  //                                         AND
  //                                         pp.custom_attribute_option_id_1 = \`Stocks->ProductPrices\`.custom_attribute_option_id_1)
  //                                     ,
  //                                         (SELECT price FROM product_price AS pp
  //                                         WHERE
  //                                         level_id is not null
  //                                         AND
  //                                         pp.id = \`Stocks->ProductPrices\`.id
  //                                         AND
  //                                         pp.product_id = \`Stocks->ProductPrices\`.product_id
  //                                         AND
  //                                         pp.agent_id =  ${_agent.id}
  //                                         AND
  //                                         pp.stock_id = \`Stocks->ProductPrices\`.stock_id
  //                                         AND
  //                                         pp.custom_attribute_option_id_1 = \`Stocks->ProductPrices\`.custom_attribute_option_id_1
  //                                         AND
  //                                         pp.custom_attribute_option_id_2 = \`Stocks->ProductPrices\`.custom_attribute_option_id_2)
  //                                     )`,
  //                             ),
  //                             'discount_price',
  //                         ],
  //                     ],
  //                     // required: false,
  //                     where: {
  //                         is_active: IS_ACTIVE.ACTIVE,
  //                     },
  //                     include: [
  //                         {
  //                             model: AgentCart,
  //                             // required: false,
  //                             where: { is_active: IS_ACTIVE.ACTIVE, agent_id: _agent.id },
  //                         },
  //                         {
  //                             model: Product,
  //                             required: false,
  //                             where: { is_active: IS_ACTIVE.ACTIVE },
  //                         },
  //                         {
  //                             attributes: ['id', 'product_custom_attribute_id', 'name', 'is_active'],
  //                             model: ProductCustomAttributeOption,
  //                             as: 'product_attribute_name_1',
  //                             where: {
  //                                 is_active: IS_ACTIVE.ACTIVE,
  //                             },
  //                             include: {
  //                                 model: ProductMedia,
  //                                 required: false,
  //                                 attributes: ["id", "media_url",
  //                                     // [
  //                                     //     sequelize.literal(
  //                                     //         `if(\`media_url\` is null, '',  CONCAT('${baseUrl}/', \`media_url\`))`,
  //                                     //     ),
  //                                     //     'media_url2',
  //                                     // ],
  //                                 ],
  //                                 where: { is_active: IS_ACTIVE.ACTIVE }
  //                             }
  //                         },
  //                         {
  //                             attributes: ["id", "product_custom_attribute_id", "name", "is_active"],
  //                             model: ProductCustomAttributeOption,
  //                             as: "product_attribute_name_2",
  //                         },

  //                     ],
  //                 }
  //             ]
  //         }
  //         ,
  //     })
  //     const finalItems = dataEn.map(t => {
  //         if (!t.Stocks) {
  //             return t
  //         }
  //         t.Stocks = t.Stocks.map(s => {
  //             s.ProductPrices.forEach(pp => {
  //                 pp.setDataValue('AgentCart', pp.AgentCarts[0])
  //                 pp.setDataValue('AgentCarts', null)
  //             })
  //             return s
  //         })
  //         return t
  //     });
  //     let lstStk = 0;
  //     for (let index = 0; index < finalItems.length; index++) {
  //         let entp = finalItems[index];
  //         for (let i = 0; i < entp.Stocks.length; i++) {
  //             let stk = entp.Stocks[i];
  //             for (let x = 0; x < stk.ProductPrices.length; x++) {
  //                 let lstPr = stk.ProductPrices[x]
  //                 lstPr.Product != null ? ((lstStk == 0) ? lstStk = 1 : lstStk += 1) : lstStk
  //             }
  //             // lstStk = stk.ProductPrices.length

  //         }
  //     }
  //     return withSuccess(lstStk);
  // }
}
