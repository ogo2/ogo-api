import { IS_ACTIVE } from '@utils/constants';

import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '..';

import Joi from '../../utils/JoiValidate';
import { Body, Get, Request, Security, Post, Put, Query, Route, Delete, Tags } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { AppTypes } from 'types';

const db = require('@models');

const { sequelize, Sequelize, User, UserAddress, DFProvince, DFDistrict, DFWard } = db.default;
const { Op } = Sequelize;

@Route('client/user-address')
@Tags('client/user-address')
export class AgentShopController extends ApplicationController {
  constructor() {
    super('UserAddress');
  }
  /**
   * @summary lấy địa chỉ địa chỉ mặc định
   */
  @Security('jwt')
  @Get('/default-shop')
  public async defaultAgentShop(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const defaultUserAddress = await UserAddress.findOne({
      attributes: {
        include: [
          [Sequelize.literal('`DFProvince`.`name`'), 'province_name'],
          [Sequelize.literal('`DFWard`.`name`'), 'ward_name'],
          [Sequelize.literal('`DFDistrict`.`name`'), 'district_name'],
        ],
      },
      where: {
        is_default: 1,
        is_active: IS_ACTIVE.ACTIVE,
        user_id: userId,
      },
      include: [
        {
          model: DFProvince,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFWard,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFDistrict,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    return withSuccess(defaultUserAddress);
  }
  /**
   * @summary tìm kiếm Shop
   */
  @Security('jwt')
  @Get('/search')
  public async listUserAddress(@Request() request: any, @Query() search?: string): Promise<any> {
    //AppTypes.PagingResponseModel<any>
    //validate
    // console.log(search)
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    // const agent = await UserAddress.findOne({ where: { user_id: userId } });
    // if (!agent) throw new AppError(ApiCodeResponse.REASON_AGENT_EXITS);
    const schema = Joi.object({
      search: Joi.string().empty(['null', null, '', '']),
    });
    const queryObj = await schema.validateAsync({ search });

    const { offset, limit, page } = handlePagingMiddleware(request);
    const { rows, count } = await UserAddress.findAndCountAll({
      attributes: {
        include: [
          [Sequelize.literal('`DFProvince`.`name`'), 'province_name'],
          [Sequelize.literal('`DFWard`.`name`'), 'ward_name'],
          [Sequelize.literal('`DFDistrict`.`name`'), 'district_name'],
        ],
      },
      include: [
        {
          model: DFProvince,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFWard,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFDistrict,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: {
        // name: { [Op.substring]: queryObj.search ? queryObj.search : { [Op.ne]: null } },
        name: { [Op.substring]: queryObj.search ? queryObj.search : '' },
        is_active: IS_ACTIVE.ACTIVE,
        user_id: userId,
      },
      logging: true,
      order: [['is_default', 'desc']],
      limit,
      offset,
    });

    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary thêm UserAddress
   */
  @Security('jwt')
  @Post('/')
  public async createUserAddress(
    @Request() request: any,
    @Body()
    body: {
      name: string;
      phone: string;
      address?: any;
      location_address?: string;
      is_default: number;
      lat?: number;
      long?: number;
      df_province_id?: number;
      df_district_id?: number;
      df_ward_id?: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const schema = Joi.object({
      name: Joi.string().empty(['null', null, '']).required(),
      address: Joi.string().empty(['null', null, '']),
      location_address: Joi.string().empty(['null', null, '']),
      lat: Joi.number().empty(['', null, 0, '0', '']),
      long: Joi.number().empty(['', null, 0, '0', '']),
      df_province_id: Joi.number().empty(['', null, 0, '0', '']),
      df_district_id: Joi.number().empty(['', null, 0, '0', '']),
      df_ward_id: Joi.number().empty(['', null, 0, '0', '']),
      phone: Joi.string().empty(['null', null, '']),
    });
    const bodyData = await schema.validateAsync(body);
    const d = await sequelize.transaction(async (transaction) => {
      if (bodyData.is_default == 1) {
        await UserAddress.update(
          { is_default: 0 },
          { where: { user_id: userId, is_active: IS_ACTIVE.ACTIVE }, transaction },
        );
      }
      const d = await UserAddress.create(
        {
          name: bodyData.name,
          address: bodyData.address,
          user_id: userId,
          location_address: bodyData.location_address,
          lat: bodyData.lat,
          long: bodyData.long,
          phone: bodyData.phone,
          is_default: bodyData.is_default,
          df_province_id: bodyData.df_province_id,
          df_district_id: bodyData.df_district_id,
          df_ward_id: bodyData.df_ward_id,
        },
        { transaction },
      );
      return d;
    });
    const data = await UserAddress.findOne({
      where: { id: d.id, is_active: IS_ACTIVE.ACTIVE, user_id: userId },
      // include: [{ model: , include: [{ model: AgentShop, as: 'default_shop' }] }],
    });
    return withSuccess(data);
  }
  /**
   * @summary Chi tiết UserAddress
   */
  // @Security('jwt', ['agent'])
  @Get('/{id}')
  public async getAgentUserAddress(@Request() request: any, id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    let userId = 0;
    const tokens = request.headers.token;
    // const baseUrl = getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({
        where: { [Op.or]: [{ token: tokens }, { app_token: tokens }], is_active: IS_ACTIVE.ACTIVE },
      });

      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      userId = loginUser ? loginUser.id : null;
    }

    const data = await UserAddress.findOne({
      attributes: [
        'id',
        'name',
        'phone',
        'user_id',
        'location_address',
        'address',
        'is_default',
        [sequelize.literal('`DFProvince`.`name`'), 'province_name'],
        [sequelize.literal('`DFDistrict`.`name`'), 'district_name'],
        [sequelize.literal('`DFWard`.`name`'), 'ward_name'],
      ],
      where: { id: id, is_active: IS_ACTIVE.ACTIVE, user_id: userId },
      include: [
        {
          model: DFProvince,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFDistrict,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: DFWard,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    return withSuccess(data);
  }
  /**
   * @summary Sửa UserAddress
   */
  @Security('jwt')
  @Put('/{id}')
  public async updateUserAddress(
    id: number,
    @Request() request: any,
    @Body()
    body: {
      name: string;
      phone: string;
      address?: any;
      location_address?: string;
      is_default: number;
      lat?: number;
      long?: number;
      df_province_id?: number;
      df_district_id?: number;
      df_ward_id?: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const schema = Joi.object({
      name: Joi.string().empty(['null', null, '']).required(),
      address: Joi.string().empty(['null', null, '']),
      location_address: Joi.string().empty(['null', null, '']),
      lat: Joi.number().empty(['', null, 0, '0', '']),
      long: Joi.number().empty(['', null, 0, '0', '']),
      df_province_id: Joi.number().empty(['', null, 0, '0', '']),
      df_district_id: Joi.number().empty(['', null, 0, '0', '']),
      df_ward_id: Joi.number().empty(['', null, 0, '0', '']),
      phone: Joi.string().empty(['null', null, '']),
    });
    const bodyData = await schema.validateAsync(body);
    const updateUserAddress = await sequelize.transaction(async (transaction) => {
      if (bodyData.is_default == 1) {
        await UserAddress.update({ is_default: 0 }, { where: { user_id: userId }, transaction });
      }
      await UserAddress.update(
        {
          name: bodyData.name,
          user_id: userId,
          address: bodyData.address,
          location_address: bodyData.location_address,
          phone: bodyData.phone,
          is_default: bodyData.is_default,
          update_at: Date.now(),
          df_province_id: bodyData.df_province_id,
          df_district_id: bodyData.df_district_id,
          df_ward_id: bodyData.df_ward_id,
        },
        {
          where: { id: id },
          transaction,
        },
      );
    });
    const data = await UserAddress.findOne({
      where: { id: id, is_active: IS_ACTIVE.ACTIVE },
      user_id: userId,
    });
    return withSuccess(data);
  }
  /**
   * @summary Xóa UserAddress
   */
  @Security('jwt')
  @Delete('/{id}')
  public async deleteUserAddress(id: number, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const deleteAll = await sequelize.transaction(async (transaction) => {
      await UserAddress.update(
        {
          is_active: IS_ACTIVE.INACTIVE,
          update_at: Date.now(),
          delete_at: Date.now(),
        },
        {
          where: { id: id },
          transaction,
        },
      );
      // if (Agent.agent_shop_id == id) {
      //   await Agent.update({ agent_shop_id: null }, { where: { id: agent.id }, transaction });
      // }
    });
    return withSuccess(deleteAll);
    // return withSuccess(null);
  }
}
