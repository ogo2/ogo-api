import {
  IS_ACTIVE,
  TAB,
  FILTER_TYPE,
  LIVESTREAM_STATUS,
  PRODUCT_STATUS,
  ORDER_STATUS,
  SHOP_STATUS,
  CATEGORY_STATUS,
  FOLLOW_STATUS,
} from '@utils/constants';
import { ApplicationController } from './../';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Request, Get, Query, Route, Tags, Put, Security, Body } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes } from 'types';
import { Utils } from '@utils/Utils';
import Joi = require('joi');
import { ApiCodeResponse } from '@utils/ApiCodeResponse';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Shop,
  Product,
  Post: Posts,
  Order,
  Livestream,
  ProductMedia,
  OrderItem,
  PostMedia,
  Category,
  Reaction,
  Topic,
  Follow,
} = db.default;
const { Op } = Sequelize;
@Route('client/shop')
@Tags('client/shop')
export class ShopClientController extends ApplicationController {
  constructor() {
    super('Shop');
  }

  /**
   *  @summary get list shop
   */
  @Security('jwt')
  @Get('/')
  public async getListShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('category_id') category_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    console.log('request', request.user);
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
      status: SHOP_STATUS.ACTIVE,
    };
    let whereOptionProduct: any = {
      is_active: IS_ACTIVE.ACTIVE,
      status: PRODUCT_STATUS.AVAILABLE,
    };
    let listSubCategory = [category_id];
    if (category_id) {
      // return withSuccess(category_id);
      const foundCategory = await Category.findAll({
        where: {
          parent_id: category_id,
          is_active: IS_ACTIVE.ACTIVE,
          status: CATEGORY_STATUS.ACTIVE,
        },
      });
      // return withSuccess(foundCategory);
      if (foundCategory.length > 0) {
        listSubCategory = foundCategory.map((item: any) => item.id);
        whereOptionProduct = {
          ...whereOptionProduct,
          category_id: { [Op.in]: listSubCategory },
        };
      }
    }
    const { count, rows } = await Shop.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM \`product\`
                    WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                    AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                    AND product.shop_id = \`Shop\`.id
                        )`),
            'count_product',
          ],
          [
            sequelize.literal(`IFNULL((
                    SELECT AVG(star)
                    FROM product
                    WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                    AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                    AND product.shop_id = \`Shop\`.id
                    ),0)`),
            'star',
          ],
          [
            sequelize.literal(`IFNULL((
                SELECT follow.status
                FROM follow
                WHERE follow.is_active = ${IS_ACTIVE.ACTIVE}
                AND follow.status = ${FOLLOW_STATUS.ACTIVE}
                AND follow.shop_id = \`Shop\`.id
                AND follow.user_id = ${loggedInUser.id}
                ),0)`),
            'status_follow',
          ],
        ],
        exclude: ['status_follow', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Follow,
          required: false,
          where: {
            user_id: loggedInUser.id,
            is_active: IS_ACTIVE.ACTIVE,
            status: FOLLOW_STATUS.ACTIVE,
          },
        },
        {
          model: Product,
          required: false,
          attributes: [],
          where: whereOptionProduct,
        },
      ],
      limit,
      offset,
      group: ['shop.id'],
      order: [['id', 'desc']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   *  @summary Chi tiết Shop
   */
  // @Security('jwt', ['shop'])
  @Get('/{shop_id}/{tab}')
  public async detailShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('time_type') time_type?: number,
    @Query('price_type') price_type?: number,
    @Query('buy_type') buy_type?: number,
    shop_id?: number,
    tab?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    let userID;
    const tokens = request.headers.token;
    if (tokens) {
      const user = await User.findOne({
        where: { [Op.or]: [{ token: tokens }, { app_token: tokens }], is_active: IS_ACTIVE.ACTIVE },
      });
      userID = user ? user.id : null;
    }
    // const loggedInUser = request.user?.data;
    // return withSuccess(shop_id);
    const { offset, limit, page } = handlePagingMiddleware(request);

    let orders = [['id', 'DESC']];
    const baseUrl = Utils.getBaseServer(request);

    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == undefined) {
      orders = [
        ['create_at', 'DESC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == undefined) {
      orders = [
        ['create_at', 'ASC'],
        // ['price', 'DESC'],
      ];
    }

    if (price_type == FILTER_TYPE.PRICE_MAX_MIN && time_type == undefined) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        // ['price', 'DESC'],
      ];
    }
    if (price_type == FILTER_TYPE.PRICE_MIN_MAX && time_type == undefined) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        // ['price', 'DESC'],
      ];
    }

    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == FILTER_TYPE.PRICE_MAX_MIN) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        ['Product.create_at', 'ASC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == FILTER_TYPE.PRICE_MIN_MAX) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        ['Product.create_at', 'ASC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == FILTER_TYPE.PRICE_MAX_MIN) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        ['Product.create_at', 'DESC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == FILTER_TYPE.PRICE_MIN_MAX) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        ['Product.create_at', 'DESC'],
        // ['price', 'ASC'],
      ];
    }
    if (buy_type) {
      orders = [
        [
          Sequelize.literal(`(SELECT 
            IFNULL(sum(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE ot.product_id = Product.id
            AND
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
          'DESC',
        ],

        // ['price', 'ASC'],
      ];
    }

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      id: shop_id,
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          //phone: { [Op.like]: `%${search}%` },
        },
      }),
      status: SHOP_STATUS.ACTIVE,
    };
    const shopDetail = await Shop.findOne({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM \`product\`
                    WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                    AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                    AND product.shop_id = \`Shop\`.id
                        )`),
            'count_product',
          ],
          [
            sequelize.literal(`IFNULL((
                        SELECT AVG(star)
                        FROM product
                        WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                        AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                        AND product.shop_id = \`Shop\`.id
                        ),0)`),
            'star',
          ],
          [
            sequelize.literal(`IFNULL((
                SELECT follow.status
                FROM follow
                WHERE follow.is_active = ${IS_ACTIVE.ACTIVE}
                AND follow.status = ${FOLLOW_STATUS.ACTIVE}
                AND follow.shop_id = \`Shop\`.id
                AND follow.user_id = ${userID}
                ),0)`),
            'status_follow',
          ],
        ],
        exclude: ['status_follow', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: {
        model: Follow,
        required: false,
        attributes: ['status'],
        where: { is_active: IS_ACTIVE.ACTIVE, user_id: userID, status: FOLLOW_STATUS.ACTIVE },
      },
      limit,
      offset,
      group: ['shop.id'],
      order: [['id', 'desc']],
      // logging: console.log,
    });
    // tab danh sách sản phẩm
    if (tab == TAB.PRODUCT) {
      // return withSuccess(1);
      const { rows, count } = await Product.findAndCountAll({
        // row: true,
        attributes: [
          'id',
          'name',
          'category_id',
          'star',
          'create_at',
          //   [sequelize.literal('IFNULL(sum(`OrderItems`.amount ),0)'), 'quantity_items'],
          [
            Sequelize.literal(`(SELECT 
            IFNULL(sum(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE ot.product_id = Product.id
            AND
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
            'quantity_items',
          ],
          [
            sequelize.literal(`(
                        SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                        FROM product_price AS product_price
                        WHERE
                        product_price.is_active = ${IS_ACTIVE.ACTIVE}
                        and product_price.product_id = Product.id
                        )`),
            'min_max_price',
          ],
          [
            sequelize.literal(`IFNULL(
                        (SELECT MAX(wishlist.is_active)
                        FROM wishlist AS wishlist
                        WHERE
                        wishlist.is_active = ${IS_ACTIVE.ACTIVE}
                        and wishlist.product_id = Product.id
                        and wishlist.user_id ${userID ? `=${userID}` : 'is not null'}
                        ),0)`),
            'check_like',
          ],
        ],
        include: [
          {
            model: ProductMedia,
            required: false,
            attributes: ['id', [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_url'], 'type'],
            where: { is_active: IS_ACTIVE.ACTIVE, product_custom_attribute_option_id: null },
            order: [['Products->ProductMedia.type', 'asc']],
          },
          {
            model: OrderItem,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: {
              model: Order,
              required: false,
              attributes: ['id'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
        ],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
          ...(search && {
            [Op.or]: {
              name: { [Op.like]: `%${search}%` },
              // phone: { [Op.like]: `%${search}%` },
            },
          }),
          status: PRODUCT_STATUS.AVAILABLE,
        },
        group: ['Product.id'],
        order: orders,
        // subQuery: false,
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(
        { rows, shopDetail },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    }
    if (tab == TAB.CATEGORY) {
      const category = await Category.findAll({
        attributes: [
          'id',
          'name',
          'icon_url',
          [
            sequelize.literal(`(
                            SELECT COUNT(pro.id)
                            FROM product as pro
                            JOIN category as cat on pro.category_id = cat.id
                            WHERE
                            pro.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            cat.is_active = ${IS_ACTIVE.ACTIVE}
                            AND pro.shop_id = ${shop_id}
                            AND pro.status = ${PRODUCT_STATUS.AVAILABLE}
                            AND pro.category_id in (select id from category as cate where cate.parent_id = Category.id)
                            )`),
            'quantity_items',
          ],
          // [sequelize.literal('IFNULL(count(`children_category->Products`.id ),0)'), 'quantity_items'],
          // [Sequelize.literal('`Category->parent_category`.`name`'), 'category_name'],
          // [sequelize.fn('CONCAT', baseUrl, '/', Sequelize.literal('`Category->parent_category`.`icon_url`')), 'category_image']
        ],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          ...(search && {
            [Op.or]: {
              name: { [Op.like]: `%${search}%` },
              // phone: { [Op.like]: `%${search}%` },
            },
          }),
          parent_id: null,
        },
        include: [
          {
            model: Category,
            as: 'children_category',
            // include:
            //     [
            //         {
            //             model: Category,
            //             as: 'children_category',
            //             atrributes: ['id', 'name'],
            //         }
            //     ]
            // ,

            where: { is_active: IS_ACTIVE.ACTIVE },
            include: {
              model: Product,
              // required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id, status: PRODUCT_STATUS.AVAILABLE },
            },
          },
          // {
          //     model: Product,
          //     // required: false,
          //     attributes: ["id", "name"],
          //     where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id },
          // },
        ],
        group: ['category.id', 'children_category.id', 'children_category->Products.id'],
      });
      return withSuccess({ category, shopDetail });
    }
    if (tab == TAB.LIVESTREAM) {
      const { rows, count } = await Livestream.findAndCountAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
          ...(search && {
            [Op.or]: {
              name: { [Op.like]: `%${search}%` },
              // phone: { [Op.like]: `%${search}%` },
            },
          }),
          status: { [Op.ne]: LIVESTREAM_STATUS.INITIAL },
        },
        order: [['create_at', 'desc']],
      });
      const livestreamStart = await Livestream.findOne({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
          status: LIVESTREAM_STATUS.STREAMING,
          ...(search && {
            [Op.or]: {
              name: { [Op.like]: `%${search}%` },
              // phone: { [Op.like]: `%${search}%` },
            },
          }),
        },
      });
      return withPagingSuccess(
        { rows, shopDetail, livestreamStart },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    }
    if (tab == TAB.POST) {
      const { rows, count } = await Posts.findAndCountAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
          ...(search && {
            [Op.or]: {
              name: { [Op.like]: `%${search}%` },
              // phone: { [Op.like]: `%${search}%` },
            },
          }),
        },
        attributes: {
          include: [
            [
              sequelize.literal(`(
                                SELECT COUNT(*)
                                FROM reaction AS reaction
                                WHERE
                                reaction.is_active = ${IS_ACTIVE.ACTIVE}
                                AND reaction.user_id ${userID ? `= ${userID}` : 'is null'}
                                AND reaction.post_id = Post.id
                                AND reaction.comment_id is null
                            )`),
              'is_reaction',
            ],
          ],
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        include: [
          {
            model: User,
            required: true,
            attributes: {
              include: [],
              exclude: [
                'password',
                'token',
                'code_reset_password',
                'referal_customer_id',
                'create_by',
                'update_by',
                'delete_by',
                'version',
              ],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Topic,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Shop,
            required: false,
            attributes: {
              include: [],
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id },
          },
          {
            model: PostMedia,
            required: false,
            attributes: {
              include: [],
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Reaction,
            required: false,
            attributes: {
              include: [],
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE, user_id: userID ? userID : { [Op.ne]: null } },
          },
        ],
        limit,
        offset,
        order: [['id', 'desc']],
        // logging: console.log,
      });
      return withPagingSuccess(
        { rows, shopDetail },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    }
    return withSuccess(shopDetail);
  }

  /**
   * @summary Ngưng Follow shop
   */
  @Security('jwt')
  @Put('/{shop_id}/change-follow')
  public async changeStatusShop(
    @Request() request,
    @Body() body: { status?: number },
    shop_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      status: Joi.number().required(),
    });
    const bodyData = await schema.validateAsync(body);
    const loggedInUser = request.user?.data;
    const shop = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id, id: loggedInUser.id } });
    if (shop) throw ApiCodeResponse.NOT_FOLLOW_SHOP;
    const findFollow = await Follow.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id, user_id: loggedInUser.id },
    });
    // const changeStatusProduct = await sequelize.transaction(async (transaction) => {
    if (findFollow) {
      await Follow.update(
        {
          status: bodyData.status,
        },
        { where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id, user_id: loggedInUser.id } },
      );
    } else {
      await Follow.create({
        shop_id,
        user_id: loggedInUser.id,
        status: bodyData.status,
      });
    }
    // });
    return withSuccess(bodyData.status);
  }

  /**
   *  @summary get list follow shop
   */
  @Security('jwt')
  @Get('/list-follow')
  public async getListShopFollow(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
      status: SHOP_STATUS.ACTIVE,
    };
    const { count, rows } = await Shop.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM \`product\`
                    WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                    AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                    AND product.shop_id = \`Shop\`.id
                        )`),
            'count_product',
          ],
          [
            sequelize.literal(`IFNULL((
                    SELECT AVG(star)
                    FROM product
                    WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                    AND product.status = ${PRODUCT_STATUS.AVAILABLE}
                    AND product.shop_id = \`Shop\`.id
                    ),0)`),
            'star',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Follow,
          required: true,
          where: {
            user_id: loggedInUser.id,
            is_active: IS_ACTIVE.ACTIVE,
            status: FOLLOW_STATUS.ACTIVE,
          },
        },
        {
          model: Product,
          required: false,
          attributes: [],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            status: PRODUCT_STATUS.AVAILABLE,
          },
        },
      ],
      limit,
      offset,
      group: ['shop.id'],
      order: [['id', 'desc']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }
}
