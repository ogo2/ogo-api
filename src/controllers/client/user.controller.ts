import {
  IS_ACTIVE,
  USER_STATUS,
  MEDIA_TYPE,
  ROLE,
  DF_NOTIFICATION,
  CONFIG_TYPE,
  CONFIG_STATUS,
  SHOP_STATUS,
  CATEGORY_STATUS,
  PRODUCT_STATUS,
  FOLLOW_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import * as express from 'express';
import { ApplicationController } from '../';
import { Body, Post, Put, Route, Tags, Request, Get, Query } from 'tsoa';
import { withPagingSuccess, withSuccess } from '@utils/BaseResponse';
import { MailService } from '@services/google/mail.service';
import { NotificationService } from '@services/internal/notification.service';
import { detectedDeviceMiddleware } from '@middleware/detectedDevice.middleware';
import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { PointService } from '@services/internal/point.service';
import { AppTypes } from 'types';
import { UserService } from '@services/internal/user.service';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import _ = require('lodash');

const db = require('@models');
const { sequelize, Sequelize, User, Config, DFTypeUser, Category, Shop, Product, Follow } = db.default;
const { Op } = Sequelize;

@Route('client/user')
@Tags('client/users')
export class ClientsersController extends ApplicationController {
  private userService: UserService;
  private pointService: PointService;
  private notificationService: NotificationService;
  private mailService: MailService;
  constructor() {
    super('User');
    this.userService = new UserService();
    this.pointService = new PointService();
    this.notificationService = new NotificationService();
    this.mailService = new MailService();
  }

  /**
   *  @summary get list shop
   */
  @Get('/shop-follow')
  public async getListShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
      status: SHOP_STATUS.ACTIVE,
    };
    const { count, rows } = await Shop.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      group: ['shop.id'],
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary Yêu cầu đổi lại mật khẩu
   */
  @Post('/request/reset-password')
  public async requestResetPassword(@Body() body: { email: string }): Promise<AppTypes.SuccessResponseModel<any>> {
    const dataValidated = await this.userService.RequestResetPasswordSchema.validateAsync(body);

    const foundUser = await this._findOne({
      where: { email: dataValidated.email, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    else if (foundUser.status === USER_STATUS.INACTIVE)
      throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST).with('Tài khoản này đã bị khóa.');
    const code_reset_password = User.generateOTP();

    try {
      await this.mailService.sendMailResetPassword({
        email_receiver: dataValidated.email,
        code: code_reset_password,
      });
    } catch (error) {
      throw new AppError({
        code: 67,
        message: 'Gửi email không thành công, vui lòng thử lại sau.',
      });
    }

    await foundUser.update({
      code_reset_password,
      expired_reset_password: new Date(new Date().getTime() + 5 * 60000), // 5 minus after currient time
      update_at: new Date(),
    });
    return withSuccess({});
  }
  /**
   * @summary reset password
   */
  @Post('/check-exist')
  public async checkUserExist(@Body() body: { phone: string }): Promise<AppTypes.SuccessResponseModel<any>> {
    const user = await this._findOne({
      where: { phone: body.phone, is_active: IS_ACTIVE.ACTIVE },
    });
    if (user) return withSuccess({ message: 'Tài khoản đã tồn tại' }, 5);
    return withSuccess({ phone: body.phone });
  }

  /**
   * @summary reset password
   */
  @Post('/reset-password')
  public async resetPassword(
    @Body() body: { email: string; code: string; password: string },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const dataValidated = await this.userService.ResetPasswordSchema.validateAsync(body);
    const foundUser = await this._findOne({
      where: { email: dataValidated.email, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) {
      throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    }
    console.log('fifoundUserrst', foundUser);
    if (new Date(foundUser.expired_reset_password).getTime() < new Date().getTime()) {
      throw new AppError(ApiCodeResponse.EXP_OTP).with('Đã hết hạn mã đổi mật khẩu này, vui lòng yêu cầu mã mới.');
    }
    if (foundUser.code_reset_password != dataValidated.code) {
      throw new AppError(ApiCodeResponse.OTP_FAIL).with('Mã xác minh không chính xác, vui lòng thử lại.');
    }

    const new_token: string = foundUser.generateToken();
    await foundUser.update({
      password: dataValidated.password,
      code_reset_password: null,
      expired_reset_password: null,
      app_token: new_token,
      update_at: new Date(),
    });
    return withSuccess({ token: new_token });
  }

  @Post('/register')
  public async registerFormData(@Request() request: express.Request): Promise<AppTypes.SuccessResponseModel<any>> {
    const { isMobile } = detectedDeviceMiddleware(request);
    await uploadMiddleware.handleSingleFile(request, 'profile_picture', MEDIA_TYPE.IMAGE);
    const { key: path } = (request.file as any) || {};
    const bodyData = await this.userService.UserSchema.concat(this.userService.PasswordSchema)
      .concat(this.userService.CustomerReferalSchema)
      // .concat(this.userService.CustomerFollowShopSchema)
      .validateAsync({
        ...request.body,
        df_type_user_id: ROLE.CUSTOMER,
      });
    console.log('bodyData', bodyData);
    delete bodyData['shop_id'];
    const shopId: any = await this.userService.UserSchema.concat(this.userService.PasswordSchema)
      .concat(this.userService.CustomerReferalSchema)
      // .concat(this.userService.CustomerFollowShopSchema)
      .validateAsync({
        ...request.body,
        df_type_user_id: ROLE.CUSTOMER,
      });
    console.log('shopId', shopId);
    let referalCustomerId = null;
    let referalCurrentPoint = null;
    if (bodyData.code) {
      const foundReferalUser = await User.findOne({
        where: {
          user_name: bodyData.code,
          // df_type_user_id: {
          //   [Op.or]: [ROLE.CUSTOMER, ROLE.SHOP,],
          // },
          is_active: IS_ACTIVE.ACTIVE,
        },
      });
      if (!foundReferalUser) {
        throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Mã giới thiệu không hợp lệ');
      }
      referalCustomerId = foundReferalUser.id;
      referalCurrentPoint = foundReferalUser.point;
    }
    const user = await this._findOne({
      where: {
        [Op.or]: [{ user_name: bodyData.phone }, { email: bodyData.email }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (user) {
      if (user?.dataValues?.phone === bodyData.phone)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Số điện thoại này đã được sử dụng.');
      if (
        user?.dataValues?.email === bodyData.email &&
        bodyData.email != null &&
        bodyData.email != undefined &&
        bodyData.email != ''
      )
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Email này đã được sử dụng.');
    }
    if (referalCustomerId) {
      bodyData.referal_customer_id = referalCustomerId;
      // add point to referaler
    }

    const dataTransaction = await sequelize.transaction(async (transaction) => {
      const createdUser = await this._create(
        {
          ...bodyData,
          user_name: bodyData.phone,
          profile_picture_url: path,
          df_type_user_id: ROLE.CUSTOMER,
        },
        { transaction },
      );
      // công điểm cho người đăng kí
      await this.pointService.registerSuccess(createdUser.id, transaction);
      // // cộng điểm cho người giới thiệu
      // if (referalCustomerId) await this.pointService.referralCode(referalCustomerId, referalCurrentPoint, transaction);
      // bắn thông báo
      await this.createNotification(DF_NOTIFICATION.REGISTER_USER, createdUser.id, transaction);

      if (referalCustomerId) {
        const pointOld = await Config.findOne({
          where: { type: CONFIG_TYPE.REGISTER, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
        });
        const configRefCode = await Config.findOne({
          where: { type: CONFIG_TYPE.REFERRAL_CODE, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
        });
        // bắn noti đến người vừa nhập mã gt thành công
        await this.createNotificationPoint(
          DF_NOTIFICATION.REFERRAL_CODE,
          configRefCode.value,
          createdUser.id,
          transaction,
        );

        await this.pointService.referralCode(createdUser.id, pointOld.value, transaction);
      }
      //follow shop

      if (_.isArray(shopId.shop_id) == true && shopId.shop_id.length > 0) {
        const promises = shopId.shop_id.map((item: number) =>
          Follow.create(
            {
              shop_id: item,
              user_id: createdUser.id,
            },
            { transaction },
          ),
        );
        await Promise.all(promises);
      } else {
        const shopIdItem = parseInt(shopId.shop_id);
        if (shopId.shop_id) {
          console.log('shopIdItem', shopIdItem);
          await Follow.create(
            {
              shop_id: shopIdItem,
              user_id: createdUser.id,
            },
            { transaction },
          );
        }
      }
      //
      return createdUser;
    });
    // login
    const foundUser = await this._findOne({
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },

      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: ['id', 'name', 'phone', 'livestream_enable', 'pancake_shop_id', 'status', 'profile_picture_url'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { id: dataTransaction.id, is_active: IS_ACTIVE.ACTIVE },
    });

    const token: string = foundUser.generateToken();
    // Đang đăng nhập bằng app mobile
    if (isMobile) {
      await User.update(
        { app_token: token, device_id: bodyData.device_id ? bodyData.device_id : null, last_login_date: new Date() },
        { where: { id: foundUser.id, is_active: IS_ACTIVE.ACTIVE } },
      );
      foundUser.setDataValue('app_token', undefined);
      foundUser.setDataValue('password', null);
      foundUser.setDataValue('token', token);
    }
    // Đăng đăng nhập trên web
    else {
      await User.update(
        { token: token, last_login_date: new Date() },
        { where: { id: foundUser.id, is_active: IS_ACTIVE.ACTIVE } },
      );
      foundUser.setDataValue('app_token', undefined);
      foundUser.setDataValue('password', null);
      foundUser.setDataValue('token', token);
    }
    return withSuccess(foundUser);
  }

  // create noti
  public async createNotificationPoint(id, point, id_user, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn được tặng ' + point + ' điểm khi nhập mã giới thiệu';
    const df_notification_id = notiType.id;
    const data = {};
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  // create notification point
  public async createNotification(id, id_user, transaction) {
    const configPoint = await Config.findOne(
      {
        where: { type: CONFIG_TYPE.REGISTER, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
      },
      { transaction },
    );
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn đã được cộng ' + configPoint.value + ' điểm khi đăng kí thành công tài khoản';
    const df_notification_id = notiType.id;
    const data = { user_id: id_user };
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }
}
