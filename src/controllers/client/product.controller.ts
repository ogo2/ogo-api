import { Security, Get, Post, Query, Route, Tags, Request } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { ProductService } from '@services/internal/products.service';
import {
  IS_ACTIVE,
  FILTER_TYPE,
  PRODUCT_STATUS,
  STOKE_STATUS,
  IS_DEFAULT,
  MEDIA_TYPE,
  ORDER_STATUS,
  SHOP_STATUS,
  BUY_TYPE,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '..';
import { AppTypes, AuthTypes } from 'types';
import { Utils } from '@utils/Utils';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Category,
  ProductMedia,
  ProductCustomAttribute,
  ProductCustomAttributeOption,
  Wishlist,
  Product,
  Cart,
  Shop,
  Stock,
  ProductStock,
  ProductPrice,
} = db.default;
const { Op } = Sequelize;

/**
 * Danh mục sản phẩm
 */

@Route('client/product')
@Tags('client/product')
export class ClientProductController extends ApplicationController {
  private productService: ProductService;
  constructor() {
    super('Product');
    this.productService = new ProductService();
  }

  /**
   * @summary Kiểm tra sản phẩm tồn tại hay không
   */
  @Get('/{id}/check-product')
  public async checkProduct(@Request() request, id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const checkProduct = await Product.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, status: PRODUCT_STATUS.AVAILABLE, id: id },
      include: {
        model: Shop,
        where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
      },
    });
    // return withSuccess(checkProduct);
    let data;
    if (checkProduct == null) data = false;
    if (checkProduct != null) data = true;
    // const detailProduct = await Product.findById(id, images_id, 0, request, type);
    return withSuccess(data);
  }

  /**
   * @summary thêm vào ds yêu thích
   */
  @Security('jwt')
  @Get('/wishlist')
  public async getWishlistProduct(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    let orders = [['id', 'DESC']];
    const baseUrl = Utils.getBaseServer(request);
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const { rows, count } = await Product.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE },
      attributes: [
        'id',
        'name',
        'category_id',
        // [sequelize.literal('IFNULL(sum(`OrderItems`.amount ),0)'), 'quantity_items'],
        [
          sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'min_max_price',
        ],
        // [
        //   sequelize.literal(`
        //       IFNULL((SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url
        //       FROM product_media
        //       WHERE
        //       product_media.is_active = ${IS_ACTIVE.ACTIVE}
        //       and
        //       product_media.product_id = Product.id
        //       limit 1
        //       ),null)`),
        //   'media_url',
        // ],
        [
          sequelize.literal(`
                    IFNULL((SELECT MIN(stock_id) as stock_id
                    FROM product_stock
                    WHERE
                    product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_stock.product_id = Product.id
                    ),0)`),
          'stock_id',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT SUM(amount) as amount
                    FROM order_item
                    WHERE
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                    and order_item.product_id = Product.id
                    ),0)`),
          'quantity_items',
        ],
      ],
      include: [
        {
          model: Wishlist,
          where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId },
          atrribute: [],
        },
        {
          model: ProductMedia,
          required: false,
          attributes: ['id', [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_url'], 'type'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            product_custom_attribute_option_id: null,
            type: { [Op.ne]: MEDIA_TYPE.VIDEO },
          },
          order: [['Products->ProductMedia.type', 'asc']],
        },
      ],
      order: orders,
      group: [['Product.id', 'ProductMedia.id']],
      // subQuery: false,
      limit,
      offset,
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
  }

  /**
   * @summary Danh SÁCH SẢN PHẨM
   */
  @Get('/list-product')
  public async listProduct(
    @Request() request: any,
    @Query('page') pageValue = 1,
    @Query('search') search?: string,
    @Query('category_id') category_id?: number,
    @Query('children_category_id') children_category_id?: number,
    @Query('time_type') time_type?: number,
    @Query('price_type') price_type?: number,
    @Query('buy_type') buy_type?: number,
    @Query('shop_id') shop_id?: number,
    @Query('star_review') star_review?: number,
    @Query('list_product_id') list_product_id?: number[],
    // @Query('limit') limitValue = CONFIG.PAGING_LIMIT,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId;
    if (tokens) {
      const user = await User.findOne({
        where: { [Op.or]: [{ token: tokens }, { app_token: tokens }], is_active: IS_ACTIVE.ACTIVE },
      });
      userId = user ? user.id : null;
    }
    // return withSuccess(list_product_id);
    const loginUser = request.user?.data as AuthTypes.AuthorizedUser;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const baseUrl = Utils.getBaseServer(request);
    let orders = [['id', 'DESC']];
    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == undefined) {
      orders = [
        ['create_at', 'DESC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == undefined) {
      orders = [
        ['create_at', 'ASC'],
        // ['price', 'DESC'],
      ];
    }

    if (price_type == FILTER_TYPE.PRICE_MAX_MIN && time_type == undefined) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        // ['price', 'DESC'],
      ];
    }
    if (price_type == FILTER_TYPE.PRICE_MIN_MAX && time_type == undefined) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        // ['price', 'DESC'],
      ];
    }

    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == FILTER_TYPE.PRICE_MAX_MIN) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        ['create_at', 'ASC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == FILTER_TYPE.PRICE_MIN_MAX) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        ['create_at', 'ASC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_OLE_NEW && price_type == FILTER_TYPE.PRICE_MAX_MIN) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'DESC',
        ],
        ['create_at', 'DESC'],
        // ['price', 'DESC'],
      ];
    }
    if (time_type == FILTER_TYPE.TIME_NEW_OLD && price_type == FILTER_TYPE.PRICE_MIN_MAX) {
      orders = [
        [
          sequelize.literal(`(
                SELECT MAX(product_price.price)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'ASC',
        ],
        ['create_at', 'DESC'],
        // ['price', 'ASC'],
      ];
    }
    if (buy_type == BUY_TYPE.SELL) {
      orders = [
        [
          sequelize.literal(`
            IFNULL((SELECT SUM(order_item.amount) as amount
            FROM order_item
            join \`order\` as od on od.id = order_item.order_id
            WHERE
            order_item.is_active = ${IS_ACTIVE.ACTIVE}
            and order_item.product_id = Product.id
            and od.status = ${ORDER_STATUS.SUCCCESS}
            ),0)`),
          'DESC',
        ],

        // ['price', 'ASC'],
      ];
    }
    if (buy_type == BUY_TYPE.NOTSELL) {
      orders = [
        [
          sequelize.literal(`
            IFNULL((SELECT SUM(order_item.amount) as amount
            FROM order_item
            join \`order\` as od on od.id = order_item.order_id
            WHERE
            order_item.is_active = ${IS_ACTIVE.ACTIVE}
            and order_item.product_id = Product.id
            and od.status = ${ORDER_STATUS.SUCCCESS}
            ),0)`),
          'ASC',
        ],

        // ['price', 'ASC'],
      ];
    }
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      category_id: children_category_id != 0 ? children_category_id : { [Op.ne]: null },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
        },
      }),
      shop_id: shop_id != 0 && shop_id != undefined ? shop_id : { [Op.ne]: null },
      id: list_product_id ? { [Op.notIn]: list_product_id } : { [Op.ne]: null },
      status: PRODUCT_STATUS.AVAILABLE,
    };
    if (star_review == 1) {
      whereOption.star = { [Op.and]: [{ [Op.gte]: 1 }, { [Op.lt]: 2 }] };
    }
    if (star_review == 2) {
      whereOption.star = { [Op.and]: [{ [Op.gte]: 2 }, { [Op.lt]: 3 }] };
    }
    if (star_review == 3) {
      whereOption.star = { [Op.and]: [{ [Op.gte]: 3 }, { [Op.lt]: 4 }] };
    }
    if (star_review == 4) {
      whereOption.star = { [Op.and]: [{ [Op.gte]: 4 }, { [Op.lt]: 5 }] };
    }
    if (star_review == 5) {
      whereOption.star = 5;
    }

    const { rows, count } = await Product.findAndCountAll({
      // row: true,
      // subQuery: false,
      attributes: [
        'id',
        'name',
        'category_id',
        'star',
        'create_at',
        [sequelize.literal('`Category`.`name`'), 'category_name'],
        [
          sequelize.literal(`(
                SELECT MIN(product_price.stock_id)
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'stock_id',
        ],
        [
          sequelize.literal(`IFNULL(
                (SELECT MAX(wishlist.is_active)
                FROM wishlist AS wishlist
                WHERE
                wishlist.is_active = ${IS_ACTIVE.ACTIVE}
                and wishlist.product_id = Product.id
                and wishlist.user_id ${userId ? `=${userId}` : 'is not null'}
                ),0)`),
          'check_like',
        ],
        [
          sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = Product.id
                )`),
          'min_max_price',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT SUM(order_item.amount) as amount
                    FROM order_item
                    join \`order\` as od on od.id = order_item.order_id
                    WHERE
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                    and order_item.product_id = Product.id
                    and od.status = ${ORDER_STATUS.SUCCCESS}
                    ),0)`),
          'quantity_items',
        ],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              and product_price.status = ${IS_ACTIVE.ACTIVE}
            )`),
          'amount',
        ],
      ],
      where: whereOption,
      include: [
        {
          model: Shop,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: Category,
          // required: false,
          attributes: ['id', 'name'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
            status: IS_DEFAULT.ACTIVE,
          },
        },
        {
          model: ProductMedia,
          required: false,
          attributes: ['id', [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_url'], 'type'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            product_custom_attribute_option_id: null,
            type: { [Op.ne]: MEDIA_TYPE.VIDEO },
          },
          order: [['Products->ProductMedia.type', 'asc']],
        },
        // {
        //   attributes: [
        //     'id',
        //     'product_id',
        //     'percent',
        //     'stock_id',
        //     'price',
        //     'status',
        //   ],
        //   model: ProductPrice,
        //   where: {
        //     is_active: IS_ACTIVE.ACTIVE,
        //   },
        //   // limit: 1,
        // },
        // {
        //   model: OrderItem,
        //   required: false,
        //   attributes: ['id', 'amount'],
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        //   include: {
        //     model: Order,
        //     required: false,
        //     attributes: ['id'],
        //     where: { is_active: IS_ACTIVE.ACTIVE },
        //   },
        // },
      ],
      order: orders,
      group: [['Product.id', 'ProductMedia.id']],
      // subQuery: false,
      limit,
      offset,
      logging: console.log,
    });
    return withPagingSuccess(rows, { page: 1, totalItemCount: rows.length, limit });
    // return withSuccess(1);
  }

  /**
   * @summary Chi tiết sản phẩm
   */
  // @Security('jwt', ['agent'])
  @Get('/{product_id}')
  public async detailProduct(
    product_id,
    @Request() request: any,
    // @Query('limit') limitValue = CONFIG.PAGING_LIMIT,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    let userId = 0;
    const tokens = request.headers.token;
    const baseUrl = Utils.getBaseServer(request);
    if (tokens) {
      const loginUser = await User.findOne({ where: { [Op.or]: [{ token: tokens }, { app_token: tokens }] } });
      // return withSuccess(loginUser);
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      // const agent = await Agent.findOne({
      //   where: { user_id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
      // });
      userId = loginUser ? loginUser.id : null;
    }
    // return withSuccess(product_id);
    let productDetail;
    let listStock;
    let totalCard;
    await Promise.all([
      (totalCard = await Cart.findAll({
        where: { is_active: IS_ACTIVE.ACTIVE, user_id: userId },
        attributes: [[sequelize.fn('SUM', sequelize.col('amount')), 'total_card']],
      })),
      (productDetail = await this.productService.productDetail(product_id, request)),
    ]);
    if (productDetail == null) throw ApiCodeResponse.PRODUCT_NOT_EXISTS;
    // return withSuccess(1);
    const { rows, count } = await Product.findAndCountAll({
      row: true,
      attributes: [
        'id',
        'name',
        'category_id',
        // [sequelize.literal('`ProductPrices`.`stock_id`'), 'stock_id'],
        [
          sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = ${product_id}
                )`),
          'min_max_price',
        ],
        [
          sequelize.literal(`
                IFNULL((SELECT is_active
                FROM wishlist
                WHERE
                is_active = ${IS_ACTIVE.ACTIVE}
                and product_id = ${product_id}
                order by id desc
                limit 1
                ),0)`),
          'check_like',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT MIN(product_price.stock_id)
                    FROM product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = ${product_id}
                    ),0)`),
          'stock_id',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT SUM(order_item.amount) as amount
                    FROM order_item
                    join \`order\` as od on od.id = order_item.order_id
                    WHERE
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                    and order_item.product_id = ${product_id}
                    and od.status = ${ORDER_STATUS.SUCCCESS}
                    ),0)`),
          'quantity_items',
        ],
        [
          sequelize.literal(`
                    IFNULL((SELECT COUNT(id) as amount
                    FROM review
                    WHERE
                    review.is_active = ${IS_ACTIVE.ACTIVE}
                    and review.product_id = ${product_id}
                    ),0)`),
          'quantity_review',
        ],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              and product_price.status = ${IS_ACTIVE.ACTIVE}
            )`),
          'amount',
        ],
      ],
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        category_id: productDetail.category_id,
        status: PRODUCT_STATUS.AVAILABLE,
        id: { [Op.ne]: productDetail.id },
      },
      include: [
        {
          model: Shop,
          attributes: ['id', 'name'],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
          // required: false,
        },
        {
          model: ProductMedia,
          required: false,
          attributes: ['id', [sequelize.fn('CONCAT', baseUrl, '/', sequelize.col('media_url')), 'media_url'], 'type'],
          where: { is_active: IS_ACTIVE.ACTIVE, product_custom_attribute_option_id: null },
          order: [['Products->ProductMedia.type', 'asc']],
        },
        // {
        //   model: OrderItem,
        //   required: false,
        //   attributes: ['id', 'amount'],
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        //   include: {
        //     model: Order,
        //     required: false,
        //     attributes: ['id'],
        //     where: { is_active: IS_ACTIVE.ACTIVE },
        //   },
        // },
      ],
      // group: [['ProductMedia.id']],
      order: [['id', 'desc']],
      limit,
      offset,
      logging: console.log,
    });
    return withPagingSuccess(
      { rows, productDetail },
      { page, limit, totalItemCount: count instanceof Array ? count.length : count },
    );
  }

  /**
   * @summary lấy thuộc tính custom
   */
  // @Security('jwt', ['agent'])
  @Get('{id}/attributeCustom')
  public async attributeCustom(
    id: number,
    @Request() request,
    @Query('stock_id') stock_id?: number,
    @Query('custom_attribute_option_id_1') custom_attribute_option_id_1?: number,
    @Query('custom_attribute_option_id_2') custom_attribute_option_id_2?: number,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    // return withSuccess(stock_id);
    const { offset, limit, page } = handlePagingMiddleware(request);

    // check phân loại
    const checkCustomoption1 = await ProductPrice.findAll({
      where: { product_id: id, is_active: IS_ACTIVE.ACTIVE, custom_attribute_option_id_1: { [Op.ne]: null } },
    });
    const checkCustomoption2 = await ProductPrice.findAll({
      where: { product_id: id, is_active: IS_ACTIVE.ACTIVE, custom_attribute_option_id_2: { [Op.ne]: null } },
    });
    let op1 = 0;
    if (checkCustomoption1.length == 0) {
      op1 = 1;
    }
    let op2 = 0;
    if (checkCustomoption2.length == 0) {
      op2 = 1;
    }
    let image_id = 0;
    const image = await ProductMedia.findOne({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        product_id: id,
        product_custom_attribute_option_id: { [Op.ne]: null },
        type: MEDIA_TYPE.IMAGE,
      },
    });
    if (custom_attribute_option_id_1 == null) {
      image_id = image ? image.product_custom_attribute_option_id : 0;
    }
    if (custom_attribute_option_id_1) {
      image_id = image ? custom_attribute_option_id_1 : 0;
    }
    // return withSuccess(image_id);
    const baseUrl = Utils.getBaseServer(request);
    const productPriceCustom = await Product.findOne({
      attributes: [
        'id',
        'name',
        [
          sequelize.literal(`
                IFNULL((SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and
                product_media.product_custom_attribute_option_id = ${image_id}
                limit 1
                ),(SELECT CONCAT( "${baseUrl}", '/',product_media.media_url ) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                AND
                product_media.product_id = ${id}
                and
                product_media.product_custom_attribute_option_id is null
                limit 1
                ))`),
          'media_url',
        ],
        [
          sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = ${id}
                )`),
          'min_max_price',
        ],
        [
          sequelize.literal(`
                (select CONCAT("[", group_concat(attributes.status separator ', '), "]") as arr_status
                from (
                SELECT CONCAT(product_price.status) as status
                FROM product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and
                product_price.custom_attribute_option_id_1 ${
                  custom_attribute_option_id_1 ? `= ${custom_attribute_option_id_1}` : '= 0'
                }
                and product_price.stock_id = ${stock_id}
                and product_price.product_id = ${id}
                ) as attributes ) `),
          'arr_status_option_2',
        ],
        [
          sequelize.literal(`
                (select CONCAT("[", group_concat(attributes.status separator ', '), "]") as arr_status
            from (
                SELECT CONCAT(product_price.status) as status
                FROM product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and
                product_price.custom_attribute_option_id_2 ${
                  custom_attribute_option_id_2 ? `= ${custom_attribute_option_id_2}` : op2 == 1 ? 'is null' : '= 0'
                }
                and product_price.product_id = ${id}
                and product_price.stock_id = ${stock_id}
                ) as attributes ) `),
          'arr_status_option_1',
        ],
        // [
        //   sequelize.literal(`(
        //   select group_concat(attributes.attribute_media separator ', ') as attribute_media
        //     from (
        //   select concat(COUNT(pro_option.id), " " ,group_concat(DISTINCT pro_att.name separator ', ')) as attribute_media
        //   FROM product_custom_attribute as pro_att
        //   join product_custom_attribute_option pro_option on pro_att.id = pro_option.product_custom_attribute_id
        //   where pro_att.product_id = ${product_id}
        //   and pro_att.is_active = ${IS_ACTIVE.ACTIVE}
        //   group by  pro_att.name) as attributes
        //   )`),
        //   'attribute_custom',
        // ],
      ],
      where: { is_active: IS_ACTIVE.ACTIVE, id: id },
      include: [
        {
          attributes: [
            'id',
            'product_id',
            'percent',
            'stock_id',
            'price',
            'amount',
            'custom_attribute_option_id_1',
            'custom_attribute_option_id_2',
            'status',
          ],
          model: ProductPrice,
          required: false,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            stock_id: stock_id ? stock_id : { [Op.ne]: null },
            custom_attribute_option_id_1: custom_attribute_option_id_1
              ? custom_attribute_option_id_1
              : op1 == 1
              ? null
              : { [Op.ne]: null },
            custom_attribute_option_id_2: custom_attribute_option_id_2
              ? custom_attribute_option_id_2
              : op2 == 1
              ? null
              : { [Op.ne]: null },
          },
        },
        {
          model: ProductCustomAttribute,
          required: false,
          attributes: ['name', 'display_order', 'id'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: ProductCustomAttributeOption,
            where: { is_active: IS_ACTIVE.ACTIVE },
            attributes: ['name', 'id'],
            // include: {
            //   model: ProductMedia,
            //   required: false,
            //   where: { is_active: IS_ACTIVE.ACTIVE },
            //   attributes: ['media_url', 'id'],
            // },
            // required: false,
          },
        },
      ],
    });
    const { rows, count } = await Stock.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE },
      attributes: ['id', 'name'],
      include: [
        {
          model: ProductStock,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, product_id: id, status: STOKE_STATUS.ACTIVE },
        },
      ],
      limit,
      offset,
    });
    return withPagingSuccess({ rows, productPriceCustom }, { page, limit, totalItemCount: count });
    // const listStock = await ProductStock
    return withSuccess(productPriceCustom);
  }

  /**
   * @summary thêm vào ds yêu thích
   */
  @Security('jwt')
  @Post('/wishlist/{product_id}')
  public async createWishlistProduct(
    @Request() request: any,
    product_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    // Kiểm tra product
    const pro = await Product.findOne({ where: { id: product_id, is_active: IS_ACTIVE.ACTIVE } });
    if (!pro) throw ApiCodeResponse.PRODUCT_NOT_EXISTS;
    // kiểm tra sản phẩm đã có trong ds like chưa
    const wishlist = await Wishlist.findOne({
      where: { product_id: product_id, is_active: IS_ACTIVE.ACTIVE, user_id: userId },
    });
    // return withSuccess(wishlist);
    if (wishlist != null) {
      await Wishlist.update(
        { is_active: IS_ACTIVE.INACTIVE },
        { where: { product_id: product_id, is_active: IS_ACTIVE.ACTIVE, user_id: userId } },
      );
    } else {
      await Wishlist.create({ user_id: userId, product_id: product_id });
    }
    return withSuccess({});
  }
}
