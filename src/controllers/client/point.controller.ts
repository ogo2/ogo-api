import { IS_ACTIVE, USER_STATUS } from '@utils/constants';
import { ApplicationController } from '../';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Request, Get, Query, Route, Tags, Security } from 'tsoa';
import { withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes } from 'types';

const db = require('@models');
const { sequelize, Sequelize, User, DFTypeTransactionPoint, PointTransactionHistory } = db.default;
const { Op } = Sequelize;

@Route('client/point')
@Tags('client/point')
export class ClientPointController extends ApplicationController {
  constructor() {
    super('PointTransactionHistory');
  }

  /**
   * @summary list gift (no security)
   */
  @Security('jwt')
  @Get('/history-point')
  public async getListGift(
    @Request() request: AppTypes.RequestAuth,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      user_id: loginUser.id,
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };

    const foundUser = await User.findOne({
      where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE, status: USER_STATUS.ACTIVE },
    });

    const { count, rows } = await PointTransactionHistory.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [sequelize.literal('`DFTypeTransactionPoint`.`id`'), 'type_transaction_id'],
          [sequelize.literal('`DFTypeTransactionPoint`.`name`'), 'type_transaction_name'],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeTransactionPoint,
          required: true,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
    });
    const res = {
      user_point: foundUser.point,
      history: rows,
    };
    return withPagingSuccess(res, { page, limit, totalItemCount: count });
  }
}
