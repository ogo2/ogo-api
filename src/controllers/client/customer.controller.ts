import {
  IS_ACTIVE,
  ROLE,
  CONFIG_TYPE,
  TOPIC_STATUS,
  DF_NOTIFICATION,
  CONFIG_STATUS,
  STATUS_REFERAL,
  TOPIC_TYPE,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';

import Joi from '@utils/JoiValidate';
import { NotificationService } from '@services/internal/notification.service';
import * as _ from 'lodash';
import { Body, Get, Request, Security, Put, Route, Tags } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';

import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { PointService } from '@services/internal/point.service';
import { AppTypes } from 'types';

const db = require('@models');

const { sequelize, Sequelize, Customer, User, Config, Topic, UserIgnoreTopic, PurchasedGift, Gift } = db.default;
const { Op } = Sequelize;

@Route('client/customer')
@Tags('client/customer')
export class ClientCustomerController extends ApplicationController {
  private pointService: PointService;
  private notificationService: NotificationService;
  constructor() {
    super('Customer');
    this.pointService = new PointService();
    this.notificationService = new NotificationService();
  }

  /**
   * @summary Nhập mã gt
   */
  @Security('jwt')
  @Put('/referal')
  public async updateReferalCustomer(
    @Request() request: any,
    @Body()
    body: {
      phone?: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    const userId = loggedInUser ? loggedInUser.id : null;
    // return withSuccess(userId);
    if (!userId) throw ApiCodeResponse.UNAUTHORIZED;
    const schema = Joi.object({
      phone: Joi.number().empty(['', null, 0, '0', '']),
    });
    const detailUser = await User.findOne({ where: { id: userId } });
    if (detailUser == null) throw ApiCodeResponse.UNAUTHORIZED;
    const { phone } = await schema.validateAsync(body);

    // Check số điện thoại có tồn tại trong hệ thống hay k
    const checkPhone = await User.findOne({
      where: { phone: phone, is_active: IS_ACTIVE.ACTIVE, df_type_user_id: ROLE.CUSTOMER },
    });
    if (checkPhone == null) throw ApiCodeResponse.REFERAL_NOT_EXITS;
    // Check xem tk đã nhập mã gt hay chưa
    const checkReferal = await User.findOne({
      where: { referal_customer_id: { [Op.ne]: null }, is_active: IS_ACTIVE.ACTIVE, id: userId },
    });
    if (checkReferal != null) throw ApiCodeResponse.REFERAL_NOT_FOUND;
    // lấy điểm trong config
    const pointConfig = await Config.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, type: CONFIG_TYPE.REFERRAL_APP },
    });
    // return withSuccess(pointConfig);

    const updateUserAddress = await sequelize.transaction(async (transaction) => {
      if (phone) {
        await User.update(
          {
            referal_customer_id: checkPhone.id,
            status_referal: STATUS_REFERAL.SUCCESS,
            // point: detailUser.point + pointConfig.value,
          },
          { where: { is_active: IS_ACTIVE.ACTIVE, id: userId } },
        );
        await this.pointService.referralCode(userId, detailUser.point, transaction);
        // // update điểm cho ng NHẬP MÃ GT
        // await User.update(
        //   { point: checkPhone.point + pointConfig.value },
        //   { where: { id: userId, is_active: IS_ACTIVE.ACTIVE, df_type_user_id: ROLE.CUSTOMER } },
        // );

        const configRefCode = await Config.findOne({
          where: { type: CONFIG_TYPE.REFERRAL_CODE, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
        });
        // bắn noti đến người vừa nhập mã gt thành công
        await this.createNotificationPoint(DF_NOTIFICATION.REFERRAL_CODE, configRefCode.value, userId, transaction);
      }
    });

    return withSuccess({});
  }

  // create noti
  public async createNotificationPoint(id, point, id_user, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn được tặng ' + point + ' điểm khi nhập mã giới thiệu';
    const df_notification_id = notiType.id;
    const data = {};
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  /**
   * @summary Danh sách danh mục quan tâm:
   */
  @Security('jwt')
  @Get('/topic')
  public async getListTopic(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.SHOP_NOT_EXIST;

    const { rows, count } = await Topic.findAndCountAll({
      attributes: [
        'id',
        'name',
        'description',
        [sequelize.literal('IFNULL((`UserIgnoreTopics`.status ),0)'), 'check_topic'],
      ],
      where: { is_active: IS_ACTIVE.ACTIVE, status: TOPIC_STATUS.ACTIVE, type_topic: TOPIC_TYPE.NORMAL },
      include: {
        model: UserIgnoreTopic,
        required: false,
        where: { user_id: userId },
      },
      order: [["id", "desc"]]
    });
    return withPagingSuccess({ rows }, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary Quan tâm danh mục
   */
  @Security('jwt')
  @Put('/{topic_id}/topic')
  public async careTopic(@Request() request: any, topic_id?: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    // check xem đã bỏ quan tâm danh mục hay chưa
    const checkIgnoreTopic = await UserIgnoreTopic.findOne({ where: { user_id: userId, topic_id: topic_id } });
    // return withSuccess(checkIgnoreTopic);
    if (checkIgnoreTopic == null) {
      await UserIgnoreTopic.create({ topic_id: topic_id, user_id: userId });
    }
    if (checkIgnoreTopic != null) {
      if (checkIgnoreTopic.status == TOPIC_STATUS.ACTIVE) {
        await UserIgnoreTopic.update(
          { status: TOPIC_STATUS.INACTIVE },
          { where: { topic_id: topic_id, user_id: userId } },
        );
      }
      if (checkIgnoreTopic.status == TOPIC_STATUS.INACTIVE) {
        await UserIgnoreTopic.update(
          { status: TOPIC_STATUS.ACTIVE },
          { where: { topic_id: topic_id, user_id: userId } },
        );
      }
    }
    // if (body.status == 0) {

    // }
    return this.getListTopic(request);
  }

  /**
   * @summary Danh sách quà tặng của bạn
   */
  @Security('jwt')
  @Get('/{status}/list-gift')
  public async getListGift(@Request() request: any, status?: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    // return withSuccess(loggedInUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    const userId = loggedInUser ? loggedInUser.id : null;
    if (!userId) throw ApiCodeResponse.SHOP_NOT_EXIST;

    const { rows, count } = await PurchasedGift.findAndCountAll({
      attributes: [
        'id',
        'gift_id',
        'user_id',
        [sequelize.literal('`Gift`.name'), 'gift_name'],
        [sequelize.literal('`Gift`.price'), 'gift_price'],
        [sequelize.literal('`Gift`.icon_url'), 'icon_url'],
      ],
      where: { is_active: IS_ACTIVE.ACTIVE, status: status },
      include: {
        model: Gift,
        attributes: [],
        where: { is_active: IS_ACTIVE.ACTIVE },
      },
    });
    return withPagingSuccess({ rows }, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }
}
