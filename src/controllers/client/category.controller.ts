import { Get, Query, Route, Tags, Request } from 'tsoa';
import { withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { ApplicationController } from '../';
import { AppTypes } from 'types';

const db = require('@models');
const { sequelize, Sequelize, User, Category } = db.default;
const { Op } = Sequelize;

interface CategoryRequestModel {
  name: string;
  type: number;
  display_order: number;
  attribute_option: any;
  // value: string;
  // product_id: number;
  // category_attribute_option_id: number;
}

/**
 * Danh mục sản phẩm
 */
@Route('client/category')
@Tags('client/category')
export class ClientCategoryController extends ApplicationController {
  constructor() {
    super('Category');
  }
  /**
   * @summary Danh sách danh mục sản phẩm
   */
  @Get('/')
  public async listCategory(
    @Request() request: any,
    // @Query('page') pageValue = 1,
    // @Query('limit') limitValue = CONFIG.PAGING_LIMIT,
    @Query() parent_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(Object.values(CATEGORY_STATUS));
    const { offset, limit, page } = handlePagingMiddleware(request);
    let whereOption;
    whereOption = {
      parent_id: parent_id ? parent_id : null,
      is_active: IS_ACTIVE.ACTIVE,
    };
    // if (parent_id) {
    //   whereOption.parent_id = parent_id;
    // }
    const { count, rows } = await Category.findAndCountAll({
      where: whereOption,
      attributes: [
        'id',
        'name',
        'icon_url',
        'order',
        'status',
        'is_active',
        'create_at',
        [
          Sequelize.literal(`(
                        SELECT max(cat.id)
                        FROM category as cat 
                        WHERE cat.parent_id = Category.id
                            AND
                            cat.is_active = ${IS_ACTIVE.ACTIVE}
                    )`),
          'children_category_id',
        ],
      ],
      include: {
        model: Category,
        as: 'parent_category',
        attributes: [],
      },
      order: [['order', 'asc']],
      page,
      offset,
    });
    return withPagingSuccess(rows, { page, totalItemCount: count, limit });
  }
}
