import { Get, Query, Route, Tags, Request } from 'tsoa';
import { withPagingSuccess } from '../../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE, IS_DEFAULT, LIVESTREAM_STATUS, LIVESTREAM_ENABLE, SHOP_STATUS, IS_POSTED } from '@utils/constants';
import { ApplicationController } from '..';
import * as express from 'express';
import { Utils } from '@utils/Utils';
import { AppTypes } from 'types';
import { SocketUtils } from '../../websocket/SocketUtils';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Category,
  Post: Posts,
  Shop,
  PostMedia,
  Reaction,
  Topic,
  Livestream,
  LivestreamProduct,
  LivestreamLayout,
} = db.default;
const { Op } = Sequelize;
interface ProductMulterRequest extends express.Request {
  file: any;
}

/**
 * Danh mục sản phẩm
 */

@Route('client/home')
@Tags('client/home')
export class HomeController extends ApplicationController {
  constructor() {
    super('Home');
  }

  /**
   * @summary Màn hình trang chủ
   */
  @Get('/')
  public async detailProductAgent(
    @Request() request: any,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userID;
    if (tokens) {
      const user = await User.findOne({
        where: { [Op.or]: [{ token: tokens }, { app_token: tokens }], is_active: IS_ACTIVE.ACTIVE },
      });
      userID = user ? user.id : null;
    }
    const { offset, limit, page } = handlePagingMiddleware(request);

    const baseUrl = Utils.getBaseServer(request);
    let listCategory;
    let countCart;
    // let listPost;
    // let whereOption;
    // whereOption = {
    //   name: search ? { [Op.substring]: search } : { [Op.ne]: null },
    //   is_active: IS_ACTIVE.ACTIVE,
    // };
    const listLiveStream = await Livestream.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE, status: LIVESTREAM_STATUS.STREAMING },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    const listPromises = listLiveStream.rows
      .filter((livestream: any) => livestream.status === LIVESTREAM_STATUS.STREAMING)
      .map((livestream) => SocketUtils.countUserInAdapterSocketLivestreamChannel(request.io, livestream.id));
    const listResPromise = await Promise.all(listPromises);
    listResPromise.forEach((item: any) => {
      if (item) {
        const livestream = listLiveStream.rows.find((livestream) => livestream.id === item.livestream_id);
        livestream.setDataValue('count_subscribe', item.count_subscribe);
      }
    });
    const { rows, count } = await Posts.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE, is_posted: IS_POSTED.POSTED },
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              AND reaction.user_id ${userID ? `= ${userID}` : 'is null'}
              AND reaction.post_id = Post.id
              AND reaction.comment_id is null
            )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Reaction,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE, user_id: userID ? userID : { [Op.ne]: null } },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      // logging: console.log,
    });
    Promise.all([
      (listCategory = await Category.findAll({
        attributes: [
          'id',
          'name',
          'icon_url',
          'order',
          // [
          //   Sequelize.literal(`(
          //               SELECT max(cat.id)
          //               FROM category as cat
          //               WHERE cat.parent_id = Category.id
          //                   AND
          //                   cat.is_active = ${IS_ACTIVE.ACTIVE}
          //           )`),
          //   'children_category_id',
          // ],
        ],
        include: {
          model: Category,
          as: 'children_category',
          attributes: ['id', 'name', 'order'],
          where: { is_active: IS_ACTIVE.ACTIVE, status: IS_DEFAULT.ACTIVE },
          // order: [['order', 'asc']],
        },
        where: { is_active: IS_ACTIVE.ACTIVE, parent_id: null, status: IS_DEFAULT.ACTIVE },
        order: [
          ['order', 'asc'],
          [sequelize.col(`children_category.order`), 'asc'],
          ['create_at', 'desc'],
        ],
      })),
    ]);
    return withPagingSuccess({ rows, listCategory, listLiveStream }, { page, limit, totalItemCount: rows.length });
  }
}
