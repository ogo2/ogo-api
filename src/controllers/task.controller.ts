import { Security, Post, Route, Tags, Request, Body } from 'tsoa';

import { AppTypes } from 'types';
import { withIOSuccess, withSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import {
  IS_ACTIVE,
  POST_STATUS,
  ROLE,
  DF_NOTIFICATION,
  IS_POSTED,
  LIVESTREAM_ENABLE,
  LIVESTREAM_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { NotificationService } from '@services/internal/notification.service';
import { Credentials as GoogleCredentials } from 'google-auth-library';
import { LivestreamService } from '@services/internal/livestream.service';
import { logger } from '@utils/Logger';
import { LIVESTREAM_EVENT, withLiveStreamChannel } from '../websocket/constants';
const db = require('@models');
const { sequelize, Sequelize, Post: _Post, User, Shop, Livestream } = db.default;
const { Op } = Sequelize;

@Route('task')
@Tags('Tasks')
export class TaskController {
  private livestreamService: LivestreamService;
  private notificationService: NotificationService;
  constructor() {
    this.livestreamService = new LivestreamService();
    this.notificationService = new NotificationService();
  }
  /**
   *  @summary exec a task cron job post schedule post
   */
  @Security('server_session')
  @Post('/{post_id}/post')
  public async execTaskPost(post_id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundPost = await _Post.findOne({
      where: { id: post_id, is_active: IS_ACTIVE.ACTIVE, is_posted: IS_POSTED.NOT_POSTED },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    const listUser = await User.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        df_type_user_id:
          foundPost.dataValues.status == POST_STATUS.ALL ? { [Op.ne]: ROLE.ADMIN } : foundPost.dataValues.status,
      },
    });
    await sequelize.transaction(async (transaction) => {
      await foundPost.update({ is_posted: IS_POSTED.POSTED, update_at: new Date() });
      const notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.NEWS_POST);
      const notifications = listUser.map((projectData) => {
        const content = foundPost.dataValues.content.substr(0, 100);
        return {
          df_notification_id: notiType.id,
          content: content,
          data: { post_id: foundPost.id },
          user_id: projectData.id,
          title: notiType.title,
        };
      });
      await this.notificationService.createMultiNotification(notifications, transaction);
    });
    return withSuccess({});
  }

  /**
   *  @summary exec a task cron job stop livestream
   */
  @Security('server_session')
  @Post('/{livestream_id}/stop-livestream')
  public async execTaskStopLivestream(
    livestream_id: number,
    @Request() request: AppTypes.RequestIO,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundStreaming = await Livestream.findOne({
      where: {
        id: livestream_id,
        status: LIVESTREAM_STATUS.STREAMING,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: { exclude: ['create_by', 'update_by', 'delete_by', 'version'] },
      include: [
        {
          model: Shop,
          required: true,
          where: { livestream_enable: LIVESTREAM_ENABLE.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            exclude: ['pancake_shop_key', 'pancake_shop_id', 'create_by', 'update_by', 'delete_by', 'version'],
          },
        },
      ],
    });

    if (!foundStreaming) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Kênh livestrean không tồn tại, hoặc livestream này đã kết thúc.',
      );
    }
    const google_tokens = foundStreaming?.dataValues?.Shop?.dataValues?.google_tokens;
    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };

    const { resultStopLivestream } = await this.livestreamService.stopStreaming(foundStreaming, tokens);

    logger.error({ message: `stope live expired at ${new Date()}` });

    foundStreaming.setDataValue('resultStopLivestream', resultStopLivestream);
    foundStreaming.setDataValue('finish_at', new Date());
    // push socket
    const room = withLiveStreamChannel(livestream_id);
    request.io.to(room).emit(room, withIOSuccess(foundStreaming, LIVESTREAM_EVENT.SERVER_STOP_LIVESTREAM));

    return withSuccess(foundStreaming);
  }

  /**
   *  @summary exec a task cron job stop livestream disconnected
   */
  @Security('server_session')
  @Post('/{livestream_id}/stop-disconnected-livestream')
  public async execTaskStopLivestreamDisconnected(
    livestream_id: number,
    @Request() request: AppTypes.RequestIO,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundStreaming = await Livestream.findOne({
      where: {
        id: livestream_id,
        status: LIVESTREAM_STATUS.STREAMING,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: { exclude: ['create_by', 'update_by', 'delete_by', 'version'] },
      include: [
        {
          model: Shop,
          required: true,
          where: { livestream_enable: LIVESTREAM_ENABLE.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            exclude: ['pancake_shop_key', 'pancake_shop_id', 'create_by', 'update_by', 'delete_by', 'version'],
          },
        },
      ],
    });

    if (!foundStreaming) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Kênh livestrean không tồn tại, hoặc livestream này đã kết thúc.',
      );
    }
    const google_tokens = foundStreaming?.dataValues?.Shop?.dataValues?.google_tokens;
    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };
    logger.error({ message: `stop-disconnected-livestream at ${new Date()}` });
    const { resultStopLivestream } = await this.livestreamService.stopStreaming(foundStreaming, tokens);

    foundStreaming.setDataValue('resultStopLivestream', resultStopLivestream);

    // push socket
    // LivestreamAction(request.io).sendServerStopLivestream(
    //   livestream_id,
    //   foundStreaming,
    //   ApiCodeResponse.LIVESTREAM_DISCONECT,
    // );

    return withSuccess(foundStreaming);
  }

  /**
   *  @summary exec a task cron job stop livestream
   */
  @Security('server_session')
  @Post('/{livestream_id}/warning-livestream')
  public async execTaskWarningLivestream(
    livestream_id: number,
    @Body() body: { warning_type: number },
    @Request() request: AppTypes.RequestIO,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // push socket
    // LivestreamAction(request.io).sendServerWarningLivestream(livestream_id, body);

    const room = withLiveStreamChannel(livestream_id);
    request.io.to(room).emit(room, withIOSuccess(body, LIVESTREAM_EVENT.WARNING_EXPIRE_TIME));
    return withSuccess({});
  }
}
