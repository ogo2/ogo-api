import { Body, Request, Get, Query, Route, Tags } from 'tsoa';
import { withSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import * as _ from 'lodash';
import * as express from 'express';
import axios from 'axios';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
const { parse } = require('node-html-parser');

const db = require('@models');
const {
  sequelize,
  Sequelize,
  Topic,
  DFTypeUser,
  DFReaction,
  DFProvince,
  DFDistrict,
  DFWard,
  DFNotification,
  DFOrderStatusHistory,
  DFTypeGift,
  DFTypeTransactionPoint,
  PakageCategory,
} = db.default;
const { Op } = Sequelize;

export const GOONG_API_KEY = 'vRJDYbHTtcbxUd43P8AHf7dhCkx5PFMMtKtwufcl';
export const PLACE_TYPES = {
  PROVINCE: 'region',
  DISTRICT: 'locality',
  WARD: 'extended-address',
};

@Route('google-address')
@Tags('GoogleAddress')
export class GoogleAddressController {
  /**
   * @summary place
   */
  @Get('/place')
  public async updatePlaceGoogle(
    @Request() request: any,
    @Query() placeId?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const res = await axios.get(`https://rsapi.goong.io/Place/Detail?place_id=${placeId}&api_key=${GOONG_API_KEY}`);

    if (!res.data.result) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Dữ liệu không tồn tại');

    const { lat, lng } = res.data.result.geometry.location;
    const address = res.data.result.formatted_address;

    // return withSuccess({ lat, long: lng, address });
    // return this.responseService.withData({ lat, long: lng, address });
    return withSuccess({ lat, long: lng, address });
  }
  /**
   * @summary place
   */
  @Get('/place-auto')
  public async getPlaceAuto(
    @Request() request: any,
    @Query() address?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    let autoCompleteLink = `https://rsapi.goong.io/Place/AutoComplete?input=${encodeURI(
      address,
    )}&api_key=${GOONG_API_KEY}`;
    console.log(
      'http',
      `https://rsapi.goong.io/Place/AutoComplete?input=${encodeURI(address)}&api_key=${GOONG_API_KEY}`,
    );

    let res = await axios.get(autoCompleteLink);
    console.log('res', res);
    if (res.data.predictions.length > 0) return withSuccess(res.data);

    return withSuccess({});
  }
}
