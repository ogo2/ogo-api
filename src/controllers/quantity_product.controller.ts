import { Body, Get, Post, Put, Query, Route, Request, Delete, Tags, Security } from 'tsoa';
import * as _ from 'lodash';
import Joi from '../utils/JoiValidate';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { AppTypes } from 'types';

import { ApplicationController } from './';
import {
  IS_ACTIVE,
  ORDER_STATUS,
  MEDIA_TYPE,
  PANCAKE,
  PRODUCT_TYPE,
  ROLE,
  IS_POSTED,
  IS_DEFAULT,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ProductService } from '@services/internal/products.service';
import * as express from 'express';
import { Utils } from '@utils/Utils';
import { getAxiosInstance } from '@config/axiosInstance';
import { AxiosInstance } from 'axios';
const axios = require('axios');

const db = require('@models');

const {
  sequelize,
  Sequelize,
  ProductCustomAttribute,
  Product,
  ProductMedia,
  Category,
  ProductStock,
  Stock,
  User,
  Order,
  OrderItem,
  Shop,
  ProductPrice,
  ProductCustomAttributeOption,
  Post: Posts,
  PostMedia,
} = db.default;
const { Op } = Sequelize;
interface ProductMulterRequest extends express.Request {
  file: any;
}

@Route('product/quantity')
@Tags('product/quantity')
export class QuantityProductController extends ApplicationController {
  public apiInstance: AxiosInstance;
  private productService: ProductService;
  constructor() {
    super('Product');
    this.apiInstance = getAxiosInstance();
    this.productService = new ProductService();
  }

  /**
   * @summary import file excel sản phẩm (shop)
   */
  @Security('jwt', ['enterprise'])
  @Post('/up-file/product')
  public async upFileProduct(
    @Request() request,
    @Body() body: { status?: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let shopId;
    let userId;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      shopId = loginUser ? loginUser.shop_id : null;
      userId = loginUser ? loginUser.id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const schema = Joi.object({
      product_price: Joi.array()
        .items(
          Joi.object()
            .keys({
              amount: Joi.number(),
              variration_pancake_id: Joi.string().required(),
            })
            .unknown(true),
        )
        .required(),
    });

    const { product_price } = await schema.validateAsync(request.body);

    return withSuccess({ product_price });
  }
}
