import { IS_ACTIVE, MEDIA_TYPE, IS_READ, CONFIG, DF_NOTIFICATION, SHOP_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import * as _ from 'lodash';
import { Get, Request, Security, Post, Put, Query, Route, Tags } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes, MessageTypes } from 'types';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { MessageService } from '@services/internal/message.service';
import { MessageAction, UserAction, ShopAction } from '../websocket/action';
import { OnesignalService } from '@services/detributed/notifications/onesignal.service';
import { NotificationService } from '@services/internal/notification.service';
import { SocketUtils } from '../websocket/SocketUtils';
import { environment } from '@config/environment';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, Message, TopicMessage, User, Shop } = db.default;
const { Op } = Sequelize;

@Route('message')
@Tags('messages')
export class MessageClientController extends ApplicationController {
  private messageService: MessageService;
  private notificationService: NotificationService;
  private onesignalService: OnesignalService;
  constructor() {
    super('Message');
    this.messageService = new MessageService();
    this.notificationService = new NotificationService();
    this.onesignalService = new OnesignalService();
  }
  /**
   * @summary get list topic message (any)
   */
  @Security('jwt')
  @Get('/topic')
  public async getListTopicMessage(
    @Request() request: AppTypes.RequestIOAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOption = {
        ...whereOption,
        [Op.and]: [
          {
            ...(search && {
              [Op.or]: [
                { '$User.phone$': { [Op.like]: `%${search}%` } },
                { '$User.name$': { [Op.like]: `%${search}%` } },
                { '$Shop.phone$': { [Op.like]: `%${search}%` } },
                { '$Shop.name$': { [Op.like]: `%${search}%` } },
              ],
            }),
          },
          { [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }] },
        ],
      };
    } else {
      whereOption = {
        ...whereOption,
        user_id: userDecoded.id,
        ...(search && {
          [Op.or]: [
            { '$User.phone$': { [Op.like]: `%${search}%` } },
            { '$User.name$': { [Op.like]: `%${search}%` } },
            { '$Shop.phone$': { [Op.like]: `%${search}%` } },
            { '$Shop.name$': { [Op.like]: `%${search}%` } },
          ],
        }),
      };
    }
    const { count, rows } = await TopicMessage.findAndCountAll({
      where: whereOption,
      subQuery: false,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`message\`
              WHERE message.is_active = ${IS_ACTIVE.ACTIVE}
              AND message.is_read = ${IS_READ.NOT_READ}
              AND message.topic_message_id = TopicMessage.id
              AND (message.user_id != ${userDecoded.id} OR message.shop_id != ${userDecoded.shop_id || null})
              )`),
            'count_message_not_read',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },

        {
          model: Message,
          required: true,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
          limit: 1,
          order: [['id', 'desc']],
        },
      ],
      limit,
      offset,
      order: [['time_last_send', 'desc']],
    });
    // rows.sort(
    //   (a: any, b: any) => a.dataValues.Messages[0].dataValues.is_read - b.dataValues.Messages[0].dataValues.is_read,
    // );

    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary count message not read (any)
   */
  @Security('jwt')
  @Get('/message-not-read')
  public async countMessageNotRead(
    @Request() request: AppTypes.RequestAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const UserTopicMessageWhereOption = {
      is_active: IS_ACTIVE.ACTIVE,
      user_id: userDecoded.id,
    };
    const countMessageUserNotRead = await Message.findAll({
      attributes: [
        [sequelize.col('`Message`.`topic_message_id`'), 'topic_message_id'],
        [sequelize.fn('COUNT', sequelize.col('`Message`.`topic_message_id`')), 'count_message_not_read'],
      ],
      where: { is_active: IS_ACTIVE.ACTIVE, is_read: IS_READ.NOT_READ, user_id: { [Op.ne]: userDecoded.id } },
      include: [
        {
          model: TopicMessage,
          require: true,
          attributes: [],
          where: UserTopicMessageWhereOption,
        },
        {
          model: Shop,
          required: true,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
      ],
      group: ['`Message`.`topic_message_id`'],
    });
    let listTopicMessageNotRead = countMessageUserNotRead;
    if (userDecoded.shop_id) {
      const shopTopicMessageWhereOption = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: userDecoded.shop_id,
      };
      const countMessageShopNotRead = await Message.findAll({
        attributes: [
          [sequelize.col('`Message`.`topic_message_id`'), 'topic_message_id'],
          [sequelize.fn('COUNT', sequelize.col('`Message`.`topic_message_id`')), 'count_message_not_read'],
        ],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          is_read: IS_READ.NOT_READ,
          [Op.or]: [{ shop_id: { [Op.ne]: userDecoded.shop_id } }, { shop_id: null }],
        },
        include: [
          {
            model: TopicMessage,
            require: true,
            attributes: [],
            where: shopTopicMessageWhereOption,
          },
          {
            model: User,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        ],
        group: ['`Message`.`topic_message_id`'],
      });
      listTopicMessageNotRead = listTopicMessageNotRead.concat(countMessageShopNotRead);
    }

    return withSuccess(listTopicMessageNotRead);
  }

  /**
   * @summary check is topic message existed
   */
  @Security('jwt')
  @Get('/{shop_id}/exist')
  public async isTopicExist(
    @Request() request: AppTypes.RequestIOAuth,
    shop_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const foundTopic = await TopicMessage.findOne({
      where: {
        user_id: userDecoded.id,
        shop_id: shop_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: [
              'pancake_shop_id',
              'agora_project_id',
              'agora_app_certificate',
              'pancake_shop_key',
              'pancake_shop_key',
              'stream_minutes_available',
              'livestream_enable',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });

    return withSuccess(foundTopic);
  }

  /**
   * @summary get detail topic message (any)
   */
  @Security('jwt')
  @Get('/{topic_message_id}/topic')
  public async getTopicMessage(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    let whereOption: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOption = { ...whereOption, [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }] };
    } else {
      whereOption = { ...whereOption, user_id: userDecoded.id };
    }
    const foundTopicMessage = await TopicMessage.findOne({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`message\`
              WHERE message.is_active = ${IS_ACTIVE.ACTIVE}
              AND message.is_read = ${IS_READ.NOT_READ}
              AND message.topic_message_id = TopicMessage.id
              AND (message.user_id != ${userDecoded.id} OR message.shop_id != ${userDecoded.shop_id || null})
              )`),
            'count_message_not_read',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!foundTopicMessage) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc hội thoại không tồn tại');
    return withSuccess(foundTopicMessage);
  }

  /**
   * @summary get list message by topic_message_id (any)
   */
  @Security('jwt')
  @Get('/{topic_message_id}')
  public async getListMessages(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    let whereOptionFoundChannel: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOptionFoundChannel = {
        ...whereOptionFoundChannel,
        [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }],
      };
    } else {
      whereOptionFoundChannel = { ...whereOptionFoundChannel, user_id: userDecoded.id };
    }
    const foundTopic = await TopicMessage.findOne({ where: whereOptionFoundChannel });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc trò chuyện không tồn tại.');

    const { count, rows } = await Message.findAndCountAll({
      where: { topic_message_id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [['id', 'desc']],
    });
    // trường hợp người lấy nhắn tin là shop
    if (userDecoded.shop_id === foundTopic.shop_id) {
      await Message.update(
        { is_read: IS_READ.READ, update_by: userDecoded.id, update_at: new Date() },
        {
          where: {
            topic_message_id,
            [Op.or]: [{ shop_id: null }, { shop_id: { [Op.ne]: userDecoded.shop_id } }],
            is_read: IS_READ.NOT_READ,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
    }
    // trường hợp người lấy nhắn tin là người dùng
    else {
      await Message.update(
        { is_read: IS_READ.READ, update_by: userDecoded.id, update_at: new Date() },
        {
          where: {
            topic_message_id,
            user_id: { [Op.ne]: userDecoded.id },
            is_read: IS_READ.NOT_READ,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
    }

    MessageAction(request.io).readMessage(foundTopic.id, foundTopic);
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary send a message to new channel use form-data (any)
   */
  @Security('jwt')
  @Post('/new-topic')
  public async sendMessageToNewChannel(
    @Request() request: AppTypes.RequestIOAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;

    await uploadMiddleware.handleSingleFile(request, 'message_media', MEDIA_TYPE.IMAGE);
    const { key: message_media_url } = (request.file as any) || {};

    const validateDataBody: MessageTypes.CreateChannelModel =
      await this.messageService.CreateChannelSchema.validateAsync({
        ...request.body,
        message_media_url,
      });
    if (!validateDataBody.type_message_media && message_media_url)
      validateDataBody.type_message_media = MEDIA_TYPE.IMAGE;
    if (
      !this.messageService.validateCreateMessage(validateDataBody) ||
      userDecoded.shop_id === validateDataBody.shop_id
    )
      throw new AppError(ApiCodeResponse.INVALID_PARAM);
    let foundTopic = await TopicMessage.findOne({
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`message\`
              WHERE message.is_active = ${IS_ACTIVE.ACTIVE}
              AND message.is_read = ${IS_READ.NOT_READ}
              AND message.topic_message_id = TopicMessage.id
              AND (message.user_id != ${userDecoded.id} OR message.shop_id != ${userDecoded.shop_id || null})
              )`),
            'count_message_not_read',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: {
        user_id: userDecoded.id, // validateDataBody.user_id,
        shop_id: validateDataBody.shop_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    let isTopicCreated = true;
    let createdMessage = null;
    // topic has existed
    if (foundTopic) {
      const dataTransaction = await sequelize.transaction(async (transaction) => {
        const createdMessage = await this._create(
          {
            topic_message_id: foundTopic.id,
            user_id: userDecoded.id,
            shop_id: userDecoded.shop_id,
            content: validateDataBody.content,
            message_media_url: validateDataBody.message_media_url
              ? validateDataBody.message_media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '')
              : null,
            type_message_media: validateDataBody.message_media_url ? validateDataBody.type_message_media : null,
          },
          { transaction },
        );
        await TopicMessage.update(
          { time_last_send: new Date() },
          { where: { id: foundTopic.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
        );
        return createdMessage;
      });
      createdMessage = dataTransaction;
    }
    // topic not existed
    else {
      const dataTransaction = await sequelize.transaction(async (transaction) => {
        const createdTopicMessage = await TopicMessage.create(
          {
            user_id: userDecoded.id, // validateDataBody.user_id,
            shop_id: validateDataBody.shop_id,
          },
          { transaction },
        );
        const listMessage = [
          {
            topic_message_id: createdTopicMessage.id,
            user_id: userDecoded.id,
            shop_id: userDecoded.shop_id,
            content: validateDataBody.content,
            message_media_url: validateDataBody.message_media_url,
            type_message_media: validateDataBody.message_media_url ? validateDataBody.type_message_media : null,
          },
          {
            topic_message_id: createdTopicMessage.id,
            shop_id: validateDataBody.shop_id,
            content: CONFIG.SHOP_REPLY_MESSAGE,
          },
        ];
        const createdMessage = await this._create(
          {
            topic_message_id: createdTopicMessage.id,
            user_id: userDecoded.id,
            // shop_id: userDecoded.shop_id,
            content: validateDataBody.content,
            message_media_url: validateDataBody.message_media_url
              ? validateDataBody.message_media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '')
              : null,
            type_message_media: validateDataBody.message_media_url ? validateDataBody.type_message_media : null,
          },
          { transaction },
        );
        // create default shop reply message
        await this._create(
          {
            topic_message_id: createdTopicMessage.id,
            shop_id: validateDataBody.shop_id,
            content: CONFIG.SHOP_REPLY_MESSAGE,
          },
          { transaction },
        );
        return createdMessage;
      });
      createdMessage = dataTransaction;
      foundTopic = await TopicMessage.findOne({
        where: { id: createdMessage.topic_message_id, is_active: IS_ACTIVE.ACTIVE },
        include: [
          {
            model: User,
            required: true,
            attributes: {
              exclude: [
                'code_reset_password',
                'expired_reset_password',
                'token',
                'password',
                'create_by',
                'update_by',
                'delete_by',
                'version',
              ],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Shop,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        ],
      });
      isTopicCreated = false;
    }
    foundTopic.setDataValue('Messages', [createdMessage]);
    MessageAction(request.io).sendMessage(foundTopic?.id, createdMessage);
    if (isTopicCreated) {
      UserAction(request.io).sendMessageNewChannel(foundTopic.user_id, foundTopic);
      ShopAction(request.io).sendMessageNewChannel(foundTopic.shop_id, foundTopic);
    } else {
      UserAction(request.io).sendMessage(foundTopic.user_id, foundTopic);
      ShopAction(request.io).sendMessage(foundTopic.shop_id, foundTopic);
    }
    this.pushNotification(foundTopic, userDecoded.id, createdMessage.dataValues.content);
    return withSuccess(createdMessage);
  }
  /**
   * @summary send a message to channel existed (any)
   */
  @Security('jwt')
  @Post('/{topic_message_id}/message')
  public async sendMessageToChannel(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    let whereOptionFoundChannel: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOptionFoundChannel = {
        ...whereOptionFoundChannel,
        [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }],
      };
    } else {
      whereOptionFoundChannel = { ...whereOptionFoundChannel, user_id: userDecoded.id };
    }
    const foundTopic = await TopicMessage.findOne({
      where: whereOptionFoundChannel,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM \`message\`
              WHERE message.is_active = ${IS_ACTIVE.ACTIVE}
              AND message.is_read = ${IS_READ.NOT_READ}
              AND message.topic_message_id = TopicMessage.id
              AND (message.user_id != ${userDecoded.id} OR message.shop_id != ${userDecoded.shop_id || null})
              )`),
            'count_message_not_read',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc trò chuyện không tồn tại.');
    await uploadMiddleware.handleSingleFile(request, 'message_media', MEDIA_TYPE.IMAGE);
    const { key: message_media_url } = (request.file as any) || {};
    request.body.user_id = userDecoded.id;
    if (userDecoded.shop_id === foundTopic.shop_id) {
      request.body.shop_id = userDecoded.shop_id;
    }
    const validateDataBody: MessageTypes.CreateMessageModel =
      await this.messageService.CreateMessageSchema.validateAsync({
        ...request.body,
        topic_message_id,
        message_media_url,
      });
    if (!validateDataBody.type_message_media && message_media_url)
      validateDataBody.type_message_media = MEDIA_TYPE.IMAGE;
    const dataTransaction = await sequelize.transaction(async (transaction) => {
      const createdMessage = await this._create(
        {
          ...validateDataBody,
          type_message_media: validateDataBody.message_media_url ? validateDataBody.type_message_media : null,
        },
        { transaction },
      );
      await TopicMessage.update(
        { time_last_send: new Date() },
        { where: { id: foundTopic.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      return createdMessage;
    });

    foundTopic.setDataValue('Messages', [dataTransaction]);
    MessageAction(request.io).sendMessage(topic_message_id, dataTransaction);
    UserAction(request.io).sendMessage(foundTopic.user_id, foundTopic);
    ShopAction(request.io).sendMessage(foundTopic.shop_id, foundTopic);

    this.pushNotification(foundTopic, userDecoded.id, dataTransaction.dataValues.content);

    return withSuccess(dataTransaction);
  }

  /**
   * @summary read all message (any)
   */
  @Security('jwt')
  @Put('/{topic_message_id}/read')
  public async readAllMessage(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;

    let whereOptionFoundChannel: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOptionFoundChannel = {
        ...whereOptionFoundChannel,
        [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }],
      };
    } else {
      whereOptionFoundChannel = { ...whereOptionFoundChannel, user_id: userDecoded.id };
    }
    const foundTopic = await TopicMessage.findOne({ where: whereOptionFoundChannel });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc trò chuyện không tồn tại.');
    // trường hợp người đọc tin là shop
    if (userDecoded.shop_id === foundTopic.shop_id) {
      await Message.update(
        { is_read: IS_READ.READ, update_by: userDecoded.id, update_at: new Date() },
        {
          where: {
            topic_message_id,
            [Op.or]: [{ shop_id: null }, { shop_id: { [Op.ne]: userDecoded.shop_id } }],
            is_read: IS_READ.NOT_READ,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
    }
    // trường hợp người đọc nhắn tin là customer
    else {
      await Message.update(
        { is_read: IS_READ.READ, update_by: userDecoded.id, update_at: new Date() },
        {
          where: {
            topic_message_id,
            user_id: { [Op.ne]: userDecoded.id },
            is_read: IS_READ.NOT_READ,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      );
    }

    MessageAction(request.io).readMessage(topic_message_id, {});

    return withSuccess({});
  }

  /**
   * @summary read a message (any) -- chưa xong
   */
  @Security('jwt')
  @Put('/{topic_message_id}/read/{message_id}/message')
  public async readAMessage(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
    message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;

    let whereOptionFoundChannel: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOptionFoundChannel = {
        ...whereOptionFoundChannel,
        [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }],
      };
    } else {
      whereOptionFoundChannel = { ...whereOptionFoundChannel, user_id: userDecoded.id };
    }
    const foundTopic = await TopicMessage.findOne({ where: whereOptionFoundChannel });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc trò chuyện không tồn tại.');
    // trường hợp người lấy nhắn tin là shop

    if (userDecoded.shop_id === foundTopic.shop_id) {
      await this._update(
        { is_read: IS_READ.READ, update_at: new Date(), update_by: userDecoded.id },
        {
          where: {
            id: message_id,
            shop_id: { [Op.ne]: userDecoded.shop_id },
            topic_message_id,
            is_read: IS_READ.NOT_READ,
          },
        },
      );
    }
    // trường hợp người lấy nhắn tin là người dùng
    else {
      await this._update(
        { is_read: IS_READ.READ, update_at: new Date(), update_by: userDecoded.id },
        {
          where: { id: message_id, user_id: { [Op.ne]: userDecoded.id }, topic_message_id, is_read: IS_READ.NOT_READ },
        },
      );
    }

    MessageAction(request.io).readMessage(topic_message_id, {});

    return withSuccess({});
  }

  /**
   * @summary typing message (any) -- chưa xong
   */
  @Security('jwt')
  @Post('/{topic_message_id}/typing')
  public async typingMessage(
    @Request() request: AppTypes.RequestIOAuth,
    topic_message_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    let whereOptionFoundChannel: any = {
      id: topic_message_id,
      is_active: IS_ACTIVE.ACTIVE,
    };
    if (userDecoded.shop_id) {
      whereOptionFoundChannel = {
        ...whereOptionFoundChannel,
        [Op.or]: [{ user_id: userDecoded.id }, { shop_id: userDecoded.shop_id }],
      };
    } else {
      whereOptionFoundChannel = { ...whereOptionFoundChannel, user_id: userDecoded.id };
    }
    const foundTopic = await TopicMessage.findOne({ where: whereOptionFoundChannel });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Cuộc trò chuyện không tồn tại.');

    return withSuccess({});
  }

  private async pushNotification(foundTopic, user_id: number, message: string) {
    // trường hợp người gửi tin nhắn là user, đẩy thông báo cho shop
    if (user_id === foundTopic.user_id) {
      const name_sender = foundTopic.dataValues.User.dataValues.name;
      const listUserInShop = await User.findAll({
        where: { shop_id: foundTopic.shop_id, is_active: IS_ACTIVE.ACTIVE },
      });
      const listDeviceId = listUserInShop
        .filter((item) => {
          const res = SocketUtils.hasUserIdInRoom(foundTopic.id, item.id);
          if (!res && item.dataValues.device_id) return true;
          return false;
        })
        .map((item) => item.dataValues.device_id);
      this.createMessageNotification(foundTopic.dataValues, name_sender, message, listDeviceId);
    }
    // trường hợp người gửi tin nhắn là shop, đẩy thông báo cho user
    else {
      const name_sender = foundTopic.dataValues.Shop.dataValues.name;
      const device_id = foundTopic.dataValues.User.dataValues.device_id;
      const res = SocketUtils.hasUserIdInRoom(foundTopic.id, foundTopic.user_id);
      if (!res && device_id) this.createMessageNotification(foundTopic.dataValues, name_sender, message, device_id);
    }
  }

  // create noti
  private async createMessageNotification(
    topic_info: any,
    name_sender: string,
    message: string,
    device_ids: Array<string> | string,
  ) {
    const notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.NEW_MESSAGE);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = `${name_sender}: ${message}`;
    await this.onesignalService.pushNotification(
      {
        id: topic_info.id,
        user_id: topic_info.user_id,
        shop_id: topic_info.shop_id,
        df_notification_id: DF_NOTIFICATION.NEW_MESSAGE,
      },
      notiType.title,
      content,
      device_ids,
    );
  }
}
