import { Get, Query, Route, Tags, Request } from 'tsoa';
import { AppTypes } from 'types';
import { withPagingSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { ApplicationController } from '.';
const db = require('@models');
const { sequelize, Sequelize, Shop, User, Reaction } = db.default;
const { Op } = Sequelize;

@Route('reaction')
@Tags('reactions')
export class ReactionController extends ApplicationController {
  constructor() {
    super('Reaction');
  }

  /**
   * @summary get list reaction (no secute)
   */
  @Get('/')
  public async listReaction(
    @Request() request: AppTypes.RequestAuth,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('post_id') post_id?: number,
    @Query('livestream_id') livestream_id?: number,
    @Query('comment_id') comment_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (post_id) {
      whereOption.post_id = post_id;
    }
    if (livestream_id) {
      whereOption.livestream_id = livestream_id;
    }
    if (comment_id) {
      whereOption.comment_id = comment_id;
    }

    const { count, rows } = await Reaction.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: {
            exclude: [
              'code_reset_password',
              'expired_reset_password',
              'token',
              'password',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }
}
