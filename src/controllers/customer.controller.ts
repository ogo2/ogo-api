import { IS_ACTIVE, ROLE, TOPIC_STATUS, ORDER_STATUS } from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Request, Get, Query, Route, Delete, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import * as _ from 'lodash';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  DFWard,
  DFDistrict,
  DFProvince,
  UserAddress,
  Order,
  OrderItem,
  Shop,
  PointTransactionHistory,
  DFTypeTransactionPoint,
} = db.default;
const { Op } = Sequelize;

@Route('customer')
@Tags('customers')
export class CustomerController extends ApplicationController {
  constructor() {
    super('User');
  }
  /**
   * @summary get list a customer pagging (admin)
   */
  // @Security('jwt', ['admin'])
  @Get('/')
  public async getListCustomer(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('province_id') province_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    // @Query('tab') tab?: any,
    // @Query('shop_id') shop_id?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    // return withSuccess(role);
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      df_type_user_id: ROLE.CUSTOMER,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
    };
    // const userAddressWhereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    // if (province_id) {
    //   userAddressWhereOption.df_province_id = province_id;
    // }

    const { count, rows } = await User.findAndCountAll({
      // subQuery: false,
      where: whereOption,
      attributes: [
        'id',
        'name',
        'user_name',
        'date_of_birth',
        'create_at',
        [
          sequelize.literal(`(
          select group_concat(attributes.topic separator ',') as toppic_name
            from (
          select concat(group_concat(DISTINCT topic.name separator ',')) as topic
          FROM topic
          where topic.is_active = ${IS_ACTIVE.ACTIVE}
          and topic.status = ${TOPIC_STATUS.ACTIVE}
          and topic.id not in (select topic_id from user_ignore_topic where user_ignore_topic.user_id = User.id and user_ignore_topic.status = 1)
          group by topic.name
          order by topic.id desc) as attributes
          )`),
          'topic',
        ],
        //doanh số trên đơn hàng
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_price_success',
        ],
        //doanh số thực tế là tất cả các trạng thái trừ trạng thái huỷ
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            )`),
          'total_price_pending',
        ],
        //doanh số trên đơn chưa ht
        // [
        //   Sequelize.literal(`(SELECT
        //     IFNULL(SUM(od.total_price), 0)
        //     FROM ogo.order as od
        //     WHERE
        //     od.is_active = ${IS_ACTIVE.ACTIVE}
        //     AND
        //     od.user_id = User.id
        //     AND
        //     od.status != ${ORDER_STATUS.CANCELED}
        //     )`),
        //   'total_price_order_pending',
        // ],
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            AND
            od.status != ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_price_order_pending',
        ],
        // tổng số lượng : tổng sl trên đơn hoàn thành
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_amount_order',
        ],
        // tổng số order
        [
          Sequelize.literal(`(SELECT 
            IFNULL(COUNT(od.id), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            )`),
          'total_count_order',
        ],
        // [sequelize.fn('SUM', sequelize.col('Orders->OrderItems.price')), 'total_price'],
      ],
      include: [
        {
          model: UserAddress,
          required: province_id != undefined && province_id != null && province_id > 0 ? true : false,
          attributes: [
            'id',
            'name',
            'phone',
            'user_id',
            'location_address',
            'address',
            'df_province_id',
            [sequelize.literal('`UserAddresses->DFDistrict`.`name`'), 'district_name'],
            [sequelize.literal('`UserAddresses->DFProvince`.`name`'), 'province_name'],
            [sequelize.literal('`UserAddresses->DFWard`.`name`'), 'ward_name'],
          ],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            df_province_id:
              province_id != undefined && province_id != null && province_id > 0 ? province_id : { [Op.ne]: null },
          },
          include: [
            {
              model: DFWard,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: ['id', 'name'],
            },
            {
              model: DFDistrict,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: ['id', 'name'],
            },
            {
              model: DFProvince,
              required: false,
              where: {
                is_active: IS_ACTIVE.ACTIVE,
                // id:
                //   province_id != undefined && province_id != null && province_id > 0 ? province_id : { [Op.ne]: null },
              },
              attributes: ['id', 'name'],
            },
          ],
        },
        {
          model: Order,
          required: role != ROLE.SHOP && role != ROLE.SHOP_MEMBER ? false : true,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            shop_id: role != ROLE.SHOP && role != ROLE.SHOP_MEMBER ? { [Op.ne]: null } : shopId,
          },
          attributes: [],
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['User.id'],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary get all list customer (admin, admin_sell_manager, shop, shop_member)
   */

  @Security('jwt', ['admin', 'admin_sell_manager', 'shop', 'shop_member'])
  @Get('/all')
  public async getAllListCustomer(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('province_id') province_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      df_type_user_id: ROLE.CUSTOMER,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
    };
    const userAddressWhereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    // if (province_id) {
    //   userAddressWhereOption.df_province_id = province_id;
    // }

    const listUser = await User.findAll({
      subQuery: false,
      where: whereOption,
      attributes: [
        'id',
        'name',
        'user_name',
        'date_of_birth',
        'create_at',
        [
          sequelize.literal(`(
          select group_concat(attributes.topic separator ',') as toppic_name
            from (
          select concat(group_concat(DISTINCT topic.name separator ',')) as topic
          FROM topic
          where topic.is_active = ${IS_ACTIVE.ACTIVE}
          and topic.status = ${TOPIC_STATUS.ACTIVE}
          and topic.id not in (select topic_id from user_ignore_topic where user_ignore_topic.user_id = User.id and user_ignore_topic.status = 1)
          group by topic.name order by topic.id desc) as attributes
          )`),
          'topic',
        ],
        //doanh số trên đơn hàng
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_price_success',
        ],
        //doanh số thực tế là tất cả các trạng thái trừ trạng thái huỷ
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            )`),
          'total_price_pending',
        ],
        //doanh số trên đơn chưa ht
        // [
        //   Sequelize.literal(`(SELECT
        //     IFNULL(SUM(od.total_price), 0)
        //     FROM ogo.order as od
        //     WHERE
        //     od.is_active = ${IS_ACTIVE.ACTIVE}
        //     AND
        //     od.user_id = User.id
        //     AND
        //     od.status != ${ORDER_STATUS.CANCELED}
        //     )`),
        //   'total_price_order_pending',
        // ],
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(od.total_price), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            AND
            od.status != ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_price_order_pending',
        ],
        // tổng số lượng : tổng sl trên đơn hoàn thành
        [
          Sequelize.literal(`(SELECT 
            IFNULL(SUM(ot.amount), 0)
            FROM ogo.order as od
            join order_item as ot on od.id = ot.order_id
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status = ${ORDER_STATUS.SUCCCESS}
            )`),
          'total_amount_order',
        ],
        // tổng số order
        [
          Sequelize.literal(`(SELECT 
            IFNULL(COUNT(od.id), 0)
            FROM ogo.order as od
            WHERE
            od.is_active = ${IS_ACTIVE.ACTIVE}
            AND
            od.user_id = User.id
            AND
            od.status != ${ORDER_STATUS.CANCELED}
            )`),
          'total_count_order',
        ],
        // [sequelize.fn('SUM', sequelize.col('Orders->OrderItems.price')), 'total_price'],
      ],
      include: [
        {
          model: UserAddress,
          required: false,
          attributes: [
            'id',
            'name',
            'phone',
            'user_id',
            'location_address',
            'address',
            [sequelize.literal('`UserAddresses->DFDistrict`.`name`'), 'district_name'],
            [sequelize.literal('`UserAddresses->DFProvince`.`name`'), 'province_name'],
            [sequelize.literal('`UserAddresses->DFWard`.`name`'), 'ward_name'],
          ],
          where: userAddressWhereOption,
          include: [
            {
              model: DFWard,
              where: { is_active: IS_ACTIVE.ACTIVE },
              required: true,
              attributes: [],
            },
            {
              model: DFDistrict,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: [],
            },
            {
              model: DFProvince,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
              attributes: [],
            },
          ],
        },
        {
          model: Order,
          required:
            loggedInUser.df_type_user_id !== ROLE.SHOP && loggedInUser.df_type_user_id !== ROLE.SHOP_MEMBER
              ? false
              : true,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            shop_id:
              loggedInUser.df_type_user_id !== ROLE.SHOP && loggedInUser.df_type_user_id !== ROLE.SHOP_MEMBER
                ? { [Op.ne]: null }
                : loggedInUser.shop_id,
          },
          attributes: [],
        },
      ],
      order: [['id', 'desc']],
      group: ['User.id'],
    });
    return withPagingSuccess(listUser);
  }

  /**
   * @summary get list order customer
   */
  // @Security('jwt', ['admin'])
  @Get('/list-order')
  public async getDetailCustomerOrder(
    @Request() request: AppTypes.RequestAuth,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('user_id') user_id?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess({});
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      user_id: user_id,
    };
    const { rows, count } = await Order.findAndCountAll({
      where: whereOption,
      attributes: [
        'id',
        'code',
        'status',
        'user_address_id',
        'shop_id',
        'create_at',
        'total_price',
        // [
        //   Sequelize.literal(`(
        //                 SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
        //                 FROM order_item AS order_item
        //                 WHERE order_item.order_id = order.id
        //                     AND
        //                     order_item.is_active = ${IS_ACTIVE.ACTIVE}
        //             )`),
        //   'total_price',
        // ],
        [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        [Sequelize.literal('`User`.`name`'), 'user_name'],
        [Sequelize.literal('OrderItems.product->"$.name"'), 'product_name'],
        [Sequelize.literal('OrderItems.product->"$.id"'), 'product_price_id'],
        [Sequelize.literal('OrderItems.product->"$.product_attribute_name_1"'), 'product_attribute_name_1'],
        [Sequelize.literal('OrderItems.product->"$.product_attribute_name_2"'), 'product_attribute_name_2'],
        [Sequelize.literal('OrderItems.product->"$.category_name"'), 'category_name'],
        [Sequelize.literal('OrderItems.amount'), 'product_amount'],
        [Sequelize.literal('OrderItems.product->"$.image"'), 'product_image'],
        [Sequelize.literal('OrderItems.product->"$.price"'), 'product_price'],
      ],
      include: [
        {
          model: OrderItem,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      group: [['Order.id']],
      order: [['id', 'desc']],
      page,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }
  /**
   * @summary get list history customer
   */
  // @Security('jwt', ['admin'])
  @Get('/list-point-history')
  public async getDetailCustomerPointHistory(
    @Request() request: AppTypes.RequestAuth,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('user_id') user_id?: any,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess({});
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      user_id: user_id,
    };
    const { rows, count } = await PointTransactionHistory.findAndCountAll({
      where: whereOption,
      // attributes: [
      //   "id", "user_id", "point", "current_point", "type",
      //   // [Sequelize.literal('DFTypeTransactionPoint.product->"$.image"'), 'product_image'],
      //   // [Sequelize.literal('OrderItems.product->"$.price"'), 'product_price'],

      // ],
      include: [
        {
          model: DFTypeTransactionPoint,
          // attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          attributes: ['id', 'name'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [['id', 'desc']],
      page,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary Xóa khách hàng (admin)
   */
  @Delete('/{user_id}')
  // @Security('jwt')
  public async deleteCustomer(user_id: number, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const checkCustomer = await User.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id: user_id },
    });
    // return withSuccess(checkCategory);
    if (!checkCustomer) throw ApiCodeResponse.NOT_FOUND;
    // lấy danh sách order trạng thái chưa hoàn thành
    const checkOrder = await Order.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, user_id: user_id, status: { [Op.ne]: ORDER_STATUS.SUCCCESS } },
    });
    if (checkOrder.length > 0) throw ApiCodeResponse.USER_NOT_DELETE;
    // check Product
    const data = await User.update(
      {
        is_active: IS_ACTIVE.INACTIVE,
      },
      {
        where: { id: user_id, is_active: IS_ACTIVE.ACTIVE },
      },
    );
    return withSuccess(data);
  }
}
