import { Body, Request, Get, Query, Route, Tags } from 'tsoa';
import { withSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import * as _ from 'lodash';
import * as express from 'express';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  Topic,
  DFTypeUser,
  DFReaction,
  DFProvince,
  DFDistrict,
  DFWard,
  DFNotification,
  DFOrderStatusHistory,
  DFTypeGift,
  DFTypeTransactionPoint,
  PakageCategory,
} = db.default;
const { Op } = Sequelize;

@Route('default')
@Tags('defaults')
export class DefaultController {
  /**
   * @summary list province
   */
  @Get('/province')
  public async getListProvince(): Promise<AppTypes.SuccessResponseModel<any>> {
    const provinceData = await DFProvince.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      order: [["name", "asc"]]
    });
    return withSuccess(provinceData);
  }

  /**
   * @summary list district
   */
  @Get('/district')
  public async getListDistrict(
    @Query('province_id') province_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const whereOption: any = {};
    if (province_id) {
      whereOption.df_province_id = province_id;
    }
    const districtData = await DFDistrict.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: whereOption,
      order: [["name", "asc"]]
    });
    return withSuccess(districtData);
  }

  /**
   * @summary list ward
   */
  @Get('/ward')
  public async getListWard(@Query('district_id') district_id?: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const whereOption: any = {};
    if (district_id) {
      whereOption.df_district_id = district_id;
    }
    const wardData = await DFWard.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: whereOption,
      order: [["name", "asc"]]
    });
    return withSuccess(wardData);
  }

  /**
   * @summary list post topic
   */
  @Get('/post-topic')
  public async getPostTopic(): Promise<AppTypes.SuccessResponseModel<any>> {
    const provinceData = await Topic.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(provinceData);
  }

  /**
   * @summary list type-notification
   */
  @Get('/type-notification')
  public async getListNotificationType(): Promise<AppTypes.SuccessResponseModel<any>> {
    const data = await DFNotification.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(data);
  }

  /**
   * @summary list order-status-history
   */
  @Get('/order-status-history')
  public async getListOrderStatusHistory(
    @Request() request: express.Request,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const data = await DFOrderStatusHistory.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(data);
  }

  /**
   * @summary list type-reaction
   */
  @Get('/type-reaction')
  public async getListReactionType(@Request() request: express.Request): Promise<AppTypes.SuccessResponseModel<any>> {
    const typeReactionData = await DFReaction.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(typeReactionData);
  }

  /**
   * @summary list type-gift
   */
  @Get('/type-gift')
  public async getListTypeGift(@Request() request: express.Request): Promise<AppTypes.SuccessResponseModel<any>> {
    const data = await DFTypeGift.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(data);
  }
  /**
   * @summary list type-transaction-point
   */
  @Get('/type-transaction-point')
  public async getListTypeTransactionPoint(
    @Request() request: express.Request,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const data = await DFTypeTransactionPoint.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(data);
  }
  /**
   * @summary list type-transaction-gift
   */
  @Get('/pakage-category')
  public async getPakageCategory(@Request() request: express.Request): Promise<AppTypes.SuccessResponseModel<any>> {
    const pakageCategory = await PakageCategory.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(pakageCategory);
  }

  /**
   * @summary list type-transaction-gift
   */
  @Get('/type-user')
  public async getListTypeUser(@Request() request: express.Request): Promise<AppTypes.SuccessResponseModel<any>> {
    const typeUserData = await DFTypeUser.findAndCountAll({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(typeUserData);
  }
}
