import { IS_ACTIVE } from '@utils/constants';
import { ApplicationController } from '.';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Request, Get, Query, Route, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes } from 'types';
const db = require('@models');
const { sequelize, Sequelize, PakageCategory } = db.default;
const { Op } = Sequelize;

@Route('pakage-category')
@Tags('pakages category')
export class PakageCategoryController extends ApplicationController {
  constructor() {
    super('PakageCategory');
  }

  /**
   * @summary list category pagake (any)
   */
  @Security('jwt')
  @Get('/')
  public async getListCategoryPakage(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    const { count, rows } = await PakageCategory.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        ...(search && {
          [Op.or]: {
            name: { [Op.like]: `%${search}%` },
          },
        }),
      },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [['order', 'asc']],
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }
}
