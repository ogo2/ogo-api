import { Security, Get, Put, Route, Tags, Request } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE, ROLE, ONESIGNAL, IS_PUSH, IS_READ, DF_NOTIFICATION } from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';

const db = require('@models');
const axios = require('axios');
const https = require('https');
const { sequelize, Sequelize, User, Notification, DFNotificationType } = db.default;
const { Op } = Sequelize;

/**
 * Danh mục sản phẩm
 */
@Route('notification')
@Tags('notification')
export class NotificationController extends ApplicationController {
  constructor() {
    super('Notification');
  }

  // đếm noti chưa đọc admin
  @Security('jwt')
  @Get('/count-noti-admin')
  public async countNotificationAdmin(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const { rows, count } = await Notification.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        user_id: null,
        is_read: IS_READ.NOT_READ,
        shop_id: null,
        df_notification_id: DF_NOTIFICATION.PURCHASE_GIFT,
      },
    });
    return withSuccess({ count: count instanceof Array ? count.length : count });
  }

  // đếm noti chưa đọc shop
  @Security('jwt')
  @Get('/count-noti-shop')
  public async countNotificationShop(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const { rows, count } = await Notification.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        user_id: null,
        is_read: IS_READ.NOT_READ,
        shop_id: loginUser.shop_id,
        [Op.or]: [
          { df_notification_id: DF_NOTIFICATION.NEW_ORDER },
          { df_notification_id: DF_NOTIFICATION.ORDER_CANCEL },
          { df_notification_id: DF_NOTIFICATION.NEW_MESSAGE },
        ],
      },
    });
    return withSuccess({ count: count instanceof Array ? count.length : count });
  }

  // đếm noti chưa đọc customer + shop
  @Security('jwt')
  @Get('/count-noti')
  public async countNotification(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const { rows, count } = await Notification.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        user_id: loginUser.df_type_user_id == ROLE.ADMIN ? null : loginUser.id,
        is_read: IS_READ.NOT_READ,
      },
    });
    return withSuccess({ count: count instanceof Array ? count.length : count });
  }

  // dánh sách noti
  @Security('jwt')
  @Get('/')
  public async getListNotification(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(request.user.data);
    const loginUser = request.user?.data;
    // return withSuccess(loginUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    // const { id } = request.user.data;
    const { rows, count } = await Notification.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        user_id: loginUser.df_type_user_id == ROLE.ADMIN ? null : loginUser.id,
        df_notification_id: loginUser.df_type_user_id == ROLE.ADMIN ? DF_NOTIFICATION.PURCHASE_GIFT : { [Op.ne]: null },
      },
      order: [['id', 'desc']],
      limit,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
  }
  // dánh sách noti shop
  @Security('jwt')
  @Get('/list-noti-shop')
  public async getListNotificationShop(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(request.user.data);
    const loginUser = request.user?.data;
    // return withSuccess(loginUser);
    const { offset, limit, page } = handlePagingMiddleware(request);
    // const { id } = request.user.data;
    const { rows, count } = await Notification.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE, shop_id: loginUser ? loginUser.shop_id : null },
      order: [['id', 'desc']],
      limit,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
  }

  // dánh sách các loại noti
  @Security('jwt')
  @Get('/noti-type')
  public async getListTypeNotification(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(request.user.data);
    // const loginUser = request.user?.data;
    const noti = await DFNotificationType.findAll({ where: { is_active: IS_ACTIVE.ACTIVE } });
    return withSuccess(noti);
  }

  // đọc noti
  @Security('jwt')
  @Put('/{id}')
  public async readNotification(@Request() request: any, id?: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const noti = await Notification.update(
      { is_read: IS_READ.READ },
      { where: { is_active: IS_ACTIVE.ACTIVE, id: id } },
    );
    return withSuccess(noti);
  }

  public async createNotification({ title, content, user_id, is_read = 0, df_notification_type_id }, transaction) {
    const notiType = await DFNotificationType.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, value: df_notification_type_id },
    });

    if (!notiType) throw ApiCodeResponse.NOT_FOUND;

    return Notification.create(
      {
        df_notification_type_id,
        title: title || notiType.title,
        content,
        user_id,
        is_read,
      },
      { transaction },
    );
  }

  public async pushNotification() {
    // console.log("START_PUSH");
    const notificationspush = await Notification.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        is_push: IS_PUSH.UN_PUSHED,
      },
      attributes: { include: [[sequelize.col('user.device_id'), 'device_id']] },
      include: [
        {
          model: User,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            [Op.and]: [{ device_id: { [Op.ne]: null } }, { device_id: { [Op.ne]: '' } }],
          },
          attributes: [],
        },
      ],
    });
    notificationspush.forEach(async (notification) => {
      const message = {
        app_id: ONESIGNAL.APP_ID,
        data: notification.dataValues,
        headings: { en: notification.title },
        contents: { en: notification.content },
        android_channel_id: ONESIGNAL.ANDROID_CHANNEL_ID,
        include_player_ids: [notification.dataValues.device_id],
        // include_player_ids: ['bd80cab6-084c-4671-aa22-ad0cc31287d0'],
      };
      await Notification.update({ is_push: IS_PUSH.PUSHED }, { where: { id: notification.id } });
      const headers = {
        'Content-Type': 'application/json; charset=utf-8',
        Authorization: `Basic ${ONESIGNAL.AUTHORIZATION}`,
      };

      const options = {
        host: 'onesignal.com',
        port: 443,
        path: '/api/v1/notifications',
        method: 'POST',
        headers,
      };

      const req = https.request(options, (res) => {
        res.on('message', async (result) => {
          console.log('message', JSON.stringify(result));
          // await onSuccess();
        });
      });

      req.on('error', async (err) => {
        console.log(err);
        // await onError();
      });

      req.write(JSON.stringify(message));
      req.end();
    });

    return withSuccess(notificationspush);
  }

  public async handlePushSucceeded(id) {
    try {
      await Notification.update({ is_push: IS_PUSH.PUSHED }, { where: { id } });
    } catch (error) {}
  }

  public async onesignal(notification) {
    axios.defaults.baseURL = 'https://onesignal.com';
    axios.defaults.headers.common.Authorization = `Basic :${ONESIGNAL.AUTHORIZATION}`;
    axios.defaults.headers.post['Content-Type'] = 'application/json';

    axios
      .post('/v1/notifications', {
        app_id: ONESIGNAL.APP_ID,
        data: notification.data,
        headings: { en: notification.title },
        contents: { en: notification.content },
        android_channel_id: ONESIGNAL.ANDROID_CHANNEL_ID,
        include_player_ids: [notification.device_id],
        // include_player_ids: ['bd80cab6-084c-4671-aa22-ad0cc31287d0'],
      })
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  public async sendNotification(data, onSuccess) {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Basic ${ONESIGNAL.AUTHORIZATION}`,
    };

    const options = {
      host: 'onesignal.com',
      port: 443,
      path: '/api/v1/notifications',
      method: 'POST',
      headers,
    };

    const req = https.request(options, (res) => {
      res.on('data', async (result) => {
        console.log(JSON.stringify(result));
        await onSuccess();
      });
    });

    req.on('error', async (err) => {
      console.log(err);
      // await onError();
    });

    req.write(JSON.stringify(data));
    req.end();
  }
}
