import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { MEDIA_TYPE } from '@utils/constants';
import * as express from 'express';
import { isEmpty } from 'lodash';
import { Post, Request, Route, Tags, UploadedFiles } from 'tsoa';
import { AppTypes } from 'types';
import { withSuccess } from '../utils/BaseResponse';
const ExcelJS = require('exceljs');
var fs = require('fs');
interface MulterRequest extends express.Request {
  file: any;
}
interface ProductMulterRequests extends express.Request {
  files: any;
}
@Route('files')
@Tags('files')
export class FilesController {
  /**
   * @summary upload single file type: <0: image, 1: video>
   */
  @Post('upload/single/{type}')
  public async uploadSingleFile(
    @Request() request: express.Request,
    type: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    if (type === MEDIA_TYPE.IMAGE) {
      await uploadMiddleware.handleSingleFile(request, 'image', type);
    } else if (type == MEDIA_TYPE.VIDEO) {
      await uploadMiddleware.handleSingleFile(request, 'video', type);
    } else {
      throw ApiCodeResponse.INVALID_PARAM;
    }
    const { file } = request as MulterRequest;
    if (!file) {
      throw ApiCodeResponse.UPLOAD_FAILED;
    }
    const { originalname, location, key } = file;
    return withSuccess({ filename: originalname, url: location, path: key });
  }

  @Post('/media/upload-multiple')
  public async uploadProductMedia(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    await uploadMiddleware.handleFiles(request, 'media', 0);
    const array_image = (request as ProductMulterRequests).files.map((file) => file.key);
    const array_image_url = (request as ProductMulterRequests).files.map((file) => file.location);
    return withSuccess({ array_image, array_image_url });
  }
}
