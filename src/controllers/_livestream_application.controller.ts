import { logger } from '@utils/Logger';
import { ApplicationController } from '.';
import { IS_ACTIVE } from '@utils/constants';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, DFReaction, Comment } = db.default;
const { Op } = Sequelize;

interface DFReactionModel {
  id: number;
  name: number;
  url_icon: string;
}
export interface IParamCreateCommentLivestream {
  user_id: number;
  shop_id: number;
  content: string;
  livestream_id: number;
}

const timeout_add_comment = 5000;

// type reaction
// helper add list comment async
const listParamCreateComment: Array<IParamCreateCommentLivestream> = [];
let timeoutAddComment: NodeJS.Timeout = null;

class LivestreamApplicationController extends ApplicationController {
  constructor(m: string) {
    super(m);
  }
  public _addCommentLivestreamAsync(commentParam: IParamCreateCommentLivestream) {
    listParamCreateComment.push(commentParam);
    if (!timeoutAddComment) {
      timeoutAddComment = setTimeout(async () => {
        let isInserted = false;
        while (true) {
          try {
            const listInsertComment = [...listParamCreateComment];
            await Comment.bulkCreate(listInsertComment);
            listParamCreateComment.splice(0, listInsertComment.length);
            logger.info({ message: `success bulkCreate comment livestream` });
            isInserted = true;
          } catch (error) {
            logger.error({ message: `error bulkCreate comment livestream ${error}` });
            isInserted = false;
          }
          if (isInserted) break;
          else await Utils.timer(1000);
        }
        timeoutAddComment = null;
      }, timeout_add_comment);
    }
  }
}
export default LivestreamApplicationController;
