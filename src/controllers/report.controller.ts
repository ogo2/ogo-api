import { Security, Get, Query, Route, Tags, Request } from 'tsoa';
import { AppTypes } from 'types';
import { withPagingSuccess, withSuccess } from '../utils/BaseResponse';

import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import {
  IS_ACTIVE,
  ROLE,
  REPORT_TYPE,
  IS_DEFAULT,
  ORDER_STATUS,
  LIVESTREAM_ENABLE,
  SHOP_STATUS,
  PRODUCT_STATUS,
  LIVESTREAM_STATUS,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import { Utils } from '@utils/Utils';
const db = require('@models');
const {
  sequelize,
  Sequelize,
  Shop,
  User,
  Category,
  Product,
  OrderItem,
  Order,
  Livestream,
  LivestreamProduct,
  LivestreamLayout,
} = db.default;
const { Op } = Sequelize;

@Route('report')
@Tags('reports')
export class ReportController extends ApplicationController {
  constructor() {
    super('Report');
  }

  /**
   * @summary get list report_sell (báo cáo bán hàng admin)
   */
  // @Security('jwt')
  @Get('/report-sell')
  public async listReportSell(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('type') type?: number, // 1 là nhóm sản phẩm, 2 là theo khách hàng
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('shop_id') shop_id?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    // return withSuccess(tokens);
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    // return withSuccess(shopId);
    // return withSuccess({ shopId, userId });
    // danh sách listShop có order
    const listShop = await Shop.findAll({
      attributes: ['id', 'name'],
      where: { is_active: IS_ACTIVE.ACTIVE, id: role == ROLE.ADMIN ? { [Op.ne]: null } : null },
      include: [
        {
          model: Order,
          attributes: [],
          required: true,
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: OrderItem,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: {
              model: Product,
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
        },
      ],
    });
    // return withSuccess(listShop)
    // return withSuccess(shopId);
    if (type == REPORT_TYPE.CATEGORY) {
      const { offset, limit, page } = handlePagingMiddleware(request);
      if (from_date) {
        from_date = new Date(from_date);
      } else from_date = 0;
      if (to_date) {
        to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
      } else to_date = new Date(Date.now());
      const fromDate = new Date(from_date).toISOString().split('T')[0];
      const toDate = new Date(to_date).toISOString().split('T')[0];
      const whereOption: any = {
        // create_at: {
        //   [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        // },
        ...(search && { [Op.or]: { code: { [Op.like]: `%${search}%` }, name: { [Op.like]: `%${search}%` } } }),
      };

      const whereOptionOrder: any = {
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        is_active: IS_ACTIVE.ACTIVE,
        status: ORDER_STATUS.SUCCCESS,
        shop_id: role == ROLE.ADMIN ? (shop_id != undefined && shop_id != 0 ? shop_id : { [Op.ne]: null }) : shopId,
      };
      const reportCategory = await Category.findAll({
        subQuery: false,
        attributes: [],
        where: { is_active: IS_ACTIVE.ACTIVE, parent_id: null },
        include: {
          model: Category,
          as: 'children_category',
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: IS_DEFAULT.ACTIVE },
          include: {
            model: Product,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE, status: IS_DEFAULT.ACTIVE, stock_status: IS_DEFAULT.ACTIVE },
            include: [
              {
                model: OrderItem,
                // required: false,
                attributes: [],
                where: { is_active: IS_ACTIVE.ACTIVE },
                include: {
                  model: Order,
                  // required: false,
                  attributes: [],
                  where: {
                    is_active: IS_ACTIVE.ACTIVE,
                    status: ORDER_STATUS.SUCCCESS,
                    shop_id:
                      shopId != null ? shopId : shop_id != undefined && shop_id != 0 ? shop_id : { [Op.ne]: null },
                  },
                },
              },
            ],
          },
          // order: [['order', 'asc']],
        },
        group: ['category.id', sequelize.literal('`children_category->Products`.`id`')],
      });
      const { rows, count } = await Product.findAndCountAll({
        where: whereOption,
        subQuery: false,
        attributes: [
          // include: [[Sequelize.fn('COUNT', 'product_prices.id'), 'stock_status']],
          'id',
          'name',
          'code',
          'status',
          'shop_id',
          [Sequelize.literal('`Category`.`name`'), 'category_name'],
          // [sequelize.fn('SUM', sequelize.col('OrderItems->Order.total_price')), 'total_price'],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            order_item.product_id = Product.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_price',
          ],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(COUNT(od.id),0)
                        FROM \`order\` as od
                        join order_item as order_item on od.id = order_item.order_id
                        WHERE 
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            order_item.product_id = Product.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_order',
          ],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount),0)
                        FROM \`order\` as od
                        join order_item as order_item on od.id = order_item.order_id
                        WHERE 
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            order_item.product_id = Product.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_amount',
          ],
          //   [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
          [sequelize.literal('Shop.name'), 'shop_name'],
          //   [sequelize.fn('COUNT', sequelize.col('OrderItems.id')), 'total_order'],
        ],
        include: [
          {
            // required: category_id != undefined && category_id != null ? true : false,
            model: Category,
            attributes: ['id', 'name'],
            include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
            // where: {
            //     is_active: IS_ACTIVE.ACTIVE, id: category_id != undefined && category_id != null && category_id != "" ? category_id : { [Op.ne]: null }
            // },
          },
          {
            model: Shop,
            // required: false,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            // where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: OrderItem,
            // required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: {
              model: Order,
              // required: false,
              attributes: ['id', 'create_at'],
              where: whereOptionOrder,
            },
          },
        ],
        group: ['product.id'],
        order: [
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount),0)
                        FROM \`order\` as od
                        join order_item as order_item on od.id = order_item.order_id
                        WHERE 
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            order_item.product_id = Product.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'desc',
          ],
          ['id', 'desc'],
        ],
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(
        { rows, totalCategory: reportCategory.length, listShop },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
      // return withSuccess(reportCategory);
    }
    if (type == REPORT_TYPE.CUSTOMER) {
      const { offset, limit, page } = handlePagingMiddleware(request);
      if (from_date) {
        from_date = new Date(from_date);
      } else from_date = 0;
      if (to_date) {
        to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
      } else to_date = new Date(Date.now());
      const fromDate = new Date(from_date).toISOString().split('T')[0];
      const toDate = new Date(to_date).toISOString().split('T')[0];
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        df_type_user_id: { [Op.in]: [ROLE.CUSTOMER, ROLE.SHOP] },
        ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, phone: { [Op.like]: `%${search}%` } } }),
      };
      const whereOptionOrder: any = {
        is_active: IS_ACTIVE.ACTIVE,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        status: ORDER_STATUS.SUCCCESS,
        shop_id: role == ROLE.ADMIN ? (shop_id != undefined && shop_id != 0 ? shop_id : { [Op.ne]: null }) : shopId,
      };
      //   return withSuccess(userId);
      const { rows, count } = await User.findAndCountAll({
        where: whereOption,
        // subQuery: false,
        attributes: [
          'id',
          'name',
          'phone',
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(od.total_price), 0)
                        FROM \`order\` as od
                        WHERE od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            od.user_id = User.id
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_price',
          ],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(COUNT(od.id),0)
                        FROM \`order\` as od
                        WHERE 
                            od.user_id = User.id
                            AND
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_order',
          ],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount),0)
                        FROM \`order_item\` as order_item
                        inner join \`order\` as od on od.id = order_item.order_id
                        WHERE 
                            od.user_id = User.id
                            AND
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'total_amount',
          ],
          //   [sequelize.fn('SUM', sequelize.col('Orders->OrderItems.amount')), 'total_amount'],
          //   [sequelize.fn('COUNT', sequelize.col('Orders.id')), 'total_order'],
        ],
        include: [
          {
            model: Order,
            // required: false,
            attributes: ['id', 'code', 'create_at'],
            where: whereOptionOrder,
            include: {
              model: OrderItem,
              // required: false,
              attributes: ['id', 'price', 'amount'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
        ],
        group: ['User.id'],
        order: [
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount),0)
                        FROM \`order_item\` as order_item
                        inner join \`order\` as od on od.id = order_item.order_id
                        WHERE 
                            od.user_id = User.id
                            AND
                            od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
            'desc',
          ],
          ['id', 'desc'],
        ],
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(
        { rows, listShop },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
      // return withSuccess(reportCategory);
    }
    // return withPagingSuccess(1);
  }

  /**
   * @summary get list report_liveStream (báo cáo livestream shop + admin)
   */
  // @Security('jwt')
  @Get('/report-livestream-admin')
  public async listReportLiveStreamShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    const { offset, limit, page } = handlePagingMiddleware(request);
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
    }
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && { name: { [Op.like]: `%${search}%` } }),
    };
    // // danh sách listShop có livestream
    const { rows, count } = await Shop.findAndCountAll({
      // subQuery: false,
      attributes: [
        'id',
        'name',
        'agora_project_id',
        'create_at',
        [
          sequelize.literal(`(
            SELECT COUNT(*)
              FROM livestream AS livestream
              WHERE
              livestream.is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and 
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'total_livestream',
        ],
        [
          sequelize.literal(`(
            SELECT SUM(livestream.count_viewed)
              FROM livestream
              WHERE
              is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'count_viewed',
        ],
        [
          sequelize.literal(`(
            SELECT SUM(livestream.count_comment)
              FROM livestream
              WHERE
              is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'count_comment',
        ],
        [
          sequelize.literal(`(
            SELECT SUM(livestream.count_reaction)
              FROM livestream
              WHERE
              is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'count_reaction',
        ],
        [
          sequelize.literal(`(
            select SEC_TO_TIME(CONVERT(Sum(TIME_TO_SEC(TIMEDIFF(IF(expire_at < finish_at,expire_at,finish_at), start_at))), SIGNED INTEGER))
              FROM livestream
              WHERE
              is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'total_time_live',
        ],
        [
          sequelize.literal(`(
            select SEC_TO_TIME(CONVERT(Sum(TIME_TO_SEC(TIMEDIFF(IF(expire_at < finish_at,expire_at,finish_at), start_at)))/COUNT(id), SIGNED INTEGER))
              FROM livestream
              WHERE
              is_active = ${IS_ACTIVE.ACTIVE}
              AND
              livestream.shop_id = Shop.id
              and
              livestream.status = ${LIVESTREAM_STATUS.FINISHED}
              )`),
          'avg_time_live',
        ],
      ],
      where: whereOption,
      // include: [
      //     {
      //         model: Livestream,
      //         attributes: {
      //             exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      //         },
      //         // attributes: [],
      //         required: true,
      //         where: { is_active: IS_ACTIVE.ACTIVE },
      //         // include: [
      //         //     {
      //         //         model: Shop,
      //         //         attributes: {
      //         //             exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      //         //         },
      //         //         where: {
      //         //             livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
      //         //             status: SHOP_STATUS.ACTIVE,
      //         //             is_active: IS_ACTIVE.ACTIVE,
      //         //         },
      //         //     },
      //         // ],
      //     }
      // ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['id'],
    });
    // return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
    // const filterPromise = rows
    //   // .filter((item: any) => item.dataValues.agora_project_id != null)
    //   .map((item: any) => {
    //     const valueItem = item.dataValues;
    //     const data = item.dataValues.agora_project_id;
    //     // return valueItem;
    //     // return valueItem[0].Shop.dataValues.agora_app_id;
    //     if (data != null) {
    //     } else {
    //       item.setDataValue('count_subcriber', 0);
    //       item.setDataValue('avg_time_live', 0);
    //     }
    //   });
    // // const filterPromisea = rows
    // //     .filter((item: any) => item.dataValues.agora_project_id == null)
    // //     .map((item: any) => {
    // //         item.setDataValue('count_subcriber', 0);
    // //     });

    // // return withSuccess(filterPromise);
    // await Promise.all(filterPromise);
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
  }

  /**
   * @summary get list report_liveStream (báo cáo livestream shop)
   */
  @Security('jwt')
  @Get('/report-livestream')
  public async listReportLiveStreamAdmin(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    // // danh sách listShop có livestream
    // const listShop = await Shop.findAll(
    //     {
    //         attributes: ["id", "name"],
    //         where: { is_active: IS_ACTIVE.ACTIVE, id: role == ROLE.ADMIN ? { [Op.ne]: null } : null },
    //         include: [
    //             {
    //                 model: Order,
    //                 attributes: [],
    //                 required: true,
    //                 where: { is_active: IS_ACTIVE.ACTIVE },
    //                 include: {
    //                     model: OrderItem,
    //                     attributes: [],
    //                     where: { is_active: IS_ACTIVE.ACTIVE },
    //                     include:
    //                     {
    //                         model: Product, where: { is_active: IS_ACTIVE.ACTIVE },
    //                     }

    //                 }
    //             }
    //         ]
    //     },
    // )
    // return withSuccess(listShop)
    // return withSuccess(shopId);
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      shop_id: shopId ? shopId : { [Op.ne]: null },
      ...(search && { title: shopId ? { [Op.like]: `%${search}%` } : { [Op.ne]: null } }),
      // ...(search && { [Op.or]: { code: { [Op.like]: `%${search}%` }, name: { [Op.like]: `%${search}%` } } }),
    };
    const { count, rows } = await Livestream.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
            ...(search && { name: shopId ? { [Op.ne]: null } : { [Op.like]: `%${search}%` } }),
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['id'],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
    // return withSuccess(reportCategory);
  }
  /**
   * @summary get list report_liveStream all (báo cáo livestream shop)
   */
  @Security('jwt')
  @Get('/report-livestream-all')
  public async listReportLiveStreamAdminAll(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      shop_id: shopId ? shopId : { [Op.ne]: null },
      ...(search && { title: shopId ? { [Op.like]: `%${search}%` } : { [Op.ne]: null } }),
    };
    const livestreamAll = await Livestream.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
            ...(search && { name: shopId ? { [Op.ne]: null } : { [Op.like]: `%${search}%` } }),
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [['id', 'desc']],
    });
    return withSuccess(livestreamAll);
    // return withSuccess(reportCategory);
  }

  /**
   * @summary get list report_Shop (báo cáo gian hàng admin)
   */
  @Security('jwt', ['admin'])
  @Get('/report-shop')
  public async listReportShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      // is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && { name: { [Op.like]: `%${search}%` } }),
      status: SHOP_STATUS.ACTIVE,
      // ...(search && { [Op.or]: { code: { [Op.like]: `%${search}%` }, name: { [Op.like]: `%${search}%` } } }),
    };
    const { count, rows } = await Shop.findAndCountAll({
      where: whereOption,
      subQuery: false,
      attributes: [
        'id',
        'name',
        'stream_minutes_available',
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(od.total_price),0)
                        FROM \`order\` as od
                        WHERE od.shop_id = Shop.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                        )`),
          'total_price',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount),0)
                        FROM \`order_item\` as order_item
                        join \`order\` as od
                        ON order_item.order_id = od.id
                        WHERE od.shop_id = Shop.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_product_order',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(pakage_history.price),0)
                        FROM \`pakage_history\` as pakage_history
                        WHERE 
                            pakage_history.shop_id = Shop.id
                            AND
                            pakage_history.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_price_package',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(COUNT(od.id),0)
                        FROM \`order\` as od
                        WHERE 
                            od.shop_id = Shop.id
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                        )`),
          'total_order',
        ],

        [
          Sequelize.literal(`(
                        SELECT IFNULL(COUNT(pro.id),0)
                        FROM \`product\` as pro
                        WHERE 
                            pro.shop_id = Shop.id
                            AND
                            pro.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            pro.status = ${PRODUCT_STATUS.AVAILABLE}
                        )`),
          'total_product',
        ],

        // [sequelize.fn('COUNT', sequelize.literal('`Products`.`id`')), 'total_product'],
        [sequelize.fn('COUNT', sequelize.literal('`Livestreams`.`id`')), 'total_live'],
      ],
      include: [
        // {
        //   model: Product,
        //   required: false,
        //   attributes: [],
        //   where: { is_active: IS_ACTIVE.ACTIVE, status: PRODUCT_STATUS.AVAILABLE },
        // },
        {
          model: Livestream,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: LIVESTREAM_STATUS.FINISHED },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['shop.id'],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
    // return withSuccess(reportCategory);
  }
}
