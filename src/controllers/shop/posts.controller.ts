import { Security, Get, Query, Route, Tags, Request, Delete } from 'tsoa';
import { AppTypes } from 'types';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE, MEDIA_TYPE, IS_POSTED, PRODUCT_STOCK_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, Post: _Post, PostMedia, Shop, User, Reaction, Topic, Product } = db.default;
const { Op } = Sequelize;

@Route('shop/post')
@Tags('shop/posts')
export class ShopPostsController extends ApplicationController {
  constructor() {
    super('Post');
  }

  /**
   *  @summary get list post ('shop', 'shop_member')
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/')
  public async getListPost(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('user_id') user_id?: number,
    @Query('topic_id') topic_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    // @Query('status') status?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id: loggedInUser.shop_id,
      is_posted: IS_POSTED.POSTED,
      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          // { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          // { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          // { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (topic_id) {
      whereOption.topic_id = topic_id;
    }

    const whereSubOption = { is_active: IS_ACTIVE.ACTIVE };
    const baseUrl = Utils.getBaseServer(request);
    const { count, rows } = await _Post.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT COUNT(*)
              FROM reaction AS reaction
              WHERE
              reaction.is_active = ${IS_ACTIVE.ACTIVE}
              AND reaction.user_id = ${loggedInUser?.id || null}
              AND reaction.post_id = Post.id
              AND reaction.comment_id is null
            )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'app_token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Reaction,
          required: false,
          attributes: [],
          // {
          //   include: [],
          //   exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          // },
          where: loggedInUser
            ? { is_active: IS_ACTIVE.ACTIVE, user_id: loggedInUser.id }
            : { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(rows) ? rows.length : count });
  }

  /**
   *  @summary get detail post (any)
   */
  @Security('jwt')
  @Get('/{id}')
  public async getDetailPost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const foundPost = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
        is_posted: IS_POSTED.POSTED,
      },
      attributes: {
        include: [
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM reaction AS reaction
            WHERE
            reaction.is_active = ${IS_ACTIVE.ACTIVE}
            AND reaction.user_id = ${loggedInUser.id}
            AND reaction.post_id = Post.id
            AND reaction.comment_id is null
          )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      // logging: console.log,
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn  tại.');
    return withSuccess(foundPost);
  }

  /**
   * @summary delete post by id (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Delete('/{id}')
  public async deletePost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPost = await this._findOne({
      where: {
        id,
        shop_id: loggedInUser.shop_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    await foundPost.update({
      is_active: IS_ACTIVE.INACTIVE,
      delete_by: loggedInUser.id,
      delete_at: new Date(),
    });
    return withSuccess({});
  }
}
