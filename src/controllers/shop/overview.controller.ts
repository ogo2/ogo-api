import { withSuccess } from '@utils/BaseResponse';
import { IS_ACTIVE, LIVESTREAM_ENABLE, LIVESTREAM_STATUS, ORDER_STATUS, SHOP_STATUS } from '@utils/constants';
import { Get, Query, Request, Route, Security, Tags } from 'tsoa';
import { AppTypes } from 'types';
import { ApplicationController } from '../';
const db = require('@models');
const {
  sequelize,
  Sequelize,
  Shop,
  Livestream,
  LivestreamLayout,
  PakageHistory,
  Order,
  Product,
  OrderItem,
  Post,
  UserAddress,
  User,
} = db.default;
const { Op } = Sequelize;

@Route('shop/overview')
@Tags('shop/overviews')
export class ShopOverviewController extends ApplicationController {
  constructor() {
    super('Overview');
  }
  /**
   * @summary Overview (shop)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/')
  public async listOverview(
    @Request() request: AppTypes.RequestAuth,
    @Query('from_date') from_date?: Date | number,
    @Query('to_date') to_date?: Date | number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    let data = {
      labels: [],
      datasets: [
        {
          label: 'Chờ xác nhận',
          data: [],
          fill: false,
          borderColor: 'orange',
          backgroundColor: 'orange',
          tension: 0.3,
        },
        {
          label: 'Đã xác nhận',
          data: [],
          fill: false,
          borderColor: '#8900f7',
          backgroundColor: '#8900f7',
          tension: 0.3,
        },
        {
          label: 'Đang giao',
          data: [],
          fill: false,
          borderColor: '#1890ff',
          backgroundColor: '#1880ff',
          tension: 0.3,
        },
        {
          label: 'Hủy',
          data: [],
          fill: false,
          borderColor: 'tomato',
          backgroundColor: 'tomato',
          tension: 0.3,
        },
        {
          label: 'Hoàn thành',
          data: [],
          fill: false,
          borderColor: '#54e854',
          backgroundColor: '#54e854',
          tension: 0.3,
        },
      ],
    };
    let dataLivestream = {
      labels: [],
      datasets: [
        {
          label: 'Lượt xem',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'steelblue',
          borderColor: 'steelblue',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'steelblue',
          pointBackgroundColor: 'rgba(75,192,192,0.4)',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'steelblue',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [],
        },
      ],
    };
    let count_pending_date = 0,
      count_confirmed_date = 0,
      count_cancel_date = 0,
      count_ship_date = 0,
      count_success_date = 0,
      count_livestream_date = 0;
    const now = new Date();
    let fromDateQueryMonth = new Date(from_date).getTime();
    let toDateQueryMonth = new Date(to_date).getTime();
    const count_date = (toDateQueryMonth - fromDateQueryMonth) / 86400000;

    for (let index = 0; index < count_date; index++) {
      const date = new Date(fromDateQueryMonth + index * 86400000);
      data.labels.push(`${date.getDate()}`);
      dataLivestream.labels.push(`${date.getDate()}`);
      const fromDateQuery = new Date(fromDateQueryMonth + index * 86400000).toISOString();
      const toDateQuery = new Date(fromDateQueryMonth + (index + 1) * 86400000).toISOString();
      await Promise.all([
        (count_pending_date = await Order.count({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            status: ORDER_STATUS.PENDING,
            shop_id: loggedInUser.shop_id,
            create_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
          },
        })),
        (count_confirmed_date = await Order.count({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            status: ORDER_STATUS.CONFIRMED,
            shop_id: loggedInUser.shop_id,
            create_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
          },
        })),
        (count_cancel_date = await Order.count({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            status: ORDER_STATUS.CANCELED,
            shop_id: loggedInUser.shop_id,
            create_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
          },
        })),
        (count_ship_date = await Order.count({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            shop_id: loggedInUser.shop_id,
            status: ORDER_STATUS.SHIP,
            create_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
          },
        })),
        (count_success_date = await Order.count({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            status: ORDER_STATUS.SUCCCESS,
            shop_id: loggedInUser.shop_id,
            create_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
          },
        })),
        (count_livestream_date = await Livestream.findAll({
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            shop_id: loggedInUser.shop_id,
            start_at: {
              [Op.and]: [{ [Op.gte]: fromDateQuery }, { [Op.lt]: toDateQuery }],
            },
            [Op.or]: [{ status: LIVESTREAM_STATUS.STREAMING }, { status: LIVESTREAM_STATUS.FINISHED }],
          },
          attributes: {
            include: [[sequelize.fn('SUM', sequelize.col('Livestream.count_viewed')), 'count_viewed']],
            exclude: ['count_viewed', 'create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          include: [
            {
              model: Shop,
              required: true,
              where: {
                livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
                status: SHOP_STATUS.ACTIVE,
                is_active: IS_ACTIVE.ACTIVE,
              },
            },
            {
              model: LivestreamLayout,
              required: false,
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        })),
      ]);

      data.datasets[0].data.push(count_pending_date);
      data.datasets[1].data.push(count_confirmed_date);
      data.datasets[2].data.push(count_ship_date);
      data.datasets[3].data.push(count_cancel_date);
      data.datasets[4].data.push(count_success_date);
      dataLivestream.datasets[0].data.push(
        count_livestream_date[0].dataValues.count_viewed ? +count_livestream_date[0].dataValues.count_viewed : 0,
      );
    }

    let count_post = 0,
      count_turnover,
      count_livestream = 0,
      count_product = 0;

    let count_pending = 0,
      count_confirmed = 0,
      count_cancel = 0,
      count_ship = 0,
      count_success = 0;

    const fromDate = new Date(from_date).toISOString().split('T')[0];
    const toDate = new Date(to_date).toISOString().split('T')[0];
    await Promise.all([
      (count_turnover = await Order.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.SUCCCESS,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
        attributes: {
          include: [
            [sequelize.fn('SUM', sequelize.col('Order.total_price')), 'total_price'],
            [
              Sequelize.literal(`(
                        SELECT SUM(IFNULL(od.total_price, 0))
                        FROM \`order\` as od
                        WHERE
                            od.shop_id = ${loggedInUser.shop_id}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND od.create_at ${from_date ? `>='${fromDate}'` : 'is not null'}
                            AND od.create_at ${from_date ? `<='${toDate}'` : 'is not null'}
                        )`),
              'price',
            ],
          ],
          exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
        },
        include: [
          {
            model: OrderItem,
            required: false,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
            },
            // where: {
            //   // is_active: IS_ACTIVE.ACTIVE,
            // },
          },
          {
            model: UserAddress,
            request: false,
            attributes: ['id', 'name', 'phone'],
            // as: 'customer',
            // where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Shop,
            request: false,
            attributes: ['id', 'name', 'phone'],
            // as: 'customer',
          },
          {
            model: User,
            request: false,
            attributes: ['id', 'name', 'phone'],
            // as: 'customer',
            // where: { is_active: IS_ACTIVE.ACTIVE },
          },
        ],
      })),
      (count_post = await Post.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_livestream = await Livestream.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_product = await Product.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_pending = await Order.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.PENDING,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_confirmed = await Order.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.CONFIRMED,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_cancel = await Order.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.CANCELED,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_ship = await Order.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.SHIP,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
      (count_success = await Order.count({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          status: ORDER_STATUS.SUCCCESS,
          shop_id: loggedInUser.shop_id,
          create_at: {
            [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
          },
        },
      })),
    ]);

    const listLivestream = await Livestream.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: loggedInUser.shop_id,
        start_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        [Op.or]: [{ status: LIVESTREAM_STATUS.STREAMING }, { status: LIVESTREAM_STATUS.FINISHED }],
      },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [
        ['status', 'asc'],
        ['id', 'desc'],
      ],
    });

    const TkLivestream = await Livestream.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: loggedInUser.shop_id,
        start_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        [Op.or]: [{ status: LIVESTREAM_STATUS.STREAMING }, { status: LIVESTREAM_STATUS.FINISHED }],
      },
      attributes: {
        include: [
          [sequelize.fn('COUNT', sequelize.col('Livestream.id')), 'count_livestream'],
          [sequelize.fn('MAX', sequelize.col('Livestream.count_viewed')), 'count_viewed'],
          [sequelize.fn('SUM', sequelize.col('Livestream.count_reaction')), 'count_reaction'],
          [sequelize.fn('SUM', sequelize.col('Livestream.count_comment')), 'count_comment'],
        ],
        exclude: [
          'count_viewed',
          'count_reaction',
          'count_comment',
          'create_by',
          'update_by',
          'delete_by',
          'delete_at',
          'version',
        ],
      },
      include: [
        {
          model: Shop,
          required: true,
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamLayout,
          required: false,
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    let start_time = 0,
      finish_time = 0;

    listLivestream.map((item, index) => {
      if (item.finish_at) {
        start_time = start_time + new Date(item.start_at).getTime();
        finish_time = finish_time + new Date(item.finish_at).getTime();
      }
    });
    return withSuccess({
      count_turnover: count_turnover[0].dataValues.price ? count_turnover[0].dataValues.price : 0,
      count_livestream: count_livestream,
      count_post: count_post,
      count_product: count_product,
      order: {
        count_pending: count_pending,
        count_confirmed: count_confirmed,
        count_ship: count_ship,
        count_cancel: count_cancel,
        count_success: count_success,
      },
      chart_order: data,
      list_livestream: listLivestream,
      chart_livestream: dataLivestream,
      livestream: TkLivestream,
      livestream_view: (finish_time - start_time) / (TkLivestream[0].dataValues.count_livestream * 60000),
    });
  }
}
