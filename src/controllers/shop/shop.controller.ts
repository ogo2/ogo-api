import {
  IS_ACTIVE,
  SHOP_STATUS,
  USER_STATUS,
  LIVESTREAM_ENABLE,
  SHOP_ROLE,
  TAB_SHOP,
  ORDER_STATUS,
  ROLE_NAMES,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Body, Request, Get, Post, Put, Query, Route, Delete, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes, ShopTypes } from 'types';
import { ShopService } from '@services/internal/shop.service';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Shop,
  Product,
  Pakage,
  PakageHistory,
  PakageCategory,
  Order,
  Livestream,
  DFTypeUser,
  UserAddress,
  DFProvince,
  DFDistrict,
  DFWard,
  ProductStock,
  OrderItem,
  Category,
  Stock,
} = db.default;
const { Op } = Sequelize;
@Route('shop/shop')
@Tags('shop/shops')
export class ShopShopController extends ApplicationController {
  private shopService: ShopService;
  constructor() {
    super('Shop');
    this.shopService = new ShopService();
  }

  /**
   * @summary create account of by shop (shop)
   */
  @Security('jwt', ['shop'])
  @Post('/account')
  public async createAccountShop(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: ShopTypes.AddShopModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const shopData = await this._findOne({
      where: { id: loggedInUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!shopData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Shop không tồn tại.');

    const bodyData = await this.shopService.AddShopSchema.validateAsync({
      ...body,
      df_type_user_id: SHOP_ROLE.SHOP_MEMBER,
    });

    const user = await User.findOne({
      where: {
        [Op.or]: [{ user_name: bodyData.phone }, { email: bodyData.email }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (user) {
      if (user.dataValues.phone === bodyData.phone)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Số điện thoại này đã tồn tại.');
      if (user?.dataValues?.email === bodyData.email)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Email này đã tồn tại.');
    }
    const createdUser = await User.create({
      ...bodyData,
      user_name: bodyData.phone,
      create_by: loggedInUser.id,
      shop_id: shopData.id,
    });
    return withSuccess(createdUser);
  }

  /**
   * @summary update info accout off shop (shop)
   */
  @Security('jwt', ['shop'])
  @Put('/{user_id}/account')
  public async updateShop(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: ShopTypes.UpdateShopModel,
    user_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundUser = await User.findOne({
      where: { id: user_id, shop_id: loggedInUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Dữ liệu không tồn tại.');
    const bodyData: ShopTypes.UpdateShopModel = await this.shopService.UpdateShopSchema.validateAsync(body);
    const user = await User.findOne({
      where: {
        id: { [Op.ne]: user_id },
        email: bodyData.email,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (user) {
      throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Email này đã tồn tại.');
    }
    const payload: any = {
      name: bodyData.name,
      email: bodyData.email,
      status: bodyData.status,
      update_by: loggedInUser.id,
      update_at: new Date(),
    };
    if (bodyData.profile_picture_url) {
      payload.profile_picture_url = bodyData.profile_picture_url;
    }
    const createdUser = await User.update(payload, { where: { id: user_id, is_active: IS_ACTIVE.ACTIVE } });
    return withSuccess(createdUser);
  }
  /**
   * @summary get list account of shop (shop)
   */
  @Security('jwt', ['shop'])
  @Get('/account')
  public async getListShopOwner(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id: loggedInUser.shop_id,
      id: { [Op.ne]: loggedInUser.id },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
    };
    if (status === SHOP_STATUS.ACTIVE || status === SHOP_STATUS.INACTIVE) {
      whereOption.status = status;
    }
    const { count, rows } = await User.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: [
          'token',
          'code_reset_password',
          'expired_reset_password',
          'password',
          'create_by',
          'update_by',
          'delete_by',
          'version',
        ],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary delete list account of shop by uid: Array<number> (shop)
   */
  @Security('jwt', ['shop'])
  @Delete('/account')
  public async deleteUserAdmin(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const is_include: boolean = body.id.includes(loggedInUser.id);
    if (is_include) {
      throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Không thể xóa tài khoản đang đăng nhập.');
    }
    const resUpdate = await User.update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          shop_id: loggedInUser.shop_id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    if (resUpdate[0] === 0) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Tài khoản không tồn tại.');
    return withSuccess({});
  }

  /**
   * @summary change account status of shop  (1 <=> 0) (shop)
   */
  @Security('jwt', ['shop'])
  @Put('/{user_id}/account/status')
  public async changeUserStatusAccount(
    @Request() request: AppTypes.RequestAuth,
    user_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    if (loggedInUser.id === user_id) {
      throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Không thay đổi trạng thái tài khoản đang đăng nhập.');
    }
    const foundUser = await User.findOne({
      where: { id: user_id, shop_id: loggedInUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST).with('Tài khoản không tồn tại.');
    await User.update(
      {
        status: foundUser.status === USER_STATUS.ACTIVE ? USER_STATUS.INACTIVE : USER_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      {
        where: { id: user_id, is_active: IS_ACTIVE.ACTIVE },
      },
    );
    return withSuccess({
      status: foundUser.status === USER_STATUS.ACTIVE ? USER_STATUS.INACTIVE : USER_STATUS.ACTIVE,
    });
  }

  /**
   * @summary reset password account of shop by uid (shop)
   */
  @Security('jwt', ['shop'])
  @Put('/{user_id}/account/reset-password')
  public async resetPasswordAccount(
    @Request() request: AppTypes.RequestAuth,
    user_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user.data;
    const foundUser = await User.findOne({
      where: { id: user_id, shop_id: loggedInUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) {
      throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST).with('Tài khoản không tồn tại.');
    }
    await foundUser.update({
      update_by: loggedInUser.id,
      password: foundUser.phone,
      token: null,
      update_at: new Date(),
    });
    return withSuccess({});
  }

  /**
   * @summary detailShop
   */
  @Security('jwt', [ROLE_NAMES.SHOP, ROLE_NAMES.SHOP_MEMBER])
  @Get('/{shop_id}/detail-shop')
  public async getDetailShop(
    @Request() request: AppTypes.RequestAuth,
    shop_id: number,
    @Query('type') type?: any,
    @Query() search?: any,
    @Query() status?: any,
    // @Query() stock?: any,
    @Query() category_id?: any,
    @Query() stock_id?: any,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('order_status') order_status?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    if (type == TAB_SHOP.HISTORY) {
      const whereOption: any = {
        shop_id,
        is_active: IS_ACTIVE.ACTIVE,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      const detailShop = Shop.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id },
      });
      const { count, rows } = await PakageHistory.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess({ detailShop, rows }, { page, limit, totalItemCount: count });
    }
    if (type == TAB_SHOP.PRODUCT) {
      const listCategory = await Category.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          parent_id: { [Op.ne]: null },
        },
        include: {
          model: Product,
          where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id },
          attributes: [],
        },
      });

      const listStock = await Stock.findAll({
        attributes: ['id', 'name'],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
        },
        // include: {
        //   model: Product,
        //   where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id },
        //   attributes: []
        // }
      });

      const whereOptions: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      if (status != undefined && status != null && status != '') {
        whereOptions.status = status;
      }
      // if (stocks_status != undefined && stocks_status != undefined && status != "") {
      //     whereOptions.stock_status = stocks_status;
      // }

      const { rows, count } = await Product.findAndCountAll({
        subQuery: false,
        attributes: [
          // include: [[Sequelize.fn('COUNT', 'product_prices.id'), 'stock_status']],
          'id',
          'name',
          'code',
          'status',
          'stock_status',
          [Sequelize.literal('`Category`.`name`'), 'category_name'],
          [
            Sequelize.literal(`(
                          SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                          FROM \`order\` as od
                          join order_item as order_item
                          ON order_item.order_id = od.id
                          WHERE od.shop_id = ${shop_id}
                              AND
                              od.is_active = ${IS_ACTIVE.ACTIVE}
                              AND
                              order_item.product_id = Product.id
                              AND
                              order_item.is_active = ${IS_ACTIVE.ACTIVE}
                          )`),
            'total_price',
          ],
          [sequelize.fn('SUM', sequelize.literal('IFNULL((`OrderItems`.`amount`),0)')), 'total_amount'],
        ],
        where: whereOptions,
        // ...(category_id && { include: [{ model: Category, where: { id: category_id } }] }),
        include: [
          {
            // required: category_id != undefined && category_id != null ? true : false,
            model: Category,
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
            include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              id:
                category_id != undefined && category_id != null && category_id != '' ? category_id : { [Op.ne]: null },
            },
          },
          {
            model: OrderItem,
            required: false,
            // attributes: ['id'],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id != undefined && stock_id != null && stock_id != '' ? stock_id : { [Op.ne]: null },
            },
            include: {
              model: Order,
              // required: false,
              attributes: ['id'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
          // { model: ProductMedia, attributes: ["id", "media_url"],required: false },
          {
            model: ProductStock,
            // required: false,
            attributes: ['id', 'product_id', 'stock_id'],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id != undefined && stock_id != null && stock_id != '' ? stock_id : { [Op.ne]: null },
            },
            // having: sequelize.where(sequelize.literal('sum(`ProductPrices`.status'), stocks_status == 1 ? ">" : "<=", stocks_status == 1 ? 0 : 0),
          },
        ],
        group: ['product.id'],
        order: [['id', 'desc']],
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(
        { rows, listCategory, listStock },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    }

    if (type == TAB_SHOP.ORDER) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && { [Op.or]: { code: { [Op.like]: `%${search}%` } } }),
      };
      if (order_status) {
        whereOption.status = {
          [Op.in]: ![null, ''].includes(order_status) ? [order_status] : Object.values(ORDER_STATUS),
        };
      }
      const { rows, count } = await Order.findAndCountAll({
        where: whereOption,
        subQuery: false,
        attributes: [
          'id',
          'code',
          'user_id',
          'user_address_id',
          [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
          [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
          'create_at',
          'update_at',
          'total_price',
          // [
          //   Sequelize.literal(`(
          //               SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
          //               FROM \`order\` as od
          //               join order_item as order_item
          //               ON order_item.order_id = od.id
          //               WHERE
          //                   od.is_active = ${IS_ACTIVE.ACTIVE}
          //                   AND
          //                   od.id = Order.id
          //                   AND
          //                   od.shop_id = ${shop_id}
          //                   AND
          //                   order_item.is_active = ${IS_ACTIVE.ACTIVE}
          //               )`),
          //   'total_price',
          // ],
          // [sequelize.fn('SUM', sequelize.col('OrderItems.price')), 'total_price'],
          [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
          'status',
        ],
        include: [
          {
            model: UserAddress,
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          {
            model: User,
            // required: false,
            attributes: ['id', 'name', 'phone'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: OrderItem,
            attributes: ['id', 'price', 'amount'],
            // as: 'customer',
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        ],
        order: [['create_at', 'DESC']],
        group: [['Order.id']],
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
    }
    if (type == TAB_SHOP.LIVESTREAM) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && { [Op.or]: { title: { [Op.like]: `%${search}%` } } }),
      };
      const { count, rows } = await Livestream.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
        },
        include: [
          {
            model: Shop,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
            },
            where: {
              livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
              status: SHOP_STATUS.ACTIVE,
              is_active: IS_ACTIVE.ACTIVE,
            },
          },
        ],
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: count });
    }
  }

  /**
   * get data statistic shop (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/statistic-shop')
  public async getDetailStatisticShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('type') type?: number | any,
    @Query('search') search?: string | any,
    @Query('status') status?: number | any,
    @Query('category_id') category_id?: number | any,
    @Query('stock_id') stock_id?: number | any,
    @Query('from_date') from_date?: Date | number | any,
    @Query('to_date') to_date?: Date | number | any,
    @Query('order_status') order_status?: number | any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const shop_id = loggedInUser.shop_id;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    if (type === TAB_SHOP.HISTORY) {
      const whereOption: any = {
        shop_id,
        is_active: IS_ACTIVE.ACTIVE,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      const detailShop = Shop.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id },
      });
      const { count, rows } = await PakageHistory.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess({ detailShop, rows }, { page, limit, totalItemCount: count });
    } else if (type === TAB_SHOP.PRODUCT) {
      const listCategory = await Category.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          parent_id: { [Op.ne]: null },
        },
        include: {
          model: Product,
          where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id },
          attributes: [],
        },
      });

      const listStock = await Stock.findAll({
        attributes: ['id', 'name'],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
        },
      });
      const whereOptions: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      if (status != undefined && status != null) {
        whereOptions.status = status;
      }
      const { rows, count } = await Product.findAndCountAll({
        subQuery: false,
        attributes: [
          'id',
          'name',
          'code',
          'status',
          'stock_status',
          [Sequelize.literal('`Category`.`name`'), 'category_name'],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id = ${shop_id}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
            'total_price',
          ],
          [sequelize.fn('SUM', sequelize.literal('IFNULL((`OrderItems`.`amount`),0)')), 'total_amount'],
        ],
        where: whereOptions,
        include: [
          {
            model: Category,
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
            include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              id: category_id != undefined && category_id != null ? category_id : { [Op.ne]: null },
            },
          },
          {
            model: OrderItem,
            required: false,
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id != undefined && stock_id != null ? stock_id : { [Op.ne]: null },
            },
            include: {
              model: Order,
              attributes: ['id'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
          {
            model: ProductStock,
            attributes: ['id', 'product_id', 'stock_id'],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id != undefined && stock_id != null ? stock_id : { [Op.ne]: null },
            },
          },
        ],
        group: ['product.id'],
        order: [['id', 'desc']],
        limit,
        offset,
        logging: console.log,
      });
      return withPagingSuccess(
        { rows, listCategory, listStock },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    } else if (type === TAB_SHOP.ORDER) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && {
          [Op.or]: [
            { code: { [Op.like]: `%${search}%` } },
            { '$User.phone$': { [Op.like]: `%${search}%` } },
            { '$User.name$': { [Op.like]: `%${search}%` } },
          ],
        }),
      };
      if (order_status) {
        whereOption.status = {
          [Op.in]: !order_status || order_status === 0 ? [order_status] : Object.values(ORDER_STATUS),
        };
      }
      const { rows, count } = await Order.findAndCountAll({
        where: whereOption,
        subQuery: false,
        attributes: [
          'id',
          'code',
          'user_id',
          'user_address_id',
          [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
          [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
          'create_at',
          'update_at',
          'total_price',
          [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
          'status',
        ],
        include: [
          {
            model: UserAddress,
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          {
            model: User,
            attributes: ['id', 'name', 'phone'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: OrderItem,
            attributes: ['id', 'price', 'amount'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        ],
        order: [['create_at', 'DESC']],
        group: [['Order.id']],
        limit,
        offset,
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
    } else if (type === TAB_SHOP.LIVESTREAM) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && { [Op.or]: { title: { [Op.like]: `%${search}%` } } }),
      };
      const { count, rows } = await Livestream.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
        },
        include: [
          {
            model: Shop,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
            },
            where: {
              livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
              status: SHOP_STATUS.ACTIVE,
              is_active: IS_ACTIVE.ACTIVE,
            },
          },
        ],
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: count });
    }
  }
}
