require('dotenv').config();
import { Body, Security, Get, Post, Route, Delete, Tags, Request, Query } from 'tsoa';
import * as _ from 'lodash';
import { withSuccess } from '../../utils/BaseResponse';
import { IS_ACTIVE, LIVESTREAM_ENABLE } from '@utils/constants';
import { AppTypes } from 'types';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { GoogleAuth2Service, CredentialsInput } from '@services/google/googleOAuth2.service';
import { GoogleService } from '@services/google/google.service';
import { UserService } from '@services/internal/user.service';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, Shop } = db.default;
const { Op } = Sequelize;

@Route('shop/config-livestream')
@Tags('shop/config-livestreams')
export class ShopConfigLivestreamController extends ApplicationController {
  private userService: UserService;
  private googleService: GoogleService;
  private googleAuth2Service: GoogleAuth2Service;
  constructor() {
    super('Shop');
    this.userService = new UserService();
    this.googleService = new GoogleService();
    this.googleAuth2Service = new GoogleAuth2Service();
  }
  /**
   *  @summary create url request token google account (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/auth')
  public async changeYoutubeLivestreamAccount(
    @Request() request: AppTypes.RequestAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const userInfo = await this.userService.findOneActiveUser(userDecoded.id);
    const url = await this.googleAuth2Service.getNewUrlRequestToken(userInfo.token);
    return withSuccess({ url });
  }

  /**
   *  @summary static authenticate google api
   */

  @Security('google_auth')
  @Get('/goolge/callback')
  public async setCallbackGoogleToken(
    @Request() request: AppTypes.RequestAuth,
    @Query('code') code: string,
    @Query('scope') scope: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    let tokens;
    let info;
    try {
      tokens = await this.googleAuth2Service.getAuthTokens(code);
      info = await this.googleService.getProfileInfo(tokens.access_token);
    } catch (error) {
      throw new AppError(ApiCodeResponse.SERVICE_ERROR).with('Bad request service');
    }
    if (!tokens || !info) {
      throw new AppError(ApiCodeResponse.SERVICE_ERROR).with('Bad request service');
    }
    if (!info.email_verified) {
      throw new AppError(ApiCodeResponse.SERVICE_ERROR).with('Email của bạn chưa được xác minh, vui lòng thử lại.');
    }
    const googleTokens = {
      email: info.email,
      id_token: tokens.id_token,
      access_token: tokens.access_token,
      expiry_date: tokens.expiry_date,
      refresh_token: tokens.refresh_token,
      scope: tokens.scope,
      token_type: tokens.token_type,
    };
    const res = await Shop.update(
      { google_tokens: googleTokens, update_at: new Date(), update_by: userDecoded.id },
      { where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess(res);
  }

  @Security('jwt', ['shop', 'shop_member'])
  @Get('/token')
  public async getGoogleToken(@Request() request: AppTypes.RequestAuth): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const foundShop = await this._findOne({
      where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE },
      attributes: ['livestream_enable', 'google_tokens'],
    });
    return withSuccess(foundShop);
  }

  /**
   *  @summary delete google token (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Delete('/token')
  public async deleteGoogleToken(
    @Request() request: AppTypes.RequestAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const foundShop = await this._findOne({
      where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE },
      attributes: ['id', 'livestream_enable', 'google_tokens'],
    });
    const resUpdate = await this._update({ google_tokens: null }, { where: { id: userDecoded.shop_id } });

    return withSuccess(resUpdate);
  }
}
