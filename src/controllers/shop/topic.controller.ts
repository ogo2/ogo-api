import { Security, Get, Query, Route, Tags, Request } from 'tsoa';
import { AppTypes } from 'types';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { AppError } from '@utils/AppError';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { ApplicationController } from '../';
const db = require('@models');
const { sequelize, Sequelize, Post: _Post, Topic } = db.default;
const { Op } = Sequelize;

@Route('shop/topic')
@Tags('shop/topics')
export class ShopTopicController extends ApplicationController {
  constructor() {
    super('Post');
  }

  /**
   *  @summary get list topic shop use in posts (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/posted')
  public async getListTopic(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          description: { [Op.like]: `%${search}%` },
        },
      }),
    };
    const { count, rows } = await Topic.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: {
        model: _Post,
        required: true,
        attributes: [],
        where: { is_active: IS_ACTIVE.ACTIVE, shop_id: loggedInUser.shop_id },
      },
      limit,
      offset,
      order: [
        ['order', 'ASC'],
        ['create_at', 'DESC'],
      ],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }
}
