import { Body, Security, Get, Post, Route, Delete, Tags, Request, Query } from 'tsoa';
import * as _ from 'lodash';
import { withIOSuccess, withPagingSuccess, withSuccess } from '../../utils/BaseResponse';
import {
  IS_ACTIVE,
  LIVESTREAM_STATUS,
  LIVESTREAM_ENABLE,
  MEDIA_TYPE,
  PRODUCT_STATUS,
  ROLE,
  SHOP_STATUS,
  DF_NOTIFICATION,
  IS_HIGHLIGHT_LIVESTREAM_PRODUCT,
  PRODUCT_STOCK_STATUS,
  LIVESTREAM_TYPE,
  FOLLOW_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { LivestreamApplicationController } from '../';
import { LivestreamService } from '@services/internal/livestream.service';
import { AppTypes, CommentTypes, LivestreamTypes } from 'types';
import { LIVESTREAM_EVENT, withLiveStreamChannel } from '../../websocket/constants';
import { NotificationService } from '@services/internal/notification.service';
import { Utils } from '@utils/Utils';
import { Credentials as GoogleCredentials } from 'google-auth-library';
import { logger } from '@utils/Logger';
import { LivestreamYoutubeService } from '@services/google/youtube.service';
import { SocketUtils } from '../../websocket/SocketUtils';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { EVENTS } from '@services/detributed/pubsub/publisher/events';
// import { publisherService } from '@services/detributed/pubsub/publisher/publisher.service';
const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Shop,
  Livestream,
  Product,
  ProductMedia,
  LivestreamProduct,
  LivestreamLayout,
  Follow,
} = db.default;
const { Op } = Sequelize;

@Route('shop/livestream')
@Tags('shop/livestreams')
export class ShopLivestreamController extends LivestreamApplicationController {
  private livestreamService: LivestreamService;
  private notificationService: NotificationService;
  private livestreamYoutubeService: LivestreamYoutubeService;
  constructor() {
    super('Livestream');
    this.livestreamService = new LivestreamService();
    this.notificationService = new NotificationService();
    this.livestreamYoutubeService = new LivestreamYoutubeService();
  }

  /**
   * @summary list Livestream (shop, shop_member) <status: 0(initial), 1(streaming), 2(finished)>
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/')
  public async list(
    @Request() request: AppTypes.RequestIOAuth,
    @Query('status') status?: number,
    @Query('user_id') user_id?: number,
    @Query('search') search?: string,
    @Query('from_date') from_date?: Date | number | string,
    @Query('to_date') to_date?: Date | number | string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const userDecoded = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const baseUrl = Utils.getBaseServer(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id: userDecoded.shop_id,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (search) whereOption.title = { [Op.like]: `%${search}%` };
    if (Object.values(LIVESTREAM_STATUS).includes(status)) whereOption = { ...whereOption, status };
    if (user_id) whereOption = { ...whereOption, user_id };

    const { count, rows } = await Livestream.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version', 'google_tokens'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`IFNULL((
                   SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                   FROM product_media
                   WHERE
                   product_media.is_active = ${IS_ACTIVE.ACTIVE}
                   and product_media.type = ${MEDIA_TYPE.IMAGE}
                   and product_media.product_custom_attribute_option_id is null
                   and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                   limit 1
                   ),null)`),
                'media_url',
              ],
            ],
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          include: {
            model: Product,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      group: ['id'],
    });

    const listPromises = rows
      .filter((livestream: any) => livestream.status === LIVESTREAM_STATUS.STREAMING)
      .map((livestream) => SocketUtils.countUserInAdapterSocketLivestreamChannel(io, livestream.id));
    const listResPromise = await Promise.all(listPromises);
    listResPromise.forEach((item: any) => {
      if (item) {
        const livestream = rows.find((livestream) => livestream.id === item.livestream_id);
        livestream.setDataValue('count_subscribe', item.count_subscribe);
      }
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary create livestream (shop)
   * @returns room info
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/')
  public async create(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: LivestreamTypes.CreateLivestreamModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const authorizeUser = request?.user?.data;
    const user = await User.findOne({
      where: { id: authorizeUser.id, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: Shop,
          required: true,
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      ],
    });
    if (!user) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Tính năng livestream của bạn đã bị tắt.');
    }
    const livestreamStreaming = await this._findOne({
      where: {
        shop_id: user.dataValues.shop_id,
        status: LIVESTREAM_STATUS.STREAMING,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (livestreamStreaming) {
      throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
        'Không thể tạo livestream lúc này, gian hàng của bạn đang được phát trực tiếp.',
      );
    }
    const minutesAvailable: number = user?.dataValues?.Shop?.dataValues?.stream_minutes_available;
    if (!minutesAvailable || minutesAvailable <= 0) {
      throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
        'Bạn không thể tạo livestream do không đủ số phút, vui lòng nạp thêm.',
      );
    }

    const google_tokens = user?.dataValues?.Shop?.dataValues?.google_tokens;
    if (!google_tokens)
      throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
        'Gian hàng này chưa được thiết lập tài khoản livestream',
      );
    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };
    const bodyData = await this.livestreamService.CreateLivestreamSchema.validateAsync(body);
    const listProductInfo = await Product.findAll({
      where: {
        id: { [Op.in]: bodyData.products.map((item: LivestreamTypes.ProductLivestream) => item.id) },
        shop_id: user.dataValues.shop_id,
        status: PRODUCT_STATUS.AVAILABLE,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: ProductMedia,
          required: false,
          where: {
            product_custom_attribute_option_id: null,
            type: MEDIA_TYPE.IMAGE,
            is_active: IS_ACTIVE.ACTIVE,
          },
          limit: 1,
        },
      ],
    });

    let createdBroadcast;
    try {
      // ATTENTION: you must call setYoutubeService to set youtube service, detected who u are
      await this.livestreamYoutubeService.setYoutubeService(tokens);
      createdBroadcast = await this.livestreamYoutubeService.createYoutubeBroadcast({
        title: bodyData.title,
        scheduledStartTime: new Date().toISOString(),
      });
    } catch (error) {
      createdBroadcast = null;
      throw new AppError(error);
    }

    const livestreamData = await sequelize.transaction(async (transaction) => {
      const createdLivestream = await this._create(
        {
          ...bodyData,
          user_id: user.id,
          count_viewed: 1,
          broadcast_id: createdBroadcast.broadcast_id,
          stream_id: createdBroadcast.stream_id,
          cdn: createdBroadcast.cdn,
          shop_id: user.dataValues.shop_id,
        },
        { transaction },
      );
      const productsData = listProductInfo.map((item) => {
        const product_item = item.dataValues;
        return {
          livestream_id: createdLivestream.id,
          product_id: product_item.id,
          code_product_livestream: bodyData.products.find(
            (e: LivestreamTypes.ProductLivestream) => e.id === product_item.id,
          )?.code_product_livestream,
          product: {
            code: product_item.code,
            name: product_item.name,
            product_media_url: product_item.ProductMedia[0].dataValues.media_url,
          },
        };
      });
      await LivestreamProduct.bulkCreate(productsData, { transaction });
      return createdLivestream;
    });
    livestreamData.setDataValue('createdBroadcast', createdBroadcast);
    return withSuccess(livestreamData);
  }

  /**
   * @summary check status livestream(shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/{livestream_id}/check-status')
  public async checkBroadcastStatus(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;

    const livestreamData = await this._findOne({
      where: {
        id: livestream_id,
        // status: { [Op.ne]: LIVESTREAM_STATUS.INITIAL },
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: Shop,
          required: true,
          where: {
            id: userDecoded.shop_id,
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      ],
    });
    if (!livestreamData) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Livestream này không tồn tại hoặc chưa được bắt đầu.');
    }
    const google_tokens = livestreamData?.dataValues?.Shop?.dataValues?.google_tokens;

    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };
    // ATTENTION: you must call setYoutubeService to set youtube service, detected who u are
    await this.livestreamYoutubeService.setYoutubeService(tokens);
    const result = await this.livestreamYoutubeService.getStatusLivestream({
      broadcast_id: livestreamData.broadcast_id,
    });
    const existedLivestream = await this.livestreamYoutubeService.getExistLivestream({
      broadcast_id: livestreamData.broadcast_id,
    });

    return withSuccess({ result, existedLivestream });
  }

  /**
   * @summary start livestream (shop)
   * @returns livestream info
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/start')
  public async startLiveStream(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const livestream = await this._findOne({
      where: { id: livestream_id, status: LIVESTREAM_STATUS.INITIAL, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: Shop,
          required: true,
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: User,
          required: true,
          attributes: [],
          where: { id: userDecoded.id, is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!livestream) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Livestream này không tồn tại hoặc đã bắt đầu.');
    }

    let streaming = null;
    let products = null;
    await Promise.all([
      (streaming = await this._findOne({
        where: {
          shop_id: userDecoded.shop_id,
          status: LIVESTREAM_STATUS.STREAMING,
          is_active: IS_ACTIVE.ACTIVE,
        },
      })),
      (products = await Product.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
        },
        attributes: {
          include: [
            [
              sequelize.literal(`(
                  SELECT product_stock.stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = Product.id
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
              'stock_id',
            ],
            [
              sequelize.literal(`(
                  SELECT id
                  FROM livestream_product AS lp
                  WHERE
                  lp.is_active = ${IS_ACTIVE.ACTIVE}
                  AND lp.livestream_id = ${livestream_id}
                  AND lp.product_id = Product.id
                )`),
              'livestream_product_id',
            ],
            [
              sequelize.literal(`(
                  SELECT
                  code_product_livestream
                  FROM livestream_product AS lp
                  WHERE
                  lp.is_active = ${IS_ACTIVE.ACTIVE}
                  AND lp.livestream_id = ${livestream_id}
                  AND lp.product_id = Product.id
                )`),
              'code_product_livestream',
            ],
            [
              sequelize.literal(`(
                  SELECT is_highlight
                  FROM livestream_product AS lp
                  WHERE
                  lp.is_active = ${IS_ACTIVE.ACTIVE}
                  AND lp.livestream_id = ${livestream_id}
                  AND lp.product_id = Product.id
                )`),
              'is_highlight',
            ],
            [
              sequelize.literal(`IFNULL((
                    SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                    FROM product_media
                    WHERE
                    product_media.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_media.type = ${MEDIA_TYPE.IMAGE}
                    and product_media.product_custom_attribute_option_id is null
                    and product_media.product_id = Product.id
                    limit 1
                    ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                    SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                    FROM product_price AS product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = Product.id
                    )`),
              'min_max_price',
            ],
          ],
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        include: {
          model: LivestreamProduct,
          required: true,
          attributes: [],
          where: {
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      })),
    ]);

    const google_tokens = livestream?.dataValues?.Shop?.dataValues?.google_tokens;
    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };
    // // open here in production

    // ATTENTION: you must call setYoutubeService to set youtube service, detected who u are
    if (livestream.type === LIVESTREAM_TYPE.STREAMING) {
      await this.livestreamYoutubeService.setYoutubeService(tokens);
      const livestreamExist = await this.livestreamYoutubeService.getExistLivestream({
        broadcast_id: livestream.broadcast_id,
        status: ['ready', 'testing'],
      });
      if (!livestreamExist)
        throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
          'Tài khoản google này đang phát trực tiếp ở nơi khác, vui lòng thử lại.',
        );
    }

    if (streaming)
      throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
        'Không thể bắt đầu livestream này, gian hàng của bạn đang được phát trực tiếp.',
      );
    const minutesAvailable: number = livestream?.dataValues?.Shop?.dataValues?.stream_minutes_available;
    if (!minutesAvailable || minutesAvailable <= 0)
      throw new AppError(ApiCodeResponse.CANT_LIVESTREAM).with(
        'Bạn không thể tạo livestream do không đủ số phút, vui lòng nạp thêm.',
      );
    // open here in production
    // active livestream
    let resultActiveLivestream = null;
    if (livestream.type === LIVESTREAM_TYPE.STREAMING) {
      resultActiveLivestream = await this.livestreamYoutubeService.activeLivestream({
        broadcast_id: livestream.broadcast_id,
      });
    }

    const expireAt: Date = Utils.calculateExpireTimeLivestream(minutesAvailable);
    await sequelize.transaction(async (transaction) => {
      await this._update(
        {
          start_at: new Date(),
          ping_at: new Date(),
          expire_at: expireAt,
          status: LIVESTREAM_STATUS.STREAMING,
          update_by: userDecoded.id,
          update_at: new Date(),
        },
        { where: { id: livestream.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      // tạo noti
      let shopName;
      let listUser;
      let notiType;

      Promise.all([
        (shopName = await this.notificationService.shopName(userDecoded.shop_id)),
        (listUser = await User.findAll({
          where: { is_active: IS_ACTIVE.ACTIVE, df_type_user_id: ROLE.CUSTOMER },
          include: {
            model: Follow,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'version'],
            },
            where: { is_active: IS_ACTIVE.ACTIVE, shop_id: userDecoded.shop_id, status: FOLLOW_STATUS.ACTIVE },
          },
        })),
        (notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.SHOP_CREATE_LIVESTREAM)),
      ]);

      const notifications = listUser.map((projectData) => {
        if (shopName)
          return {
            df_notification_id: notiType.id,
            content: shopName.name + ' đang phát trực tiếp ' + livestream.title,
            data: { live_stream_id: livestream.id },
            user_id: projectData.id,
            title: notiType.title,
          };
      });
      if (shopName) await this.notificationService.createMultiNotification(notifications, transaction);
      // end noti
    });
    return withSuccess({
      product: products,
      channel: withLiveStreamChannel(livestream_id),
      broadcast_id: livestream.broadcast_id,
      stream_id: livestream.stream_id,
      resultActiveLivestream,
    });
  }

  /**
   * @summary push notification livestream (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/notification')
  public async pushNotificationLivestream(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: CommentTypes.NotificationLivestreamModel,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const livestream = await this._findOne({
      where: { id: livestream_id, status: LIVESTREAM_STATUS.STREAMING, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: Shop,
          required: true,
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: User,
          required: true,
          attributes: [],
          where: { id: userDecoded.id, is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!livestream) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Livestream này không tồn tại!');
    }
    // tạo noti
    let shopName;
    let listUser;
    let notiType;

    Promise.all([
      (shopName = await this.notificationService.shopName(userDecoded.shop_id)),
      (listUser = await User.findAll({
        where: { is_active: IS_ACTIVE.ACTIVE, df_type_user_id: ROLE.CUSTOMER },
        include: {
          model: Follow,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE, shop_id: userDecoded.shop_id, status: FOLLOW_STATUS.ACTIVE },
        },
      })),
      (notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.SHOP_CREATE_LIVESTREAM)),
    ]);
    if (!body.content) {
      const notifications = listUser.map((projectData) => {
        if (shopName)
          return {
            df_notification_id: notiType.id,
            content: shopName.name + ' đang phát trực tiếp ' + livestream.title,
            data: { live_stream_id: livestream.id },
            user_id: projectData.id,
            title: notiType.title,
          };
      });
      if (shopName) await this.notificationService.createMultiNotification(notifications);
    } else {
      const notifications = listUser.map((projectData) => {
        if (shopName)
          return {
            df_notification_id: notiType.id,
            content: body.content,
            data: { live_stream_id: livestream.id },
            user_id: projectData.id,
            title: shopName.name,
          };
      });
      if (shopName) await this.notificationService.createMultiNotification(notifications);
    }
    // end noti
    return withSuccess({});
  }

  /**
   * @summary stop livestream (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/stop')
  public async stopLivestream(
    @Request() request: AppTypes.RequestIOAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const userDecoded = request?.user?.data;
    const streaming = await this._findOne({
      where: {
        id: livestream_id,
        user_id: userDecoded.id,
        status: LIVESTREAM_STATUS.STREAMING,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: { exclude: ['create_by', 'update_by', 'delete_by', 'version'] },
      include: [
        {
          model: Shop,
          required: true,
          where: { livestream_enable: LIVESTREAM_ENABLE.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
          attributes: {
            exclude: ['pancake_shop_key', 'pancake_shop_id', 'create_by', 'update_by', 'delete_by', 'version'],
          },
        },
        {
          model: User,
          required: true,
          where: { id: userDecoded.id, is_active: IS_ACTIVE.ACTIVE },
          attributes: { exclude: ['token', 'password', 'create_by', 'update_by', 'delete_by', 'version'] },
        },
      ],
    });
    if (!streaming) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Kênh livestream không tồn tại, hoặc livestream này đã kết thúc.',
      );
    }
    const google_tokens = streaming?.dataValues?.Shop?.dataValues?.google_tokens;
    const tokens: GoogleCredentials = {
      id_token: google_tokens.id_token,
      access_token: google_tokens.access_token,
      token_type: google_tokens.token_type,
      expiry_date: google_tokens.expiry_date,
      refresh_token: google_tokens.refresh_token,
      scope: google_tokens.scope,
    };

    const { resultStopLivestream } = await this.livestreamService.stopStreaming(streaming, tokens);
    streaming.dataValues.Shop.setDataValue('Users', undefined);
    streaming.setDataValue('resultStopLivestream', resultStopLivestream);
    streaming.setDataValue('finish_at', new Date());
    const data = { livestream_id: streaming.id };
    const room = withLiveStreamChannel(livestream_id);
    io.to(room).emit(room, withIOSuccess({}, LIVESTREAM_EVENT.SHOP_STOP_LIVESTREAM));
    // await publisherService.publish(EVENTS.STOP_LIVESTREAM, data);
    return withSuccess(streaming);
  }

  /**
   * @summary create or update product while streaming (shop)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/product')
  public async createUpdateProduct(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: { products: Array<LivestreamTypes.ProductLivestream> },
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseImageUrl();
    const livestream = await this._findOne({
      where: {
        id: livestream_id,
        shop_id: userDecoded.shop_id,
        status: { [Op.ne]: LIVESTREAM_STATUS.FINISHED },
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: {
        model: Shop,
        required: true,
        attributes: ['id', 'name', 'profile_picture_url', 'agora_app_id', 'agora_app_certificate'],
        where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE },
      },
    });
    if (!livestream) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Livestream này không tổn tại hoặc đang không được phát trực tiếp!',
      );
    }
    const { products } = await this.livestreamService.CreateOrUpdateLivestreamProductSchema.validateAsync(body);
    const listProductInfo = await Product.findAll({
      where: {
        id: { [Op.in]: products.map((item: LivestreamTypes.ProductLivestream) => item.id) },
        shop_id: userDecoded.shop_id,
        status: PRODUCT_STATUS.AVAILABLE,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: ProductMedia,
          required: false,
          where: {
            product_custom_attribute_option_id: null,
            type: MEDIA_TYPE.IMAGE,
            is_active: IS_ACTIVE.ACTIVE,
          },
          limit: 1,
        },
      ],
    });
    await sequelize.transaction(async (transaction) => {
      await LivestreamProduct.update(
        {
          is_active: IS_ACTIVE.INACTIVE,
          delete_by: userDecoded.id,
          delete_at: new Date(),
        },
        {
          where: {
            livestream_id,
            product_id: { [Op.in]: products.map((item: LivestreamTypes.ProductLivestream) => item.id) },
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
      const productsData = listProductInfo.map((item) => {
        const product_item = item.dataValues;
        return {
          livestream_id: livestream_id,
          product_id: product_item.id,
          code_product_livestream: products.find((e: LivestreamTypes.ProductLivestream) => e.id === product_item.id)
            ?.code_product_livestream,
          product: {
            code: product_item.code,
            name: product_item.name,
            product_media_url: product_item.ProductMedia[0].dataValues.media_url,
          },
        };
      });
      await LivestreamProduct.bulkCreate(productsData, { transaction });
    });
    const productsLivestream = await Product.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT id
              FROM livestream_product AS lp
              WHERE
              lp.is_active = ${IS_ACTIVE.ACTIVE}
              AND lp.livestream_id = ${livestream_id}
              AND lp.product_id = Product.id
            )`),
            'livestream_product_id',
          ],
          [
            sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = Product.id
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
            'stock_id',
          ],

          [
            sequelize.literal(`(
                SELECT
                code_product_livestream
                FROM livestream_product AS lp
                WHERE
                lp.is_active = ${IS_ACTIVE.ACTIVE}
                AND lp.livestream_id = ${livestream_id}
                AND lp.product_id = Product.id
              )`),
            'code_product_livestream',
          ],
          [
            sequelize.literal(`(
                SELECT is_highlight
                FROM livestream_product AS lp
                WHERE
                lp.is_active = ${IS_ACTIVE.ACTIVE}
                AND lp.livestream_id = ${livestream_id}
                AND lp.product_id = Product.id
              )`),
            'is_highlight',
          ],
          [
            sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = Product.id
                  limit 1
                  ),null)`),
            'media_url',
          ],
          [
            sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = Product.id
                  )`),
            'min_max_price',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: true,
          attributes: [],
          where: {
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      ],
    });
    const room = withLiveStreamChannel(livestream_id);
    request.io.to(room).emit(room, withIOSuccess(productsLivestream, LIVESTREAM_EVENT.CREATE_UPDATE_PRODUCT));

    // const data = { livestream_id };
    // await publisherService.publish(EVENTS.CREATE_UPDATE_LIVESTREAM_PRODUCT, data);
    return withSuccess({});
  }

  /**
   * @summary delete product while streaming (shop)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Delete('/{livestream_id}/product')
  public async deleteProductWhileStreaming(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: { products_id: Array<number> },
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const livestream = await this._findOne({
      where: {
        id: livestream_id,
        shop_id: userDecoded.shop_id,
        status: { [Op.ne]: LIVESTREAM_STATUS.FINISHED },
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: {
        model: Shop,
        required: true,
        attributes: ['id', 'name', 'profile_picture_url', 'agora_app_id', 'agora_app_certificate'],
        where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE },
      },
    });
    if (!livestream)
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Livestream này không tổn tại hoặc đang không được phát trực tiếp!',
      );
    const res = await LivestreamProduct.update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: userDecoded.id,
        delete_at: new Date(),
      },
      {
        where: {
          livestream_id,
          product_id: { [Op.in]: body.products_id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    const count_records_deleted = _.isArray(res) ? res[0] : res;
    const room = withLiveStreamChannel(livestream_id);
    const data = { livestream_id, products_livestream_id: body.products_id, count_records_deleted };
    request.io.to(room).emit(room, withIOSuccess(data, LIVESTREAM_EVENT.DELETE_PRODUCT));
    // await publisherService.publish(EVENTS.DELETE_PRODUCT_LIVESTREAM, data);
    return withSuccess({
      products_livestream_id: body.products_id,
      count_records_deleted,
    });
  }

  /**
   * @summary highlight product while streaming (shop)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/highlight/{product_id}')
  public async highlightProductWhileStreaming(
    @Request() request: AppTypes.RequestIOAuth,
    livestream_id: number,
    product_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const product = await Product.findOne({
      where: {
        id: product_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [
          [
            sequelize.literal(`(
                SELECT is_highlight
                FROM livestream_product AS lp
                WHERE
                lp.is_active = ${IS_ACTIVE.ACTIVE}
                AND lp.livestream_id = ${livestream_id}
                AND lp.product_id = Product.id
              )`),
            'is_highlight',
          ],
          [
            sequelize.literal(`(
              SELECT id
              FROM livestream_product AS lp
              WHERE
              lp.is_active = ${IS_ACTIVE.ACTIVE}
              AND lp.livestream_id = ${livestream_id}
              AND lp.product_id = Product.id
            )`),
            'livestream_product_id',
          ],
          [
            sequelize.literal(`(
                SELECT
                code_product_livestream
                FROM livestream_product AS lp
                WHERE
                lp.is_active = ${IS_ACTIVE.ACTIVE}
                AND lp.livestream_id = ${livestream_id}
                AND lp.product_id = Product.id
              )`),
            'code_product_livestream',
          ],
          [
            sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = Product.id
                  limit 1
                  ),null)`),
            'media_url',
          ],
          [
            sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = Product.id
                  )`),
            'min_max_price',
          ],
          [
            sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = Product.id
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
            'stock_id',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: true,
          attributes: [],
          where: {
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          include: {
            model: Livestream,
            required: true,
            attributes: [],
            where: {
              id: livestream_id,
              shop_id: userDecoded.shop_id,
              status: LIVESTREAM_STATUS.STREAMING,
              is_active: IS_ACTIVE.ACTIVE,
            },
          },
        },
      ],
    });
    if (!product)
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Sản phẩm này không tồn tại trong livestream.');
    await sequelize.transaction(async (transaction) => {
      await LivestreamProduct.update(
        {
          is_highlight: IS_HIGHLIGHT_LIVESTREAM_PRODUCT.NOT_HIGHLIGHT,
          update_by: userDecoded.id,
          update_at: new Date(),
        },
        {
          where: {
            product_id: { [Op.ne]: product_id },
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
      await LivestreamProduct.update(
        {
          is_highlight: IS_HIGHLIGHT_LIVESTREAM_PRODUCT.HIGHLIGHT,
          update_by: userDecoded.id,
          update_at: new Date(),
        },
        {
          where: {
            product_id,
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
    });
    const room = withLiveStreamChannel(livestream_id);
    request.io.to(room).emit(room, withIOSuccess(product, LIVESTREAM_EVENT.HIGHLIGHT_PRODUCT));
    // const data = { livestream_id, product_id: product.id };
    // await publisherService.publish(EVENTS.HIGHLIGHT_PRODUCT_LIVESTREAM, data);
    return withSuccess(product);
  }

  /**
   * @summary get livestream streaming (shop, shop_member)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/streaming')
  public async getLivestreamStreaming(
    @Request() request: AppTypes.RequestIOAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const foundStreaming = await this._findOne({
      where: {
        user_id: userDecoded.id,
        shop_id: userDecoded.shop_id,
        status: LIVESTREAM_STATUS.STREAMING,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: false,
          attributes: [
            [sequelize.col('id'), 'livestream_product_id'],
            'code_product_livestream',
            'is_highlight',
            [sequelize.literal('`LivestreamProducts->Product`.`id`'), 'id'],
            [sequelize.literal('`LivestreamProducts->Product`.`shop_id`'), 'shop_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`category_id`'), 'category_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`code`'), 'code'],
            [sequelize.literal('`LivestreamProducts->Product`.`name`'), 'name'],
            [sequelize.literal('`LivestreamProducts->Product`.`description`'), 'description'],
            [sequelize.literal('`LivestreamProducts->Product`.`price`'), 'price'],
            [sequelize.literal('`LivestreamProducts->Product`.`star`'), 'star'],
            [sequelize.literal('`LivestreamProducts->Product`.`status`'), 'status'],
            [sequelize.literal('`LivestreamProducts->Product`.`stock_status`'), 'stock_status'],
            [sequelize.literal('`LivestreamProducts->Product`.`is_active`'), 'is_active'],
            [sequelize.literal('`LivestreamProducts->Product`.`create_at`'), 'create_at'],
            [sequelize.literal('`LivestreamProducts->Product`.`update_at`'), 'update_at'],

            [
              sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                  limit 1
                  ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  )`),
              'min_max_price',
            ],
          ],
          include: {
            model: Product,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: [
            'id',
            'name',
            'profile_picture_url',
            'agora_project_id',
            'agora_app_id',
            'agora_app_certificate',
            'create_at',
          ],
          where: { id: userDecoded.shop_id, is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!foundStreaming) {
      return withSuccess(null);
    }

    const HighlightProduct = foundStreaming.dataValues.LivestreamProducts.find(
      (item: any) => item.dataValues.is_highlight === IS_HIGHLIGHT_LIVESTREAM_PRODUCT.HIGHLIGHT,
    );

    const res = await SocketUtils.countUserInAdapterSocketLivestreamChannel(request.io, foundStreaming.id);
    if (res) {
      const { count_subscribe } = res;
      foundStreaming.setDataValue('count_subscribe', count_subscribe);
    }
    foundStreaming.setDataValue('HighlightProduct', HighlightProduct);
    return withSuccess(foundStreaming);
  }

  /**
   * @summary delete Livestream (shop, admin)
   */
  @Security('jwt', ['shop', 'shop_member', 'admin'])
  @Delete('/{livestream_id}')
  public async deleteLiveStream(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const whereOption: any = { id: livestream_id, is_active: IS_ACTIVE.ACTIVE };
    if (userDecoded.shop_id) {
      whereOption.shop_id = userDecoded.shop_id;
    }
    await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: userDecoded.id,
        delete_at: new Date(),
      },
      { where: whereOption },
    );
    return withSuccess({});
  }

  /**
   * @summary update broadcast livestream (shop)
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/broadcast')
  public async updateBroadcast(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: { broadcast_id: string },
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { broadcast_id } = await this.livestreamService.UpdateBroadcast.validateAsync(body);
    const user = request?.user?.data;
    const livestream = await this._findOne({
      where: {
        id: livestream_id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: {
        model: Shop,
        required: true,
        attributes: ['id', 'name', 'profile_picture_url', 'agora_app_id', 'agora_app_certificate'],
        where: { id: user.shop_id, is_active: IS_ACTIVE.ACTIVE },
      },
    });
    if (!livestream)
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Livestream này không tổn tại hoặc đang không được phát trực tiếp!',
      );
    if (livestream.status !== LIVESTREAM_STATUS.INITIAL)
      throw new AppError(ApiCodeResponse.CONFLICT_ERROR).with('Chỉ có thể sửa broadcast trước khi bắt đầu livestream.');
    await livestream.update({
      type: LIVESTREAM_TYPE.REPLAY,
      broadcast_id,
      update_at: new Date(),
    });
    return withSuccess(livestream);
  }

  /**
   * @summary ping livestream
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Post('/{livestream_id}/ping')
  public async pingLivestream(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const res = await this._update(
      {
        ping_at: new Date(),
        update_by: userDecoded.id,
        update_at: new Date(),
      },
      {
        where: {
          id: livestream_id,
          user_id: userDecoded.id,
          shop_id: userDecoded.shop_id,
          is_active: IS_ACTIVE.ACTIVE,
          status: LIVESTREAM_STATUS.STREAMING,
        },
      },
    );
    return withSuccess({ message: 'pong' });
  }
}
