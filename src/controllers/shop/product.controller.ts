import { Get, Query, Route, Request, Tags, Security } from 'tsoa';
import { withSuccess } from '../../utils/BaseResponse';
import { ApplicationController } from './../';
import { IS_ACTIVE } from '@utils/constants';
import { AppTypes } from 'types';
import axios, { AxiosInstance } from 'axios';
const db = require('@models');
import { getAxiosInstance } from '@config/axiosInstance';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import _ = require('lodash');
const { sequelize, Sequelize, Product, User, Category, ProductStock, Stock, Order, OrderItem, Shop } = db.default;
const { Op } = Sequelize;

@Route('shop/product')
@Tags('shop/product')
export class ProductShopController extends ApplicationController {
  public apiInstance: AxiosInstance;
  constructor() {
    super('Product');
    this.apiInstance = getAxiosInstance();
  }

  /**
   * @summary Danh sách tất cả các sản phẩm ('shop', 'shop_member')
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/all')
  public async getFullListProducts(
    @Request() request: AppTypes.RequestAuth,
    @Query() search?: string,
    @Query() status?: number,
    @Query() category_id?: any,
    @Query() children_category_id?: any,
    @Query() stock_id?: number,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const userDecoded = request.user.data;
    const whereOptions: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id: userDecoded.shop_id,
      ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
    };
    if (children_category_id != undefined && children_category_id != null) {
      whereOptions.category_id = children_category_id != 0 ? children_category_id : { [Op.ne]: null };
    }
    if (status != undefined && status != null) {
      whereOptions.status = status;
    }
    const listProduct = await Product.findAll({
      subQuery: false,
      attributes: [
        'id',
        'name',
        'code',
        'status',
        'description',
        'price',
        [
          Sequelize.literal(`(
            SELECT IFNULL(MIN(product_price.price), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
            )`),
          'price_stock',
        ],
        'stock_status',
        [Sequelize.literal('`Category`.`name`'), 'category_name'],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        [Sequelize.literal('`Category`.`name`'), 'sub_category_name'],
        [Sequelize.literal('`Category->parent_category`.`name`'), 'category_name'],
        [sequelize.fn('SUM', sequelize.col('OrderItems.price')), 'total_price'],
        [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
            )`),
          'amount',
        ],
      ],
      where: whereOptions,
      include: [
        // {
        //   model: Category,
        //   attributes: [],
        //   include: { model: Category, as: 'parent_category', attributes: [] },
        //   where: {
        //     is_active: IS_ACTIVE.ACTIVE,
        //     id: category_id != undefined && category_id != null ? category_id : { [Op.ne]: null },
        //   },
        // },
        {
          model: Category,
          // required: false,
          attributes: ['id', 'name', 'parent_id'],
          where: {
            // is_active: IS_ACTIVE.ACTIVE,
            parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
            // status: IS_DEFAULT.ACTIVE,
          },
          include: {
            model: Category,
            as: 'parent_category',
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
          },
        },
        {
          model: OrderItem,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: Order,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
        {
          model: ProductStock,
          attributes: [],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            stock_id: stock_id != undefined && stock_id != null ? stock_id : { [Op.ne]: null },
          },
        },
        {
          model: Shop,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      group: ['product.id'],
      order: [['id', 'desc']],
    });
    return withSuccess(listProduct);
  }
}
