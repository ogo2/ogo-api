import { IS_ACTIVE, IS_DEFAULT, PAKAGE_STATUS } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Body, Request, Get, Post, Put, Query, Route, Delete, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import * as _ from 'lodash';
import { AppTypes, PakageTypes } from 'types';
import { PakageService } from '@services/internal/pakage.service';
const db = require('@models');
const { sequelize, Sequelize, Pakage, PakageCategory } = db.default;
const { Op } = Sequelize;

@Route('pakage')
@Tags('pakages')
export class PakageController extends ApplicationController {
  private pakageService: PakageService;
  constructor() {
    super('Pakage');
    this.pakageService = new PakageService();
  }

  /**
   * @summary create a pakage (admin)
   */
  @Security('jwt', ['admin', 'admin_service_manager'])
  @Post('/')
  public async createPakage(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: PakageTypes.CreateUpdatePakageModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const bodyData: PakageTypes.CreateUpdatePakageModel = await this.pakageService.CreatePakageSchema.validateAsync(
      body,
    );

    const foundPakage = await this._findOne({ where: { name: bodyData.name, is_active: IS_ACTIVE.ACTIVE } });
    if (foundPakage) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Tên gói đã tồn tại, vui lòng nhập tên khác.');

    const dataTransaction = await sequelize.transaction(async (transaction) => {
      const createdPakage = await this._create(
        {
          ...bodyData,
          create_by: loggedInUser.id,
        },
        { transaction },
      );
      if (createdPakage?.dataValues?.is_default === IS_DEFAULT.ACTIVE) {
        await this._update(
          { is_default: IS_DEFAULT.INACTIVE, update_by: loggedInUser.id, update_at: new Date() },
          { where: { id: { [Op.ne]: createdPakage.id }, is_active: IS_ACTIVE.ACTIVE }, transaction },
        );
      }
      return createdPakage;
    });
    return withSuccess(dataTransaction);
  }

  /**
   * @summary list category pagake (any)
   */
  @Security('jwt')
  @Get('/')
  public async getListPakage(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('pakage_category_id') pakage_category_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (search) {
      whereOption.name = { [Op.like]: `%${search}%` };
    }
    if (pakage_category_id) {
      whereOption.pakage_category_id = pakage_category_id;
    }
    if (status === PAKAGE_STATUS.ACTIVE || status === PAKAGE_STATUS.INACTIVE) {
      whereOption.status = status;
    }
    const { count, rows } = await Pakage.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [sequelize.literal('`PakageCategory`.`name`'), 'pakage_category_name'],
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM \`pakage_history\`
            WHERE pakage_history.is_active = ${IS_ACTIVE.ACTIVE}
            AND pakage_history.pakage_id = \`Pakage\`.id
              )`),
            'count_sold',
          ],
          [
            sequelize.literal(`(
            SELECT COUNT(*) 
            FROM 
            ( SELECT  COUNT(*)
              FROM
              \`pakage_history\`
              WHERE
              pakage_history.is_active = 1
              AND pakage_history.pakage_id = \`Pakage\`.id
              GROUP BY pakage_history.shop_id
            ) AS \`PakageHistoryGrouped\`
              )`),
            'count_shop_used',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: PakageCategory,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary update pakage data (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin', 'admin_service_manager'])
  @Put('/{id}')
  public async updatePakage(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: PakageTypes.CreateUpdatePakageModel,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const bodyData: any = await this.pakageService.CreatePakageSchema.validateAsync(body);

    const dataTransaction = await sequelize.transaction(async (transaction) => {
      if (bodyData.is_default) {
        await this._update(
          {
            is_default: IS_DEFAULT.INACTIVE,
            update_by: loggedInUser.id,
            update_at: new Date(),
          },
          { where: { is_active: IS_ACTIVE.ACTIVE }, transaction },
        );
      }
      const updatedPakage = await this._update(
        {
          ...bodyData,
          update_by: loggedInUser.id,
          update_at: new Date(),
        },
        { where: { id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      return updatedPakage;
    });
    const pakageData = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(pakageData);
  }

  /**
   * @summary change pakage status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin', 'admin_service_manager'])
  @Put('/{id}/status')
  public async changePakageStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPakage = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundPakage) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Gói không tồn tại.');
    await this._update(
      {
        status: foundPakage.status === PAKAGE_STATUS.ACTIVE ? PAKAGE_STATUS.INACTIVE : PAKAGE_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      { where: { id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess({
      status: foundPakage.status === PAKAGE_STATUS.ACTIVE ? PAKAGE_STATUS.INACTIVE : PAKAGE_STATUS.ACTIVE,
    });
  }

  /**
   * @summary delete list pakage by id: Array<number> (admin)
   */
  @Security('jwt', ['admin', 'admin_service_manager'])
  @Delete('/')
  public async deleteListPakage(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const resUpdate = await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess(resUpdate);
  }
}
