import { Body, Security, Get, Post, Put, Query, Route, Delete, Tags, Request } from 'tsoa';
import { TOPIC_STATUS, TOPIC_TYPE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { AppTypes, TopicTypes } from 'types';
import { TopicService } from '@services/internal/topic.service';
import { withSuccess, withPagingSuccess } from '@utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { IS_ACTIVE } from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '..';

const db = require('@models');
const { sequelize, Sequelize, Topic } = db.default;
const { Op } = Sequelize;

@Route('admin/topic')
@Tags('admin/topics')
export class TopicAdminController extends ApplicationController {
  private topicService: TopicService;
  constructor() {
    super('Topic');
    this.topicService = new TopicService();
  }

  /**
   *  @summary get list topic ('admin', 'admin_editor')
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Get('/')
  public async getListTopic(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      type_topic: TOPIC_TYPE.NORMAL,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          description: { [Op.like]: `%${search}%` },
        },
      }),
    };
    if (status || status === 0) {
      whereOption.status = status;
    }
    const { count, rows } = await Topic.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [
        ['order', 'asc'],
        ['create_at', 'desc'],
      ],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   *  @summary get list topic for admin create post ('admin', 'admin_editor')
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Get('/admin-create-post')
  public async getListTopicCreatePost(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      type_topic: TOPIC_TYPE.ADMIN,
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          description: { [Op.like]: `%${search}%` },
        },
      }),
    };
    const { count, rows } = await Topic.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [
        ['order', 'asc'],
        ['create_at', 'asc'],
      ],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   *  @summary get detail topic (any)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Get('/{id}')
  public async getDetailTopic(id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundTopic = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`
                (
                  SELECT COUNT(*)
                  FROM user
                  WHERE user.is_active = ${IS_ACTIVE.ACTIVE}
                )
                    -
                (
                  SELECT COUNT(*)
                  FROM user_ignore_topic
                  WHERE user_ignore_topic.is_active = ${IS_ACTIVE.ACTIVE}
                  AND user_ignore_topic.id = ${id}
                ) 
                  `),
            'count_care',
          ],
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM post
                WHERE post.is_active = ${IS_ACTIVE.ACTIVE}
                AND post.topic_id = ${id}
                  )`),
            'count_post',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    if (!foundTopic) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    return withSuccess(foundTopic);
  }

  /**
   *  @summary create a topic (admin)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Post('/')
  public async createTopic(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: TopicTypes.TopicCreateModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const bodyValidated = await this.topicService.TopicCreateSchema.validateAsync(body);
    const topicCreated = await this._create({
      ...bodyValidated,
      create_by: loggedInUser.id,
    });
    return withSuccess(topicCreated);
  }

  /**
   *  @summary update a topic (admin)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Put('/{id}')
  public async updateTopic(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: TopicTypes.TopicUpdateModel,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const bodyValidated = await this.topicService.TopicUpdateSchema.validateAsync(body);
    await this._update(
      {
        ...bodyValidated,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      { where: { id, is_active: IS_ACTIVE.ACTIVE } },
    );
    const foundTopic = await this._findOne({
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(foundTopic);
  }
  /**
   * @summary change user topic (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Put('/{id}/status')
  public async changeTopicStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundTopic = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundTopic) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    await this._update(
      {
        status: foundTopic.status === TOPIC_STATUS.ACTIVE ? TOPIC_STATUS.INACTIVE : TOPIC_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      {
        where: {
          id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess({
      status: foundTopic.status === TOPIC_STATUS.ACTIVE ? TOPIC_STATUS.INACTIVE : TOPIC_STATUS.ACTIVE,
    });
  }

  /**
   * @summary delete list topic admin by id: Array<number> (admin)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Delete('/')
  public async deleteListTopic(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess({});
  }
}
