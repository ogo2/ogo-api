import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { ProductService } from '@services/internal/products.service';
import { IS_ACTIVE, IS_DEFAULT, MEDIA_TYPE, ORDER_STATUS, SHOP_STATUS } from '@utils/constants';
import * as express from 'express';
import { Get, Query, Request, Route, Security, Tags } from 'tsoa';
import { AppTypes } from 'types';
import { withPagingSuccess, withSuccess } from '../../utils/BaseResponse';
import { ApplicationController } from './../';

const db = require('@models');

const {
  sequelize,
  Sequelize,
  Product,
  ProductMedia,
  Category,
  ProductStock,
  Stock,
  Order,
  OrderItem,
  Shop,
  ProductPrice,
} = db.default;
const { Op } = Sequelize;
interface ProductMulterRequest extends express.Request {
  file: any;
}

@Route('admin/product')
@Tags('admin/product')
export class ProductAdminController extends ApplicationController {
  private productService: ProductService;
  constructor() {
    super('Product');
    this.productService = new ProductService();
  }

  /**
   * @summary Danh sách sản phẩm
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_sell_manager', 'admin_service_manager'])
  @Get('/')
  public async listProducts(
    @Request() request: any,
    @Query() search?: any,
    @Query() status?: any,
    @Query() shop_id?: number,
    @Query() category_id?: number,
    @Query() children_category_id?: number,
    @Query() stocks_status?: number,
    @Query() stock_id?: any,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const { offset, limit, page } = handlePagingMiddleware(request);
    // const { search, status, category_id, is_public, stocks_status } = request.query;
    // return withSuccess(search == null);
    const listStock = await Stock.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id ? shop_id : { [Op.ne]: null } },
    });
    // return withSuccess(1);
    const tokens = request.headers.token;
    // let userI;
    // if (tokens) {
    //     const loginUser = await User.findOne({ where: { token: tokens } });
    //     // return withSuccess(loginUser.shop_id);
    //     if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
    //     // const shop = await Shop.findOne({
    //     //     where: { id: loginUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
    //     // });
    //     // return withSuccess(shop);
    //     shopId = loginUser ? loginUser.shop_id : null;
    //     if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    // }

    const whereOptions: any = {
      is_active: IS_ACTIVE.ACTIVE,
      // shop_id: shopId,
      ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
    };
    if (status != null && status != undefined && status != '') {
      whereOptions.status = status;
    }
    if (shop_id != null && shop_id != undefined) {
      whereOptions.shop_id = shop_id;
    }
    if (stocks_status != undefined && stocks_status != null) {
      whereOptions.stock_status = stocks_status;
    }
    if (children_category_id != undefined && children_category_id != null) {
      whereOptions.category_id = children_category_id != 0 ? children_category_id : { [Op.ne]: null };
    }

    // return withSuccess(whereOptions);
    const { rows, count } = await Product.findAndCountAll({
      // subQuery: false,
      attributes: [
        'id',
        'name',
        'code',
        'description',
        'price',
        'status',
        'stock_status',
        [
          sequelize.literal(`
                    IFNULL((SELECT MIN(product_price.stock_id)
                    FROM product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = Product.id
                    ),0)`),
          'stock_id',
        ],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        'product_pancake_id',
        [Sequelize.literal('`Shop`.`pancake_shop_key`'), 'pancake_shop_key'],
        [Sequelize.literal('`Shop`.`pancake_shop_id`'), 'pancake_shop_id'],
        // [Sequelize.literal('`Category`.`name`'), 'sub_category_name'],
        // [Sequelize.literal('`Category->parent_category`.`name`'), 'category_name'],
        // [Sequelize.literal('`Category->parent_category`.`name`'), 'sub_category_name'],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id is not null
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_price',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join \`order_item\` as order_item
                        ON order_item.order_id = od.id
                        WHERE 
                            od.shop_id is not null
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              and product_price.status = ${IS_ACTIVE.ACTIVE}
            )`),
          'amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IF((SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id) > 0,true,false)
            )`),
          'status_stock',
        ],
        [
          Sequelize.literal(`(
            SELECT product_price.price from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              limit 1
            )`),
          'price_stock',
        ],
      ],
      where: whereOptions,
      include: [
        // {
        //   required: category_id != null && category_id != undefined ? true : false,
        //   model: Category,
        //   attributes: ['id', 'name', 'parent_id', 'icon_url'],
        //   include: {
        //     model: Category,
        //     as: 'parent_category',
        //     attributes: ['id', 'name', 'parent_id', 'icon_url'],
        //   },
        //   where: {
        //     is_active: IS_ACTIVE.ACTIVE,
        //     id: category_id != null && category_id != undefined ? category_id : { [Op.ne]: null },
        //   },
        // },
        {
          model: Category,
          // required: false,
          attributes: ['id', 'name', 'parent_id'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
            // status: IS_DEFAULT.ACTIVE,
          },
          include: {
            model: Category,
            as: 'parent_category',
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
          },
        },
        {
          model: Shop,
          attributes: ['id', 'name', 'pancake_shop_key', 'pancake_shop_id'],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: OrderItem,
          required: false,
          // attributes: ['id'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: Order,
            required: false,
            attributes: ['id'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
        {
          model: ProductStock,
          // required: false,
          required: stock_id != null && stock_id != undefined && stock_id != '' && stock_id != 0 ? true : false,
          attributes: ['id', 'product_id', 'stock_id'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            stock_id:
              stock_id != null && stock_id != undefined && stock_id != '' && stock_id != 0
                ? stock_id
                : { [Op.ne]: null },
          },
          // having: sequelize.where(sequelize.literal('sum(`ProductPrices`.status'), stocks_status == 1 ? ">" : "<=", stocks_status == 1 ? 0 : 0),
        },
      ],
      group: ['Product.id'],
      order: [['id', 'desc']],
      limit,
      offset,
      // logging: console.log,
    });
    // rows.map((item) => {

    // let pancakeDetail = await this.apiInstance.get(
    //   '/shops/' + pancake_shop_id + '/variations?api_key=' + pancake_shop_key,
    // );

    // });
    return withPagingSuccess(
      { rows, listStock },
      { page, limit, totalItemCount: count instanceof Array ? count.length : count },
    );
  }

  /**
   * @summary Danh sách tất cả các sản phẩm ('admin', 'admin_sell_manager')
   */
  @Security('jwt', ['admin', 'admin_sell_manager'])
  @Get('/all')
  public async getFullListProducts(
    @Request() request: AppTypes.RequestAuth,
    @Query() search?: string,
    @Query() status?: number,
    @Query() category_id?: number,
    @Query() children_category_id?: number,
    @Query() stock_id?: number,
    @Query() shop_id?: number,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const whereOptions: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
    };
    if (shop_id) {
      whereOptions.shop_id = shop_id;
    }
    if (category_id) {
      whereOptions.category_id = category_id;
    }
    if (status != undefined && status != null) {
      whereOptions.status = status;
    }
    if (children_category_id != undefined && children_category_id != null) {
      whereOptions.category_id = children_category_id != 0 ? children_category_id : { [Op.ne]: null };
    }
    const listProduct = await Product.findAll({
      // subQuery: false,
      attributes: [
        'id',
        'name',
        'code',
        'status',
        'stock_status',
        [
          sequelize.literal(`
                    IFNULL((SELECT MIN(product_price.stock_id)
                    FROM product_price
                    WHERE
                    product_price.is_active = ${IS_ACTIVE.ACTIVE}
                    and product_price.product_id = Product.id
                    ),0)`),
          'stock_id',
        ],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        // [Sequelize.literal('`Category`.`name`'), 'sub_category_name'],
        // [Sequelize.literal('`Category->parent_category`.`name`'), 'category_name'],
        // [Sequelize.literal('`Category->parent_category`.`name`'), 'sub_category_name'],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id is not null
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_price',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join \`order_item\` as order_item
                        ON order_item.order_id = od.id
                        WHERE 
                            od.shop_id is not null
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              and product_price.status = ${IS_ACTIVE.ACTIVE}
            )`),
          'amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IF((SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id) > 0,true,false)
            )`),
          'status_stock',
        ],
      ],
      where: whereOptions,
      include: [
        {
          model: Category,
          // required: false,
          attributes: ['id', 'name'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
            status: IS_DEFAULT.ACTIVE,
          },
          include: {
            model: Category,
            as: 'parent_category',
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
          },
        },
        {
          model: OrderItem,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: Order,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
        {
          model: ProductStock,
          attributes: [],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            stock_id: stock_id != undefined && stock_id != null ? stock_id : { [Op.ne]: null },
          },
        },
        {
          model: Shop,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
      ],
      group: ['product.id'],
      order: [['id', 'desc']],
    });
    return withSuccess(listProduct);
  }

  @Get('/check-code')
  public async checkProductCodeExist(@Query() code: string): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundProduct = await this.productService.isProductCodeExist(code);
    return withSuccess(!!foundProduct);
  }

  /**
   * @summary Chi tiết sản phẩm
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_sell_manager', 'admin_service_manager'])
  @Get('/{id}')
  public async detailProduct(
    @Request() request,
    id: number,
    @Query() type?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const images = await ProductMedia.findOne({
      where: {
        product_id: id,
        is_active: IS_ACTIVE.ACTIVE,
        type: MEDIA_TYPE.IMAGE,
        product_custom_attribute_option_id: null,
      },
      order: [['id', 'asc']],
    });
    const listStock = await ProductStock.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, product_id: id },
      attributes: ['id', 'product_id', 'stock_id', [sequelize.literal('`Stock`.`name`'), 'stock_name']],
      include: {
        model: Stock,
        attributes: [],
        where: { is_active: IS_ACTIVE.ACTIVE },
      },
    });
    let images_id = 0;
    if (images) images_id = images.id;
    const detailProduct = await this.productService.findById(id, images_id, 0, request, type);
    return withSuccess({ detailProduct, listStock });
  }
}
