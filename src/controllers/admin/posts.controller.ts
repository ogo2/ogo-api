import {
  Body,
  Security,
  Controller,
  Get,
  Path,
  Post,
  Put,
  Query,
  Route,
  SuccessResponse,
  Delete,
  Tags,
  Header,
  Request,
} from 'tsoa';
import { AppTypes, PostTypes } from 'types';
import { withError, withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import * as _ from 'lodash';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import {
  IS_ACTIVE,
  MEDIA_TYPE,
  POST_STATUS,
  ROLE,
  DF_NOTIFICATION,
  IS_POSTED,
  PRODUCT_STOCK_STATUS,
  SHOP_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import * as uploadMiddleware from '@middleware/uploadAwsMiddleware';
import { Utils } from '@utils/Utils';
import { PostService } from '@services/internal/post.service';
import { NotificationService } from '@services/internal/notification.service';
import { environment } from '@config/environment';
const db = require('@models');
const { sequelize, Sequelize, Post: _Post, PostMedia, Shop, User, Reaction, Comment, Topic, Product } = db.default;
const { Op } = Sequelize;

@Route('admin/post')
@Tags('admin/posts')
export class AdminPostsController extends ApplicationController {
  private postService: PostService;
  private notificationService: NotificationService;
  constructor() {
    super('Post');
    this.notificationService = new NotificationService();
    this.postService = new PostService();
  }
  /**
   *  @summary create a post by form data (admin, admin_editor)
   *  push noti all (status = 1), shop (status = 2) (KH NHẬN ĐC TBAO ), customer(status =3) (TB CHO KHÁCH HÀNG + GIAN HÀNGD)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Post('/')
  public async createPostFormData(
    @Request() request: AppTypes.RequestIOAuth,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    let array_image = [];
    const uploads = await uploadMiddleware.handleFiles(request, 'post_media', MEDIA_TYPE.IMAGE);
    if (uploads) {
      array_image = (request as any).files.map((file) => file.key);
    }
    const dataValidated: PostTypes.AdminPostCreateModel = await this.postService.AdminPostCreateSchema.validateAsync({
      ...request.body,
      user_id: loggedInUser.id,
    });

    // đăng bài ngay, không hẹn giờ đăng bài
    if (!dataValidated.schedule) {
      const listUser = await User.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          df_type_user_id:
            dataValidated.status == POST_STATUS.ALL
              ? { [Op.ne]: ROLE.ADMIN }
              : dataValidated.status == 3
              ? { [Op.in]: [ROLE.SHOP, ROLE.SHOP_MEMBER] }
              : ROLE.CUSTOMER,
        },
      });
      // let listShop = [];

      // // lấy danh sách shop
      // if (dataValidated.status == POST_STATUS.ALL || dataValidated.status == POST_STATUS.SHOP) {
      //   listShop = await Shop.findAll({
      //     where: { is_active: IS_ACTIVE.ACTIVE, user_id: listUser.map((data) => data.id), status: SHOP_STATUS.ACTIVE },
      //   });
      // }
      const postTransaction = await sequelize.transaction(async (transaction) => {
        const createdPost = await this._create({ ...dataValidated, is_posted: IS_POSTED.POSTED }, { transaction });
        const post_media = array_image.map((media_url: string) => {
          return {
            post_id: createdPost.id,
            media_url: media_url ? media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '') : null,
          };
        });
        const mediaCreated = await PostMedia.bulkCreate(post_media, { transaction });
        createdPost.setDataValue('PostMedia', mediaCreated);
        const notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.NEWS_POST);
        const notifications = listUser.map((projectData) => {
          const content = dataValidated.content.substr(0, 100);
          return {
            df_notification_id: notiType.id,
            content: content,
            data: { post_id: createdPost.id },
            user_id: projectData.id,
            title: notiType.title,
          };
        });
        // if (listShop.length > 0) {
        //   const notifications_subshop = listShop.map((projectData) => {
        //     const content = dataValidated.content.substr(0, 100);
        //     return {
        //       df_notification_id: notiType.id,
        //       content: content,
        //       data: { post_id: createdPost.id },
        //       user_id: projectData.user_id,
        //       title: notiType.title,
        //     };
        //   });
        //   await this.notificationService.createMultiNotification(notifications_subshop, transaction);
        // }
        await this.notificationService.createMultiNotification(notifications, transaction);
        return createdPost;
      });
      return withSuccess(postTransaction);
    }
    // đăng bài hẹn giờ
    else {
      const postTransaction = await sequelize.transaction(async (transaction) => {
        const createdPost = await this._create({ ...dataValidated, is_posted: IS_POSTED.NOT_POSTED }, { transaction });
        const post_media = array_image.map((media_url: string) => {
          return {
            post_id: createdPost.id,
            media_url: media_url ? media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '') : null,
          };
        });
        const mediaCreated = await PostMedia.bulkCreate(post_media, { transaction });
        createdPost.setDataValue('PostMedia', mediaCreated);
        return createdPost;
      });
      return withSuccess(postTransaction);
    }
  }

  /**
   *  @summary get list post (admin, admin_editor)
   */
  @Security('jwt', ['admin'])
  @Get('/')
  public async getListPost(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
    @Query('topic_id') topic_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('status') status?: number,
    @Query('is_posted') is_posted?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && {
        [Op.or]: [
          { content: { [Op.like]: `%${search}%` } },
          // { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
          // { '$Shop.phone$': { [Op.like]: `%${search}%` } },
          // { '$Shop.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (user_id) {
      whereOption.user_id = user_id;
    }
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (topic_id) {
      whereOption.topic_id = topic_id;
    }
    if (is_posted || is_posted === 0) {
      whereOption.is_posted = is_posted;
    }
    if (status === POST_STATUS.ALL || status === POST_STATUS.CUSTOMER || status === POST_STATUS.SHOP) {
      whereOption.status = status;
    }
    const baseUrl = Utils.getBaseServer(request);
    const { count, rows } = await _Post.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
               SELECT COUNT(*)
               FROM reaction AS reaction
               WHERE
               reaction.is_active = ${IS_ACTIVE.ACTIVE}
               AND reaction.user_id = ${loggedInUser.id}
               AND reaction.post_id = Post.id
               AND reaction.comment_id is null
             )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(rows) ? rows.length : count });
  }

  /**
   *  @summary get detail post (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Get('/{id}')
  public async getDetailPost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;

    const baseUrl = Utils.getBaseServer(request);
    const foundPost = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`(
           SELECT COUNT(*)
           FROM reaction AS reaction
           WHERE
           reaction.is_active = ${IS_ACTIVE.ACTIVE}
           AND reaction.user_id = ${loggedInUser.id}
           AND reaction.post_id = Post.id
           AND reaction.comment_id is null
         )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Product,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT stock_id
                  FROM product_stock AS product_stock, stock AS stock
                  WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                  AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                  AND product_stock.product_id = \`Product\`.\`id\`
                  AND product_stock.stock_id = stock.id
                  AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                  LIMIT 1
                  )`),
                'stock_id',
              ],
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`Product\`.\`id\`
                  )`),
                'min_max_price',
              ],
            ],
            exclude: ['version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      // logging: console.log,
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn  tại.');
    return withSuccess(foundPost);
  }

  /**
   * @summary update post by id (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Put('/{id}')
  public async updatePost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPost = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      include: {
        model: User,
        required: true,
        attributes: [],
        where: {
          [Op.or]: [{ df_type_user_id: ROLE.ADMIN }, { df_type_user_id: ROLE.ADMIN_EDITOR }],
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');

    let array_image = [];
    const uploads = await uploadMiddleware.handleFiles(request, 'post_media', MEDIA_TYPE.IMAGE);
    if (uploads) {
      array_image = (request as any).files.map((file) => file.key);
    }

    let list_id_image_delete: Array<number> = [];
    if (request.body.image_delete) {
      if (typeof request.body.image_delete === 'number' || typeof request.body.image_delete === 'string')
        list_id_image_delete[0] = request.body.image_delete;
      if (Array.isArray(request.body.image_delete)) list_id_image_delete = [...request.body.image_delete];
    }

    const dataValidated: PostTypes.AdminPostUpdateModel = await this.postService.AdminPostUpdateSchema.validateAsync({
      ...request.body,
      image_delete: list_id_image_delete,
    });

    if (dataValidated.schedule && foundPost.dataValues.is_posted === IS_POSTED.POSTED) {
      throw new AppError(ApiCodeResponse.INVALID_PARAM).with('Bài đăng đã được đăng, không thể chỉnh sửa thời gian.');
    }
    console.log('dataValidated', dataValidated);
    if (dataValidated.status && foundPost.dataValues.is_posted === IS_POSTED.POSTED) {
      throw new AppError(ApiCodeResponse.INVALID_PARAM).with(
        'Bài đăng đã được đăng, không thể chọn được đối tượng nhận thông báo.',
      );
    }

    const payload: PostTypes.AdminPostUpdateModel = {};
    if (dataValidated.content) {
      payload.content = dataValidated.content;
    }
    if (dataValidated.topic_id) {
      payload.topic_id = dataValidated.topic_id;
    }
    if (dataValidated.schedule) {
      payload.schedule = dataValidated.schedule;
    }
    if (dataValidated.status || dataValidated.status === 0) {
      payload.status = dataValidated.status;
    }

    await sequelize.transaction(async (transaction) => {
      // edit post posted
      await PostMedia.update(
        {
          update_by: loggedInUser.id,
          delete_at: new Date(),
          is_active: IS_ACTIVE.INACTIVE,
        },
        {
          where: {
            id: { [Op.in]: dataValidated.image_delete },
            post_id: id,
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
      await this._update(payload, {
        where: { id, is_active: IS_ACTIVE.ACTIVE },
        transaction,
      });
      const post_media = array_image.map((media_url: string) => {
        return {
          post_id: id,
          media_url: media_url ? media_url.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', '') : null,
        };
      });
      await PostMedia.bulkCreate(post_media, { transaction });
    });
    const foundPostUpdated = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM reaction AS reaction
            WHERE
            reaction.is_active = ${IS_ACTIVE.ACTIVE}
            AND reaction.user_id = ${loggedInUser.id}
            AND reaction.post_id = Post.id
            AND reaction.comment_id is null
          )`),
            'is_reaction',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: Topic,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: User,
          required: true,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: PostMedia,
          required: false,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      // logging: console.log,
    });
    return withSuccess(foundPostUpdated);
  }

  /**
   * @summary delete post by id (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor'])
  @Delete('/{id}')
  public async deletePost(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundPost = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
    await foundPost.update({
      is_active: IS_ACTIVE.INACTIVE,
      delete_by: loggedInUser.id,
      delete_at: new Date(),
    });
    return withSuccess({});
  }
}
