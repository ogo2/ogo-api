import { IS_ACTIVE, USER_STATUS, ADMIN_ROLE } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Body, Request, Get, Post, Put, Query, Route, Delete, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';
import { AppTypes, UserTypes, AuthTypes } from '../../types';
import { UserService } from '@services/internal/user.service';

const db = require('@models');
const { sequelize, Sequelize, User, Shop, DFTypeUser, UserAddress, DFProvince, DFDistrict, DFWard } = db.default;
const { Op } = Sequelize;

@Route('admin/users')
@Tags('admin/user')
export class AdminUsersController extends ApplicationController {
  private userService: UserService;
  constructor() {
    super('User');
    this.userService = new UserService();
  }

  /**
   * @summary reset password by user id (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/reset-password')
  public async resetPassword(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user.data;
    const foundUser = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) {
      throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    }
    await foundUser.update({
      update_by: loggedInUser.id,
      password: foundUser.phone,
      token: null,
      update_at: new Date(),
    });
    return withSuccess({});
  }

  /**
   * @summary update user info admin account by user admin token (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}')
  public async updateUserInfo(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: UserTypes.UserUpdateModel,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const user = await User.findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!user) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    const dataUpdateBody: UserTypes.UserUpdateModel = await this.userService.UpdateUserAdminInfor.validateAsync(body);
    const payload: UserTypes.UserUpdatePayloadModel = {
      update_by: loginUser.id,
      update_at: new Date(),
    };
    if (dataUpdateBody.name) {
      payload.name = dataUpdateBody.name;
    }
    if (dataUpdateBody.email) {
      const userCheckEmail = await this._findOne({
        where: {
          id: { [Op.ne]: id },
          email: dataUpdateBody.email,
          is_active: IS_ACTIVE.ACTIVE,
        },
      });
      if (userCheckEmail) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Email này đã được sử dụng.');
      payload.email = dataUpdateBody.email;
    }
    if (dataUpdateBody.profile_picture_url) {
      payload.profile_picture_url = dataUpdateBody.profile_picture_url;
    }
    if (dataUpdateBody.role) {
      payload.df_type_user_id = dataUpdateBody.role;
    }
    // payload.df_type_user_id = ROLE.ADMIN;
    if (dataUpdateBody.status || dataUpdateBody.status === 0) {
      payload.status = dataUpdateBody.status;
    }
    await User.update(payload, {
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    const updatedUser = await this._findOne({
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    return withSuccess(updatedUser);
  }

  /**
   * @summary get detail user info by id (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/{id}')
  public async getDetailUserInfo(@Request() request: any, id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loginUser = request.user.data;
    const foundUser = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: false,
          attributes: [
            'id',
            'name',
            'phone',
            'livestream_enable',
            'status',
            'stream_minutes_available',
            'profile_picture_url',
          ],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: UserAddress,
          required: false,
          attributes: ['id', 'name', 'lat', 'long', 'location_address', 'address', 'is_default'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
      ],
    });
    if (!foundUser) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    }
    return withSuccess(foundUser);
  }

  /**
   * @summary change user status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/status')
  public async changeUserStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    if (loggedInUser.id === id) {
      throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Không thay đổi trạng thái tài khoản đang đăng nhập.');
    }
    const foundUser = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundUser) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    await this._update(
      {
        status: foundUser.status === USER_STATUS.ACTIVE ? USER_STATUS.INACTIVE : USER_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      {
        where: {
          id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess({
      status: foundUser.status === USER_STATUS.ACTIVE ? USER_STATUS.INACTIVE : USER_STATUS.ACTIVE,
    });
  }

  /**
   * @summary delete list user admin by id: Array<number> (admin)
   */
  @Security('jwt', ['admin'])
  @Delete('/')
  public async deleteUserAdmin(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const is_inclue: boolean = body.id.includes(loggedInUser.id);
    if (is_inclue) {
      throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Không thể xóa tài khoản đang đăng nhập.');
    }
    const resUpdate = await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess(resUpdate);
  }

  /**
   * @summary get list user role admin (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/')
  public async listUserAdmin(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('df_type_user_id') df_type_user_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (status === USER_STATUS.ACTIVE || status === USER_STATUS.INACTIVE) {
      whereOption = { ...whereOption, status };
    }
    if (df_type_user_id) {
      whereOption = {
        ...whereOption,
        [Op.and]: [
          {
            df_type_user_id,
          },
          {
            ...(search && {
              [Op.or]: {
                name: { [Op.like]: `%${search}%` },
                phone: { [Op.like]: `%${search}%` },
              },
            }),
          },
        ],
      };
    } else {
      whereOption = {
        ...whereOption,
        [Op.and]: [
          {
            [Op.or]: [
              { df_type_user_id: ADMIN_ROLE.ADMIN },
              { df_type_user_id: ADMIN_ROLE.ADMIN_EDITOR },
              { df_type_user_id: ADMIN_ROLE.ADMIN_SELL_MANAGER },
              { df_type_user_id: ADMIN_ROLE.ADMIN_SERVICE_MANAGER },
            ],
          },
          {
            ...(search && {
              [Op.or]: {
                name: { [Op.like]: `%${search}%` },
                phone: { [Op.like]: `%${search}%` },
              },
            }),
          },
        ],
      };
    }
    const { count, rows } = await User.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: [
          'token',
          'code_reset_password',
          'expired_reset_password',
          'password',
          'create_by',
          'update_by',
          'delete_by',
          'version',
        ],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   *  @summary create admin account (admin)
   */
  @Security('jwt', ['admin'])
  @Post('/')
  public async createUser(
    @Request() request: AppTypes.RequestAuth,
    @Body()
    body: {
      name: string;
      phone: string;
      email?: string;
      password: string;
      df_type_user_id?: number;
      profile_picture_url?: string;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const bodyData: any = await this.userService.UserSchema.concat(this.userService.UserTypeSchema)
      .concat(this.userService.PasswordSchema)
      .validateAsync({
        ...body,
        // df_type_user_id: ROLE.ADMIN,
      });
    const user = await this._findOne({
      where: {
        [Op.or]: [{ user_name: bodyData.phone }, { email: bodyData.email }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (user) {
      if (user?.dataValues?.phone === bodyData.phone)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Số điện thoại này đã được sử dụng.');
      if (user?.dataValues?.email === bodyData.email)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Email này đã được sử dụng.');
    }
    const createdUser = await this._create({
      ...bodyData,
      user_name: bodyData.phone,
      profile_picture_url: bodyData.profile_picture_url || null,
      create_by: loggedInUser.id,
    });

    const foundUser = await this._findOne({
      attributes: {
        include: [[sequelize.literal('`DFTypeUser`.`name`'), 'type_user_name']],
        exclude: ['password', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeUser,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { id: createdUser.id, is_active: IS_ACTIVE.ACTIVE },
    });

    return withSuccess(foundUser);
  }
}
