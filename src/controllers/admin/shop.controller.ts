import {
  IS_ACTIVE,
  IS_DEFAULT,
  IS_POSTED,
  LIVESTREAM_ENABLE,
  ORDER_STATUS,
  PAKAGE_CATEGORY,
  PAKAGE_STATUS,
  PANCAKE,
  ROLE,
  SHOP_STATUS,
  TAB_SHOP,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import axios from 'axios';
import * as _ from 'lodash';
import { Body, Delete, Get, Post, Put, Query, Request, Route, Security, Tags } from 'tsoa';
import { ApplicationController } from '../';
import { withPagingSuccess, withSuccess } from '@utils/BaseResponse';
import { ShopService } from '@services/internal/shop.service';
import { AppTypes, ShopTypes } from 'types';
import { UserService } from '@services/internal/user.service';
import Joi = require('joi');

const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Shop,
  Pakage,
  PakageHistory,
  PakageCategory,
  Category,
  Product,
  Stock,
  Order,
  OrderItem,
  ProductStock,
  UserAddress,
  DFWard,
  DFDistrict,
  DFProvince,
  Livestream,
  Follow,
} = db.default;
const { Op } = Sequelize;
@Route('admin/shop')
@Tags('admin/shops')
export class AdminShopController extends ApplicationController {
  private shopService: ShopService;
  private userService: UserService;
  constructor() {
    super('Shop');
    this.shopService = new ShopService();
    this.userService = new UserService();
  }

  /**
   *  @summary create a shop by admin (admin)
   */
  @Security('jwt', ['admin'])
  @Post('/')
  public async createShop(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: ShopTypes.AddShopModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const {
      name,
      name_user,
      phone,
      email,
      pancake_shop_key,
      pancake_shop_id,
      df_type_user_id,
      password,
      profile_picture_url,
    }: any = await this.shopService.AddShopSchema.concat(this.userService.UserTypeSchema)
      .concat(this.userService.PasswordSchema)
      .validateAsync({
        ...body,
        df_type_user_id: ROLE.SHOP,
      });
    const user = await User.findOne({
      where: {
        [Op.or]: [{ user_name: phone }, { email: email }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (user) {
      if (user.dataValues.phone === phone)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Số điện thoại này đã được sử dụng.');
      if (user?.dataValues?.email === email)
        throw new AppError(ApiCodeResponse.ACCOUNT_EXIST).with('Email này đã được sử dụng.');
    }
    const shop = await this._findOne({
      where: {
        [Op.or]: [{ phone: phone }, { email: email }],
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (shop) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Shop đã tồn tại.');
    // return withSuccess({
    //   pancake_shop_key,
    //   pancake_shop_id
    // })
    // kiểm tra xem shop này có tồn tại bên pancake ko
    if (
      (pancake_shop_id != null && pancake_shop_id != undefined) ||
      (pancake_shop_key != null && pancake_shop_key != undefined)
    ) {
      axios.defaults.baseURL = PANCAKE;
      axios.defaults.headers.post['Content-Type'] = 'application/json';
      const pancakeShopId = await axios
        .get('/shops/' + pancake_shop_id + '/orders?api_key=' + pancake_shop_key, {})
        .then((result) => {
          console.log('logData', result);
          // if (result.data.data == false) {
          //   return false;
          // }
          return result.data;
        })
        .catch((err) => {
          console.error(err);
        });
      // https://pos.pages.fm/api/v1/shops/3123004?api_key=d18818a86cdf41bcb1c9048a318da83b
      // console.log("pancakeShopId", pancakeShopId);
      // if (pancakeShopId?.error_code) throw ApiCodeResponse.PANCAKE_SHOP_FOUNT;
      if (pancakeShopId.error_code == 104) throw ApiCodeResponse.PANCAKE_SHOP_FOUNT;
    }
    // return withSuccess(1);

    const defaultPakage = await Pakage.findOne({
      attributes: {
        include: [
          [sequelize.literal('`PakageCategory`.`id`'), 'pakage_category_id'],
          [sequelize.literal('`PakageCategory`.`name`'), 'pakage_category_name'],
        ],
      },
      include: [
        {
          model: PakageCategory,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { status: PAKAGE_STATUS.ACTIVE, is_default: IS_DEFAULT.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });

    const createdShop = await sequelize.transaction(async (transaction) => {
      const createdShop = await this._create(
        {
          name,
          phone,
          email,
          pancake_shop_key,
          pancake_shop_id,
          stream_minutes_available: defaultPakage?.dataValues?.minus || 0,
          profile_picture_url,
        },
        { transaction },
      );
      if (defaultPakage) {
        await PakageHistory.create(
          {
            shop_id: createdShop.id,
            pakage_id: defaultPakage.dataValues.id,
            pakage_category_id: defaultPakage.dataValues.pakage_category_id,
            pakage_category_name: defaultPakage.dataValues.pakage_category_name,
            name: defaultPakage.dataValues.name,
            current_minutes: defaultPakage.dataValues.minus,
            minus: defaultPakage.dataValues.minus,
            price: 0,
          },
          { transaction },
        );
      }
      await User.create(
        {
          user_name: phone,
          password,
          name: name_user || name,
          phone,
          email,
          df_type_user_id,
          shop_id: createdShop.id,
          create_by: loggedInUser.id,
          profile_picture_url,
        },
        { transaction },
      );
      return createdShop;
    });
    const foundCreatedShop = await this._findOne({
      where: { id: createdShop.id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.literal(`(
               SELECT COUNT(*)
               FROM \`product\`
               WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
               AND product.shop_id = \`Shop\`.id
                 )`),
            'count_product',
          ],
          [
            sequelize.literal(`(
               SELECT COUNT(*)
               FROM \`order\`
               WHERE order.is_active = ${IS_ACTIVE.ACTIVE}
               AND order.shop_id = \`Shop\`.id
             )`),
            'count_order',
          ],
          [
            sequelize.literal(`(
               SELECT COUNT(*)
               FROM livestream
               WHERE livestream.is_active = ${IS_ACTIVE.ACTIVE}
               AND livestream.shop_id = \`Shop\`.id
             )`),
            'count_livestream',
          ],
          [
            sequelize.literal(`(
                 SELECT name
                 FROM pakage_history
                 WHERE pakage_history.is_active = ${IS_ACTIVE.ACTIVE}
                 AND pakage_history.shop_id = \`Shop\`.id
                 ORDER BY pakage_history.id DESC
                 LIMIT 1
               )`),
            'pakage_name',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    return withSuccess(foundCreatedShop);
  }

  /**
   *  @summary get list shop (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/')
  public async getListShop(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: {
          name: { [Op.like]: `%${search}%` },
          phone: { [Op.like]: `%${search}%` },
        },
      }),
    };
    if (status === SHOP_STATUS.ACTIVE || status === SHOP_STATUS.INACTIVE) {
      whereOption.status = status;
    }
    const { count, rows } = await Shop.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM \`product\`
                WHERE product.is_active = ${IS_ACTIVE.ACTIVE}
                AND product.shop_id = \`Shop\`.id
                  )`),
            'count_product',
          ],
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM \`order\`
                WHERE order.is_active = ${IS_ACTIVE.ACTIVE}
                AND order.shop_id = \`Shop\`.id
              )`),
            'count_order',
          ],
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM livestream
                WHERE livestream.is_active = ${IS_ACTIVE.ACTIVE}
                AND livestream.shop_id = \`Shop\`.id
              )`),
            'count_livestream',
          ],
          [
            sequelize.literal(`(
                SELECT name
                FROM pakage_history
                WHERE pakage_history.is_active = ${IS_ACTIVE.ACTIVE}
                AND pakage_history.shop_id = \`Shop\`.id
                ORDER BY pakage_history.id DESC
                LIMIT 1
              )`),
            'pakage_name',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: PakageHistory,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      group: ['shop.id'],
      order: [['id', 'desc']],
      // logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary list history pakage by shop_id (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/{shop_id}/history-pakage')
  public async getListHistoryPakage(
    @Request() request: AppTypes.RequestAuth,
    shop_id: number,
    @Query('pakage_category_id') pakage_category_id?: any,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      shop_id,
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (pakage_category_id) {
      whereOption.pakage_category_id = pakage_category_id;
    }

    const { count, rows } = await PakageHistory.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      limit,
      offset,
      order: [['id', 'desc']],
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary get detail Shop info by id (any)
   */
  @Security('jwt', ['admin'])
  @Get('/{id}/detail')
  public async getDetailShopInfo(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loginUser = request.user.data;
    const foundShop = await this._findOne({
      where: {
        id,
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
    });
    if (!foundShop) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST);
    }
    return withSuccess(foundShop);
  }

  /**
   * @summary delete list shop by id: Array<number> (admin)
   */
  @Security('jwt', ['admin'])
  @Delete('/')
  public async deleteShops(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const resUpdated = await sequelize.transaction(async (transaction) => {
      const resUpdate = await this._update(
        {
          is_active: IS_ACTIVE.INACTIVE,
          delete_by: loggedInUser.id,
          delete_at: new Date(),
        },
        {
          where: {
            id: { [Op.in]: body.id },
            is_active: IS_ACTIVE.ACTIVE,
          },
          transaction,
        },
      );
      await User.update(
        {
          update_by: loggedInUser.id,
          update_at: new Date(),
          is_active: IS_ACTIVE.INACTIVE,
        },
        { where: { shop_id: { [Op.in]: body.id }, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      return resUpdate;
    });

    return withSuccess(resUpdated);
  }

  /**
   * @summary change shop status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/status')
  public async changeShopStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundShop = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundShop) throw new AppError(ApiCodeResponse.ACCOUNT_NOT_EXIST);
    const checkOrder = await Order.findAll({
      where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.PENDING, shop_id: id },
    });
    if (checkOrder.length > 0) throw new AppError(ApiCodeResponse.SHOP_NOT_CHANGE_STATUS);
    await this._update(
      {
        status: foundShop.status === SHOP_STATUS.ACTIVE ? SHOP_STATUS.INACTIVE : SHOP_STATUS.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      {
        where: {
          id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    return withSuccess({
      status: foundShop.status === SHOP_STATUS.ACTIVE ? SHOP_STATUS.INACTIVE : SHOP_STATUS.ACTIVE,
    });
  }

  /**
   * @summary change livestream status (1 <=> 0) (admin)
   */
  @Security('jwt', ['admin'])
  @Put('/{id}/livestream-status')
  public async changeLivestreamStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const foundShop = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundShop) throw new AppError(ApiCodeResponse.SHOP_NOT_EXIST);

    const livestream_enable =
      foundShop.livestream_enable === LIVESTREAM_ENABLE.ACTIVE ? LIVESTREAM_ENABLE.INACTIVE : LIVESTREAM_ENABLE.ACTIVE;
    await foundShop.update({
      livestream_enable,
      update_by: loggedInUser.id,
      update_at: new Date(),
    });

    return withSuccess({ livestream_enable });
  }

  /**
   * @summary add pakage (admin)
   */
  @Security('jwt', ['admin'])
  @Post('/add-pakage')
  public async createPakage(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { shop_id: number; pakage_id: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;

    const shopData = await this._findOne({
      where: { id: body.shop_id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!shopData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Shop không tồn tại');
    const pakageData = await Pakage.findOne({
      attributes: {
        include: [
          [sequelize.literal('`PakageCategory`.`id`'), 'pakage_category_id'],
          [sequelize.literal('`PakageCategory`.`name`'), 'pakage_category_name'],
        ],
      },
      include: [
        {
          model: PakageCategory,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      where: { id: body.pakage_id, is_active: IS_ACTIVE.ACTIVE, status: PAKAGE_STATUS.ACTIVE },
    });
    if (!pakageData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Gói không tồn tại.');

    const dataTransaction = await sequelize.transaction(async (transaction) => {
      let current_minutes = 0;
      if (pakageData.dataValues.pakage_category_id === PAKAGE_CATEGORY.ADD_PAKAGE) {
        current_minutes = shopData.dataValues.stream_minutes_available + pakageData.dataValues.minus;
        await shopData.increment({ stream_minutes_available: pakageData.dataValues.minus }, { transaction });
      } else {
        current_minutes = shopData.dataValues.stream_minutes_available - pakageData.dataValues.minus;
        if (current_minutes < 0) current_minutes = 0;
        await shopData.update({ stream_minutes_available: current_minutes }, { transaction });
        // await shopData.decrement({ stream_minutes_available: pakageData.dataValues.minus }, { transaction });
      }
      const pakageHistory = await PakageHistory.create(
        {
          shop_id: shopData.id,
          pakage_id: pakageData.dataValues.id,
          pakage_category_id: pakageData.dataValues.pakage_category_id,
          pakage_category_name: pakageData.dataValues.pakage_category_name,
          name: pakageData.dataValues.name,
          current_minutes: current_minutes,
          minus: pakageData.dataValues.minus,
          price: pakageData.dataValues.price,
        },
        { transaction },
      );

      return pakageHistory;
    });
    return withSuccess(dataTransaction);
  }

  /**
   * @summary get data statistic shop (admin)
   */
  @Security('jwt', ['admin'])
  @Get('/{shop_id}/statistic-shop')
  public async getDetailStatisticShop(
    shop_id: number,
    @Request() request: AppTypes.RequestAuth,
    @Query('type') type?: number,
    @Query('search') search?: string | any,
    @Query('status') status?: number | any,
    @Query('category_id') category_id?: number | any,
    @Query('children_category_id') children_category_id?: number | any,
    @Query('stock_id') stock_id?: number | any,
    @Query('from_date') from_date?: Date | number | any,
    @Query('to_date') to_date?: Date | number | any,
    @Query('order_status') order_status?: number | any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    if (type === TAB_SHOP.HISTORY) {
      const whereOption: any = {
        shop_id,
        is_active: IS_ACTIVE.ACTIVE,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      const detailShop = Shop.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id },
      });
      const { count, rows } = await PakageHistory.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'version'],
        },
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess({ detailShop, rows }, { page, limit, totalItemCount: count });
    } else if (type === TAB_SHOP.PRODUCT) {
      const listCategory = await Category.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          parent_id: { [Op.ne]: null },
        },
        include: {
          model: Product,
          where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shop_id },
          attributes: [],
        },
      });

      const listStock = await Stock.findAll({
        attributes: ['id', 'name'],
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          shop_id: shop_id,
        },
      });
      const whereOptions: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
      };
      if (status || status === 0) {
        whereOptions.status = status;
      }
      if (children_category_id != undefined && children_category_id != null && children_category_id != 0) {
        whereOptions.category_id = children_category_id != 0 ? children_category_id : { [Op.ne]: null };
      }

      const { rows, count } = await Product.findAndCountAll({
        // subQuery: false,
        attributes: [
          'id',
          'name',
          'code',
          'status',
          'stock_status',
          // [Sequelize.literal('`Category`.`name`'), 'category_name'],
          [
            Sequelize.literal(`(
                         SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                         FROM \`order\` as od
                         join order_item as order_item
                         ON order_item.order_id = od.id
                         WHERE od.shop_id = ${shop_id}
                             AND
                             od.is_active = ${IS_ACTIVE.ACTIVE}
                             AND
                             order_item.product_id = Product.id
                             AND
                             order_item.is_active = ${IS_ACTIVE.ACTIVE}
                             AND
                             od.status = ${ORDER_STATUS.SUCCCESS}
                         )`),
            'total_price',
          ],
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join \`order_item\` as order_item
                        ON order_item.order_id = od.id
                        WHERE 
                            od.shop_id is not null
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
            'total_amount',
          ],
        ],
        where: whereOptions,
        include: [
          // {
          //   model: Category,
          //   attributes: ['id', 'name', 'parent_id', 'icon_url'],
          //   include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
          //   where: {
          //     is_active: IS_ACTIVE.ACTIVE,
          //     id: category_id ? category_id : { [Op.ne]: null },
          //   },
          // },
          {
            model: Category,
            // required: false,
            attributes: ['id', 'name', 'parent_id'],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
              // status: IS_DEFAULT.ACTIVE,
            },
            include: {
              model: Category,
              as: 'parent_category',
              attributes: ['id', 'name', 'parent_id', 'icon_url'],
            },
          },
          {
            model: OrderItem,
            required: false,
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id ? stock_id : { [Op.ne]: null },
            },
            include: {
              model: Order,
              required: false,
              attributes: ['id'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          },
          {
            model: ProductStock,
            required: false,
            attributes: ['id', 'product_id', 'stock_id'],
            where: {
              is_active: IS_ACTIVE.ACTIVE,
              stock_id: stock_id ? stock_id : { [Op.ne]: null },
            },
          },
        ],
        group: ['product.id'],
        order: [['id', 'desc']],
        limit,
        offset,
      });
      return withPagingSuccess(
        { rows, listCategory, listStock },
        { page, limit, totalItemCount: _.isArray(count) ? count.length : count },
      );
    } else if (type === TAB_SHOP.ORDER) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && {
          [Op.or]: [
            { code: { [Op.like]: `%${search}%` } },
            { '$User.phone$': { [Op.like]: `%${search}%` } },
            { '$User.name$': { [Op.like]: `%${search}%` } },
          ],
        }),
      };
      // return withSuccess(order_status);
      if (order_status != null && order_status != undefined && order_status != '') {
        whereOption.status = order_status;
      }
      const { rows, count } = await Order.findAndCountAll({
        where: whereOption,
        // subQuery: false,
        attributes: [
          'id',
          'code',
          'user_id',
          'user_address_id',
          // [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
          // [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
          'create_at',
          'update_at',
          'total_price',
          'shop_id',
          [
            Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id = ${shop_id}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.order_id = Order.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
            'total_amount',
          ],
          'status',
        ],
        include: [
          {
            model: UserAddress,
            required: false,
            where: { is_active: IS_ACTIVE.ACTIVE },
            include: [
              {
                model: DFWard,
                where: { is_active: IS_ACTIVE.ACTIVE },
                required: false,
                attributes: ['id', 'name'],
              },
              {
                model: DFDistrict,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
              {
                model: DFProvince,
                required: false,
                where: { is_active: IS_ACTIVE.ACTIVE },
                attributes: ['id', 'name'],
              },
            ],
          },
          {
            model: User,
            attributes: ['id', 'name', 'phone'],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          {
            model: Shop,
            // required: true,
            where: {
              status: SHOP_STATUS.ACTIVE,
              is_active: IS_ACTIVE.ACTIVE,
            },
          },
        ],
        order: [['create_at', 'DESC']],
        group: [['Order.id']],
        limit,
        offset,
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
    } else if (type === TAB_SHOP.LIVESTREAM) {
      const whereOption: any = {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shop_id,
        create_at: {
          [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
        },
        ...(search && { [Op.or]: { title: { [Op.like]: `%${search}%` } } }),
      };
      const { count, rows } = await Livestream.findAndCountAll({
        where: whereOption,
        attributes: {
          exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
        },
        include: [
          {
            model: Shop,
            required: true,
            attributes: {
              exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
            },
            where: {
              livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
              status: SHOP_STATUS.ACTIVE,
              is_active: IS_ACTIVE.ACTIVE,
            },
          },
        ],
        limit,
        offset,
        order: [['id', 'desc']],
      });
      return withPagingSuccess(rows, { page, limit, totalItemCount: count });
    }
  }

  /**
   * @summary delete post by id (shop, shop_member)
   */
  // @Security('jwt', ['shop', 'shop_member'])
  // @Delete('/{id}')
  // public async deletePost(
  //   @Request() request: AppTypes.RequestAuth,
  //   id: number,
  // ): Promise<AppTypes.SuccessResponseModel<any>> {
  //   const loggedInUser = request?.user?.data;

  //   const foundPost = await this._findOne({
  //     where: {
  //       id,
  //       is_active: IS_ACTIVE.ACTIVE,
  //     },
  //   });
  //   if (!foundPost) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bài đăng không tồn tại.');
  //   if (
  //     foundPost.dataValues.user_id !== loggedInUser.id &&
  //     foundPost.dataValues.shop_id !== loggedInUser.shop_id &&
  //     loggedInUser.df_type_user_id !== ROLE.ADMIN &&
  //     loggedInUser.df_type_user_id !== ROLE.ADMIN_EDITOR
  //   )
  //     throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Bạn không có quyền xóa bài đăng này.');
  //   await foundPost.update({
  //     is_active: IS_ACTIVE.INACTIVE,
  //     delete_by: loggedInUser.id,
  //     delete_at: new Date(),
  //   });
  //   if (foundPost.dataValues.is_posted === IS_POSTED.NOT_POSTED) schedulePost.cancelSchedulePost(id);
  //   return withSuccess({});
  // }

  /**
   * @summary Ngưng Follow shop
   */
  @Security('jwt', ['admin'])
  @Put('/{shop_id}/status-follow')
  public async changeStatusShopAdmin(
    @Request() request,
    @Body() body: { status?: number },
    shop_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      status: Joi.number().required(),
    });
    const bodyData = await schema.validateAsync(body);
    const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id } });
    if (!shop) throw ApiCodeResponse.SHOP_NOT_EXIST;
    const shopUpdate = await Shop.update(
      {
        status_follow: bodyData.status,
      },
      { where: { is_active: IS_ACTIVE.ACTIVE, id: shop_id } },
    );
    return withSuccess(shopUpdate);
  }
}
