import { Security, Get, Query, Route, Tags, Request, Delete } from 'tsoa';
import * as _ from 'lodash';
import { withSuccess, withPagingSuccess } from '@utils/BaseResponse';
import {
  IS_ACTIVE,
  LIVESTREAM_STATUS,
  LIVESTREAM_ENABLE,
  SHOP_STATUS,
  IS_HIGHLIGHT_LIVESTREAM_PRODUCT,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '../';
import { AppTypes } from 'types';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Utils } from '@utils/Utils';
const db = require('@models');
const { sequelize, Sequelize, Shop, Livestream, Product, LivestreamProduct, LivestreamLayout } = db.default;
const { Op } = Sequelize;

@Route('admin/livestream')
@Tags('admin/livestreams')
export class AdminLivestreamController extends ApplicationController {
  constructor() {
    super('Livestream');
  }
  /**
   * @summary list Livestream (admin)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_sell_manager', 'admin_service_manager'])
  @Get('/')
  public async listLiveStream(
    @Request() request: AppTypes.RequestAuth,
    @Query('shop_id') shop_id?: number,
    @Query('search') search?: string,
    @Query('from_date') from_date?: Date | number,
    @Query('to_date') to_date?: Date | number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { offset, limit, page } = handlePagingMiddleware(request);
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    let whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      start_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      [Op.or]: [{ status: LIVESTREAM_STATUS.STREAMING }, { status: LIVESTREAM_STATUS.FINISHED }],
    };
    if (shop_id) {
      whereOption.shop_id = shop_id;
    }
    if (search) {
      whereOption.title = { [Op.like]: `%${search}%` };
    }
    const { count, rows } = await Livestream.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [
        ['status', 'asc'],
        ['id', 'desc'],
      ],
    });
    // const fileredLivestream = rows.map((item: any) => {
    //   const valueItem = item.dataValues;
    //   if (valueItem.status === LIVESTREAM_STATUS.STREAMING) {
    //     // const count_subcriber = this._countSubcriber(valueItem.id);
    //     const count_subcriber = 0;
    //     item.setDataValue('count_subcriber', count_subcriber);
    //   }
    //   return item;
    // });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary detail Livestream (admin)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_sell_manager', 'admin_service_manager'])
  @Get('/{livestream_id}/detail')
  public async detailLiveStream(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const baseUrl = Utils.getBaseServer(request);
    const livestreamData = await this._findOne({
      where: { id: livestream_id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: [
            [sequelize.col('id'), 'livestream_product_id'],
            'code_product_livestream',
            'is_highlight',
            [sequelize.literal('`LivestreamProducts->Product`.`id`'), 'id'],
            [sequelize.literal('`LivestreamProducts->Product`.`shop_id`'), 'shop_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`category_id`'), 'category_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`code`'), 'code'],
            [sequelize.literal('`LivestreamProducts->Product`.`name`'), 'name'],
            [sequelize.literal('`LivestreamProducts->Product`.`description`'), 'description'],
            [sequelize.literal('`LivestreamProducts->Product`.`price`'), 'price'],
            [sequelize.literal('`LivestreamProducts->Product`.`star`'), 'star'],
            [sequelize.literal('`LivestreamProducts->Product`.`status`'), 'status'],
            [sequelize.literal('`LivestreamProducts->Product`.`stock_status`'), 'stock_status'],
            [sequelize.literal('`LivestreamProducts->Product`.`is_active`'), 'is_active'],
            [sequelize.literal('`LivestreamProducts->Product`.`create_at`'), 'create_at'],
            [sequelize.literal('`LivestreamProducts->Product`.`update_at`'), 'update_at'],

            [
              sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                  limit 1
                  ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                  SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                  FROM product_price AS product_price
                  WHERE
                  product_price.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  )`),
              'min_max_price',
            ],
          ],
          include: {
            model: Product,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });
    if (!livestreamData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Livestream không tồn tại!');

    const HighlightProduct = livestreamData.dataValues.LivestreamProducts.find(
      (item: any) => item.dataValues.is_highlight === IS_HIGHLIGHT_LIVESTREAM_PRODUCT.HIGHLIGHT,
    );
    livestreamData.setDataValue('HighlightProduct', HighlightProduct);
    return withSuccess(livestreamData);
  }

  /**
   * @summary delete Livestream (shop, admin)
   */
  @Security('jwt', ['admin'])
  @Delete('/{livestream_id}')
  public async deleteLiveStream(
    @Request() request: AppTypes.RequestAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: userDecoded.id,
        delete_at: new Date(),
      },
      { where: { id: livestream_id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess({});
  }
}
