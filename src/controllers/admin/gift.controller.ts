import { IS_ACTIVE, IS_GIFT_ACTIVE, GIFT_STATUS, DF_NOTIFICATION } from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from '..';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { NotificationService } from '@services/internal/notification.service';
import { Body, Request, Get, Post, Put, Query, Route, Delete, Tags, Security } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../../utils/BaseResponse';

import * as _ from 'lodash';
import { UserTypes, AppTypes, GiftTypes } from 'types';
import { GiftService } from '@services/internal/gift.service';

const db = require('@models');
const { sequelize, Sequelize, Gift, DFTypeGift, PurchasedGift, User } = db.default;
const { Op } = Sequelize;

@Route('admin/gift')
@Tags('admin/gifts')
export class AdminGiftController extends ApplicationController {
  private giftService: GiftService;
  private notificationService: NotificationService;
  constructor() {
    super('Gift');
    this.giftService = new GiftService();
    this.notificationService = new NotificationService();
  }

  /**
   * @summary create a gift (admin, admin_editor, admin_service_manager)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Post('/')
  public async createGift(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: GiftTypes.CreateGiftModel,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const bodyData: any = await this.giftService.CreateGiftSchema.validateAsync(body);
    const createdGift = await this._create({
      ...bodyData,
      create_by: loggedInUser.id,
    });
    return withSuccess(createdGift);
  }

  /**
   * @summary list gift (admin, admin_editor, admin_service_manager)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Get('/')
  public async getListGift(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('df_type_gift_id') df_type_gift_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };
    if (search) {
      whereOption.name = { [Op.like]: `%${search}%` };
    }
    if (df_type_gift_id) {
      whereOption.df_type_gift_id = df_type_gift_id;
    }

    if (status === IS_GIFT_ACTIVE.ACTIVE || status === IS_GIFT_ACTIVE.INACTIVE) {
      whereOption.status = status;
    }
    const { count, rows } = await Gift.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [sequelize.literal('`DFTypeGift`.`name`'), 'type_gift_name'],
          [
            sequelize.literal(`(
            SELECT COUNT(*)
            FROM \`purchased_gift\`
            WHERE purchased_gift.is_active = ${IS_ACTIVE.ACTIVE}
            AND purchased_gift.gift_id = \`Gift\`.id
            
            )`),
            'count_purchased_gift',
          ],
          /**
           AND 
            (
              purchased_gift.status = ${GIFT_STATUS.USED}
              OR
              purchased_gift.status = ${GIFT_STATUS.SENT_GIFT}
            ) 
           */
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: DFTypeGift,
          required: false,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        // {
        //   model: PurchasedGift,
        //   required: false,
        //   attributes: ['id'],
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        // },
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary update gift data (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Put('/{id}')
  public async updateGift(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: GiftTypes.UpdateGiftModel,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const bodyData: GiftTypes.UpdateGiftModel = await this.giftService.UpdateGiftSchema.validateAsync(body);

    const updatedPakage = await this._update(
      {
        ...bodyData,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      { where: { id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess(updatedPakage);
  }

  /**
   * @summary change gift status (1 <=> 0) (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Put('/{id}/status')
  public async changeGiftStatus(
    @Request() request: AppTypes.RequestAuth,
    id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const foundGift = await this._findOne({
      where: { id, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!foundGift) throw new AppError(ApiCodeResponse.DATA_EXIST).with('Quà tặng không tồn tại.');
    await this._update(
      {
        status: foundGift.status === IS_GIFT_ACTIVE.ACTIVE ? IS_GIFT_ACTIVE.INACTIVE : IS_GIFT_ACTIVE.ACTIVE,
        update_by: loggedInUser.id,
        update_at: new Date(),
      },
      { where: { id, is_active: IS_ACTIVE.ACTIVE } },
    );
    return withSuccess({
      status: foundGift.status === IS_GIFT_ACTIVE.ACTIVE ? IS_GIFT_ACTIVE.INACTIVE : IS_GIFT_ACTIVE.ACTIVE,
    });
  }

  /**
   * @summary delete list gift by id: Array<number> (admin, admin_editor)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Delete('/')
  public async deleteListGift(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const resUpdate = await this._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
        delete_by: loggedInUser.id,
        delete_at: new Date(),
      },
      {
        where: {
          id: { [Op.in]: body.id },
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    );
    // if (resUpdate[0] === 0) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Quà tặng không tồn tại.');
    return withSuccess({});
  }
  /**
   * @summary list request purchase gift (admin, admin_editor, admin_service_manager)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Get('/purchase-gift')
  public async getListRequestPurchaseGift(
    @Request() request: AppTypes.RequestAuth,
    @Query('search') search?: string,
    @Query('status') status?: number,
    @Query('df_type_gift_id') df_type_gift_id?: number,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };

    if (status || status === 0) {
      whereOption.status = status;
    }

    const SearchWhereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (search) {
      SearchWhereOption.name = { [Op.like]: `%${search}%` };
    }

    const giftWhereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (df_type_gift_id) {
      giftWhereOption.df_type_gift_id = df_type_gift_id;
    }

    const { count, rows } = await PurchasedGift.findAndCountAll({
      where: whereOption,
      attributes: {
        include: [
          [sequelize.literal('`User`.`name`'), 'customer_name'],
          [sequelize.literal('`User`.`phone`'), 'phone'],
          [sequelize.literal('`Gift`.`name`'), 'gift_name'],
          [sequelize.literal('`Gift`.`price`'), 'point'],
          [sequelize.literal('`Gift->DFTypeGift`.`name`'), 'type_gift_name'],
          // [sequelize.literal('`Gift->Order`.`code`'), 'order_code'],
          [
            sequelize.literal(`(
              SELECT name
              FROM df_province AS df_province
              WHERE df_province.id =  JSON_EXTRACT(address, "$.df_province_id") 
              AND df_province.is_active = ${IS_ACTIVE.ACTIVE}
              )`),
            'province_name',
          ],
          [
            sequelize.literal(`(
              SELECT name
              FROM df_district AS df_district
              WHERE df_district.id = JSON_EXTRACT(address, "$.df_district_id") 
              AND df_district.is_active = ${IS_ACTIVE.ACTIVE}
              )`),
            'district_name',
          ],
          [
            sequelize.literal(`(
              SELECT name
              FROM df_ward AS df_ward
              WHERE df_ward.id = JSON_EXTRACT(address, "$.df_ward_id") 
              AND df_ward.is_active = ${IS_ACTIVE.ACTIVE}
              )`),
            'ward_name',
          ],

          [
            sequelize.literal(`(
              SELECT code
              FROM \`order\`
              WHERE JSON_EXTRACT(gift_code, "$.purchase_gift_id") =  \`PurchasedGift\`.\`id\`
              AND  \`order\`.\`is_active\` = ${IS_ACTIVE.ACTIVE}
              LIMIT 1
              )`),
            'order_code',
          ],

          [
            sequelize.literal(`(
              SELECT user_address.name
              FROM \`order\`
              JOIN user_address
              ON \`order\`.user_address_id = user_address.id
              WHERE JSON_EXTRACT(gift_code, "$.purchase_gift_id") =  \`PurchasedGift\`.\`id\`
              AND  \`order\`.\`is_active\` = ${IS_ACTIVE.ACTIVE}
              LIMIT 1
              )`),
            'order_user_name',
          ],

          [
            sequelize.literal(`(
              SELECT user_address.phone
              FROM \`order\`
              JOIN user_address
              ON \`order\`.user_address_id = user_address.id
              WHERE JSON_EXTRACT(gift_code, "$.purchase_gift_id") =  \`PurchasedGift\`.\`id\`
              AND  \`order\`.\`is_active\` = ${IS_ACTIVE.ACTIVE}
              LIMIT 1
              )`),
            'order_user_phone',
          ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: User,
          required: true,
          attributes: [],
          where: SearchWhereOption,
        },
        // {
        //   model: Order,
        //   required: false,
        //   attributes: [],
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        // },
        {
          model: Gift,
          required: true,
          attributes: {
            include: [],
            exclude: ['create_by', 'update_by', 'delete_by', 'version'],
          },
          where: giftWhereOption,
          include: {
            model: DFTypeGift,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
      ],
      limit,
      offset,
      order: [['id', 'DESC']],
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count });
  }

  /**
   * @summary confirm purchase gift (admin, admin_editor, admin_service_manager)
   */
  @Security('jwt', ['admin', 'admin_editor', 'admin_service_manager'])
  @Put('/purchase-gift/confirm')
  public async confirmPurchaseGift(
    @Request() request: AppTypes.RequestAuth,
    @Body() body: { purchase_gifts_id: Array<number> },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loggedInUser = request?.user?.data;
    const updated = [];
    const not_found = [];
    await sequelize.transaction(async (transaction) => {
      for (let i = 0; i < body.purchase_gifts_id.length; i++) {
        const findPurchaseGift = await PurchasedGift.findOne({
          where: { id: body.purchase_gifts_id[i], status: GIFT_STATUS.PENDING, is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: Gift,
            required: true,
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        });
        if (findPurchaseGift) {
          // xác nhận gửi quà cho user
          await findPurchaseGift.update({
            status: GIFT_STATUS.SENT_GIFT,
            update_by: loggedInUser.id,
            update_at: new Date(),
          });
          // findPurchaseGift.Gift.name
          await this.createNotification(
            DF_NOTIFICATION.CONFIRM_PURCHASE_GIFT,
            findPurchaseGift.user_id,
            GIFT_STATUS.CONFIRMED,
            body.purchase_gifts_id[i],
            findPurchaseGift.Gift.name,
            transaction,
          );
          updated.push(body.purchase_gifts_id[i]);
        } else {
          not_found.push(body.purchase_gifts_id[i]);
        }
      }
    });
    return withSuccess({ updated, not_found });
  }

  // /**
  //  * @summary cancel purchase gift (admin, admin_editor)
  //  */
  // @Security('jwt', ['admin', 'admin_editor'])
  // @Put('/purchase-gift/cancel')
  // public async cancelPurchaseGift(
  //   @Request() request: AppTypes.RequestAuth,
  //   @Body() body: { purchase_gifts_id: Array<number> },
  // ): Promise<AppTypes.SuccessResponseModel<any>> {
  //   const loggedInUser = request?.user?.data;
  //   const updated = [];
  //   const not_found = [];
  //   await sequelize.transaction(async (transaction) => {
  //     for (let i = 0; i < body.purchase_gifts_id.length; i++) {
  //       const findPurchaseGift = await PurchasedGift.findOne({
  //         where: { id: body.purchase_gifts_id[i], status: GIFT_STATUS.PENDING, is_active: IS_ACTIVE.ACTIVE },
  //         include: {
  //           model: Gift,
  //           required: true,
  //           where: { is_active: IS_ACTIVE.ACTIVE },
  //         },
  //       });
  //       if (findPurchaseGift) {
  //         await findPurchaseGift.update({
  //           status: GIFT_STATUS.CANCELED,
  //           update_by: loggedInUser.id,
  //           update_at: new Date(),
  //         });
  //         await this.createNotification(
  //           DF_NOTIFICATION.CONFIRM_PURCHASE_GIFT,
  //           findPurchaseGift.user_id,
  //           GIFT_STATUS.CANCELED,
  //           body.purchase_gifts_id[i],
  //           findPurchaseGift.Gift.name,
  //           transaction,
  //         );
  //         updated.push(body.purchase_gifts_id[i]);
  //       } else {
  //         not_found.push(body.purchase_gifts_id[i]);
  //       }
  //     }
  //   });

  //   return withSuccess({ updated, not_found });
  // }

  // create gift noti
  public async createNotification(id, id_user, status, purchase_gifts_id, name, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content =
      status == GIFT_STATUS.CONFIRMED
        ? 'Yêu cầu đổi quà "' + name + '" của bạn đã được xác nhận'
        : 'Yêu cầu đổi quà "' + name + '" của bạn đã bị từ chối';
    const df_notification_id = notiType.id;
    const data = { purchase_gifts_id: purchase_gifts_id };
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }
}
