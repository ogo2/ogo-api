import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { GOOGLE_API_KEY, IS_ACTIVE, PANCAKE, PLACE_TYPES } from '@utils/constants';
import { Body, Delete, Get, Post, Put, Query, Request, Route, Tags } from 'tsoa';
import { AppTypes } from 'types';
import { ApplicationController } from '.';
import { withPagingSuccess, withSuccess } from '../utils/BaseResponse';
import Joi from '../utils/JoiValidate';

import _ = require('lodash');
import { parseInt } from 'lodash';
import { AppError } from '@utils/AppError';

const db = require('@models');
const {
  sequelize,
  Sequelize,
  DFProvince,
  Product,
  User,
  Enterprise,
  ProductPrice,
  Stock,
  District,
  Ward,
  ProductStock,
  Shop,
} = db.default;
const { Op } = Sequelize;
const axios = require('axios');
const { parse } = require('node-html-parser');

@Route('stock')
@Tags('stock')
export class StockController extends ApplicationController {
  constructor() {
    super('Stock');
  }

  /**
   * @summary Danh sách kho shop
   * @param search
   * @param status
   * @param category_id
   * @param is_public
   */
  // @Security('jwt', ['enterprise'])
  @Get('/')
  public async listStock(
    @Request() request: any,
    @Query() search?: string,
    @Query() status?: number,
    @Query() category_id?: number,
    @Query() is_public?: number,
    @Query() product_id?: number,
    @Query() array_stock_id?: string,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const tokens = request.headers.token;
    let shopId;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      // const shop = await Shop.findOne({
      //   where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
      // });
      shopId = loginUser ? loginUser.shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const paging = handlePagingMiddleware(request);
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` } } }),
      shop_id: shopId,
    };
    // const stockArr = JSON.parse(`[${array_stock_id}]`);
    const { rows, count } = await Stock.findAndCountAll({
      where: whereOption,
      include: {
        model: ProductStock,
        required: false,
        // required: product_id ? true : false,
        where: { is_active: IS_ACTIVE.ACTIVE, product_id: product_id ? product_id : { [Op.ne]: null } },
      },
      order: [['id', 'desc']],
      limit: paging.limit,
      offset: paging.offset,
      group: ['id'],
    });
    return withPagingSuccess(rows, {
      page: paging.page,
      totalItemCount: _.isArray(count) ? count.length : count,
      limit: paging.limit,
    });
  }

  /**
   * @summary lấy địa chỉ
   */
  // @Security('jwt', [ROLE_NAMES.ENTERPRISE])
  @Get('/place')
  public async getPlaceData(@Request() request: any, @Query() placeId?: string): Promise<any> {
    //AppTypes.PagingResponseModel<any>
    const res = await axios.get(
      `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${GOOGLE_API_KEY}&language=vi`,
    );

    if (!res.data.result) throw ApiCodeResponse.NOT_FOUND;

    const html = parse(res.data.result.adr_address);
    const listPlace = html.childNodes;

    let province = '';
    let district = '';
    let ward = '';

    listPlace.forEach((item) => {
      if (item.childNodes.length > 0 && item.classNames.length > 0) {
        if (item.classNames.includes(PLACE_TYPES.PROVINCE)) province = item.childNodes[0].rawText;
        if (item.classNames.includes(PLACE_TYPES.DISTRICT)) {
          district = item.childNodes[0].rawText.split('.');

          district = district[district.length - 1];
        }
        if (item.classNames.includes(PLACE_TYPES.WARD)) {
          ward = item.childNodes[0].rawText.split(',');

          ward = ward[ward.length - 1];
        }
      }
    });
    const { lat, lng } = res.data.result.geometry.location;
    const address = res.data.result.formatted_address;

    return withSuccess({ lat, long: lng, address });
  }
  @Get('/place-auto')
  public async getPlaceAuto(@Request() request: any, @Query() address?: string): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    let autoCompleteLink = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${encodeURI(
      address,
    )}&key=${GOOGLE_API_KEY}&language=vi&components=country:vn`;

    let res = await axios.get(autoCompleteLink);

    if (res.data.predictions.length > 0) return withSuccess(res.data);

    throw ApiCodeResponse.NOT_FOUND;
  }

  /**
   * @summary Tạo địa chỉ kho hàng
   */
  // @Security('jwt', ['enterprise'])
  @Post('/')
  public async createStock(
    @Request() request: any,
    @Body()
    body: {
      name: string;
      address: string;
      df_province_id: number;
      df_district_id: number;
      df_ward_id: number;
      lat: number;
      long: number;
      product_id?: number;
    },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let shopId;
    let phone;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;

      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      // return withSuccess(shop);
      // const shop = await Shop.findOne({
      //   where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
      // });
      shopId = loginUser ? loginUser.shop_id : null;
      phone = loginUser ? loginUser.phone : null;
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    // return withSuccess({ pancake_shop_key, pancake_shop_id });
    const schema = Joi.object({
      name: Joi.string(),
      address: Joi.string().required().label('Tên'),
      df_province_id: Joi.number().empty(['', null, 'null']).required(),
      df_district_id: Joi.number().empty(['', null, 'null']).required(),
      df_ward_id: Joi.number().empty(['', null, 'null']).required(),
      lat: Joi.number(),
      long: Joi.number(),
      // enterprise_id: Joi.number().empty(['null', 'undefined']).required(),
    });
    const bodyData = await schema.validateAsync(body);
    axios.defaults.baseURL = PANCAKE;
    // const options = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'access_token': bodyData.access_token
    //   }
    // };

    axios.defaults.headers.post['Content-Type'] = 'application/json';
    const pancakeStockId = await axios
      .post('/shops/' + pancake_shop_id + '/warehouses?api_key=' + pancake_shop_key, {
        address: bodyData.address,
        phone_number: phone,
        name: bodyData.name,
      })
      .then((result) => {
        console.log('logData', result);
        // if (result.data.data == false) {
        //   return false;
        // }
        return result.data.data.id;
      })
      .catch((err) => {
        console.error(err);
      });
    // console.log("pancakeStockId", pancakeStockId);
    // return withSuccess(pancakeStockId);
    bodyData.shop_id = shopId;
    bodyData.create_by = shopId;
    bodyData.df_province_id = bodyData.df_province_id == 0 ? null : bodyData.df_province_id;
    bodyData.df_district_id = bodyData.df_district_id == 0 ? null : bodyData.df_district_id;
    bodyData.df_ward_id = bodyData.df_ward_id == 0 ? null : bodyData.df_ward_id;
    bodyData.pancake_stock_id = pancakeStockId ? pancakeStockId : null;

    const data = await Stock.findAll({
      where: {
        name: { [Op.substring]: bodyData.name },
        shop_id: shopId,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });

    // return withSuccess(data);

    if (data.length > 0) throw ApiCodeResponse.STOKE_NOT_FOUND;

    const createdStock = await Stock.create(bodyData);
    if (bodyData.product_id > 0) {
      const createdProductStock = await ProductStock.create({
        product_id: bodyData.product_id,
        stock_id: createdStock.id,
      });
    }
    return withSuccess(createdStock);
  }
  /**
   * @summary Sửa địa chỉ kho hàng
   */
  // @Security('jwt', ['enterprise'])
  @Put('/{id}')
  public async updateStock(
    @Request() request: any,
    @Body()
    body: {
      name: string;
      address: string;
      df_province_id: number;
      df_district_id: number;
      df_ward_id: number;
      lat: number;
      long: number;
    },
    id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let shopId;
    let phone;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;

      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      // return withSuccess(shop);
      // const shop = await Shop.findOne({
      //   where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
      // });
      shopId = loginUser ? loginUser.shop_id : null;
      phone = loginUser ? loginUser.phone : null;
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const schema = Joi.object({
      name: Joi.string(),
      address: Joi.string().required().label('Tên'),
      df_province_id: Joi.number().empty(['', null, 'null']).required(),
      df_district_id: Joi.number().empty(['', null, 'null']).required(),
      df_ward_id: Joi.number().empty(['', null, 'null']).required(),
      lat: Joi.number(),
      long: Joi.number(),
      // enterprise_id: Joi.number().empty(['null', 'undefined']).required(),
    });
    axios.defaults.baseURL = PANCAKE;
    // const options = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //     'access_token': bodyData.access_token
    //   }
    // };
    const bodyData = await schema.validateAsync(body);

    bodyData.shop_id = shopId;
    bodyData.create_by = shopId;
    bodyData.df_province_id = bodyData.df_province_id == 0 ? null : bodyData.df_province_id;
    bodyData.df_district_id = bodyData.df_district_id == 0 ? null : bodyData.df_district_id;
    bodyData.df_ward_id = bodyData.df_ward_id == 0 ? null : bodyData.df_ward_id;
    // check stock
    const checkStock = await Stock.findOne({ where: { id: id, is_active: IS_ACTIVE.ACTIVE, shop_id: shopId } });
    if (checkStock == null) throw ApiCodeResponse.DATA_NOT_EXIST;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    const pancakeStockId = await axios
      .put(
        '/shops/' + pancake_shop_id + '/warehouses/' + checkStock.pancake_stock_id + '?api_key=' + pancake_shop_key,
        {
          address: bodyData.address,
          phone_number: phone,
          name: bodyData.name,
          id: checkStock.pancake_stock_id,
        },
      )
      .then((result) => {
        console.log('logData', result);
        // if (result.data.data == false) {
        //   return false;
        // }
        return result.data.data.id;
      })
      .catch((err) => {
        console.error(err);
      });
    const data = await Stock.findAll({
      where: {
        name: { [Op.substring]: bodyData.name },
        shop_id: shopId,
        is_active: IS_ACTIVE.ACTIVE,
        id: { [Op.ne]: id },
      },
    });

    // return withSuccess(data);

    if (data.length > 0) throw ApiCodeResponse.STOKE_NOT_FOUND;

    const updateStock = await Stock.update(bodyData, {
      where: { id: id, is_active: IS_ACTIVE.ACTIVE, shop_id: shopId },
    });
    return this.listStock(request);
  }

  // @Security('jwt', [ROLE_NAMES.ENTERPRISE])
  @Delete('/{id}')
  public async deleteStock(@Request() request, id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    // const loggedInUser = request?.user?.data as AuthorizedUser;
    const tokens = request.headers.token;
    let shopId;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      // const shop = await Shop.findOne({
      //   where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
      // });
      shopId = loginUser ? loginUser.shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const product = await Product.findAll({
      where: { shop_id: shopId, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        include: [
          [
            sequelize.fn(
              'SUM',
              Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
              and product_price.stock_id = ${id}
            )`),
            ),
            'amount',
          ],
          // [
          //   Sequelize.literal(`(
          //   SELECT IFNULL(SUM(product_price.amount), 0) from product_price
          //     where product_price.is_active = ${IS_ACTIVE.ACTIVE}
          //     and product_price.status = ${IS_ACTIVE.ACTIVE}
          //     and product_price.product_id = Product.id
          //     and product_price.stock_id = ${id}
          //   )`),
          //   'amount',
          // ],
        ],
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: {
        model: ProductPrice,
        required: false,
        where: {
          stock_id: id,
          is_active: IS_ACTIVE.ACTIVE,
        },
      },
    });
    if (parseInt(product[0].dataValues.amount) > 0) {
      throw new AppError(ApiCodeResponse.CANNOT_DELETE_STOCK).with('Trong kho còn sản phẩm. Không thể xóa kho!');
    }
    await Stock.update({ is_active: IS_ACTIVE.INACTIVE, delete_at: new Date(), delete_by: shopId }, { where: { id } });
    return withSuccess({});
  }
}
