import { Body, Get, Post, Put, Query, Route, Request, Delete, Tags, Security, UploadedFiles } from 'tsoa';
import * as _ from 'lodash';
import Joi from '../utils/JoiValidate';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { AppTypes } from 'types';

import { ApplicationController } from './';
import {
  IS_ACTIVE,
  ORDER_STATUS,
  MEDIA_TYPE,
  PANCAKE,
  PRODUCT_TYPE,
  ROLE,
  IS_POSTED,
  IS_DEFAULT,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ProductService } from '@services/internal/products.service';
import * as express from 'express';
import { Utils } from '@utils/Utils';
import { getAxiosInstance } from '@config/axiosInstance';
import { AxiosInstance } from 'axios';
import { environment } from '@config/environment';
const axios = require('axios');
const ExcelJS = require('exceljs');

const db = require('@models');

const {
  sequelize,
  Sequelize,
  ProductCustomAttribute,
  Product,
  ProductMedia,
  Category,
  ProductStock,
  Stock,
  User,
  Order,
  OrderItem,
  Shop,
  ProductPrice,
  ProductCustomAttributeOption,
  Post: Posts,
  PostMedia,
} = db.default;
const { Op } = Sequelize;
interface ProductMulterRequest extends express.Request {
  file: any;
}

@Route('product')
@Tags('product')
export class ProductController extends ApplicationController {
  public apiInstance: AxiosInstance;
  private productService: ProductService;
  constructor() {
    super('Product');
    this.apiInstance = getAxiosInstance();
    this.productService = new ProductService();
  }

  /**
   * @summary Danh sách sản phẩm
   */
  // @Security('jwt', ['enterprise'])
  @Get('/')
  public async listProducts(
    @Request() request: any,
    @Query() search?: any,
    @Query() status?: any,
    // @Query() stock?: any,
    @Query() category_id?: any,
    @Query() children_category_id?: any,
    @Query() stock_id?: any,
    @Query() shop_id?: any,
  ): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const { offset, limit, page } = handlePagingMiddleware(request);
    // const { search, status, category_id, is_public, stocks_status } = request.query;
    // return withSuccess(search == null);
    const tokens = request.headers.token;
    let shopId;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      // const shop = await Shop.findOne({
      //     where: { id: loginUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
      // });
      // return withSuccess(shop);
      shopId = loginUser.shop_id ? loginUser.shop_id : null;
      // if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    // return withSuccess(shopId);
    const listCategoryChild = await Category.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        parent_id: { [Op.ne]: null },
      },
      include: {
        model: Product,
        where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shopId || { [Op.ne]: null } },
        attributes: [],
      },
    });
    const listCategory = await Category.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        id: { [Op.in]: listCategoryChild.map((data) => data.parent_id) },
      },
    });
    // return withSuccess(listCategory);

    let query_shop_id = { [Op.ne]: null };
    if (shopId) query_shop_id = shopId;
    else if (shop_id) query_shop_id = shop_id;
    const whereOptions: any = {
      is_active: IS_ACTIVE.ACTIVE,
      // shop_id: shopId ? shopId : { [Op.ne]: null },
      shop_id: query_shop_id,
      ...(search && { [Op.or]: { name: { [Op.like]: `%${search}%` }, code: { [Op.like]: `%${search}%` } } }),
      // category_id: children_category_id ? children_category_id : { [Op.ne]: null },
    };
    if (status || status === 0) {
      whereOptions.status = status;
    }
    if (children_category_id != undefined && children_category_id != null) {
      whereOptions.category_id = children_category_id != 0 ? children_category_id : { [Op.ne]: null };
    }
    // if (stocks_status != undefined && stocks_status != undefined && status != "") {
    //     whereOptions.stock_status = stocks_status;
    // }

    const { rows, count } = await Product.findAndCountAll({
      subQuery: false,
      attributes: [
        // include: [[Sequelize.fn('COUNT', 'product_prices.id'), 'stock_status']],
        'id',
        'name',
        'code',
        'status',
        'description',
        'price',
        'product_pancake_id',
        [Sequelize.literal('`Category`.`name`'), 'category_name'],
        [
          Sequelize.literal(`(
            SELECT IFNULL(MIN(product_price.price), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
            )`),
          'price_stock',
        ],
        // [sequelize.fn('SUM', sequelize.col('OrderItems.price')), 'total_price'],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id ${shopId ? `=${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_price',
        ],
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id ${shopId ? `= ${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'total_amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id
            )`),
          'amount',
        ],
        [
          Sequelize.literal(`(
            SELECT IF((SELECT IFNULL(SUM(product_price.amount), 0) from product_price
              where product_price.is_active = ${IS_ACTIVE.ACTIVE}
              and product_price.status = ${IS_ACTIVE.ACTIVE}
              and product_price.product_id = Product.id) > 0,true,false)
            )`),
          'status_stock',
        ],
      ],
      where: whereOptions,
      // ...(category_id && { include: [{ model: Category, where: { id: category_id } }] }),
      include: [
        // {
        //   // required: category_id != undefined && category_id != null ? true : false,
        //   model: Category,
        //   attributes: ['id', 'name', 'parent_id', 'icon_url'],
        //   include: { model: Category, as: 'parent_category', attributes: ['id', 'name', 'parent_id', 'icon_url'] },
        //   where: {
        //     is_active: IS_ACTIVE.ACTIVE,
        //     id: category_id != undefined && category_id != null && category_id != '' ? category_id : { [Op.ne]: null },
        //   },
        // },
        {
          model: Category,
          // required: false,
          attributes: ['id', 'name', 'parent_id'],
          where: {
            // is_active: IS_ACTIVE.ACTIVE,
            parent_id: category_id != 0 ? category_id : { [Op.ne]: null },
            // status: IS_DEFAULT.ACTIVE,
          },
          include: {
            model: Category,
            as: 'parent_category',
            attributes: ['id', 'name', 'parent_id', 'icon_url'],
          },
        },
        {
          model: OrderItem,
          required: false,
          // attributes: ['id'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: Order,
            // required: false,
            attributes: ['id'],
            where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.SUCCCESS },
          },
        },
        {
          model: ProductPrice,
          attributes: ['id', 'amount'],
          required: false,
          where: {
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: ProductStock,
          // required: false,
          attributes: ['id', 'product_id', 'stock_id'],
          where: {
            is_active: IS_ACTIVE.ACTIVE,
            stock_id: stock_id ? stock_id : { [Op.ne]: null },
          },
          // having: sequelize.where(sequelize.literal('sum(`ProductPrices`.status'), stocks_status == 1 ? ">" : "<=", stocks_status == 1 ? 0 : 0),
        },
      ],
      group: ['product.id'],
      order: [
        [
          Sequelize.literal(`(
                        SELECT IFNULL(SUM(order_item.amount), 0)
                        FROM \`order\` as od
                        join order_item as order_item
                        ON order_item.order_id = od.id
                        WHERE od.shop_id ${shopId ? `= ${shopId}` : 'is not null'}
                            AND
                            od.is_active = ${IS_ACTIVE.ACTIVE}
                            AND
                            od.status = ${ORDER_STATUS.SUCCCESS}
                            AND
                            order_item.product_id = Product.id
                            AND
                            order_item.is_active = ${IS_ACTIVE.ACTIVE}
                        )`),
          'desc',
        ],
        ['id', 'desc'],
      ],
      limit,
      offset,
    });
    return withPagingSuccess(
      { rows, listCategory },
      { page, limit, totalItemCount: count instanceof Array ? count.length : count },
    );
  }

  /**
   * @summary Danh sách kho hiện tại của sản phẩm
   */
  @Security('jwt', ['shop', 'shop_member'])
  @Get('/{product_id}/lits-stock')
  public async listStockProducts(@Request() request: any, product_id?: number): Promise<any> {
    // AppTypes.PagingResponseModel<any>
    const { offset, limit, page } = handlePagingMiddleware(request);
    // const { search, status, category_id, is_public, stocks_status } = request.query;
    // return withSuccess(search == null);
    const tokens = request.headers.token;
    let shopId;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      // const shop = await Shop.findOne({
      //     where: { id: loginUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
      // });
      // return withSuccess(shop);
      shopId = loginUser ? loginUser.shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }

    const { rows, count } = await ProductStock.findAndCountAll({
      attributes: ['id', 'product_id', 'stock_id', [Sequelize.col('stock.name'), 'name']],
      where: { product_id: product_id != 0 ? product_id : { [Op.ne]: null }, is_active: IS_ACTIVE.ACTIVE },
      include: [
        {
          model: Product,
          attributes: [],
          where: { shop_id: shopId, is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Stock,
          attributes: [],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      order: [['id', 'desc']],
      group: [['stock_id']],
      limit,
      offset,
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  @Get('/check-code')
  public async checkProductCodeExist(@Query() code: string): Promise<AppTypes.SuccessResponseModel<any>> {
    const foundProduct = await this.productService.isProductCodeExist(code);
    return withSuccess(!!foundProduct);
  }

  /**
   * @summary Tạo sản phẩm
   */
  // @Security('jwt', ['enterprise'])
  @Post('/')
  public async createProductGeneralInfo(@Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const baseUrl = Utils.getBaseServer(request);
    const tokens = request.headers.token;
    let shopId;
    let userId;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      shopId = loginUser ? loginUser.shop_id : null;
      userId = loginUser ? loginUser.id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    // return withSuccess(shopId);
    const schema = Joi.object({
      code: Joi.string().required().allow('null', null).label('Mã sản phẩm'),
      array_stocks: Joi.number().allow('null', null),
      name: Joi.string().required().label('Tên'),
      category_id: Joi.number().empty(['null', 'undefined']).required(),
      // enterprise_id: Joi.number().empty(['null', 'undefined']).required(),
      description: Joi.string(),
      price: Joi.number(),
      status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
      is_post: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
      stock_status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
      product_custom_attribute: Joi.array()
        .items(
          Joi.object()
            .keys({
              name: Joi.string().required(),
              product_group_items: Joi.array().required(),
            })
            .unknown(true),
        )
        .required(),
      items: Joi.array().items(
        Joi.object()
          .keys({
            stock_id: Joi.number().integer().allow(null, ''),
            tier_index: Joi.array().items(Joi.number().integer()).required(),
            price: Joi.number().integer().required(),
            status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
            percent: Joi.number().integer().allow(null, ''),
            amount: Joi.number().integer().allow(null, ''),
            // custom_attribute_option_1: Joi.number().integer().allow(null, ''),
            // custom_attribute_option_2: Joi.number().integer().allow(null, '')
          })
          .unknown(true),
      ),
      images: Joi.array().items(Joi.string()).allow(null, ''),
    });

    const {
      images,
      array_stocks,
      is_post,
      status,
      code,
      name,
      category_id,
      stock_status,
      description,
      items,
      price,
      product_custom_attribute,
    } = await schema.validateAsync(request.body);
    let checkProduct;
    let productCategory;
    await Promise.all([
      (checkProduct = await Product.findOne({ where: { code, shop_id: shopId, is_active: IS_ACTIVE.ACTIVE } })),
      (productCategory = await Category.count({
        where: { id: { [Op.in]: category_id }, is_active: IS_ACTIVE.ACTIVE },
      })),
    ]);
    if (checkProduct) throw ApiCodeResponse.CODE_PRODUCT_ERROR;
    if (productCategory) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
    // return withSuccess(1);
    const listStock = await Stock.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shopId } });
    // return withSuccess(1);
    // return withSuccess(product_custom_attribute[1]);
    // if ((items.length = 0)) {
    //   throw ApiCodeResponse.CANNOT_CREATE_PRODUCT_STOCK;
    // }
    const product_id = await sequelize.transaction(async (transaction) => {
      // create product
      const product = await Product.create(
        {
          code,
          name,
          category_id,
          amount: 0,
          // vehicle_brand_id,
          description,
          price,
          status,
          stock_status,
          shop_id: shopId,
        },
        { transaction },
      );
      if (listStock.length > 0) {
        const stockContents = listStock.map((data) => ({ stock_id: data.id, product_id: product.id }));
        await ProductStock.bulkCreate(stockContents, { transaction });
      }
      if (is_post > 0) {
        const postCreate = await Posts.create(
          {
            user_id: userId,
            shop_id: shopId,
            topic_id: 1,
            is_posted: IS_POSTED.POSTED,
            content: name,
            status: 1,
            product_id: product.id,
          },
          { transaction },
        );
        await PostMedia.bulkCreate(
          images.map((image) => ({
            post_id: postCreate.id,
            media_url: image.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', ''),
            type: MEDIA_TYPE.IMAGE,
          })),
          { transaction },
        );
        PostMedia.update(
          { type: 1 },
          {
            where: { post_id: postCreate.id, is_active: IS_ACTIVE.ACTIVE, media_url: { [Op.substring]: '.mp4' } },
            transaction,
          },
        );
      }
      let productCustom = [];
      await Promise.all([
        // create product image
        ProductMedia.bulkCreate(
          images.map((image) => ({
            product_id: product.id,
            media_url: image.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', ''),
            type: MEDIA_TYPE.IMAGE,
          })),
          { transaction },
        ),
        ProductMedia.update(
          { type: 1 },
          {
            where: { product_id: product.id, is_active: IS_ACTIVE.ACTIVE, media_url: { [Op.substring]: '.mp4' } },
            transaction,
          },
        ),
        // create group product
        (productCustom = await ProductCustomAttribute.bulkCreate(
          product_custom_attribute.map((group) => ({ name: group.name, product_id: product.id })),
          { transaction },
        )),
      ]);
      const productCustomAtt = [];
      const imageCustomAtt = [];
      product_custom_attribute.forEach((group, groupIndex) => {
        group.product_group_items.forEach((item, index) => {
          productCustomAtt.push({
            name: item,
            display_order: index,
            product_custom_attribute_id: productCustom[groupIndex].id,
            group: groupIndex,
            product_id: product.id,
            create_by: shopId,
          });
        });
      });
      console.log('productCustomAtt', productCustomAtt);

      // create item of group product
      const newProductCustomAttribute = await ProductCustomAttributeOption.bulkCreate(productCustomAtt, {
        transaction,
      });
      // product_custom_attribute.forEach((group, groupIndex) => {
      if (product_custom_attribute[0] != undefined) {
        if (
          product_custom_attribute[0].image_customs != null &&
          product_custom_attribute[0].image_customs != undefined
        ) {
          product_custom_attribute[0].image_customs.forEach((item, index) => {
            imageCustomAtt.push({
              media_url: item.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', ''),
              product_id: product.id,
              type: 0,
              product_custom_attribute_option_id: newProductCustomAttribute[index].id,
            });
          });
          // });
          console.log(imageCustomAtt);
          const newImageCustomAttribute = await ProductMedia.bulkCreate(imageCustomAtt, {
            transaction,
          });
        }
      }

      const productProductCustomAttributeObject = {};

      productCustomAtt.forEach((item, index) => {
        if (!productProductCustomAttributeObject[item.group]) productProductCustomAttributeObject[item.group] = {};
        productProductCustomAttributeObject[item.group][item.display_order] = newProductCustomAttribute[index].id;
      });
      console.log('productProductCustomAttributeObject', productProductCustomAttributeObject);

      const atributeOption = items.map((model) => ({
        ...model,
        product_id: product.id,
        custom_attribute_option_id_1: productProductCustomAttributeObject['0']
          ? productProductCustomAttributeObject['0'][model.tier_index[0]]
          : null,
        custom_attribute_option_id_2: productProductCustomAttributeObject['1']
          ? productProductCustomAttributeObject['1'][model.tier_index[1]]
          : null,
        tier_index: model.tier_index,
        create_by: shopId,
        status: model.status,
      }));

      await ProductPrice.bulkCreate(atributeOption, { transaction });

      return product.id;
    });
    // // lấy stock đồng bộ vs pancake
    // const checkStock = await Stock.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shopId }, order: [["id", "asc"]] });
    // const check_stock = checkStock ? checkStock.id : 0;
    let detailProduct = await this.productService.findById(product_id, 0, 0, request, PRODUCT_TYPE.DETAIL);
    // return withSuccess(detailProduct);
    axios.defaults.baseURL = PANCAKE;
    const fields = {};
    let product = {
      name: detailProduct.name,
      product_attributes: detailProduct.ProductCustomAttributes.map((att) => ({
        name: att.name,
        values: att.ProductCustomAttributeOptions.map((a) => a.name),
      })),
      product_id: detailProduct.id,
      variations: [],
      weight: 0,
    };

    detailProduct.ProductPrices.map((pro) => {
      if (pro.stock_id === listStock[0].id) {
        product.variations.push({
          fields: [
            pro.product_attribute_name_1
              ? {
                  name: pro.product_attribute_name_1 ? pro.product_attribute_name_1.ProductCustomAttribute.name : null,
                  value: pro.product_attribute_name_1 ? pro.product_attribute_name_1.name : null,
                }
              : null,
            pro.product_attribute_name_2
              ? {
                  name: pro.product_attribute_name_2 ? pro.product_attribute_name_2.ProductCustomAttribute.name : null,
                  value: pro.product_attribute_name_2 ? pro.product_attribute_name_2.name : null,
                }
              : null,
          ].filter(function (el) {
            return el != null;
          }),
          images: pro.product_attribute_name_1
            ? pro.product_attribute_name_1.ProductMedium
              ? [pro.product_attribute_name_1.ProductMedium.media_url]
              : []
            : [],
          retail_price: pro.price,
          weight: 0,
        });
      }
    });
    // return withSuccess({ detailProduct, product });

    axios.defaults.headers.post['Content-Type'] = 'application/json';
    const pancakeStockId = await axios
      .post('/shops/' + pancake_shop_id + '/products?api_key=' + pancake_shop_key, {
        product,
      })
      .then((result) => {
        // console.log('logData', result);
        console.log('logData.data', result.data.data);
        // if (result.data.data == false) {
        //   return false;
        // }
        return result.data.data;
      })
      .catch((err) => {
        console.log('err', err);
        console.error(err);
      });

    if (pancakeStockId != undefined) {
      const pancakeDetail = await axios
        .get('/shops/' + pancake_shop_id + '/products/' + pancakeStockId.id + '?api_key=' + pancake_shop_key, {
          product,
        })
        .then((result) => {
          // console.log('logData', result.data);
          console.log('logData.data', result.data.data);
          // if (result.data.data == false) {
          //   return false;
          // }
          return result.data.data;
        })
        .catch((err) => {
          console.error(err);
        });
      // let pancakeStock;
      // if (pancakeDetail) {
      //   // pancakeStock = pancakeDetail.variations.map((item) => {
      //   // });
      // }
      // pancakeStock = array_stocks.map((stockItem) => {
      //   return { warehouse_id: stockItem.pancake_stock_id, remain_quantity: stockItem.remain_quantity };
      // });
      const productCustomAtt = [];
      let x = 0;
      detailProduct.ProductPrices.map((group: any, index) => {
        if (pancakeDetail.variations[index]) {
          productCustomAtt.push({
            // // group,
            id: group.id,
            tier_index: group.tier_index,
            product_id: group.product_id,
            stock_id: group.stock_id,
            amount: group.amount,
            price: group.price,
            custom_attribute_option_id_1: group.custom_attribute_option_id_1,
            custom_attribute_option_id_2: group.custom_attribute_option_id_2,
            status: group.status,
            percent: group.percent,
            is_active: group.is_active,
            create_at: group.create_at,
            update_by: group.update_by,
            custom_attribute_option_pancake_id_1:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[0]?.id
                : null,
            custom_attribute_option_pancake_id_2:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[1]?.id
                : null,
            variration_pancake_id: pancakeDetail.variations
              ? pancakeDetail.variations.sort(function (a: any, b: any) {
                  return a.display_id - b.display_id;
                })[x]?.id
              : null,
          });
          x++;
        } else {
          if (x == pancakeDetail.variations.length) {
            console.log('x', x);
            x = 0;
          }

          productCustomAtt.push({
            // // group,
            id: group.id,
            tier_index: group.tier_index,
            product_id: group.product_id,
            stock_id: group.stock_id,
            amount: group.amount,
            price: group.price,
            custom_attribute_option_id_1: group.custom_attribute_option_id_1,
            custom_attribute_option_id_2: group.custom_attribute_option_id_2,
            status: group.status,
            percent: group.percent,
            is_active: group.is_active,
            create_at: group.create_at,
            update_by: group.update_by,
            custom_attribute_option_pancake_id_1:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[0]?.id
                : null,
            custom_attribute_option_pancake_id_2:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[1]?.id
                : null,
            variration_pancake_id: pancakeDetail.variations
              ? pancakeDetail.variations.sort(function (a: any, b: any) {
                  return a.display_id - b.display_id;
                })[x]?.id
              : null,
          });
          x++;
        }
      });
      // return withSuccess({ productCustomAtt, pancakeStockId });
      await sequelize.transaction(async (transaction) => {
        await Product.update({ product_pancake_id: pancakeStockId.id }, { where: { id: product_id } });
        await ProductPrice.update({ product_pancake_id: pancakeStockId.id }, { where: { product_id: product_id } });
        await ProductPrice.bulkCreate(productCustomAtt, {
          updateOnDuplicate: [
            'id',
            'update_by',
            'create_at',
            'is_active',
            'product_id',
            'stock_id',
            'price',
            'variration_pancake_id',
            'custom_attribute_option_id_1',
            'custom_attribute_option_id_2',
            'custom_attribute_option_pancake_id_1',
            'custom_attribute_option_pancake_id_2',
            'tier_index',
            'percent',
            'status',
          ],
          transaction,
        });
      });
      try {
        // lay lai du lieu khi update ogo
        detailProduct = await this.productService.findById(product_id, 0, 0, request, PRODUCT_TYPE.DETAIL);
        for (let index = 0; index < detailProduct.ProductPrices.length; index++) {
          await axios
            .post(
              '/shops/' +
                pancake_shop_id +
                '/variations/' +
                detailProduct.ProductPrices[index].variration_pancake_id +
                '/update_quantity?api_key=' +
                pancake_shop_key,
              {
                variations_warehouses: [
                  {
                    warehouse_id: detailProduct.ProductPrices[index].Stock.pancake_stock_id,
                    remain_quantity: detailProduct.ProductPrices[index].amount,
                  },
                ],
              },
            )
            .then((result) => {
              // console.log('config', result.config);
              console.log('data', result.data);
              // return result.data.data;
            })
            .catch((err) => {
              console.log('err', err);
              console.error(err);
            });
        }
      } catch (error) {
        console.error(error);
      }
    } else {
      // await Product.update(
      //   { is_active: IS_ACTIVE.INACTIVE },
      //   { where: { id: product_id, is_active: IS_ACTIVE.ACTIVE } },
      // );
      // throw ApiCodeResponse.CANNOT_CREATE_PRODUCT;
    }
    // console.log("productCustomAtt", productCustomAtt);
    // return withSuccess(productCustomAtt);
    // //pancakeStockId.variations.sort(function (a: any, b: any) { return a.display_id - b.display_id })
    // return withSuccess(pancakeStockId);

    return withSuccess(detailProduct);
    // return productService.getProductDetail(product_id);
    // return withSuccess({ code, name, category_id, description, items, price, product_custom_attribute });
    // return withSuccess(1);
  }

  /**
   * @summary Chi tiết sản phẩm
   */
  @Get('/{id}')
  public async detailProduct(
    @Request() request: any,
    id?: number,
    @Query() type?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(1);
    const tokens = request.headers.token;
    let shopId;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      shopId = loginUser ? loginUser.shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const images = await ProductMedia.findOne({
      where: {
        product_id: id,
        is_active: IS_ACTIVE.ACTIVE,
        type: MEDIA_TYPE.IMAGE,
        product_custom_attribute_option_id: null,
      },
      order: [['id', 'asc']],
    });
    let images_id = 0;
    if (images) images_id = images.id;
    // // lấy stock đồng bộ vs pancake
    const checkStock = await Stock.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, shop_id: 117 },
      order: [['id', 'desc']],
    });
    const check_stock = checkStock ? checkStock.id : 0;
    const detailProduct = await this.productService.findById(id, images_id, 0, request, type, shopId);
    return withSuccess(detailProduct);
  }

  /**
   * @summary Sửa sản phẩm
   */
  @Put('/:id')
  public async updateProductGeneralInfo(id, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const baseUrl = Utils.getBaseServer(request);
    const tokens = request.headers.token;
    let shopId;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      // return withSuccess(loginUser.shop_id);
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      // const shop = await Shop.findOne({
      //     where: { id: loginUser.shop_id, is_active: IS_ACTIVE.ACTIVE },
      // });
      // return withSuccess(shop);
      shopId = loginUser ? loginUser.shop_id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    const imageDetail = await ProductMedia.findOne({
      where: {
        product_id: id,
        is_active: IS_ACTIVE.ACTIVE,
        type: MEDIA_TYPE.IMAGE,
        product_custom_attribute_option_id: null,
      },
      order: [['id', 'asc']],
    });
    let images_id = 0;
    if (imageDetail) images_id = imageDetail.id;
    // return withSuccess(shopId);
    const schema = Joi.object({
      code: Joi.string().required().allow('null', null).label('Mã sản phẩm'),
      name: Joi.string().required().label('Tên'),
      category_id: Joi.number().empty(['null', 'undefined']).required(),
      // enterprise_id: Joi.number().empty(['null', 'undefined']).required(),
      description: Joi.string(),
      price: Joi.number(),
      status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
      stock_status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
      product_custom_attribute: Joi.array()
        .items(
          Joi.object()
            .keys({
              name: Joi.string().required(),
              product_group_items: Joi.array().required(),
              image_customs: Joi.array().items(Joi.string()).allow(null, ''),
            })
            .unknown(true),
        )
        .required(),
      items: Joi.array().items(
        Joi.object()
          .keys({
            stock_id: Joi.number().integer().allow(null, ''),
            tier_index: Joi.array().items(Joi.number().integer()).required(),
            price: Joi.number().integer().required(),
            status: Joi.number().empty([null, 'null']).default(0).valid(1, 0),
            percent: Joi.number().integer().allow(null, ''),
            amount: Joi.number().integer().allow(null, ''),
            // custom_attribute_option_1: Joi.number().integer().allow(null, ''),
            // custom_attribute_option_2: Joi.number().integer().allow(null, '')
          })
          .unknown(true),
      ),
      images: Joi.array().items(Joi.string()).allow(null, ''),
    });
    const {
      images,
      status,
      code,
      name,
      category_id,
      stock_status,
      description,
      items,
      price,
      product_custom_attribute,
    } = await schema.validateAsync(request.body);
    let checkProduct;
    let productCategory;
    await Promise.all([
      (checkProduct = await Product.findOne({ where: { id, shop_id: shopId, is_active: IS_ACTIVE.ACTIVE } })),
      (productCategory = await Category.count({
        where: { id: { [Op.in]: category_id }, is_active: IS_ACTIVE.ACTIVE },
      })),
    ]);
    if (!checkProduct) throw ApiCodeResponse.CODE_PRODUCT_ERROR;
    if (productCategory) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
    const listProductStock = await ProductStock.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, product_id: id } });
    // // return withSuccess(listProductStock.map(data => data.id));
    const listStock = await Stock.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: shopId,
        id: { [Op.in]: listProductStock.map((data) => data.stock_id) },
      },
    });

    let detailProduct = await this.productService.findById(id, images_id, 0, request, PRODUCT_TYPE.DETAIL);

    await sequelize.transaction(async (transaction) => {
      // if (listStock.length > 0) {
      //   const stockContents = listStock.map((data) => ({ stock_id: data.id, product_id: id }));
      //   await ProductStock.bulkCreate(stockContents, { transaction });
      // }
      await Promise.all([
        // update product
        Product.update(
          {
            code,
            name,
            category_id,
            amount: 0,
            // vehicle_brand_id,
            description,
            price,
            status,
            stock_status,
            shop_id: shopId,
          },
          { where: { id }, transaction },
        ),
        // delete old image product
        ProductMedia.update({ is_active: IS_ACTIVE.INACTIVE }, { where: { product_id: id }, transaction }),
        // delete old group product
        ProductCustomAttribute.update(
          { is_active: IS_ACTIVE.INACTIVE },
          { where: { product_id: id, is_active: IS_ACTIVE.ACTIVE }, transaction },
        ),
        ProductPrice.update(
          { is_active: IS_ACTIVE.INACTIVE },
          { where: { product_id: id, is_active: IS_ACTIVE.ACTIVE }, transaction },
        ),
        // productService.handleNotiUpdateProductDetails(updateProductDetails, transaction),
        // productService.handleDeleteProductDetails(deleteProductDetails, transaction),
        // productService.handleNotiUpdateAmountOfProduct(id, oldProductDetails, product_details, transaction),
      ]);

      let productCustom = [];
      await Promise.all([
        // create product image

        ProductMedia.bulkCreate(
          images.map((image) => ({
            product_id: id,
            media_url: image.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', ''),
            type: MEDIA_TYPE.IMAGE,
          })),
          { transaction },
        ),
        ProductMedia.update(
          { type: 1 },
          {
            where: { product_id: id, is_active: IS_ACTIVE.ACTIVE, media_url: { [Op.substring]: '.mp4' } },
            transaction,
          },
        ),
        // create group product
        (productCustom = await ProductCustomAttribute.bulkCreate(
          product_custom_attribute.map((group) => ({ name: group.name, product_id: id })),
          { transaction },
        )),
      ]);

      const productCustomAtt = [];
      const imageCustomAtt = [];
      product_custom_attribute.forEach((group, groupIndex) => {
        group.product_group_items.forEach((item, index) => {
          productCustomAtt.push({
            name: item,
            display_order: index,
            product_custom_attribute_id: productCustom[groupIndex].id,
            group: groupIndex,
            product_id: id,
            create_by: shopId,
          });
        });
      });

      // create item of group product
      const newProductCustomAttribute = await ProductCustomAttributeOption.bulkCreate(productCustomAtt, {
        transaction,
      });
      // product_custom_attribute.forEach((group, groupIndex) => {
      if (product_custom_attribute[0] != undefined) {
        if (
          product_custom_attribute[0].image_customs != null &&
          product_custom_attribute[0].image_customs != undefined
        ) {
          product_custom_attribute[0].image_customs.forEach((item, index) => {
            imageCustomAtt.push({
              media_url: item.replace('https://windsoftdev.s3-ap-southeast-1.amazonaws.com/', ''),
              product_id: id,
              type: 0,
              product_custom_attribute_option_id: newProductCustomAttribute[index].id,
            });
          });
          // });
          const newImageCustomAttribute = await ProductMedia.bulkCreate(imageCustomAtt, {
            transaction,
          });
        }
      }

      const productProductCustomAttributeObject = {};

      productCustomAtt.forEach((item, index) => {
        if (!productProductCustomAttributeObject[item.group]) productProductCustomAttributeObject[item.group] = {};
        productProductCustomAttributeObject[item.group][item.display_order] = newProductCustomAttribute[index].id;
      });

      const atributeOption = items.map((model) => ({
        ...model,
        product_id: id,
        custom_attribute_option_id_1: productProductCustomAttributeObject['0']
          ? productProductCustomAttributeObject['0'][model.tier_index[0]]
          : null,
        custom_attribute_option_id_2: productProductCustomAttributeObject['1']
          ? productProductCustomAttributeObject['1'][model.tier_index[1]]
          : null,
        tier_index: model.tier_index,
        create_by: shopId,
        status: model.status,
      }));

      await ProductPrice.bulkCreate(atributeOption, {
        updateOnDuplicate: [
          'id',
          'update_by',
          'create_at',
          'is_active',
          'product_id',
          'stock_id',
          'price',
          'custom_attribute_option_id_1',
          'custom_attribute_option_id_2',
          'tier_index',
          'percent',
          'status',
        ],
        transaction,
      });
    });
    axios.defaults.baseURL = PANCAKE;
    const fields = {};
    // console.log('detailProduct', detailProduct.ProductPrices.length);
    // return withSuccess(detailProduct);
    const product = {
      name: detailProduct.name,
      product_attributes: detailProduct.ProductCustomAttributes.map((att) => ({
        name: att.name,
        values: att.ProductCustomAttributeOptions.map((a) => a.name),
      })),
      product_id: detailProduct.id,
      variations: [],
      weight: 0,
    };

    detailProduct.ProductPrices.map((pro) => {
      if (pro.stock_id === listStock[0].id) {
        product.variations.push({
          id: pro.variration_pancake_id ? pro.variration_pancake_id : null,
          fields: [
            pro.product_attribute_name_1
              ? {
                  name: pro.product_attribute_name_1 ? pro.product_attribute_name_1.ProductCustomAttribute.name : null,
                  value: pro.product_attribute_name_1 ? pro.product_attribute_name_1.name : null,
                }
              : null,
            pro.product_attribute_name_2
              ? {
                  name: pro.product_attribute_name_2 ? pro.product_attribute_name_2.ProductCustomAttribute.name : null,
                  value: pro.product_attribute_name_2 ? pro.product_attribute_name_2.name : null,
                }
              : null,
          ].filter(function (el) {
            return el != null;
          }),
          images: pro.product_attribute_name_1
            ? pro.product_attribute_name_1.ProductMedium
              ? [pro.product_attribute_name_1.ProductMedium.media_url]
              : []
            : [],
          retail_price: pro.price,
          weight: 0,
        });
      }
    });
    console.log('product', product);
    // return withSuccess({ detailProduct, product });
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    const pancakeStockId = await axios
      .put(
        '/shops/' + pancake_shop_id + '/products/' + checkProduct.product_pancake_id + '?api_key=' + pancake_shop_key,
        {
          product,
        },
      )
      .then((result) => {
        console.log('logData', result);
        // if (result.data.data == false) {
        //   return false;
        // }
        return result.data.data;
      })
      .catch((err) => {
        console.error(err);
      });
    if (pancakeStockId != undefined) {
      const pancakeDetail = await axios
        .get(
          '/shops/' + pancake_shop_id + '/products/' + checkProduct.product_pancake_id + '?api_key=' + pancake_shop_key,
          {
            product,
          },
        )
        .then((result) => {
          console.log('logData', result.data);
          // if (result.data.data == false) {
          //   return false;
          // }
          return result.data.data;
        })
        .catch((err) => {
          console.error(err);
        });
      const productCustomAtt = [];
      let x = 0;
      detailProduct.ProductPrices.map((group: any, index) => {
        if (pancakeDetail.variations[index]) {
          productCustomAtt.push({
            // // group,
            id: group.id,
            tier_index: group.tier_index,
            product_id: group.product_id,
            stock_id: group.stock_id,
            amount: group.amount,
            price: group.price,
            custom_attribute_option_id_1: group.custom_attribute_option_id_1,
            custom_attribute_option_id_2: group.custom_attribute_option_id_2,
            status: group.status,
            percent: group.percent,
            is_active: group.is_active,
            create_at: group.create_at,
            update_by: group.update_by,
            custom_attribute_option_pancake_id_1:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[0]?.id
                : null,
            custom_attribute_option_pancake_id_2:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[1]?.id
                : null,
            variration_pancake_id: pancakeDetail.variations
              ? pancakeDetail.variations.sort(function (a: any, b: any) {
                  return a.display_id - b.display_id;
                })[x]?.id
              : null,
          });
          x++;
        } else {
          if (x == pancakeDetail.variations.length) {
            console.log('x', x);
            x = 0;
          }

          productCustomAtt.push({
            // // group,
            id: group.id,
            tier_index: group.tier_index,
            product_id: group.product_id,
            stock_id: group.stock_id,
            amount: group.amount,
            price: group.price,
            custom_attribute_option_id_1: group.custom_attribute_option_id_1,
            custom_attribute_option_id_2: group.custom_attribute_option_id_2,
            status: group.status,
            percent: group.percent,
            is_active: group.is_active,
            create_at: group.create_at,
            update_by: group.update_by,
            custom_attribute_option_pancake_id_1:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[0]?.id
                : null,
            custom_attribute_option_pancake_id_2:
              pancakeDetail.variations[x]?.fields.length != 0
                ? pancakeDetail.variations.sort(function (a: any, b: any) {
                    return a.display_id - b.display_id;
                  })[x]?.fields[1]?.id
                : null,
            variration_pancake_id: pancakeDetail.variations
              ? pancakeDetail.variations.sort(function (a: any, b: any) {
                  return a.display_id - b.display_id;
                })[x]?.id
              : null,
          });
          x++;
        }
      });
      await sequelize.transaction(async (transaction) => {
        await ProductPrice.bulkCreate(productCustomAtt, {
          updateOnDuplicate: [
            'id',
            'update_by',
            'create_at',
            'is_active',
            'product_id',
            'stock_id',
            'price',
            'product_pancake_id',
            'variration_pancake_id',
            'custom_attribute_option_id_1',
            'custom_attribute_option_id_2',
            'custom_attribute_option_pancake_id_1',
            'custom_attribute_option_pancake_id_2',
            'tier_index',
            'percent',
            'status',
          ],
          transaction,
        });
      });
      try {
        // lay lai du lieu khi update ogo
        detailProduct = await this.productService.findById(id, 0, 0, request, PRODUCT_TYPE.DETAIL);

        for (let index = 0; index < detailProduct.ProductPrices.length; index++) {
          await axios
            .post(
              '/shops/' +
                pancake_shop_id +
                '/variations/' +
                detailProduct.ProductPrices[index].variration_pancake_id +
                '/update_quantity?api_key=' +
                pancake_shop_key,
              {
                variations_warehouses: [
                  {
                    warehouse_id: detailProduct.ProductPrices[index].Stock.pancake_stock_id,
                    remain_quantity: detailProduct.ProductPrices[index].amount,
                  },
                ],
              },
            )
            .then((result) => {
              console.log('logData', result.data);
              // return result.data.data;
            })
            .catch((err) => {
              console.log('err', err);
              console.error(err);
            });
        }
      } catch (error) {
        console.error(error);
      }
    }
    return withSuccess(detailProduct.ProductPrices);
  }

  /**
   * @summary Xóa sản phẩm (cả admin và shop => any)
   */
  // @Security('jwt', ['enterprise'])
  @Delete('/')
  public async deleteProduct(
    @Request() request,
    @Body() body: { id: number[] },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      id: Joi.array().items(Joi.number()).required(),
    });
    const bodyData = await schema.validateAsync(body);
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    const checkProduct = await Product.findAll({
      where: {
        id: { [Op.in]: bodyData.id },
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: role != ROLE.SHOP && role != ROLE.SHOP_MEMBER ? { [Op.ne]: null } : shopId,
      },
    });
    if (checkProduct.length < bodyData.id.length) throw ApiCodeResponse.NOT_FOUND;
    // const checkOrder = await Order.findAll({
    //   where: { is_active: IS_ACTIVE.ACTIVE, status: { [Op.ne]: ORDER_STATUS.CANCELED } },
    //   include: [
    //     {
    //       model: OrderItem,
    //       where: { product_id: { [Op.in]: bodyData.id }, is_active: IS_ACTIVE.ACTIVE },
    //       include: {
    //         model: Product,
    //         where: { id: { [Op.in]: bodyData.id }, is_active: IS_ACTIVE.ACTIVE },
    //       },
    //     },
    //   ],
    //   subQuery: false,
    // });
    // if (checkOrder.length > 0) throw ApiCodeResponse.DELETE_PRODUCT_ERROR;
    const deleteProduct = await sequelize.transaction(async (transaction) => {
      await Product.update(
        {
          is_active: IS_ACTIVE.INACTIVE,
        },
        { where: { is_active: IS_ACTIVE.ACTIVE, id: { [Op.in]: bodyData.id } }, transaction },
      );
    });
    return withSuccess(deleteProduct);
  }

  /**
   * @summary Ngưng hoạt động sản phẩm (admin + shop)
   */
  // @Security('jwt', ['enterprise'])
  @Put('/change-status/{product_id}')
  public async changeStatusProduct(
    @Request() request,
    @Body() body: { status?: number },
    product_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      status: Joi.number().required(),
    });
    const bodyData = await schema.validateAsync(body);
    // return withSuccess(bodyData.status)
    const tokens = request.headers.token;
    let userId;
    let shopId;
    let role;
    if (!tokens) throw ApiCodeResponse.UNAUTHORIZED;
    if (tokens) {
      const user = await User.findOne({ where: { token: tokens, is_active: IS_ACTIVE.ACTIVE } });
      if (!user) throw ApiCodeResponse.UNAUTHORIZED;
      // return withSuccess(user);
      userId = user ? user.id : null;
      shopId = user ? user.shop_id : null;
      role = user ? user.df_type_user_id : null;
      if (user.df_type_user_id == ROLE.CUSTOMER) throw ApiCodeResponse.UNAUTHORIZED;
    }
    const checkProduct = await Product.findOne({
      where: {
        id: product_id,
        is_active: IS_ACTIVE.ACTIVE,
        shop_id: role != ROLE.SHOP && role != ROLE.SHOP_MEMBER ? { [Op.ne]: null } : shopId,
      },
    });
    if (checkProduct == null) throw ApiCodeResponse.NOT_FOUND;
    // const checkOrder = await Order.findAll({
    //   where: { is_active: IS_ACTIVE.ACTIVE, status: { [Op.ne]: ORDER_STATUS.CANCELED } },
    //   include: [
    //     {
    //       model: OrderItem,
    //       where: { product_id: { [Op.in]: bodyData.id }, is_active: IS_ACTIVE.ACTIVE },
    //       include: {
    //         model: Product,
    //         where: { id: { [Op.in]: bodyData.id }, is_active: IS_ACTIVE.ACTIVE },
    //       },
    //     },
    //   ],
    //   subQuery: false,
    // });
    // if (checkOrder.length > 0) throw ApiCodeResponse.DELETE_PRODUCT_ERROR;
    const changeStatusProduct = await sequelize.transaction(async (transaction) => {
      await Product.update(
        {
          status: bodyData.status,
        },
        { where: { is_active: IS_ACTIVE.ACTIVE, id: product_id }, transaction },
      );
    });
    return withSuccess(changeStatusProduct);
  }

  @Post('/excel/upload-product')
  public async uploadExcelProduct(
    @Request() request: any,
    @UploadedFiles() file: Express.Multer.File,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const tokens = request.headers.token;
    let shopId;
    let userId;
    let pancake_shop_id;
    let pancake_shop_key;
    if (tokens) {
      const loginUser = await User.findOne({ where: { token: tokens } });
      if (loginUser == null) throw ApiCodeResponse.UNAUTHORIZED;
      const shop = await Shop.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: loginUser.shop_id } });
      pancake_shop_key = shop ? shop.pancake_shop_key : null;
      pancake_shop_id = shop ? shop.pancake_shop_id : null;
      shopId = loginUser ? loginUser.shop_id : null;
      userId = loginUser ? loginUser.id : null;
      if (!shopId) throw ApiCodeResponse.SHOP_NOT_EXIST;
    }
    let data = [];
    try {
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.load(file[0].buffer);
      workbook.eachSheet(async (worksheet, sheetId) => {});

      const startRow = 2;
      const startCol = 1;
      const worksheet = workbook.worksheets[0];
      let isEnd = false;
      // const sttRowId = startRow;
      const nameRowId = startCol + 1;
      const codeRowId = startCol + 2;
      const priceRowId = startCol + 3;
      const priceRetailRowId = startCol + 4;
      const descriptionRowId = startCol + 5;

      worksheet.eachRow((row, rowNumber) => {
        if (rowNumber < startRow) {
          return;
        }
        const nameRow = (row.getCell(nameRowId).text || '').trim();
        const codeRow = (row.getCell(codeRowId).text || '').trim();
        const priceRow = (row.getCell(priceRowId).text || '').trim();
        const priceRetailRow = (row.getCell(priceRetailRowId).text || '').trim();
        const descriptionRow = (row.getCell(descriptionRowId).text || '').trim();
        if (codeRow && nameRow) {
          data.push({ codeRow, nameRow, priceRow, priceRetailRow, descriptionRow });
        }
      });
    } catch (err) {
      console.log('err', err);
    }
    for (let index = 0; index < data.length; index++) {
      let checkProduct;
      let productCategory;
      await Promise.all([
        (checkProduct = await Product.findOne({
          where: { code: data[index].codeRow, shop_id: shopId, is_active: IS_ACTIVE.ACTIVE },
        })),
        (productCategory = await Category.findOne({
          where: { name: 'Mặc Định', parent_id: { [Op.not]: null } },
        })),
      ]);
      if (!productCategory) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
      if (!checkProduct) {
        const listStock = await Stock.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shopId } });
        const product_id = await sequelize.transaction(async (transaction) => {
          // create product
          const product = await Product.create(
            {
              code: data[index].codeRow,
              name: data[index].nameRow,
              category_id: productCategory.id,
              description: data[index].descriptionRow,
              price: data[index].priceRow,
              status: 1,
              stock_status: 1,
              shop_id: shopId,
            },
            { transaction },
          );
          if (listStock.length > 0) {
            const stockContents = listStock.map((data) => ({ stock_id: data.id, product_id: product.id }));
            await ProductStock.bulkCreate(stockContents, { transaction });
          }

          // create product image
          await ProductMedia.create(
            {
              product_id: product.id,
              media_url: '',
              type: MEDIA_TYPE.IMAGE,
            },
            { transaction },
          );
          await ProductMedia.update(
            { type: 1 },
            {
              where: { product_id: product.id, is_active: IS_ACTIVE.ACTIVE, media_url: { [Op.substring]: '.mp4' } },
              transaction,
            },
          );

          const stockPrice = listStock.map((dataStock) => ({
            product_id: product.id,
            price: data[index].priceRetailRow,
            stock_id: dataStock.id,
            tier_index: [0],
            create_by: shopId,
            status: 1,
          }));
          await ProductPrice.bulkCreate(stockPrice, { transaction });
          return product.id;
        });
        let detailProduct = await this.productService.findById(product_id, 0, 0, request, PRODUCT_TYPE.DETAIL);
        axios.defaults.baseURL = PANCAKE;
        const fields = {};
        const product = {
          name: detailProduct.name,
          product_attributes: detailProduct.ProductCustomAttributes.map((att) => ({
            name: att.name,
            values: att.ProductCustomAttributeOptions.map((a) => a.name),
          })),
          product_id: detailProduct.id,
          variations: [],
          weight: 0,
        };

        detailProduct.ProductPrices.map((pro) => {
          if (pro.stock_id === listStock[0].id) {
            product.variations.push({
              fields: [
                pro.product_attribute_name_1
                  ? {
                      name: pro.product_attribute_name_1
                        ? pro.product_attribute_name_1.ProductCustomAttribute.name
                        : null,
                      value: pro.product_attribute_name_1 ? pro.product_attribute_name_1.name : null,
                    }
                  : null,
                pro.product_attribute_name_2
                  ? {
                      name: pro.product_attribute_name_2
                        ? pro.product_attribute_name_2.ProductCustomAttribute.name
                        : null,
                      value: pro.product_attribute_name_2 ? pro.product_attribute_name_2.name : null,
                    }
                  : null,
              ].filter(function (el) {
                return el != null;
              }),
              images: pro.product_attribute_name_1
                ? pro.product_attribute_name_1.ProductMedium
                  ? [pro.product_attribute_name_1.ProductMedium.media_url]
                  : []
                : [],
              retail_price: pro.price,
              weight: 0,
            });
          }
        });
        axios.defaults.headers.post['Content-Type'] = 'application/json';
        const pancakeStockId = await axios
          .post('/shops/' + pancake_shop_id + '/products?api_key=' + pancake_shop_key, {
            product,
          })
          .then((result) => {
            console.log('logData', result);
            return result.data.data;
          })
          .catch((err) => {
            console.log('err', err);
            console.error(err);
          });

        console.log('pancakeStockId', pancakeStockId);
        if (pancakeStockId != undefined) {
          const pancakeDetail = await axios
            .get('/shops/' + pancake_shop_id + '/products/' + pancakeStockId.id + '?api_key=' + pancake_shop_key, {
              product,
            })
            .then((result) => {
              console.log('logData', result.data);
              return result.data.data;
            })
            .catch((err) => {
              console.error(err);
            });
          const productCustomAtt = [];
          let x = 0;
          detailProduct.ProductPrices.map((group: any, index) => {
            if (pancakeDetail.variations[index]) {
              productCustomAtt.push({
                // // group,
                id: group.id,
                tier_index: group.tier_index,
                product_id: group.product_id,
                stock_id: group.stock_id,
                amount: group.amount,
                price: group.price,
                custom_attribute_option_id_1: group.custom_attribute_option_id_1,
                custom_attribute_option_id_2: group.custom_attribute_option_id_2,
                status: group.status,
                percent: group.percent,
                is_active: group.is_active,
                create_at: group.create_at,
                update_by: group.update_by,
                custom_attribute_option_pancake_id_1:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[0]?.id
                    : null,
                custom_attribute_option_pancake_id_2:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[1]?.id
                    : null,
                variration_pancake_id: pancakeDetail.variations
                  ? pancakeDetail.variations.sort(function (a: any, b: any) {
                      return a.display_id - b.display_id;
                    })[x]?.id
                  : null,
              });
              x++;
            } else {
              if (x == pancakeDetail.variations.length) {
                console.log('x', x);
                x = 0;
              }

              productCustomAtt.push({
                // // group,
                id: group.id,
                tier_index: group.tier_index,
                product_id: group.product_id,
                stock_id: group.stock_id,
                amount: group.amount,
                price: group.price,
                custom_attribute_option_id_1: group.custom_attribute_option_id_1,
                custom_attribute_option_id_2: group.custom_attribute_option_id_2,
                status: group.status,
                percent: group.percent,
                is_active: group.is_active,
                create_at: group.create_at,
                update_by: group.update_by,
                custom_attribute_option_pancake_id_1:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[0]?.id
                    : null,
                custom_attribute_option_pancake_id_2:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[1]?.id
                    : null,
                variration_pancake_id: pancakeDetail.variations
                  ? pancakeDetail.variations.sort(function (a: any, b: any) {
                      return a.display_id - b.display_id;
                    })[x]?.id
                  : null,
              });
              x++;
            }
          });
          await sequelize.transaction(async (transaction) => {
            await Product.update({ product_pancake_id: pancakeStockId.id }, { where: { id: product_id } });
            await ProductPrice.update({ product_pancake_id: pancakeStockId.id }, { where: { product_id: product_id } });
            await ProductPrice.bulkCreate(productCustomAtt, {
              updateOnDuplicate: [
                'id',
                'update_by',
                'create_at',
                'is_active',
                'product_id',
                'stock_id',
                'price',
                'variration_pancake_id',
                'custom_attribute_option_id_1',
                'custom_attribute_option_id_2',
                'custom_attribute_option_pancake_id_1',
                'custom_attribute_option_pancake_id_2',
                'tier_index',
                'percent',
                'status',
              ],
              transaction,
            });
          });
          try {
            // lay lai du lieu khi update ogo
            detailProduct = await this.productService.findById(product_id, 0, 0, request, PRODUCT_TYPE.DETAIL);
            for (let index = 0; index < detailProduct.ProductPrices.length; index++) {
              await axios
                .post(
                  '/shops/' +
                    pancake_shop_id +
                    '/variations/' +
                    detailProduct.ProductPrices[index].variration_pancake_id +
                    '/update_quantity?api_key=' +
                    pancake_shop_key,
                  {
                    variations_warehouses: [
                      {
                        warehouse_id: detailProduct.ProductPrices[index].Stock.pancake_stock_id,
                        remain_quantity: detailProduct.ProductPrices[index].amount,
                      },
                    ],
                  },
                )
                .then((result) => {
                  console.log('logData', result.data);
                })
                .catch((err) => {
                  console.log('err', err);
                  console.error(err);
                });
            }
          } catch (error) {
            console.error(error);
          }
        } else {
        }
      } else {
        // const product_id = await Product.findOne({
        //   where: { code: data[index].codeRow, shop_id: shopId, is_active: IS_ACTIVE.ACTIVE },
        //   include: [
        //     {
        //       model: ProductPrice,
        //       required: false,
        //       where: { is_active: IS_ACTIVE.ACTIVE },
        //     },
        //   ],
        // });
        // if (product_id.ProductPrice.length > 1) {
        //   throw ApiCodeResponse.CANNOT_UPDATE_PRODUCT_IMPORT;
        // }
        const listStock = await Stock.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, shop_id: shopId } });
        const productMedia = await ProductMedia.findOne({
          where: { product_id: checkProduct.id, is_active: IS_ACTIVE.ACTIVE },
        });
        await sequelize.transaction(async (transaction) => {
          await Promise.all([
            // update product
            Product.update(
              {
                name: data[index].nameRow,
                category_id: productCategory.id,
                description: data[index].descriptionRow,
                price: data[index].priceRow,
              },
              { where: { id: checkProduct.id, code: data[index].codeRow, shop_id: shopId }, transaction },
            ),
            ProductPrice.update(
              { price: data[index].priceRetailRow },
              { where: { product_id: checkProduct.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
            ),
            // ProductPrice.update(
            //   { is_active: IS_ACTIVE.INACTIVE },
            //   { where: { product_id: checkProduct.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
            // ),
          ]);

          if (listStock.length > 0) {
            const stockContents = listStock.map((data) => ({ stock_id: data.id, product_id: checkProduct.id }));
            await ProductStock.bulkCreate(stockContents, { transaction });
          }
          // const stockPrice = listStock.map((dataStock) => ({
          //   product_id: checkProduct.id,
          //   price: data[index].priceRetailRow,
          //   stock_id: dataStock.id,
          //   tier_index: [0],
          //   create_by: shopId,
          //   status: 1,
          // }));
          // await ProductPrice.bulkCreate(stockPrice, { transaction });
        });
        let detailProduct = await this.productService.findById(
          checkProduct.id,
          productMedia.id,
          0,
          request,
          PRODUCT_TYPE.DETAIL,
        );
        // return withSuccess(tokens);
        axios.defaults.baseURL = PANCAKE;
        const fields = {};
        const product = {
          name: detailProduct.name,
          product_attributes: detailProduct.ProductCustomAttributes.map((att) => ({
            name: att.name,
            values: att.ProductCustomAttributeOptions.map((a) => a.name),
          })),
          product_id: detailProduct.id,
          variations: [],
          weight: 0,
        };

        detailProduct.ProductPrices.map((pro) => {
          if (pro.stock_id === listStock[0].id) {
            product.variations.push({
              id: pro.variration_pancake_id ? pro.variration_pancake_id : null,
              fields: [
                pro.product_attribute_name_1
                  ? {
                      name: pro.product_attribute_name_1
                        ? pro.product_attribute_name_1.ProductCustomAttribute.name
                        : null,
                      value: pro.product_attribute_name_1 ? pro.product_attribute_name_1.name : null,
                    }
                  : null,
                pro.product_attribute_name_2
                  ? {
                      name: pro.product_attribute_name_2
                        ? pro.product_attribute_name_2.ProductCustomAttribute.name
                        : null,
                      value: pro.product_attribute_name_2 ? pro.product_attribute_name_2.name : null,
                    }
                  : null,
              ].filter(function (el) {
                return el != null;
              }),
              images: pro.product_attribute_name_1
                ? pro.product_attribute_name_1.ProductMedium
                  ? [pro.product_attribute_name_1.ProductMedium.media_url]
                  : []
                : [],
              retail_price: pro.price,
              weight: 0,
            });
          }
        });
        // return withSuccess({ detailProduct, product });

        axios.defaults.headers.post['Content-Type'] = 'application/json';
        const pancakeStockId = await axios
          .put(
            '/shops/' +
              pancake_shop_id +
              '/products/' +
              checkProduct.product_pancake_id +
              '?api_key=' +
              pancake_shop_key,
            {
              product,
            },
          )
          .then((result) => {
            console.log('logData', result);
            // if (result.data.data == false) {
            //   return false;
            // }
            return result.data.data;
          })
          .catch((err) => {
            console.error(err);
          });
        if (pancakeStockId != undefined) {
          const pancakeDetail = await axios
            .get(
              '/shops/' +
                pancake_shop_id +
                '/products/' +
                checkProduct.product_pancake_id +
                '?api_key=' +
                pancake_shop_key,
              {
                product,
              },
            )
            .then((result) => {
              console.log('logData', result);
              // if (result.data.data == false) {
              //   return false;
              // }
              return result.data.data;
            })
            .catch((err) => {
              console.error(err);
            });
          const productCustomAtt = [];
          let x = 0;
          detailProduct.ProductPrices.map((group: any, index) => {
            if (pancakeDetail.variations[index]) {
              productCustomAtt.push({
                // // group,
                id: group.id,
                tier_index: group.tier_index,
                product_id: group.product_id,
                stock_id: group.stock_id,
                amount: group.amount,
                price: group.price,
                custom_attribute_option_id_1: group.custom_attribute_option_id_1,
                custom_attribute_option_id_2: group.custom_attribute_option_id_2,
                status: group.status,
                percent: group.percent,
                is_active: group.is_active,
                create_at: group.create_at,
                update_by: group.update_by,
                custom_attribute_option_pancake_id_1:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[0]?.id
                    : null,
                custom_attribute_option_pancake_id_2:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[1]?.id
                    : null,
                variration_pancake_id: pancakeDetail.variations
                  ? pancakeDetail.variations.sort(function (a: any, b: any) {
                      return a.display_id - b.display_id;
                    })[x]?.id
                  : null,
              });
              x++;
            } else {
              if (x == pancakeDetail.variations.length) {
                console.log('x', x);
                x = 0;
              }

              productCustomAtt.push({
                // // group,
                id: group.id,
                tier_index: group.tier_index,
                product_id: group.product_id,
                stock_id: group.stock_id,
                amount: group.amount,
                price: group.price,
                custom_attribute_option_id_1: group.custom_attribute_option_id_1,
                custom_attribute_option_id_2: group.custom_attribute_option_id_2,
                status: group.status,
                percent: group.percent,
                is_active: group.is_active,
                create_at: group.create_at,
                update_by: group.update_by,
                custom_attribute_option_pancake_id_1:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[0]?.id
                    : null,
                custom_attribute_option_pancake_id_2:
                  pancakeDetail.variations[x]?.fields.length != 0
                    ? pancakeDetail.variations.sort(function (a: any, b: any) {
                        return a.display_id - b.display_id;
                      })[x]?.fields[1]?.id
                    : null,
                variration_pancake_id: pancakeDetail.variations
                  ? pancakeDetail.variations.sort(function (a: any, b: any) {
                      return a.display_id - b.display_id;
                    })[x]?.id
                  : null,
              });
              x++;
            }
          });
          await sequelize.transaction(async (transaction) => {
            await ProductPrice.bulkCreate(productCustomAtt, {
              updateOnDuplicate: [
                'id',
                'update_by',
                'create_at',
                'is_active',
                'product_id',
                'stock_id',
                'price',
                'product_pancake_id',
                'variration_pancake_id',
                'custom_attribute_option_id_1',
                'custom_attribute_option_id_2',
                'custom_attribute_option_pancake_id_1',
                'custom_attribute_option_pancake_id_2',
                'tier_index',
                'percent',
                'status',
              ],
              transaction,
            });
          });
          try {
            // lay lai du lieu khi update ogo
            detailProduct = await this.productService.findById(checkProduct.id, 0, 0, request, PRODUCT_TYPE.DETAIL);
            for (let i = 0; i < detailProduct.ProductPrices.length; i++) {
              await axios
                .post(
                  '/shops/' +
                    pancake_shop_id +
                    '/variations/' +
                    detailProduct.ProductPrices[i].variration_pancake_id +
                    '/update_quantity?api_key=' +
                    pancake_shop_key,
                  {
                    variations_warehouses: [
                      {
                        warehouse_id: detailProduct.ProductPrices[i].Stock.pancake_stock_id,
                        remain_quantity: detailProduct.ProductPrices[i].amount,
                      },
                    ],
                  },
                )
                .then((result) => {
                  console.log('logData', result.data);
                  // return result.data.data;
                })
                .catch((err) => {
                  console.log('err', err);
                  console.error(err);
                });
            }
          } catch (error) {
            console.error(error);
          }
        }
      }
    }

    return withSuccess({ data });
  }
}
