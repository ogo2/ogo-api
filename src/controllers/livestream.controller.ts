import { Body, Security, Get, Post, Query, Route, Tags, Request } from 'tsoa';
import * as _ from 'lodash';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import {
  IS_ACTIVE,
  LIVESTREAM_STATUS,
  LIVESTREAM_ENABLE,
  MEDIA_TYPE,
  SHOP_STATUS,
  IS_HIGHLIGHT_LIVESTREAM_PRODUCT,
  PRODUCT_STOCK_STATUS,
  USER_STATUS,
} from '@utils/constants';
import { AppError } from '@utils/AppError';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { LivestreamApplicationController } from './';
import { AppTypes } from 'types';
import { CommentTypes, ReactionTypes } from 'types';
import { withLiveStreamChannel } from '../websocket/constants';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Utils } from '@utils/Utils';
import { IParamCreateCommentLivestream } from './_livestream_application.controller';
import { SocketUtils } from '../websocket/SocketUtils';
import { CommentService } from '@services/internal/comment.service';
import { ReactionService } from '@services/internal/reaction.service';
// import { publisherService } from '@services/detributed/pubsub/publisher/publisher.service';
import { EVENTS } from '@services/detributed/pubsub/publisher/events';
import { livestreamCommentService } from '@services/internal/livestream-store-comment.service';
import { livestreamReactionService } from '@services/internal/livestream-reaction.service';
import { livestreamPushCommentService } from '@services/ws/push-comment-livestream.service';
import { livestreamPushReactionService } from '@services/ws/push-reaction-livestream.service';
import { livestreamTypeReactionService } from '@services/internal/type-reaction.service';
const db = require('@models');
const {
  sequelize,
  Sequelize,
  User,
  Shop,
  Livestream,
  UserWatched,
  Product,
  LivestreamProduct,
  LivestreamLayout,
  Comment,
  ProductMedia,
} = db.default;
const { Op } = Sequelize;

@Route('livestream')
@Tags('livestream')
export class LivestreamController extends LivestreamApplicationController {
  private reactionService: ReactionService;
  private commentService: CommentService;
  constructor() {
    super('Livestream');
    this.reactionService = new ReactionService();
    this.commentService = new CommentService();
  }

  /**
   * @summary join livestream (any)
   * @returns room info
   */
  @Security('jwt')
  @Post('/{livestream_id}/join')
  public async join(
    @Request() request: AppTypes.RequestIOAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const foundStreaming = await this._findOne({
      where: { id: livestream_id, status: LIVESTREAM_STATUS.STREAMING, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: false,
          attributes: [
            [sequelize.col('id'), 'livestream_product_id'],
            'code_product_livestream',
            'is_highlight',
            [sequelize.literal('`LivestreamProducts->Product`.`id`'), 'id'],
            [sequelize.literal('`LivestreamProducts->Product`.`shop_id`'), 'shop_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`category_id`'), 'category_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`code`'), 'code'],
            [sequelize.literal('`LivestreamProducts->Product`.`name`'), 'name'],
            [sequelize.literal('`LivestreamProducts->Product`.`description`'), 'description'],
            [sequelize.literal('`LivestreamProducts->Product`.`price`'), 'price'],
            [sequelize.literal('`LivestreamProducts->Product`.`star`'), 'star'],
            [sequelize.literal('`LivestreamProducts->Product`.`status`'), 'status'],
            [sequelize.literal('`LivestreamProducts->Product`.`stock_status`'), 'stock_status'],
            [sequelize.literal('`LivestreamProducts->Product`.`is_active`'), 'is_active'],
            [sequelize.literal('`LivestreamProducts->Product`.`create_at`'), 'create_at'],
            [sequelize.literal('`LivestreamProducts->Product`.`update_at`'), 'update_at'],
            [
              sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
              'stock_id',
            ],

            [
              sequelize.literal(`(
                  select price
                  from product_price 
                  where product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  and product_price.stock_id = (SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1)
                and product_price.is_active = ${IS_ACTIVE.ACTIVE}
                order by product_price.id desc
                limit 1
                )`),
              'price',
            ],
            [
              sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.type = ${MEDIA_TYPE.IMAGE}
                and product_media.product_custom_attribute_option_id is null
                and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                limit 1
                ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                )`),
              'min_max_price',
            ],
          ],
          include: {
            model: Product,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: ['name', 'phone', 'email', 'profile_picture_url', 'agora_app_id', 'agora_app_certificate'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });

    if (!foundStreaming) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Kênh livestream không tồn tại.');
    }
    await sequelize.transaction(async (transaction) => {
      await Livestream.increment(
        { count_viewed: 1 },
        { where: { id: foundStreaming.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
      );
      const foundWatched = await UserWatched.findOne(
        {
          where: {
            user_id: userDecoded.id,
            livestream_id,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        { transaction },
      );
      if (foundWatched) {
        await UserWatched.update(
          {
            update_at: new Date(),
            update_by: userDecoded.id,
          },
          { where: { id: foundWatched.id, is_active: IS_ACTIVE.ACTIVE }, transaction },
        );
      } else {
        await UserWatched.create({ user_id: userDecoded.id, livestream_id }, { transaction });
      }
    });
    const foundStreamingNew = await this._findOne({
      where: { id: livestream_id, status: LIVESTREAM_STATUS.STREAMING, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: false,
          attributes: [
            [sequelize.col('id'), 'livestream_product_id'],
            'code_product_livestream',
            'is_highlight',
            [sequelize.literal('`LivestreamProducts->Product`.`id`'), 'id'],
            [sequelize.literal('`LivestreamProducts->Product`.`shop_id`'), 'shop_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`category_id`'), 'category_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`code`'), 'code'],
            [sequelize.literal('`LivestreamProducts->Product`.`name`'), 'name'],
            [sequelize.literal('`LivestreamProducts->Product`.`description`'), 'description'],
            [sequelize.literal('`LivestreamProducts->Product`.`price`'), 'price'],
            [sequelize.literal('`LivestreamProducts->Product`.`star`'), 'star'],
            [sequelize.literal('`LivestreamProducts->Product`.`status`'), 'status'],
            [sequelize.literal('`LivestreamProducts->Product`.`stock_status`'), 'stock_status'],
            [sequelize.literal('`LivestreamProducts->Product`.`is_active`'), 'is_active'],
            [sequelize.literal('`LivestreamProducts->Product`.`create_at`'), 'create_at'],
            [sequelize.literal('`LivestreamProducts->Product`.`update_at`'), 'update_at'],
            [
              sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
              'stock_id',
            ],

            [
              sequelize.literal(`(
                  select price
                  from product_price 
                  where product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  and product_price.stock_id = (SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1)
                and product_price.is_active = ${IS_ACTIVE.ACTIVE}
                order by product_price.id desc
                limit 1
                )`),
              'price',
            ],
            [
              sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.type = ${MEDIA_TYPE.IMAGE}
                and product_media.product_custom_attribute_option_id is null
                and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                limit 1
                ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                )`),
              'min_max_price',
            ],
          ],
          include: {
            model: Product,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          required: true,
          attributes: ['name', 'phone', 'email', 'profile_picture_url', 'agora_app_id', 'agora_app_certificate'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
    });

    const HighlightProduct = foundStreamingNew.dataValues.LivestreamProducts.find(
      (item: any) => item.dataValues.is_highlight === IS_HIGHLIGHT_LIVESTREAM_PRODUCT.HIGHLIGHT,
    );
    foundStreamingNew.setDataValue('HighlightProduct', HighlightProduct);
    foundStreamingNew.setDataValue('channel', withLiveStreamChannel(livestream_id));
    return withSuccess(foundStreamingNew);
  }

  /**
   * @summary list Livestream (any) <status: 0(initial), 1(streaming), 2(finished)>
   */
  @Security('jwt')
  @Get('/')
  public async list(
    @Request() request: AppTypes.RequestIOAuth,
    @Query('status') status?: number,
    @Query('user_id') user_id?: number,
    @Query('shop_id') shop_id?: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const { offset, limit, page } = handlePagingMiddleware(request);
    const baseUrl = Utils.getBaseServer(request);

    let whereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (status || status === 0) whereOption = { ...whereOption, status };
    if (user_id) whereOption = { ...whereOption, user_id };
    if (shop_id) whereOption = { ...whereOption, shop_id };

    const { count, rows } = await Livestream.findAndCountAll({
      where: whereOption,
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            include: [
              [
                sequelize.literal(`IFNULL((
                  SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                  FROM product_media
                  WHERE
                  product_media.is_active = ${IS_ACTIVE.ACTIVE}
                  and product_media.type = ${MEDIA_TYPE.IMAGE}
                  and product_media.product_custom_attribute_option_id is null
                  and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                  limit 1
                  ),null)`),
                'media_url',
              ],
              [
                sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
                'stock_id',
              ],
              [
                sequelize.literal(`(
                  select price
                  from product_price 
                  where product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  and product_price.stock_id = (SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1)
                and product_price.is_active = ${IS_ACTIVE.ACTIVE}
                order by product_price.id desc
                limit 1
                )`),
                'price',
              ],
            ],

            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          include: {
            model: Product,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
      ],
      limit,
      offset,
      order: [['id', 'desc']],
    });
    const listPromises = rows
      .filter((livestream: any) => livestream.status === LIVESTREAM_STATUS.STREAMING)
      .map((livestream) => SocketUtils.countUserInAdapterSocketLivestreamChannel(io, livestream.id));
    const listResPromise = await Promise.all(listPromises);
    listResPromise.forEach((item: any) => {
      if (item) {
        const livestream = rows.find((livestream) => livestream.id === item.livestream_id);
        livestream.setDataValue('count_subscribe', item.count_subscribe);
      }
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }
  /**
   * @summary detail Livestream (any)
   */
  @Security('jwt')
  @Get('/{livestream_id}/detail')
  public async detail(
    @Request() request: AppTypes.RequestIOAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const baseUrl = Utils.getBaseServer(request);
    const livestreamData = await this._findOne({
      where: { id: livestream_id, is_active: IS_ACTIVE.ACTIVE },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: LivestreamProduct,
          required: false,
          attributes: [
            [sequelize.col('id'), 'livestream_product_id'],
            'code_product_livestream',
            'is_highlight',
            [sequelize.literal('`LivestreamProducts->Product`.`id`'), 'id'],
            [sequelize.literal('`LivestreamProducts->Product`.`shop_id`'), 'shop_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`category_id`'), 'category_id'],
            [sequelize.literal('`LivestreamProducts->Product`.`code`'), 'code'],
            [sequelize.literal('`LivestreamProducts->Product`.`name`'), 'name'],
            [sequelize.literal('`LivestreamProducts->Product`.`description`'), 'description'],
            [sequelize.literal('`LivestreamProducts->Product`.`price`'), 'price'],
            [sequelize.literal('`LivestreamProducts->Product`.`star`'), 'star'],
            [sequelize.literal('`LivestreamProducts->Product`.`status`'), 'status'],
            [sequelize.literal('`LivestreamProducts->Product`.`stock_status`'), 'stock_status'],
            [sequelize.literal('`LivestreamProducts->Product`.`is_active`'), 'is_active'],
            [sequelize.literal('`LivestreamProducts->Product`.`create_at`'), 'create_at'],
            [sequelize.literal('`LivestreamProducts->Product`.`update_at`'), 'update_at'],

            [
              sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.type = ${MEDIA_TYPE.IMAGE}
                and product_media.product_custom_attribute_option_id is null
                and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                limit 1
                ),null)`),
              'media_url',
            ],
            [
              sequelize.literal(`(
                SELECT CONCAT(format(MIN(product_price.price),0),"đ - ",format(MAX(product_price.price),0),"đ")
                FROM product_price AS product_price
                WHERE
                product_price.is_active = ${IS_ACTIVE.ACTIVE}
                and product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                )`),
              'min_max_price',
            ],
          ],
          include: {
            model: Product,
            required: true,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Comment,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          include: [
            {
              model: User,
              required: false,
              attributes: ['name', 'phone', 'email', 'profile_picture_url'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
          where: { is_active: IS_ACTIVE.ACTIVE },
          limit: 10,
          order: [['id', 'desc']],
        },
      ],
    });
    if (!livestreamData) throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with('Livestream không tồn tại!');

    const HighlightProduct = livestreamData.dataValues.LivestreamProducts.find(
      (item: any) => item.dataValues.is_highlight === IS_HIGHLIGHT_LIVESTREAM_PRODUCT.HIGHLIGHT,
    );
    const res = await SocketUtils.countUserInAdapterSocketLivestreamChannel(io, livestream_id);
    if (res) {
      const { count_subscribe } = res;
      livestreamData.setDataValue('count_subscribe', count_subscribe);
    }
    livestreamData.setDataValue('HighlightProduct', HighlightProduct);
    return withSuccess(livestreamData);
  }

  /**
   * @summary list Livestream history (any)
   */
  @Security('jwt')
  @Get('/history')
  public async history(@Request() request: AppTypes.RequestIOAuth): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const userDecoded = request?.user?.data;
    const baseUrl = Utils.getBaseServer(request);
    const { offset, limit, page } = handlePagingMiddleware(request);

    const { count, rows } = await Livestream.findAndCountAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
      },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
      },
      include: [
        {
          model: Shop,
          required: true,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: {
            livestream_enable: LIVESTREAM_ENABLE.ACTIVE,
            status: SHOP_STATUS.ACTIVE,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
        {
          model: LivestreamProduct,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
            include: [
              [
                sequelize.literal(`(
                SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1
                )`),
                'stock_id',
              ],
              [
                sequelize.literal(`(
                  select price
                  from product_price 
                  where product_price.product_id = \`LivestreamProducts->Product\`.\`id\`
                  and product_price.stock_id = (SELECT stock_id
                FROM product_stock AS product_stock, stock AS stock
                WHERE product_stock.is_active = ${IS_ACTIVE.ACTIVE}
                AND product_stock.status = ${PRODUCT_STOCK_STATUS.AVAILABLE}
                AND product_stock.product_id = \`LivestreamProducts->Product\`.\`id\`
                AND product_stock.stock_id = stock.id
                AND stock.is_active = ${IS_ACTIVE.ACTIVE}
                LIMIT 1)
                and product_price.is_active = ${IS_ACTIVE.ACTIVE}
                order by product_price.id desc
                limit 1
                )`),
                'price',
              ],
              [
                sequelize.literal(`IFNULL((
                SELECT  CONCAT('${baseUrl}/',\`product_media\`.\`media_url\`) as media_url
                FROM product_media
                WHERE
                product_media.is_active = ${IS_ACTIVE.ACTIVE}
                and product_media.type = ${MEDIA_TYPE.IMAGE}
                and product_media.product_custom_attribute_option_id is null
                and product_media.product_id = \`LivestreamProducts->Product\`.\`id\`
                limit 1
                ),null)`),
                'media_url',
              ],
            ],
          },
          include: {
            model: Product,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: LivestreamLayout,
          required: false,
          attributes: {
            exclude: ['create_by', 'update_by', 'delete_by', 'delete_at', 'version'],
          },
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: UserWatched,
          required: true,
          attributes: [],
          where: {
            user_id: userDecoded.id,
            is_active: IS_ACTIVE.ACTIVE,
          },
        },
      ],
      limit,
      offset,
      order: [[UserWatched, 'update_at', 'DESC']],
    });
    const listPromises = rows
      .filter((livestream: any) => livestream.status === LIVESTREAM_STATUS.STREAMING)
      .map((livestream) => SocketUtils.countUserInAdapterSocketLivestreamChannel(io, livestream.id));
    const listResPromise = await Promise.all(listPromises);
    listResPromise.forEach((item: any) => {
      if (item) {
        const livestream = rows.find((livestream) => livestream.id === item.livestream_id);
        livestream.setDataValue('count_subscribe', item.count_subscribe);
      }
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary list comment Livestream history (any)
   */
  @Security('jwt')
  @Get('/{livestream_id}/comment')
  public async historyComment(
    @Request() request: AppTypes.RequestIOAuth,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { io } = request;
    const userDecoded = request?.user?.data;
    const { offset, limit, page } = handlePagingMiddleware(request);

    let whereOption: any = { is_active: IS_ACTIVE.ACTIVE };
    if (!userDecoded.shop_id) whereOption = { ...whereOption, id: userDecoded.id };

    const { count, rows } = await Comment.findAndCountAll({
      where: { is_active: IS_ACTIVE.ACTIVE, livestream_id },
      attributes: {
        exclude: ['create_by', 'update_by', 'delete_by', 'post_id', 'parent_id', 'delete_at', 'version', 'is_active'],
      },
      include: [
        {
          model: User,
          attributes: ['name', 'phone', 'email', 'profile_picture_url'],
          where: whereOption,
        },
      ],
      limit,
      offset,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: _.isArray(count) ? count.length : count });
  }

  /**
   * @summary create livestream comment (any)
   */
  @Security('jwt')
  @Post('/{livestream_id}/comment')
  public async comment(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: CommentTypes.CommentLivestreamModel,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const authorizeUser = request?.user?.data;
    const { content }: CommentTypes.CommentLivestreamModel =
      await this.commentService.CommentLivestreamSchema.validateAsync(body);
    const streaming = await this._findOne({
      where: { id: livestream_id, status: LIVESTREAM_STATUS.STREAMING, is_active: IS_ACTIVE.ACTIVE },
    });
    if (!streaming) {
      throw new AppError(ApiCodeResponse.DATA_NOT_EXIST).with(
        'Nội dung phát trực tuyến không tồn tại, hoặc đã được kết thúc.',
      );
    }
    const data = {
      user_id: authorizeUser.id,
      shop_id: authorizeUser.shop_id,
      livestream_id,
      content,
    };
    await Livestream.increment({ count_comment: 1 }, { where: { id: livestream_id, is_active: IS_ACTIVE.ACTIVE } });
    // add comments async
    livestreamCommentService.push(data);

    const user = await User.findOne({
      where: {
        id: authorizeUser.id,
        is_active: IS_ACTIVE.ACTIVE,
        status: USER_STATUS.ACTIVE,
      },
      attributes: {
        exclude: ['password', 'token', 'device_id', 'app_token', 'create_by', 'update_by', 'delete_by', 'version'],
      },
      include: {
        model: Shop,
        required: false,
        attributes: {
          exclude: [],
        },
        where: { is_active: IS_ACTIVE.ACTIVE },
      },
    });

    const UserInfo = {
      id: user.id,
      name: user.name,
      profile_picture_url: user.profile_picture_url,
    };
    const ShopInfo =
      user.shop_id && user.shop_id === authorizeUser.shop_id
        ? {
            id: user.shop_id,
            name: user.dataValues.Shop.name,
            profile_picture_url: user.dataValues.Shop.profile_picture_url,
          }
        : undefined;
    const comment = {
      content: body.content,
      livestream_id,
      User: UserInfo,
      Shop: ShopInfo,
    };
    // push comment ws interval
    livestreamPushCommentService.push(livestream_id, comment);

    // await publisherService.publish(EVENTS.PUSH_COMMENT_LIVESTREAM_WS, data);
    return withSuccess(data);
  }

  /**
   * @summary create livestream reaction while streaming (any)
   */
  @Security('jwt')
  @Post('/{livestream_id}/reaction')
  public async reaction(
    @Request() request: AppTypes.RequestIOAuth,
    @Body() body: ReactionTypes.ReactionModel,
    livestream_id: number,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const { df_reaction_id }: ReactionTypes.ReactionModel = await this.reactionService.ReactionSchema.validateAsync(
      body,
    );
    // increment livestream async
    livestreamReactionService.incrementReaction(livestream_id);
    // push reaction ws interval
    const typeReaction = livestreamTypeReactionService.getTypeReaction(body.df_reaction_id);
    livestreamPushReactionService.push(livestream_id, typeReaction);
    // const data = { df_reaction_id, livestream_id };
    // await publisherService.publish(EVENTS.PUSH_REACTION_LIVESTREAM_WS, data);
    return withSuccess(typeReaction);
  }
}
