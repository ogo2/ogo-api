import { Body, Security, Get, Post, Put, Query, Route, Delete, Tags, Request } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import { handlePagingMiddleware } from '../middleware/pagingMiddleware';
import { IS_ACTIVE, CATEGORY_STATUS } from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import Joi from '../utils/JoiValidate';
import { AppError } from '@utils/AppError';

const db = require('@models');
const { sequelize, Sequelize, Category, Product, Shop } = db.default;
const { Op } = Sequelize;

/**
 * Danh mục sản phẩm
 */
@Route('category')
@Tags('category')
export class CategoryController extends ApplicationController {
  constructor() {
    super('Category');
  }
  /**
   * @summary Danh sách danh mục cha ( nếu parent_id =  null là danh mục cha)
   */
  @Get('/')
  public async listCategory(
    @Request() request: any,
    // @Query('page') pageValue = 1,
    // @Query('limit') limitValue = CONFIG.PAGING_LIMIT,
    @Query() search?: string,
    @Query() status?: any,
    @Query() parent_id?: any,
    @Query() from_date?: any,
    @Query() to_date?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    // return withSuccess(Object.values(CATEGORY_STATUS));
    const { offset, limit, page } = handlePagingMiddleware(request);
    let whereOption;
    whereOption = {
      name: search ? { [Op.substring]: search } : { [Op.ne]: null },
      // status: { [Op.in]: ![null, ''].includes(status) ? [status] : Object.values(CATEGORY_STATUS) },
      parent_id: parent_id ? parent_id : null,
      is_active: IS_ACTIVE.ACTIVE,
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
    };

    if (status) {
      whereOption.status = { [Op.in]: ![null, ''].includes(status) ? [status] : Object.values(CATEGORY_STATUS) };
    }
    // if (parent_id) {
    //   whereOption.parent_id = parent_id;
    // }
    const { count, rows } = await Category.findAndCountAll({
      where: whereOption,
      attributes: [
        'id',
        'name',
        'order',
        [sequelize.fn('COUNT', sequelize.col('children_category.id')), 'total_children_category'],
        'parent_id',
        'status',
        'is_active',
        'icon_url',
        'create_at',
      ],
      include: [
        {
          model: Category,
          required: false,
          as: 'parent_category',
          attributes: ['id', 'name', 'create_at'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Category,
          required: false,
          as: 'children_category',
          where: { is_active: IS_ACTIVE.ACTIVE },
          attributes: ['id', 'name', 'create_at'],
        },
      ],
      order: [
        ['order', 'ASC'],
        ['id', 'DESC'],
      ],
      subQuery: false,
      group: ['id'],
      page,
      limit,
      offset,
    });
    return withPagingSuccess(rows, { page, totalItemCount: count instanceof Array ? count.length : count, limit });
  }
  /**
   * @summary Thêm danh mục cha / hoặc con
   */
  @Security('jwt')
  @Post('/')
  public async createCategory(
    @Body() body: { name: string; parent_id?: number; order?: number; icon_url?: string },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      name: Joi.string().required(),
      icon_url: Joi.string().allow(null, ''),
      parent_id: Joi.number().allow(null, ''),
      order: Joi.number().allow('', null),
      status: Joi.string().default(IS_ACTIVE.ACTIVE),
    });
    const { name, parent_id, order, icon_url } = await schema.validateAsync(body);
    if (parent_id) {
      const checkCategory = await super._findOne({
        where: { id: body.parent_id, is_active: IS_ACTIVE.ACTIVE },
      });
      // return withSuccess(category);
      if (checkCategory === null) throw new AppError(ApiCodeResponse.CATEGORY_PARENT_EXIST);
    }

    const category = await super._findOne({
      where: { name: { [Op.substring]: name }, parent_id: parent_id || null, is_active: IS_ACTIVE.ACTIVE },
    });
    // return withSuccess(category);
    if (category !== null) throw new AppError(ApiCodeResponse.CATEGORY_EXIST);

    const data = await super._create({ ...body, name });
    return await this.getDetailCategory(data.id);
  }

  /**
   * @summary Sửa danh mục cha/ con
   */
  @Security('jwt')
  @Put('/{id}')
  // @Security('jwt')
  public async updateCategory(
    id: number,
    @Request() request: any,
    @Body() body: { name: string; parent_id?: any; order?: number; icon_url?: string },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const schema = Joi.object({
      name: Joi.string().required(),
      icon_url: Joi.string().allow(null, ''),
      parent_id: Joi.number().allow('', null),
      order: Joi.number().allow('', null),
    });
    const { name, parent_id, order, icon_url } = await schema.validateAsync(body);
    const checkCategory = await super._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id },
    });
    if (!checkCategory) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
    const category = await super._findOne({
      where: {
        id: { [Op.ne]: id },
        name: body.name,
        parent_id: body.parent_id ? body.parent_id : null,
        is_active: IS_ACTIVE.ACTIVE,
      },
    });
    if (category) throw ApiCodeResponse.CATEGORY_EXIST;
    const data = await super._update(
      {
        ...body,
        name: body.name,
      },
      {
        where: { id: id, is_active: IS_ACTIVE.ACTIVE },
      },
    );
    return this.getDetailCategory(id);
  }
  /**
   * @summary Update trạng thái danh mục cha/ con
   */
  @Security('jwt')
  @Put('/{id}/change-status')
  public async changeStatus(
    id: number,
    @Request() request: any,
    @Body() body: { status: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const checkCategory = await super._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id },
    });
    if (!checkCategory) throw ApiCodeResponse.CATEGORY_ERROR;
    const data = await super._update(
      {
        status: body.status,
      },
      {
        where: { id: id, is_active: IS_ACTIVE.ACTIVE },
      },
    );
    return withSuccess(data);
  }
  /**
   * @summary Xóa danh mục cha /con
   */
  @Security('jwt')
  @Delete('/{id}')
  // @Security('jwt')
  public async deleteCategory(id: number, @Request() request: any): Promise<AppTypes.SuccessResponseModel<any>> {
    const checkCategory = await super._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id },
    });
    // return withSuccess(checkCategory);
    if (!checkCategory) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
    const checkParentCategory = await super._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, parent_id: id },
    });
    if (checkParentCategory) throw ApiCodeResponse.CATEGORY_PARENT_NOT_EXIST;
    // check Product
    const checkProduct = await Product.findAll({
      where: { category_id: id, is_active: IS_ACTIVE.ACTIVE },
      include: { model: Shop, where: { is_active: IS_ACTIVE.ACTIVE } },
    });
    // return withSuccess(checkProduct);
    if (checkProduct.length > 0) throw ApiCodeResponse.CATEGORY_NOT_DELETE;
    const data = await super._update(
      {
        is_active: IS_ACTIVE.INACTIVE,
      },
      {
        where: { id: id, is_active: IS_ACTIVE.ACTIVE },
      },
    );
    return withSuccess(data);
  }
  /**
   * @summary Chi tiết danh mục
   */
  @Get('/{id}')
  public async getDetailCategory(id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const checkCategory = await super._findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, id },
    });
    if (checkCategory == null) throw ApiCodeResponse.CATEGORY_NOT_EXIST;
    return withSuccess(checkCategory);
  }
}
