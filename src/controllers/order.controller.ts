import {
  IS_ACTIVE,
  ORDER_STATUS,
  DF_NOTIFICATION,
  CONFIG_TYPE,
  CONFIG_STATUS,
  STATUS_REFERAL,
  GIFT_STATUS,
  SHOP_STATUS,
  PANCAKE,
} from '@utils/constants';
import { ApiCodeResponse } from '@utils/ApiCodeResponse';
import { ApplicationController } from './';
import * as _ from 'lodash';
import Joi from '../utils/JoiValidate';
import { NotificationService } from '@services/internal/notification.service';
import { PointService } from '@services/internal/point.service';
import { Body, Get, Request, Security, Put, Query, Post, Route, Tags } from 'tsoa';
import { withSuccess, withPagingSuccess } from '../utils/BaseResponse';
import { AppTypes } from 'types';
import { handlePagingMiddleware } from '@middleware/pagingMiddleware';
import { Utils } from '@utils/Utils';
const axios = require('axios');
const db = require('@models');
const {
  sequelize,
  Sequelize,
  Order,
  OrderItem,
  UserAddress,
  OrderStatusHistory,
  DFOrderStatusHistory,
  Shop,
  User,
  Config,
  DFProvince,
  DFDistrict,
  DFWard,
  PurchasedGift,
  ProductPrice,
} = db.default;
const { Op } = Sequelize;

@Route('order')
@Tags('order')
export class OrderShopController extends ApplicationController {
  private pointService: PointService;
  private notificationService: NotificationService;
  constructor() {
    super('Order');
    this.pointService = new PointService();
    this.notificationService = new NotificationService();
  }

  /**
   * @summary Xác nhận/ Từ chối toàn bộ đơn hàng
   */
  @Security('jwt', ['shop'])
  @Put('/confirm-list-order')
  public async confirmListOrder(
    @Request() request: any,
    @Body() body: { list_order_id: Array<number>; status?: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;

    const shopId = loginUser ? loginUser.shop_id : null;
    // return withSuccess(shopId);
    const schema = Joi.object({
      list_order_id: Joi.array().items(Joi.number().integer()).required().label('id'),
      status: Joi.number().required(),
    });
    const { list_order_id, status } = await schema.validateAsync(body);

    // check all order
    const checkOrder = await Order.findAll({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        [Op.or]: [{ status: ORDER_STATUS.PENDING }, { status: ORDER_STATUS.CONFIRMED }],
        shop_id: shopId,
        id: { [Op.in]: list_order_id },
      },
    });
    // return withSuccess(checkOrder);
    let shopDetail;
    const changeStatus = status == ORDER_STATUS.SUCCCESS ? 3 : 6;
    for (let ca = 0; ca < checkOrder.length; ca++) {
      shopDetail = await Shop.findOne({
        where: { id: checkOrder[ca].shop_id, is_active: IS_ACTIVE.ACTIVE, pancake_shop_key: { [Op.ne]: null } },
      });
      // return withSuccess(shopDetail);
      axios.defaults.baseURL = PANCAKE;
      axios.defaults.headers.post['Content-Type'] = 'application/json';

      if (shopDetail != null) {
        await axios
          .put(
            '/shops/' +
              shopDetail.pancake_shop_id +
              '/orders/' +
              checkOrder[ca].pancake_order_id +
              '?api_key=' +
              shopDetail.pancake_shop_key,
            {
              status: changeStatus,
            },
          )
          .then((result) => {
            console.log('logData', result);
            // if (result.data.data == false) {
            //   return false;
            // }
            return result.data.data;
          })
          .catch((err) => {
            console.error(err);
          });
      }
    }

    // return withSuccess(checkOrder.map((data) => data.gift_code.id).filter(function (el) {
    //   return el != null;
    // }));
    let listGiftId = [];
    listGiftId = checkOrder
      .map(function (obj) {
        if (obj.gift_code != null) {
          return obj.gift_code.purchase_gift_id;
        }
      })
      .filter(function (el) {
        return el != null;
      });
    // return withSuccess(listGiftId)

    // const listGiftId = checkOrder.map((data) => data.gift_code.purchase_gift_id).filter(function (el) {
    //   return el != null;
    // });

    // //
    // // kiểm tra xem người tạo đơn có nhập mã gt hay k
    // const checkCode = await User.findAll({
    //   where: {
    //     is_active: IS_ACTIVE.ACTIVE,
    //     id: { [Op.in]: checkOrder.map((data) => data.user_id) },
    //     referal_customer_id: { [Op.ne]: null },
    //     status_referal: { [Op.lt]: STATUS_REFERAL.FINISHED }
    //   },
    // });
    // // return withSuccess(checkCode);
    // // Kiểm tra xem người được nhập mã gt có tồn tại hay k (lấy danh sách những ng được nhập mã)
    // const checkUser = await User.findAll({
    //   where: {
    //     is_active: IS_ACTIVE.ACTIVE,
    //     id: checkCode ? { [Op.in]: checkCode.map((data) => data.referal_customer_id) } : null,
    //   },
    // }).filter(function (el) {
    //   return el != null;
    // });

    // // cộng điểm cho người được nhập mã giới thiệu ( tạo noti all)
    // const configRefCode = await Config.findOne({
    //   where: { type: CONFIG_TYPE.REFERRAL_APP, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    // });
    // const notiTypeRef = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.REFERRAL_APP);
    // if (!notiTypeRef) throw ApiCodeResponse.NOT_FOUND;

    // const notificationRefAll = checkUser.map((projectData) => {
    //   const content = 'Bạn được tặng ' + configRefCode.value + ' điểm khi đã giới thiệu ứng dụng cho bạn bè';
    //   // const content = 'Bạn được tặng ' + point + ' điểm khi đơn hàng' + code_order + ' đã hoàn thành';
    //   // const notiType = await this.notificationService.findOneTypeNotification(id);
    //   // if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    //   return {
    //     df_notification_id: notiTypeRef.id,
    //     content: content,
    //     data: { order_id: projectData.id, order_status: ORDER_STATUS.SUCCCESS },
    //     user_id: projectData.user_id,
    //     title: notiTypeRef.title,
    //   };
    // });

    // return withSuccess(checkUser);
    // lấy thông tin người nhập mã xem đã được cộng điểm khi hoàn thành đơn đầu tiên hay chưa
    // const checkUserRef = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: checkOrder.user_id } });

    // const configPromotion = await Config.findOne({
    //   where: { type: CONFIG_TYPE.ORDER_PROMOTION, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    // });
    // // return withSuccess(configPromotion);

    // const notiTypeSucess = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.PROMOTION_POINT);
    // if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    // const notificationPointAll = checkOrder.map((projectData) => {
    //   const content = 'Bạn được tặng ' + configPromotion.value + ' điểm khi đơn hàng' + projectData.code + ' đã hoàn thành'
    //   // const content = 'Bạn được tặng ' + point + ' điểm khi đơn hàng' + code_order + ' đã hoàn thành';
    //   return {
    //     df_notification_id: notiTypeSucess.id,
    //     content: content,
    //     data: { order_id: projectData.id, order_status: ORDER_STATUS.SUCCCESS },
    //     user_id: projectData.user_id,
    //     title: notiTypeSucess.title,
    //   };
    // });
    if (list_order_id.length > checkOrder.length) throw ApiCodeResponse.ORDER_NOT_UPDATE_SHOP;
    const notiType = await this.notificationService.findOneTypeNotification(DF_NOTIFICATION.ORDER_SHOP);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;

    const notifications = checkOrder.map((projectData) => {
      const content =
        status === ORDER_STATUS.CONFIRMED
          ? 'Đơn hàng ' + projectData.code + ' của bạn đã được xác nhận'
          : 'Đơn hàng ' + projectData.code + ' của bạn đã bị từ chối';

      return {
        df_notification_id: notiType.id,
        content: content,
        data: { order_id: projectData.id, order_status: status },
        user_id: projectData.user_id,
        title: notiType.title,
      };
    });

    const data_transaction: any = await sequelize.transaction(async (transaction: any) => {
      // danh sách id hợp lệ
      await Order.update(
        { status: status },
        {
          where: {
            id: {
              [Op.in]: list_order_id,
            },
          },
          transaction,
        },
      );
      if (status == ORDER_STATUS.CANCELED) {
        if (listGiftId.length > 0) {
          await PurchasedGift.update(
            { status: GIFT_STATUS.CONFIRMED, order_id: null },
            { where: { id: { [Op.in]: listGiftId } } },
          );
        }

        // hoàn lại so luong san pham sau khi huy
        const orderItem = await OrderItem.findAll({
          where: { is_active: IS_ACTIVE.ACTIVE, order_id: { [Op.in]: list_order_id } },
        });
        for (let index = 0; index < orderItem.length; index++) {
          await ProductPrice.increment(
            { amount: orderItem[index].amount },
            { where: { product_id: orderItem[index].product_id, stock_id: orderItem[index].stock_id }, transaction },
          );
        }
      }
      // // return withSuccess(countOrder);
      // if (checkUser.length > 0 && status == ORDER_STATUS.SUCCCESS) {

      //   const configRefCode = await Config.findOne({
      //     where: { type: CONFIG_TYPE.REFERRAL_APP, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
      //   });
      //   await User.update(
      //     { status_referal: STATUS_REFERAL.FINISHED },
      //     { where: { is_active: IS_ACTIVE.ACTIVE, id: { [Op.in]: checkCode.map((data) => data.id) } } },
      //   );

      //   // thông báo cộng điểm cho những người giới thiệu app
      //   for (let index = 0; index < checkUser.length; index++) {
      //     // thông tin người cho mã giới thiệu
      //     const detailUserRef = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: checkUser[index].id } });
      //     await pointService.referralApp(checkUser[index].id, detailUserRef.point, transaction);
      //   }
      //   await this.notificationService.createMultiNotification(notificationRefAll, transaction);
      // }
      const listDataOrderHistory: Array<any> = list_order_id.map((order_id: any) => {
        return {
          order_id: order_id,
          df_order_status_history_id: status,
        };
      });
      await OrderStatusHistory.bulkCreate(listDataOrderHistory, { transaction });
      await this.notificationService.createMultiNotification(notifications, transaction);
      // if (status == ORDER_STATUS.SUCCCESS) {
      //   for (let index = 0; index < checkOrder.length; index++) {
      //     const detailUser = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: checkOrder[index].user_id } });
      //     await pointService.orderPromotion(checkOrder[index].user_id, detailUser.point, transaction);
      //   }

      //   // thông báo cộng điểm cho những đơn ht
      //   await this.notificationService.createMultiNotification(notificationPointAll, transaction);
      //   // thông báo cộng điểm cho những người giới thiệu app
      //   await this.notificationService.createMultiNotification(notificationRefAll, transaction);

      // }if (status == ORDER_STATUS.SUCCCESS) {
      //   for (let index = 0; index < checkOrder.length; index++) {
      //     const detailUser = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: checkOrder[index].user_id } });
      //     await pointService.orderPromotion(checkOrder[index].user_id, detailUser.point, transaction);
      //   }

      //   // thông báo cộng điểm cho những đơn ht
      //   await this.notificationService.createMultiNotification(notificationPointAll, transaction);
      //   // thông báo cộng điểm cho những người giới thiệu app
      //   await this.notificationService.createMultiNotification(notificationRefAll, transaction);

      // }
    });
    return withSuccess(data_transaction);
  }

  /**
   * @summary Xác nhận/ Từ chối/ Hoàn thành đơn hàng pancake
   */
  // @Security('jwt', ['shop'])
  @Post('/confirm-order-pancake')
  public async confirmOrderPancake(
    @Request() request: any,
    // @Body() body: { order_id?: number, shop_key?: string, status?: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    // return withSuccess(request);
    // const schema = Joi.object({
    //     // list_order_id: Joi.array().items(Joi.number().integer()).required().label('id'),
    //     status: Joi.number().required(),
    //     order_id: Joi.number().required(),
    //     shop_key: Joi.string().required(),
    // });
    // const { order_id, status, shop_key } = await schema.validateAsync(body);
    // check order pancake gửi
    const status = request.data.data.status;
    const order_id = request.data.data.id;
    const pancake_shop_key = request.data.data.assigning_seller_id;
    console.log({ status, order_id, pancake_shop_key });
    // const shop_key = request.order_id;

    const checkOrderPC = await Order.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, pancake_order_id: order_id } });
    if (!checkOrderPC) throw ApiCodeResponse.ORDER_NOT_EXIST;
    // return withSuccess({ status, order_id });
    const checkShopPC = await Shop.findOne({
      where: { is_active: IS_ACTIVE.ACTIVE, pancake_shop_key: pancake_shop_key },
    });
    if (!checkOrderPC) throw ApiCodeResponse.SHOP_NOT_EXIST;
    // let checkOrder;
    // const shopId = checkShopPC ? checkShopPC.id : null;

    // status = 0,11 chờ xác nhận
    // status = 1,8,9,12,13 đã xác nhận
    // status = 2,4,5,15 đang giao
    // status = 6,7 huỷ
    // status = 3 hoàn thành
    // check all order
    // if (status == 2 || status == 4 || status == 5 || status == 15) {
    //     checkOrder = await Order.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.CONFIRMED, shop_id: shopId, id: order_id } });
    //     // return withSuccess(checkOrder);
    //     if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_CONFIRM;
    // }
    // if (status == 3) {
    //     checkOrder = await Order.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.SHIP, shop_id: shopId, id: order_id } });
    //     // return withSuccess(checkOrder);
    //     if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_SHIP;
    // } if (status == 1 || status == 8 || status == 9 || status == 12 || status == 13) {
    //     checkOrder = await Order.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.PENDING, shop_id: shopId, id: order_id } });
    //     // return withSuccess(checkOrder);
    //     if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_PENDING;
    // }
    // if (status == 6 || status == 7) {
    //     checkOrder = await Order.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, [Op.or]: [{ status: ORDER_STATUS.PENDING }, { status: ORDER_STATUS.CONFIRMED }], shop_id: shopId, id: order_id } });
    //     // return withSuccess(checkOrder);
    //     if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_PENDING;
    // }

    // return withSuccess(checkOrderPC);
    // cập nhật đơn hàng sang đã xác nhận
    let data_transaction;
    if (status == 1 || status == 8 || status == 9 || status == 12 || status == 13) {
      if (checkOrderPC.status == ORDER_STATUS.PENDING) {
        data_transaction = await sequelize.transaction(async (transaction: any) => {
          // danh sách id hợp lệ
          await Order.update(
            { status: ORDER_STATUS.CONFIRMED },
            {
              where: {
                id: checkOrderPC.id,
                shop_id: checkShopPC.shop_id,
              },
              transaction,
            },
          );
          await OrderStatusHistory.create(
            { order_id: checkOrderPC.id, df_order_status_history_id: status },
            { transaction },
          );
          await this.createNotification(
            DF_NOTIFICATION.ORDER_SHOP,
            checkOrderPC.id,
            ORDER_STATUS.CONFIRMED,
            checkOrderPC.user_id,
            checkOrderPC.code,
            transaction,
          );
        });
      }
    }
    // cập nhật đơn hàng sang đang giao
    if (status == 2 || status == 4 || status == 5 || status == 15) {
      if (checkOrderPC.status == ORDER_STATUS.CONFIRMED) {
        data_transaction = await sequelize.transaction(async (transaction: any) => {
          // danh sách id hợp lệ
          await Order.update(
            { status: ORDER_STATUS.SHIP },
            {
              where: {
                id: checkOrderPC.id,
                shop_id: checkShopPC.id,
              },
              transaction,
            },
          );
          await OrderStatusHistory.create(
            { order_id: checkOrderPC.id, df_order_status_history_id: status },
            { transaction },
          );
          await this.createNotification(
            DF_NOTIFICATION.ORDER_SHOP,
            checkOrderPC.id,
            ORDER_STATUS.SHIP,
            checkOrderPC.user_id,
            checkOrderPC.code,
            transaction,
          );
        });
      }
    }

    // cập nhật đơn hàng sang hoàn thành
    if (status == 3) {
      if (checkOrderPC.status == ORDER_STATUS.SHIP) {
        data_transaction = await sequelize.transaction(async (transaction: any) => {
          // danh sách id hợp lệ
          await Order.update(
            { status: ORDER_STATUS.SUCCCESS },
            {
              where: {
                id: checkOrderPC.id,
                shop_id: checkShopPC.id,
              },
              transaction,
            },
          );
          await OrderStatusHistory.create(
            { order_id: checkOrderPC.id, df_order_status_history_id: status },
            { transaction },
          );
          await this.createNotification(
            DF_NOTIFICATION.ORDER_SHOP,
            checkOrderPC.id,
            ORDER_STATUS.SUCCCESS,
            checkOrderPC.user_id,
            checkOrderPC.code,
            transaction,
          );
        });
      }
    }

    // cập nhật đơn hàng sang huỷ
    if (status == 6) {
      if (checkOrderPC.status == ORDER_STATUS.PENDING || checkOrderPC.status == ORDER_STATUS.CONFIRMED) {
        data_transaction = await sequelize.transaction(async (transaction: any) => {
          // danh sách id hợp lệ

          // hoàn lại so luong san pham sau khi huy
          const orderItem = await OrderItem.findAll({
            where: { is_active: IS_ACTIVE.ACTIVE, order_id: checkOrderPC.id },
          });
          for (let index = 0; index < orderItem.length; index++) {
            await ProductPrice.increment(
              { amount: orderItem[index].amount },
              { where: { product_id: orderItem[index].product_id, stock_id: orderItem[index].stock_id }, transaction },
            );
          }
          await Order.update(
            { status: ORDER_STATUS.CANCELED },
            {
              where: {
                id: checkOrderPC.id,
                shop_id: checkShopPC.id,
              },
              transaction,
            },
          );
          await OrderStatusHistory.create(
            { order_id: checkOrderPC.id, df_order_status_history_id: status },
            { transaction },
          );
          await this.createNotification(
            DF_NOTIFICATION.ORDER_SHOP,
            checkOrderPC.id,
            ORDER_STATUS.CANCELED,
            checkOrderPC.user_id,
            checkOrderPC.code,
            transaction,
          );
        });
      }
    }

    return withSuccess(data_transaction);
  }

  /**
   * @summary Xác nhận/ Từ chối/ Hoàn thành đơn hàng
   */
  @Security('jwt')
  @Put('/confirm-order')
  public async confirmOrder(
    @Request() request: any,
    @Body() body: { order_id?: number; status?: number },
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;

    const shopId = loginUser ? loginUser.shop_id : null;
    // return withSuccess(shopId);
    const schema = Joi.object({
      // list_order_id: Joi.array().items(Joi.number().integer()).required().label('id'),
      status: Joi.number().required(),
      order_id: Joi.number().required(),
    });
    const { order_id, status } = await schema.validateAsync(body);
    const detailUser = await User.findOne({ where: { id: loginUser.id } });
    if (detailUser == null) throw ApiCodeResponse.UNAUTHORIZED;
    let checkOrder;
    let changeStatus;
    // check all order
    if (status == ORDER_STATUS.SHIP) {
      checkOrder = await Order.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.CONFIRMED, shop_id: shopId, id: order_id },
      });
      // return withSuccess(checkOrder);
      if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_CONFIRM;
      changeStatus = 2;
    }
    if (status == ORDER_STATUS.SUCCCESS) {
      checkOrder = await Order.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.SHIP, shop_id: shopId, id: order_id },
      });
      changeStatus = 3;
      // return withSuccess(checkOrder);
      if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_SHIP;
    }
    if (status == ORDER_STATUS.CONFIRMED) {
      checkOrder = await Order.findOne({
        where: { is_active: IS_ACTIVE.ACTIVE, status: ORDER_STATUS.PENDING, shop_id: shopId, id: order_id },
      });
      changeStatus = 13;
      // return withSuccess(checkOrder);
      if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_PENDING;
    }
    if (status == ORDER_STATUS.CANCELED) {
      checkOrder = await Order.findOne({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          [Op.or]: [{ status: ORDER_STATUS.PENDING }, { status: ORDER_STATUS.CONFIRMED }],
          shop_id: shopId,
          id: order_id,
        },
      });
      changeStatus = 3;
      // return withSuccess(checkOrder);
      if (checkOrder == null) throw ApiCodeResponse.ORDER_NOT_UPDATE_PENDING;
    }
    // return withSuccess(checkOrder);

    const shopDetail = await Shop.findOne({
      where: { id: checkOrder.shop_id, is_active: IS_ACTIVE.ACTIVE, pancake_shop_key: { [Op.ne]: null } },
    });
    // return withSuccess(shopDetail)
    axios.defaults.baseURL = PANCAKE;
    axios.defaults.headers.post['Content-Type'] = 'application/json';

    if (shopDetail != null) {
      await axios
        .put(
          '/shops/' +
            shopDetail.pancake_shop_id +
            '/orders/' +
            checkOrder.pancake_order_id +
            '?api_key=' +
            shopDetail.pancake_shop_key,
          {
            status: changeStatus,
          },
        )
        .then((result) => {
          console.log('logData', result);
          // if (result.data.data == false) {
          //   return false;
          // }
          return result.data.data;
        })
        .catch((err) => {
          console.error(err);
        });
    }

    // return withSuccess(checkOrder.gift_code);
    // kiểm tra xem người tạo đơn có nhập mã gt hay k ( người đặt đơn)
    const checkCode = await User.findOne({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        id: checkOrder.user_id,
        referal_customer_id: { [Op.ne]: null },
      },
    });
    const checkUserOrder = await User.findOne({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        id: checkOrder.user_id,
      },
    });
    // Kiểm tra xem người được nhập mã gt có tồn tại hay k
    const checkUser = await User.findOne({
      where: {
        is_active: IS_ACTIVE.ACTIVE,
        id: checkCode ? checkCode.referal_customer_id : null,
      },
    });
    // lấy thông tin người nhập mã xem đã được cộng điểm khi hoàn thành đơn đầu tiên hay chưa
    // const checkUserRef = await User.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, id: checkOrder.user_id } });

    const configPromotion = await Config.findOne({
      where: { type: CONFIG_TYPE.ORDER_PROMOTION, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
    });
    // return withSuccess(configPromotion);

    const data_transaction: any = await sequelize.transaction(async (transaction: any) => {
      // danh sách id hợp lệ
      const order = await Order.update(
        { status: status },
        {
          where: {
            id: order_id,
          },
          transaction,
        },
      );
      // hoàn lại mã nếu đơn từ chối có nhập mã
      if (status == ORDER_STATUS.CANCELED) {
        if (checkOrder.gift_code != null && checkOrder.gift_code != undefined) {
          await PurchasedGift.update(
            { status: GIFT_STATUS.CONFIRMED, order_id: null },
            { where: { id: checkOrder.gift_code.purchase_gift_id } },
          );
        }
        // hoàn lại so luong san pham sau khi huy
        const orderItem = await OrderItem.findAll({ where: { is_active: IS_ACTIVE.ACTIVE, order_id: order_id } });
        for (let index = 0; index < orderItem.length; index++) {
          await ProductPrice.increment(
            { amount: orderItem[index].amount },
            { where: { product_id: orderItem[index].product_id, stock_id: orderItem[index].stock_id }, transaction },
          );
        }
      }
      // kiểm tra xem đơn hàng vừa xác nhận có phải là đơn đầu tiên của khách hàng hay k
      const countOrder = await Order.findAll({
        where: {
          is_active: IS_ACTIVE.ACTIVE,
          user_id: checkOrder.user_id,
          status: ORDER_STATUS.SUCCCESS,
        },
      });
      // return withSuccess(countOrder);
      const configRefCode = await Config.findOne({
        where: { type: CONFIG_TYPE.REFERRAL_APP, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
      });
      const configOrderCode = await Config.findOne({
        where: { type: CONFIG_TYPE.ORDER_PROMOTION, status: CONFIG_STATUS.ACTIVE, is_active: IS_ACTIVE.ACTIVE },
      });
      if (checkUser != null && checkCode.status_referal < STATUS_REFERAL.FINISHED && status == ORDER_STATUS.SUCCCESS) {
        await User.update(
          { status_referal: STATUS_REFERAL.FINISHED },
          { where: { is_active: IS_ACTIVE.ACTIVE, id: checkCode.id } },
        );
        await this.createNotificationPoint(
          DF_NOTIFICATION.REFERRAL_APP,
          configRefCode.value,
          checkUser.id,
          transaction,
        );
        await this.pointService.referralApp(checkUser.id, checkUser.point, transaction);
      }
      await OrderStatusHistory.create({ order_id: order_id, df_order_status_history_id: status }, { transaction });
      await this.createNotification(
        DF_NOTIFICATION.ORDER_SHOP,
        order_id,
        status,
        checkOrder.user_id,
        checkOrder.code,
        transaction,
      );
      const price = (checkOrder.total_price * configOrderCode.value) / 100;
      if (status == ORDER_STATUS.SUCCCESS) {
        await this.pointService.orderPromotion(checkOrder.user_id, checkUserOrder.point, price, transaction);
        await this.createNotificationPromotionPoint(
          DF_NOTIFICATION.PROMOTION_POINT,
          checkOrder.code,
          price,
          checkOrder.user_id,
          transaction,
        );
      }
      return withSuccess(order);
    });

    return withSuccess(data_transaction);
  }

  public async createNotification(id, order_id, status, id_user, code, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    let content;
    if (status == ORDER_STATUS.CONFIRMED) content = 'Đơn hàng ' + code + ' của bạn đã được xác nhận';
    if (status == ORDER_STATUS.SHIP) content = 'Đơn hàng ' + code + ' của bạn đang được giao';
    if (status == ORDER_STATUS.CANCELED) content = 'Đơn hàng ' + code + ' của bạn đã bị từ chối';
    if (status == ORDER_STATUS.SUCCCESS) content = 'Đơn hàng ' + code + ' của bạn đã hoàn thành';
    const df_notification_id = notiType.id;
    const data = { order_id: order_id, order_status: status };
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  public async createNotificationPoint(id, point, id_user, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn được tặng ' + point + ' điểm khi đã giới thiệu ứng dụng cho bạn bè';
    const df_notification_id = notiType.id;
    const data = {};
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  public async createNotificationPromotionPoint(id, code_order, point, id_user, transaction) {
    const notiType = await this.notificationService.findOneTypeNotification(id);
    if (!notiType) throw ApiCodeResponse.NOT_FOUND;
    const content = 'Bạn được tặng ' + point + ' điểm khi đơn hàng ' + code_order + ' đã hoàn thành';
    const df_notification_id = notiType.id;
    const data = {};
    const user_id = id_user;
    const titleNoti = notiType.title;
    await this.notificationService.createNotification(
      user_id,
      titleNoti,
      content,
      0,
      df_notification_id,
      data,
      null,
      transaction,
    );
  }

  /**
   * @summary DS Đơn hàng phân trang
   * param( search, from_date, to_date, order_status, ship_status, payment_status )
   */
  @Security('jwt')
  @Get('/')
  public async getListOrders(
    @Request() request: any,
    @Query('page') page?: number,
    @Query('search') search?: any,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('order_status') order_status?: any,
    @Query('shop_id') shop_id?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const shopId = loginUser ? loginUser.shop_id : null;
    // const enterprise = await User.findOne({
    //     attributes: ['enterprise_id'],
    //     where: { id: loginUser.id, is_active: IS_ACTIVE.ACTIVE },
    // });

    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    console.log('from_date', from_date);
    console.log('to_date', to_date);
    const { offset, limit } = handlePagingMiddleware(request);
    // const schema = Joi.object({
    //   search: Joi.string().allow(null, ''),
    //   from_date: Joi.date().allow(null, ''),
    //   to_date: Joi.date().allow(null, ''),
    //   order_status: Joi.number().integer().allow(null, ''),
    //   ship_status: Joi.number().integer().allow(null, ''),
    //   payment_status: Joi.number().integer().allow(null, ''),
    // });
    // const { search: search, from_date, to_date, order_status, ship_status, payment_status } = await schema.validateAsync(
    //   request.body,
    // );

    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id:
        loginUser.shop_id != null
          ? loginUser.shop_id
          : shop_id != undefined && shop_id != 0
          ? shop_id
          : { [Op.ne]: null },
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: [
          { code: { [Op.like]: `%${search}%` } },
          { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
    };
    // return withSuccess(whereOption);
    if (order_status) {
      whereOption.status = {
        [Op.in]: ![null, ''].includes(order_status) ? [order_status] : Object.values(ORDER_STATUS),
      };
    }
    const { rows, count } = await Order.findAndCountAll({
      where: whereOption,
      // subQuery: false,
      attributes: [
        'id',
        'code',
        'user_id',
        'shop_id',
        'user_address_id',
        // [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
        // [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
        [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
        [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
        [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
        [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
        [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
        'create_at',
        'update_at',
        'total_price',
        // [
        //     Sequelize.literal(`(
        //         SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
        //         FROM \`order\` as od
        //         join order_item as order_item
        //         ON order_item.order_id = od.id
        //         WHERE
        //             od.is_active = ${IS_ACTIVE.ACTIVE}
        //             AND
        //             od.id = Order.id
        //             AND
        //             order_item.is_active = ${IS_ACTIVE.ACTIVE}
        //         )`),
        //     'total_price',
        // ],
        // [sequelize.fn('SUM', sequelize.col('OrderItems.price')), 'total_price'],
        [
          Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`amount\`), 0)
                FROM \`order\` as od
                join order_item as order_item
                ON order_item.order_id = od.id
                WHERE
                    od.is_active = ${IS_ACTIVE.ACTIVE}
                    AND
                    od.id = Order.id
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                )`),
          'total_amount',
        ],
        'status',
      ],
      include: [
        {
          model: UserAddress,
          request: false,
          attributes: ['id', 'name', 'phone'],
          // as: 'customer',
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          request: false,
          attributes: ['id', 'name', 'phone'],
          // as: 'customer',
        },
        {
          model: User,
          request: false,
          attributes: ['id', 'name', 'phone'],
          // as: 'customer',
          where: { phone: { [Op.ne]: null } },
        },
        {
          model: OrderItem,
          request: false,
          attributes: ['id', 'price', 'amount', 'product_id'],
          // as: 'customer',
          // where: { is_active: IS_ACTIVE.ACTIVE },
        },
        // {
        //   model: Gift,
        //   required: false,
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        // },
      ],
      order: [['create_at', 'DESC']],
      group: ['Order.id'],
      page,
      limit,
      offset,
      logging: console.log,
    });
    return withPagingSuccess(rows, { page, limit, totalItemCount: count instanceof Array ? count.length : count });
  }

  /**
   * @summary DS tất cả Đơn hàng (admin, admin_sell_manager, shop, shop_member)
   * @param ( search, from_date, to_date, order_status, ship_status, payment_status )
   */
  @Security('jwt', ['admin', 'admin_sell_manager', 'shop', 'shop_member'])
  @Get('/all')
  public async getAllListOrders(
    @Request() request: any,
    @Query('search') search?: any,
    @Query('from_date') from_date?: any,
    @Query('to_date') to_date?: any,
    @Query('order_status') order_status?: any,
    @Query('shop_id') shop_id?: any,
  ): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    const whereOption: any = {
      is_active: IS_ACTIVE.ACTIVE,
      shop_id:
        loginUser.shop_id != null
          ? loginUser.shop_id
          : shop_id != undefined && shop_id != 0
          ? shop_id
          : { [Op.ne]: null },
      create_at: {
        [Op.and]: [{ [Op.gte]: from_date }, { [Op.lt]: to_date }],
      },
      ...(search && {
        [Op.or]: [
          { code: { [Op.like]: `%${search}%` } },
          { '$User.phone$': { [Op.like]: `%${search}%` } },
          { '$User.name$': { [Op.like]: `%${search}%` } },
        ],
      }),
    };
    if (order_status) {
      whereOption.status = {
        [Op.in]: ![null, ''].includes(order_status) ? [order_status] : Object.values(ORDER_STATUS),
      };
    }
    const listOrder = await Order.findAll({
      where: whereOption,
      subQuery: false,
      attributes: [
        'id',
        'code',
        'user_id',
        'shop_id',
        'user_address_id',
        [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
        [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
        [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
        [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
        [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
        [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
        'create_at',
        'update_at',
        'total_price',
        [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
        'status',
      ],
      include: [
        {
          model: UserAddress,
          attributes: ['id', 'name', 'phone'],
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
        {
          model: Shop,
          attributes: ['id', 'name', 'phone'],
          // where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: User,
          request: false,
          attributes: {
            include: [],
            exclude: [
              'password',
              'token',
              'code_reset_password',
              'referal_customer_id',
              'create_by',
              'update_by',
              'delete_by',
              'version',
            ],
          },
          // where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: OrderItem,
          attributes: ['id', 'price', 'amount', 'product_id'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        // {
        //   model: Gift,
        //   required: false,
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        // },
      ],
      order: [['create_at', 'DESC']],
      group: ['Order.id'],
    });
    return withPagingSuccess(listOrder);
  }

  /**
   * @summary Chi tiết đơn hàng
   */
  @Security('jwt')
  @Get('/{order_id}')
  public async orderDetail(@Request() request: any, order_id: number): Promise<AppTypes.SuccessResponseModel<any>> {
    const loginUser = request.user?.data;
    const shopId = loginUser ? loginUser.shop_id : null;
    const baseUrl = Utils.getBaseServer(request);
    // return withSuccess(shopId);

    // const schema = Joi.object({
    //     order_id: Joi.number().integer().required(),
    // });
    // const { order_id: orderId } = await schema.validateAsync({ order_id });
    const checkOrder = OrderItem.findOne({ where: { is_active: IS_ACTIVE.ACTIVE, order_id: order_id } });

    const detailOrder = await Order.findOne({
      // subQuery: false,
      attributes: [
        'id',
        'code',
        'user_id',
        'user_address_id',
        [Sequelize.literal('`UserAddress`.`name`'), 'user_name'],
        [Sequelize.literal('`Shop`.`name`'), 'shop_name'],
        [Sequelize.literal('`UserAddress`.`phone`'), 'phone'],
        [Sequelize.literal('Order.gift_code->"$.id"'), 'gift_id'],
        [Sequelize.literal('Order.gift_code->"$.name"'), 'gift_name'],
        [Sequelize.literal('Order.gift_code->"$.price"'), 'gift_price'],
        [Sequelize.literal('Order.gift_code->"$.discount_percent"'), 'gift_discount_percent'],
        [Sequelize.literal('Order.gift_code->"$.max_discount_money"'), 'gift_max_discount_money'],
        'create_at',
        'update_at',
        'total_price',
        [
          Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
                FROM \`order\` as od
                join order_item as order_item
                ON order_item.order_id = od.id
                WHERE
                    od.is_active = ${IS_ACTIVE.ACTIVE}
                    AND
                    od.id = ${order_id}
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                )`),
          'total_price_old',
        ],
        [
          Sequelize.literal(`(
                SELECT IFNULL(SUM(\`order_item\`.\`amount\`), 0)
                FROM \`order\` as od
                join order_item as order_item
                ON order_item.order_id = od.id
                WHERE
                    od.is_active = ${IS_ACTIVE.ACTIVE}
                    AND
                    od.id = ${order_id}
                    AND
                    order_item.is_active = ${IS_ACTIVE.ACTIVE}
                )`),
          'total_amount',
        ],
        // [
        //     Sequelize.literal(`(
        //         SELECT IFNULL(SUM(\`order_item\`.\`price\` * \`order_item\`.\`amount\`), 0)
        //         FROM \`order\` as od
        //         join order_item as order_item
        //         ON order_item.order_id = od.id
        //         WHERE
        //             od.is_active = ${IS_ACTIVE.ACTIVE}
        //             AND
        //             od.id = ${order_id}
        //             AND
        //             order_item.is_active = ${IS_ACTIVE.ACTIVE}
        //         )`),
        //     'total_price',
        // ],
        // [sequelize.fn('SUM', sequelize.col('OrderItems.amount')), 'total_amount'],
        'status',
      ],
      where: {
        id: order_id,
        // shop_id: shopId,
        is_active: IS_ACTIVE.ACTIVE,
      },
      include: [
        {
          model: UserAddress,
          // attributes: ["id", "name", "phone", "address"],
          // as: 'customer',
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: [
            {
              model: DFProvince,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFDistrict,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
            {
              model: DFWard,
              required: false,
              attributes: ['id', 'name'],
              where: { is_active: IS_ACTIVE.ACTIVE },
            },
          ],
        },
        {
          model: User,
          attributes: ['id', 'name', 'phone'],
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        // {
        //   model: Gift,
        //   required: false,
        //   where: { is_active: IS_ACTIVE.ACTIVE },
        // },
        {
          model: OrderItem,
          attributes: [
            'id',
            'price',
            'amount',
            'product_id',
            [
              Sequelize.fn(
                'CONCAT',
                baseUrl,
                '/',
                Sequelize.literal(
                  'IFNULL((OrderItems.product->"$.image"),(SELECT media_url FROM product_media WHERE product_id = OrderItems.product_id and is_active = 1 and product_custom_attribute_option_id is null limit 1 ))',
                ),
              ),
              'product_image',
            ],
            [Sequelize.literal('OrderItems.product->"$.name"'), 'name'],
            [Sequelize.literal('OrderItems.product->"$.stock_name"'), 'stock_name'],
          ],
          // as: 'customer',
          where: { is_active: IS_ACTIVE.ACTIVE },
        },
        {
          model: Shop,
          attributes: ['id', 'name'],
          // where: { is_active: IS_ACTIVE.ACTIVE, status: SHOP_STATUS.ACTIVE },
        },
        {
          model: OrderStatusHistory,
          required: false,
          attributes: [
            'id',
            'order_id',
            'df_order_status_history_id',
            'create_at',
            [Sequelize.literal('`OrderStatusHistories->DFOrderStatusHistory`.`describe`'), 'describe'],
          ],
          // as: 'customer',
          where: { is_active: IS_ACTIVE.ACTIVE },
          include: {
            model: DFOrderStatusHistory,
            required: false,
            attributes: [],
            where: { is_active: IS_ACTIVE.ACTIVE },
          },
        },
      ],
      group: ['OrderStatusHistories.id', 'OrderItems.id'],
      // logging: console.log,
    });

    return withSuccess(detailOrder);
  }
}
